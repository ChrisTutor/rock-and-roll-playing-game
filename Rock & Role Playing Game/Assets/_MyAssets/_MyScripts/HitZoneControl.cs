﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitZoneControl : MonoBehaviour {

    public bool m_isInZone;
    public GameObject m_targetEnemy;
    public PlayerManager playerHealthManager;

    public void Awake()
    {
        playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == this.gameObject.layer)
        { 
                m_isInZone = true;
                m_targetEnemy = other.gameObject;
        }
        else
        {   
                m_isInZone = false;
        }

    }

    public void OnTriggerExit(Collider other)
    {
        //May need to be moved to another script
        if (other.gameObject.layer == this.gameObject.layer)
        {
            m_targetEnemy = null;
        }
    }
}
