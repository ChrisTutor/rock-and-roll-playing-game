﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public RhythmTool m_rhythmTool; //Reference to the rhythm tool on this object
    public AudioClip m_audioClip;

    private PlayerManager playerHealthManager;
    private Canvas failScreenCanvas, winScreenCanvas;
    public GameObject[] allEnemies;

    public void Awake()
    {
        playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();
        failScreenCanvas = GameObject.FindWithTag("FailScreen").GetComponent<Canvas>();
        winScreenCanvas = GameObject.FindWithTag("WinScreen").GetComponent<Canvas>();
       
    }
    private void Start()
    {
        m_audioClip = Resources.Load(PlayerPrefs.GetString("NextSong")) as AudioClip;
        m_rhythmTool = GetComponent<RhythmTool>();

        m_rhythmTool.NewSong(m_audioClip);
        m_rhythmTool.SongLoaded += OnSongLoaded;
    }

    public void OnSongLoaded()
    {
        m_rhythmTool.Play();
    }

    private void Update()
    {
        //m_rhythmTool.DrawDebugLines();
        if (playerHealthManager.playerHealth <= 0)
        {
            m_rhythmTool.Stop();
            failScreenCanvas.enabled = true;
            ClearBoard();
        }
    }

    public void OnSongEnded()
    {
        winScreenCanvas.enabled = true;
        ClearBoard();
    }

    public void ClearBoard()
    {
        allEnemies = GameObject.FindGameObjectsWithTag("Enemy0");
        for (int i = 0; i < allEnemies.Length; i++)
        {
            GameObject.Destroy(allEnemies[i]);
        }
        allEnemies = GameObject.FindGameObjectsWithTag("Enemy1");
        for (int i = 0; i < allEnemies.Length; i++)
        {
            GameObject.Destroy(allEnemies[i]);
        }
        allEnemies = GameObject.FindGameObjectsWithTag("Enemy2");
        for (int i = 0; i < allEnemies.Length; i++)
        {
            GameObject.Destroy(allEnemies[i]);
        }
        allEnemies = GameObject.FindGameObjectsWithTag("Enemy3");
        for (int i = 0; i < allEnemies.Length; i++)
        {
            GameObject.Destroy(allEnemies[i]);
        }
    }

}
