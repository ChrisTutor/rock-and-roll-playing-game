﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUtility : MonoBehaviour {

    RhythmTool rhythmTool;
    Text debugText01;

    public void Start()
    {
        rhythmTool = GameObject.FindWithTag("GameController").GetComponent<RhythmTool>();
        debugText01 = GameObject.FindWithTag("DebugText01").GetComponent<Text>();
    }

    public void Update()
    {
        debugText01.text = rhythmTool.bpm.ToString();
    }
}
