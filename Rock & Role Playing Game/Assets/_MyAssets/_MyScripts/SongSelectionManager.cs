﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SongSelectionManager : MonoBehaviour {

    public AudioClip[] allAudioClips;
    //public List<string> songNameList;
    public PlayerPrefsManager playerPrefsManager;
    public bool hasSelectedSong;

    public GameObject buttonParent;
    private int namingCounter;

    public void Awake()
    {
        buttonParent = GameObject.FindWithTag("ButtonParent");
    }

    public void Start()
    {
        namingCounter = buttonParent.transform.childCount;
        DrawSongNames();
        hasSelectedSong = false;
    }
    public void DrawSongNames()
    {
        for(int i = 0; i < namingCounter; i++)
        {
            buttonParent.transform.GetChild(i).transform.GetChild(0).GetComponent<Text>().text = allAudioClips[i].name;
        }
    }

    public void SaveSongName()
    {
        PlayerPrefs.SetString("NextSong", GetComponent<Text>().text);
        Debug.Log(PlayerPrefs.GetString("NextSong"));
    }
}
