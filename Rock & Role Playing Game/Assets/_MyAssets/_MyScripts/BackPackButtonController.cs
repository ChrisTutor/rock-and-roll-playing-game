﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackPackButtonController : MonoBehaviour {

    public int buttonIndex;
    public BackPackManager backPackManager;
    public CharacterBackPack characterBackPack;
    public PlayerEquipment playerEquipment;

    public void Start()
    {
        backPackManager = GameObject.FindWithTag("GameController").GetComponent<BackPackManager>();
        characterBackPack = GameObject.FindWithTag("Player").GetComponent<CharacterBackPack>();
        

    }

    public void Update()
    {
        
    }

    public void OnClick()
    {
        backPackManager.SelectItem(characterBackPack.backPack[buttonIndex]);
    }
}
