﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStatManager : MonoBehaviour {

    public int bossHealth;
    public int bossMaxHealth;

    public void Start()
    {
        bossHealth = bossMaxHealth;
    }
}
