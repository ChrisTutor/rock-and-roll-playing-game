﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour {
    #region Variables

    public PlayerEquipment playerEquipment;

    public int playerBaseMaxHealth;
    public int playerBaseDamage;
    public int playerBaseMaxEnergy;

    public int equipmentHealthBonus;
    public int equipmentDamageBonus;
    public int equipmentEnergyBonus;

    public int playerHealth;
    public int playerMaxHealth;

    public int playerLevel;
    public int playerExp; 
    public int playerDamage;
    public int playerEnergy;
    public int playerMaxEnergy;

    public Slider playerHealthBar;
    public Slider playerExpBar;
    public Slider playerEnergyBar;



    private Text playerHealthText, playerLevelText, playerExpText, playerDamageText, playerEnergyText;
#endregion

    public void Awake()
    {

        playerExpText = GameObject.FindWithTag("PlayerExpText").GetComponent<Text>();

        if (SceneManager.GetActiveScene().name == "CharacterScreen" || SceneManager.GetActiveScene().name == "EquipmentScene")
        {
            CharacterScreenReferenceInit();
        }
        if (SceneManager.GetActiveScene().name == "Prototype Scene 02")
        {
            PlaySceneReferenceInit();
        }

        if(SceneManager.GetActiveScene().name == "EquipmentScene")
        {
            playerEquipment = GetComponent<PlayerEquipment>();
        }

    }

    private void PlaySceneReferenceInit()
    {
        playerHealthBar = GameObject.FindWithTag("PlayerHealthBar").GetComponent<Slider>();
        playerEnergyBar = GameObject.FindWithTag("EnergyBar").GetComponent<Slider>();
    }

    private void CharacterScreenReferenceInit()
    {
        playerHealthText = GameObject.FindWithTag("PlayerHealthText").GetComponent<Text>();
        playerLevelText = GameObject.FindWithTag("PlayerLevelText").GetComponent<Text>();
        playerDamageText = GameObject.FindWithTag("PlayerDamageText").GetComponent<Text>();
        playerEnergyText = GameObject.FindWithTag("PlayerEnergyText").GetComponent<Text>();
    }

    public void Start()
    {
        //AdjustStatsWithEquipment();

        playerExpBar = GameObject.FindWithTag("PlayerExpBar").GetComponent<Slider>();

        if (SceneManager.GetActiveScene().name == "Prototype Scene 02")
        {
            playerHealthBar.minValue = 0;
            playerHealthBar.maxValue = playerMaxHealth;
        }

        playerHealth = playerMaxHealth;

        if (playerLevel == 0)
        {
            playerLevel = 1;
            playerDamage = 3;
            playerMaxHealth = 50;
            playerMaxEnergy = 500;
        }
    }

    public void Update()
    {

        //AdjustStatsWithEquipment();

        if (SceneManager.GetActiveScene().name == "Prototype Scene 02")
        {
            playerHealthBar.value = playerHealth;
            playerExpText.text = playerExp + " / " + playerExpBar.maxValue;
        }

        //playerExpBar.value = playerExp;

        if (SceneManager.GetActiveScene().name == "CharacterScreen" || SceneManager.GetActiveScene().name == "EquipmentScene")
        {
            CharacterScreenTextDraw();
        }
    }
    private void AdjustStatsWithEquipment()
    {
        AdjustEquipmentStats();

        playerDamage = playerBaseDamage + equipmentDamageBonus;//add in equipment bonus;
        playerMaxHealth = playerBaseMaxHealth + equipmentHealthBonus;//add in equipment bonus;
        playerMaxEnergy = playerBaseMaxEnergy + equipmentEnergyBonus;//add in equipment bonus;
    }

    private void CharacterScreenTextDraw()
    {
        playerLevelText.text = "Level: " + playerLevel.ToString();
        playerHealthText.text = "Health: " + playerMaxHealth.ToString();
        playerDamageText.text = "Damage: " + playerDamage.ToString();
        playerEnergyText.text = "Energy: " + playerMaxEnergy.ToString();
        playerExpText.text = playerExp + " / " + playerExpBar.maxValue;
    }

    private void AdjustEquipmentStats()
    {

        if (playerEquipment.weapon[0] != null && playerEquipment.weapon[1] != null && playerEquipment.armor[0] != null && playerEquipment.armor[1] != null)
        {
            equipmentDamageBonus = playerEquipment.weapon[0].GetComponent<Item>().damageBonus +
            playerEquipment.weapon[1].GetComponent<Item>().damageBonus +
            playerEquipment.armor[0].GetComponent<Item>().damageBonus +
            playerEquipment.armor[1].GetComponent<Item>().damageBonus;

            equipmentHealthBonus = playerEquipment.weapon[0].GetComponent<Item>().healthBonus +
                playerEquipment.weapon[1].GetComponent<Item>().healthBonus +
                playerEquipment.armor[0].GetComponent<Item>().healthBonus +
                playerEquipment.armor[1].GetComponent<Item>().healthBonus;

            equipmentEnergyBonus = playerEquipment.weapon[0].GetComponent<Item>().energyBonus +
                playerEquipment.weapon[1].GetComponent<Item>().energyBonus +
                playerEquipment.armor[0].GetComponent<Item>().energyBonus +
                playerEquipment.armor[1].GetComponent<Item>().energyBonus;
        }
        else
        {
            equipmentDamageBonus = 0;
            equipmentHealthBonus = 0;
            equipmentEnergyBonus = 0;
        }
        
    }
}
