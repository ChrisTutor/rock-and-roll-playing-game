﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GearButtonController : MonoBehaviour {

	public void OnGearButton()
    {
        SceneManager.LoadScene("EquipmentScene");
    }

    public void OnBackButton()
    {
        SceneManager.LoadScene("CharacterScreen");
    }
}
