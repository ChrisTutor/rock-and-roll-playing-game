﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_1_Controller : MonoBehaviour {

    public float debugLineOffset;

    public RhythmTool rhythmTool;
    public AudioClip audioClip;
    private AnalysisData low,mid,high;

    public GameObject[] spawnArray;
    public GameObject[] enemyArray;

    public Vector3 startPosition;
    private Vector3 endPosition;

    public GameObject sphere;

    public GameObject enemyObj = null;

    public int lowCount, midCount, hightCount;

    bool firstSpawned;

    public int wholeCounter, halfCounter, quarterCounter;


    void Start () {

        rhythmTool = GetComponent<RhythmTool>();
        low = rhythmTool.low;
        mid = rhythmTool.mid;
        high = rhythmTool.high;

        lowCount = midCount = hightCount = 0;

	}
	
    void OnSongLoaded()
    {


    }

    public void Update()
    {
        if(!rhythmTool.songLoaded)
        {
            return;
        }

        for(int i = 0; i < 200; i++)
        {
            int frameIndex = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames);
            
            startPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex], i);
            endPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex + 1], i+1);

            Debug.DrawLine(startPosition, endPosition, Color.black);

            float lowOnset = low.GetOnset(frameIndex);
            float midOnset = mid.GetOnset(frameIndex);
            float highOnset = high.GetOnset(frameIndex);

            if (lowOnset > 0)
            {
                //lowCount++;
                //startPosition = new Vector3(debugLineOffset, 12, i);
                //endPosition = new Vector3(debugLineOffset, 12+lowOnset, i + 1);
                //Debug.DrawLine(startPosition, endPosition, Color.red);
            }

            if(midOnset > 0)
            {
                //midCount++;
                //startPosition = new Vector3(debugLineOffset, 16, i);
                //endPosition = new Vector3(debugLineOffset, midOnset, i + 1);
                //Debug.DrawLine(startPosition, endPosition, Color.green);
            }

            if (highOnset > 0)
            {
                //hightCount++;
                //startPosition = new Vector3(debugLineOffset, 20, i);
                //endPosition = new Vector3(debugLineOffset, 20 + highOnset, i + 1);
                //Debug.DrawLine(startPosition, endPosition, Color.blue);
            }

            if (rhythmTool.IsBeat(frameIndex) == 4)
            {
                //if (i == 199)
                //{
                //    if (quarterCounter == 0)
                //    {
                //        //Instantiate(enemyArray[0],spawnArray[0].transform.position, spawnArray[0].transform.rotation);
                //        //Instantiate(enemyArray[2], spawnArray[2].transform.position, spawnArray[2].transform.rotation);
                //    }

                //    quarterCounter++;

                //    if (1 < quarterCounter)
                //    {
                //        quarterCounter = 0;
                //    }
                //}
                //startPosition = new Vector3(debugLineOffset, 0, i);
                //endPosition = new Vector3(debugLineOffset, 10, i+1);
                //Debug.DrawLine(startPosition, endPosition, Color.magenta);
   
            }

            if (rhythmTool.IsBeat(frameIndex) == 2)
            {
                //if (i == 199)
                //{
                //    if (halfCounter == 0)
                //    {
                //        //Instantiate(enemyArray[1], spawnArray[1].transform.position, spawnArray[1].transform.rotation);

                //    }

                //    halfCounter++;

                //    if (1 < halfCounter)
                //    {
                //        halfCounter = 0;
                //    }
                //}
                //startPosition = new Vector3(debugLineOffset, 0, i);
                //endPosition = new Vector3(debugLineOffset, 10, i + 1);
                //Debug.DrawLine(startPosition, endPosition, Color.cyan);

            }
            if (rhythmTool.IsBeat(frameIndex) == 1)
            {
                //if (i == 199)
                //{
                //    if (wholeCounter == 0)
                //    {
                //        Instantiate(enemyArray[0], spawnArray[0].transform.position, spawnArray[0].transform.rotation);

                //    }

                //    wholeCounter++;

                //    if (1 < wholeCounter)
                //    {
                //        wholeCounter = 0;
                //    }
                //}
                //startPosition = new Vector3(debugLineOffset, 0, i);
                //endPosition = new Vector3(debugLineOffset, 10, i + 1);
                //Debug.DrawLine(startPosition, endPosition, Color.yellow);

            }

            if (rhythmTool.IsChange(frameIndex))
            {
                float change = rhythmTool.NextChange(frameIndex);
                startPosition = new Vector3(debugLineOffset, 0, i);
                endPosition = new Vector3(debugLineOffset, Mathf.Abs(change),i+1);
                Debug.DrawLine(startPosition, endPosition, Color.yellow);
            }
        }
    }

}
