﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossController : MonoBehaviour {

    public enum BossState
    {
        alive, dead
    };

    public BossState bossState;
    public BossStatManager bossStatManager;
    public GameObject winScreen;
    GameManager gameManager;

    public void Awake()
    {
        bossStatManager = GetComponent<BossStatManager>();
        winScreen = GameObject.FindWithTag("WinScreen");
        gameManager = GameObject.FindWithTag("GameController").GetComponent<GameManager>();
    }

    public void Update()
    {
        switch (bossState)
        {
            case BossState.alive:
                if (bossStatManager.bossHealth <= 0)
                {
                    bossState = BossState.dead;
                }
                break;

            case BossState.dead:
                winScreen.GetComponent<Canvas>().enabled = true;
                gameManager.m_rhythmTool.Stop();
                gameManager.ClearBoard();


                break;
        }
    }
}
