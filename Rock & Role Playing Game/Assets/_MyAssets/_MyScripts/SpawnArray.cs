﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SpawnArray : MonoBehaviour {

    public GameObject[,] spawnArray, shifterArray;

    public GameObject[] lane1;
    public GameObject[] lane2;
    public GameObject[] lane3;


    public void Start()
    {
        spawnArray = new GameObject[10, 3];
        shifterArray = new GameObject[10, 3];
        lane1 = lane2 = lane3  = new GameObject[20];
    }

    public void Update()
    {
        
        for (int i = 0; i < 20; i++)
        {
            lane1[i] = spawnArray[i, 0];
            lane2[i] = spawnArray[i, 1];
            lane3[i] = spawnArray[i, 2];
        }
    }

    public void AddSpawn(GameObject spawn, int row)
    {
        Array.Copy(spawnArray, 0, shifterArray, 1, 20);
        Array.Copy(shifterArray, spawnArray, 20);
        //for (int i = 19 ;i > 0; i--)
        //{
        //    for (int j = 0; j < 3; j++)
        //    {
        //        if(spawnArray[i,j] != null )

        //        {
            
        //            spawnArray[i + 1, j] = spawnArray[i, j];
        //        }
        //    }
        //}
        
        spawnArray[0, row] = spawn;
        Debug.Log("Is getting here: " + spawnArray[0, row].name);
    }

    public void AddSpawns(GameObject spawn1, int row1, GameObject spawn2, int row2)
    {
        //for (int i = 19; i > 0; i--)
        //{
        //    for (int j = 0; j < 3; j++)
        //    {
        //        if (spawnArray[i, j] != null)
        //        {
                    
        //            spawnArray[i + 1, j] = spawnArray[i, j];
        //        }
        //    }
        //}

        spawnArray[0, row1] = spawn1;
        spawnArray[0, row2] = spawn2;
    }
}
