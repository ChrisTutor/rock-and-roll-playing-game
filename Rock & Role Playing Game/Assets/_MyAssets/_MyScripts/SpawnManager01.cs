﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager01 : MonoBehaviour {

    public GameObject[] enemyArray;
    public GameObject[] spawnPoints;


    public void OnSubBeat(Beat beat, int beatType)
    {
        Instantiate(enemyArray[0], spawnPoints[0].transform.position, spawnPoints[0].transform.rotation);
        Instantiate(enemyArray[0], spawnPoints[1].transform.position, spawnPoints[1].transform.rotation);
        Instantiate(enemyArray[0], spawnPoints[2].transform.position, spawnPoints[2].transform.rotation);
        Instantiate(enemyArray[0], spawnPoints[3].transform.position, spawnPoints[3].transform.rotation);
    }
}
