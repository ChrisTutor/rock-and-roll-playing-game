﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    Vector3 m_startPos;
    Vector3 m_endPos;

    public float m_bpm;
    public float t = 0;

    GameObject gameManagerObj;
    GameManager gameManager;
    PlayerManager playerHealthManager;
    RhythmTool rhythmTool;
    ScoreManager scoreManager;
    EnergyController energyController;

    public int level;
    public int hitPoints;
    public int damage;

    public bool beingHit;
    public bool leftClear = true;
    public bool rightClear = true;
    public bool forwardClear = true;
    public bool backClear = true;

    Vector3 hitReactionDest;
    int beingHitCounter = 0;

    EnemyUnbuncherManager unbunchManager;

    //the death particl effect
    GameObject deathParticel;

    public void Awake()
    {
        deathParticel = Resources.Load("DeathParticle") as GameObject;
        gameManagerObj = GameObject.FindWithTag("GameController");
        scoreManager = gameManagerObj.GetComponent<ScoreManager>();
        playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();
        energyController = GameObject.FindWithTag("Player").GetComponent<EnergyController>();
        unbunchManager = GetComponent<EnemyUnbuncherManager>();
    }
    // Use this for initialization
    void Start()
    {
        m_startPos = transform.position;
        m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
        rhythmTool = gameManagerObj.GetComponent<RhythmTool>();
        m_bpm = 12;
    }

    // Update is called once per frame
    void Update()
    {
        //m_bpm = rhythmTool.bpm;
        t = m_bpm / 60; //gameManagerObj.GetComponent<RhythmEventProvider>().interpolation;

        if (beingHit)
        {
            unbunchManager.BeingHit(true);
            rightClear = true;
            leftClear = true;
            forwardClear = true;
            backClear = true;

            transform.Translate(Vector3.back * 40 * Time.deltaTime);
            beingHitCounter++;

            if (beingHitCounter > 15)
            {
                
                #region Raycast Spawn
                //Debug.DrawRay(transform.position, Vector3.right * 3, Color.green);
                //if (Physics.Raycast(transform.position, Vector3.right, 3))
                //{
                //    rightClear = false;
                //}


                //Debug.DrawRay(transform.position, Vector3.left * 3, Color.red);
                //if (Physics.Raycast(transform.position, Vector3.left,3))
                //{
                //    leftClear = false;
                //}


                //Debug.DrawRay(transform.position, Vector3.forward * 2, Color.yellow);
                //if (Physics.Raycast(transform.position, Vector3.forward, 2))
                //{
                //    forwardClear = false;
                //}


                //Debug.DrawRay(transform.position, Vector3.back * 2,Color.green,2);
                //if (Physics.Raycast(transform.position, Vector3.back, 2))
                //{
                //    backClear = false;
                //}


                //if (backClear && forwardClear)
                //{
                //    if (leftClear || rightClear)
                //    {

                #endregion

                if (unbunchManager.IsClear())
                {
                    beingHit = false;
                    unbunchManager.BeingHit(false);
                }
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, m_endPos, t);
        }


            if (transform.position == m_endPos)
            {
                Despawn();
            }

            if (hitPoints <= 0)
            {
                Kill();
            }
        
        
    }
    public void Kill()
    {
        scoreManager.Hit();
        playerHealthManager.playerExp +=5;
        Instantiate(deathParticel, this.transform.position, this.transform.rotation);
        energyController.AddEnergyPoints();
        GameObject.Destroy(this.gameObject);
    }

    public void Despawn()
    {
        scoreManager.miss = true;
        playerHealthManager.playerHealth -= damage;
        GameObject.Destroy(this.gameObject);
    }

    public void IsHit(int damage)
    {
        hitPoints -= damage;
        hitReactionDest = transform.position + new Vector3(0,0,5);
        beingHit = true;
        beingHitCounter = 0;
    }
}
