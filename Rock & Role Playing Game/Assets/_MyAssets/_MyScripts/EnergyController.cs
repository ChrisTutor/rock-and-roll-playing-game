﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyController : MonoBehaviour {

    PlayerManager playerManager;
    ScoreManager scoreManager;

    public int energy;
    public int maxEnergy;

    public void Awake()
    {
        playerManager = GetComponent<PlayerManager>();
        scoreManager = GameObject.FindWithTag("GameController").GetComponent<ScoreManager>();
    }

    public void Start()
    {
        playerManager.playerEnergyBar.minValue = 0;
        playerManager.playerEnergyBar.maxValue = maxEnergy;
    }

    public void Update()
    {
        playerManager.playerEnergyBar.value = energy;
    }

    public void AddEnergyPoints()
    {
        if (scoreManager.streak > 0)
        {
            energy += scoreManager.pointModifier * scoreManager.combo;
            if (energy > maxEnergy)
            {
                energy = maxEnergy;
            }
        }
    }

}
