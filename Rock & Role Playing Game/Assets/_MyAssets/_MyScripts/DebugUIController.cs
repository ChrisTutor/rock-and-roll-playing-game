﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUIController : MonoBehaviour {

    bool isOn;
    Canvas canvasRef;

    public void Awake()
    {
        canvasRef = GetComponent<Canvas>();
    }
    public void Start()
    {
        isOn = false;
        ToggleDebugCanvas();
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.F1))
        {
            if(isOn)
            {
                isOn = false;
            }
            else
            {
                isOn = true;
            }

            ToggleDebugCanvas();

        }
    }

    private void ToggleDebugCanvas()
    {
        if (isOn)
        {
            canvasRef.enabled = true;
        }
        else
        {
            canvasRef.enabled = false;
        }
    }
}
