﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AttackButtonManager : MonoBehaviour
{
    HitZoneManager hitZoneManager;
    Button button;

    PlayerManager playerHealthManager;
    GameObject ability1Particle;
    GameObject[] allEnemies;
    BossStatManager bossStatManager;
    EnergyController energyController;

    private void Awake()
    {
        hitZoneManager = GameObject.FindWithTag("HitZoneParent").GetComponent<HitZoneManager>();
        button = GetComponent<Button>();
        playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();
        button.onClick.AddListener(OnClick);
        ability1Particle = Resources.Load("Ability1Effect") as GameObject;
        bossStatManager = GameObject.FindWithTag("Boss").GetComponent<BossStatManager>();
        energyController = GameObject.FindWithTag("Player").GetComponent<EnergyController>();
    }

    public void Ability1()
    {
        if (energyController.energy >= 300)
        {
            Instantiate(ability1Particle, playerHealthManager.gameObject.transform.position, playerHealthManager.gameObject.transform.rotation);
            allEnemies = GameObject.FindGameObjectsWithTag("Enemy0");
            for (int i = 0; i < allEnemies.Length; i++)
            {
                GameObject.Destroy(allEnemies[i]);
            }
            allEnemies = GameObject.FindGameObjectsWithTag("Enemy1");
            for (int i = 0; i < allEnemies.Length; i++)
            {
                GameObject.Destroy(allEnemies[i]);
            }
            allEnemies = GameObject.FindGameObjectsWithTag("Enemy2");
            for (int i = 0; i < allEnemies.Length; i++)
            {
                GameObject.Destroy(allEnemies[i]);
            }
            allEnemies = GameObject.FindGameObjectsWithTag("Enemy3");
            for (int i = 0; i < allEnemies.Length; i++)
            {
                GameObject.Destroy(allEnemies[i]);
            }
            bossStatManager.bossHealth -= 10;
            energyController.energy -= 300;
            if (energyController.energy <0)
            {
                energyController.energy = 0;
            }
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Ability 1"))
        {
            Ability1();
        }
    }

    public void OnClick()
    {
        if (this.transform.tag == "Green")
        {
            if (hitZoneManager.m_green)
            {
                hitZoneManager.m_greenTarget.GetComponent<EnemyController>().IsHit(playerHealthManager.playerDamage);
                hitZoneManager.m_green = false;
            }
        }
        if (this.transform.tag == "Red")
        {
            if (hitZoneManager.m_red)
            {
                if(hitZoneManager.m_redTarget != null)
                {
                    hitZoneManager.m_redTarget.GetComponent<EnemyController>().IsHit(playerHealthManager.playerDamage);
                    hitZoneManager.m_red = false;
                }
                
            }
        }

        if (this.transform.tag == "Yellow")
        {
            if (hitZoneManager.m_yellow)
            {
                if (hitZoneManager.m_yellowTarget != null)
                {
                    hitZoneManager.m_yellowTarget.GetComponent<EnemyController>().IsHit(playerHealthManager.playerDamage);
                    hitZoneManager.m_yellow = false;
                }
                
            }
        }

        //if (this.transform.tag == "Blue")
        //{
        //    if (hitZoneManager.m_blue)
        //    {
        //        hitZoneManager.m_blueTarget.GetComponent<EnemyController>().IsHit(playerHealthManager.playerDamage);
        //        hitZoneManager.m_blue = false;
        //    }
        //}
    }
}
