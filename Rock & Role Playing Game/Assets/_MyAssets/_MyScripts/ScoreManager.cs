﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text pointText;
    public Text comboText;
    public Text totalHitText;

    public int combo;
    public int totalHits;
    public int points;
    public int pointModifier = 5;
    public int streak;
    public bool miss;

    public void Awake()
    {
        pointText = GameObject.FindWithTag("PointsText").GetComponent<Text>();
        comboText = GameObject.FindWithTag("ComboText").GetComponent<Text>();
        totalHitText = GameObject.FindWithTag("TotalHitText").GetComponent<Text>();
    }

    public void Update()
    {
        pointText.text  = points.ToString();
        comboText.text = streak.ToString();
        totalHitText.text = totalHits.ToString();
        Combo();
        
    }

    public void Combo()
    {
        if(miss)
        {
            streak = 0;
            miss = false;
        }

        if(streak > 35)
        {
            combo = 8;
        }
        else if(streak > 15)
        {
            combo = 4;
        }
        else if(streak > 5)
        {
            combo = 2;
        }
        else
        {
            combo = 1;
        }
    }

    public void AddPoints()
    {
        points += pointModifier * combo;
    }

    public void Hit()
    {
        totalHits++;
        streak++;
        AddPoints();
    }

}
