﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsManager : MonoBehaviour {

    public string nextSong;
    public int playerLevel, playerExp, playerHealth, playerDamage;

    public PlayerManager playerHealthManager;

    public void Awake()
    {
        playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();
    }

    public void Start()
    {
        playerHealthManager.playerLevel = PlayerPrefs.GetInt("PlayerLevel");
        playerHealthManager.playerExp = PlayerPrefs.GetInt("PlayerExp");
        playerHealthManager.playerBaseMaxHealth = PlayerPrefs.GetInt("PlayerHealth");
        playerHealthManager.playerBaseDamage = PlayerPrefs.GetInt("PlayerDamage");
    }

    public void Update()
    {
        playerLevel = playerHealthManager.playerLevel;
        playerExp = playerHealthManager.playerExp;
        playerHealth = playerHealthManager.playerBaseMaxHealth;
        playerDamage = playerHealthManager.playerBaseDamage;

        CheckPrefs();
    }

    private void CheckPrefs()
    {
        if (PlayerPrefs.GetInt("PlayerLevel") != playerLevel)
        {
            PlayerPrefs.SetInt("PlayerLevel", playerLevel);
        }

        if (PlayerPrefs.GetInt("PlayerExp") != playerExp)
        {
            PlayerPrefs.SetInt("PlayerExp", playerExp);
        }

        if (PlayerPrefs.GetInt("PlayerHealth") != playerHealth)
        {
            PlayerPrefs.SetInt("PlayerHealth", playerHealth);
        }

        if (PlayerPrefs.GetInt("PlayerDamage") != playerDamage)
        {
            PlayerPrefs.SetInt("PlayerDamage", playerDamage);
        }
    }

    public void SaveSong(string songName)
    {
        PlayerPrefs.SetString("NextSong", songName);
    }
}
