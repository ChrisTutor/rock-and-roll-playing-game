﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathParticleManager : MonoBehaviour {

    ParticleSystem deathParticle;

	// Use this for initialization
	void Start () {
        deathParticle = GetComponent<ParticleSystem>();
        deathParticle.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if(!deathParticle.isPlaying)
        {
            GameObject.Destroy(this.gameObject);
        }
	}
}
