﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackPackManager : MonoBehaviour {

    public CharacterBackPack characterBackPack;
    public GameObject[] backPackButtons;
    bool itemSelected;
    public GameObject selectedItem;
    public PlayerEquipment playerEquipment;

    

    public void Awake()
    {
        characterBackPack = GameObject.FindWithTag("Player").GetComponent<CharacterBackPack>();
        playerEquipment = GameObject.FindWithTag("Player").GetComponent<PlayerEquipment>();
    }

    public void Start()
    {
       int backPackButtonCount = GameObject.FindWithTag("BackPackPanel").transform.childCount;

        backPackButtons = new GameObject[backPackButtonCount];

        for(int i = 0; i < backPackButtonCount; i++)
        {
            backPackButtons[i] = GameObject.FindWithTag("BackPackPanel").transform.GetChild(i).gameObject;
            backPackButtons[i].GetComponent<BackPackButtonController>().buttonIndex = i;
        }
        AssignBackPack();
    }

    public void AssignBackPack()
    {
        int itemCount = characterBackPack.backPack.Length;

        for(int i = 0; i < itemCount; i++ )
        {
            backPackButtons[i].transform.GetChild(0).gameObject.GetComponent<Image>().sprite = characterBackPack.backPack[i].GetComponent<Item>().sprite;
        }
    }

    public void SelectItem(GameObject item)
    {
        if(!itemSelected)
        {
            itemSelected = true;
        }
        selectedItem = item;
        GameObject.FindWithTag("SelectedItemImage").GetComponent<Image>().sprite = item.GetComponent<Item>().sprite;
        DrawSelectedItemStats();
    }

    public void OnEquip()
    {
        if(itemSelected)
        {   
            if(selectedItem.GetComponent<Weapon>() != null)
            {
                playerEquipment.AssignLeftWeapon(selectedItem);
                GameObject.Find("WeaponLeft").transform.GetChild(0).GetComponent<Image>().sprite = selectedItem.GetComponent<Item>().sprite;
            }
            else if(selectedItem.GetComponent<Helmet>() != null)
            {
                playerEquipment.AssignArmor2(selectedItem);
                GameObject.Find("HelmetImage").transform.GetChild(0).GetComponent<Image>().sprite = selectedItem.GetComponent<Item>().sprite;
            }
            else
            {
                playerEquipment.AssignArmor1(selectedItem);
                GameObject.Find("ArmorImage").transform.GetChild(0).GetComponent<Image>().sprite = selectedItem.GetComponent<Item>().sprite;
            }
            
        }
    }

    void DrawSelectedItemStats()
    {
        GameObject.Find("ItemName").GetComponent<Text>().text = selectedItem.name;
        GameObject.Find("HPBonus").GetComponent<Text>().text = "HP Bonus: " + selectedItem.GetComponent<Item>().healthBonus.ToString();
        GameObject.Find("DamageBonus").GetComponent<Text>().text = "Damage Bonus: " + selectedItem.GetComponent<Item>().damageBonus.ToString();
        GameObject.Find("EnergyBonus").GetComponent<Text>().text = " Energy Bonus: "+ selectedItem.GetComponent<Item>().energyBonus.ToString();
        GameObject.Find("Worth").GetComponent<Text>().text = "Worth: " + selectedItem.GetComponent<Item>().worth.ToString();



    }

}
