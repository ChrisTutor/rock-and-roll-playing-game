﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FailScreenButtonControl : MonoBehaviour {

    public void OnRetry()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }
    public void OnSongSelect()
    {
        SceneManager.LoadScene("LandingScene");
    }
    public void OnQuit()
    {
        SceneManager.LoadScene("CharacterScreen");
    }
}
