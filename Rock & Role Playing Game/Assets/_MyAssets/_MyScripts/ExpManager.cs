﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpManager : MonoBehaviour {

    PlayerManager playerManager;
    

    public void Awake()
    {
        playerManager = GetComponent<PlayerManager>();
    }

    public void Update()
    {
        StatScaler();
        playerManager.playerExpBar.value = playerManager.playerExp;
    }

    public void StatScaler()
    {
        if(playerManager.playerExp > 1200)
        {
            playerManager.playerLevel = 2;
            playerManager.playerExpBar.minValue = 1200;
            playerManager.playerExpBar.maxValue = 3600;
            playerManager.playerBaseMaxHealth = 12;
            playerManager.playerBaseMaxEnergy = 625;
            playerManager.playerBaseDamage = 4;
        }
        else
        {
            playerManager.playerLevel = 1;
            playerManager.playerExpBar.minValue = 0;
            playerManager.playerExpBar.maxValue = 1200;
            playerManager.playerBaseMaxHealth = 10;
            playerManager.playerBaseMaxEnergy = 500;
            playerManager.playerBaseDamage = 3;
        }
        if (playerManager.playerExp > 3600)
        {
            
            playerManager.playerLevel = 3;
            playerManager.playerExpBar.minValue = 3600;
            playerManager.playerExpBar.maxValue = 10800;
            playerManager.playerBaseMaxHealth = 14;
            playerManager.playerBaseMaxEnergy = 781;
            playerManager.playerBaseDamage = 6;
        }
        if(playerManager.playerExp > 10800)
        {
            playerManager.playerLevel = 4;
            playerManager.playerExpBar.minValue = 10800;
            playerManager.playerExpBar.maxValue = 32400;
            playerManager.playerBaseMaxHealth = 16;
            playerManager.playerBaseMaxEnergy = 977;
            playerManager.playerBaseDamage = 9;
        }
        if (playerManager.playerExp > 32400)
        {
            playerManager.playerLevel = 5;
            playerManager.playerExpBar.minValue = 32400;
            playerManager.playerExpBar.maxValue = 97200;
            playerManager.playerBaseMaxHealth = 18;
            playerManager.playerBaseMaxEnergy = 1221;
            playerManager.playerBaseDamage = 12;
        }
        if (playerManager.playerExp > 97200)
        {
            playerManager.playerLevel = 6;
            playerManager.playerExpBar.minValue = 97200;
            playerManager.playerExpBar.maxValue = 291600;
            playerManager.playerBaseMaxHealth = 20;
            playerManager.playerBaseMaxEnergy = 1526;
            playerManager.playerBaseDamage = 15;
        }
        if (playerManager.playerExp > 291600)
        {
            playerManager.playerLevel = 7;
            playerManager.playerExpBar.minValue = 291600;
            playerManager.playerExpBar.maxValue = 874800;
            playerManager.playerBaseMaxHealth = 22;
            playerManager.playerBaseMaxEnergy = 1907;
            playerManager.playerBaseDamage = 18;
        }
        if (playerManager.playerExp > 874800)
        {
            playerManager.playerLevel = 24;
            playerManager.playerExpBar.minValue = 874800;
            playerManager.playerExpBar.maxValue = 2624400;
            playerManager.playerBaseMaxHealth = 12;
            playerManager.playerBaseMaxEnergy = 2384;
            playerManager.playerBaseDamage = 21;
        }
        if (playerManager.playerExp > 2624400)
        {
            playerManager.playerLevel = 9;
            playerManager.playerExpBar.minValue = 262400;
            playerManager.playerExpBar.maxValue = 7873200;
            playerManager.playerBaseMaxHealth = 26;
            playerManager.playerBaseMaxEnergy = 2980;
            playerManager.playerBaseDamage = 24;
        }
        if (playerManager.playerExp > 7873200)
        {
            playerManager.playerLevel = 28;
            playerManager.playerExpBar.minValue = 7873200;
            playerManager.playerExpBar.maxValue = 23619600;
            playerManager.playerBaseMaxHealth = 12;
            playerManager.playerBaseMaxEnergy = 3725;
            playerManager.playerBaseDamage = 27;
        }
        if (playerManager.playerExp > 23619600)
        {
            playerManager.playerLevel = 30;
            playerManager.playerExpBar.minValue = 23619600;
            playerManager.playerExpBar.maxValue = 708558800;
            playerManager.playerBaseMaxHealth = 12;
            playerManager.playerBaseMaxEnergy = 4656;
            playerManager.playerBaseDamage = 30;
        }
        if (playerManager.playerExp > 708558800)
        {
            playerManager.playerLevel = 10;
            playerManager.playerExpBar.minValue = 78558800;
            playerManager.playerExpBar.maxValue = 212576400;
            playerManager.playerBaseMaxHealth = 32;
            playerManager.playerBaseMaxEnergy = 5821;
            playerManager.playerBaseDamage = 33;
        }
        if (playerManager.playerExp > 212576400)
        {
            playerManager.playerLevel = 11;
            playerManager.playerExpBar.minValue = 212576400;
            playerManager.playerExpBar.maxValue = Mathf.Infinity;
            playerManager.playerBaseMaxHealth = 34;
            playerManager.playerBaseMaxEnergy  = 7276;
            playerManager.playerBaseDamage = 36;
        }
        

    }
}
