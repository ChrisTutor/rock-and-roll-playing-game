﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipment : MonoBehaviour {

    public GameObject[] armor;
    public GameObject[] weapon;
    public GameObject[] playerSockets;
    public PlayerManager playerManager;

    public void Awake()
    {

        playerManager = GetComponent<PlayerManager>();
        playerSockets = new GameObject[4];
        playerSockets[0] = GameObject.FindWithTag("LeftHandSocket");
        playerSockets[1] = GameObject.FindWithTag("RightHandSocket");
        playerSockets[2] = GameObject.FindWithTag("Armor1Socket");
        playerSockets[3] = GameObject.FindWithTag("Armor2Socket");
    }

    void Start () {
        armor = new GameObject[2];
        weapon = new GameObject[2];
	}

    public void AssignLeftWeapon(GameObject _weapon)
    {
        if (weapon[0] != null)
        {

        }
        GameObject.Destroy(GameObject.FindWithTag("WeaponL"));
        weapon[0] =  Instantiate(_weapon,playerSockets[0].transform.position,playerSockets[0].transform.rotation);
        weapon[0].transform.parent = playerSockets[0].transform;
    }

    public void AssignRightWeapon(GameObject _weapon)
    {
       
        GameObject.Destroy(GameObject.FindWithTag("WeaponR"));
        weapon[1] = Instantiate(_weapon, playerSockets[1].transform.position, playerSockets[1].transform.rotation);
        weapon[1].transform.parent = playerSockets[1].transform;
    }

    public void AssignArmor1(GameObject _armor)
    {
        
        
        GameObject.Destroy(GameObject.FindWithTag("Armor"));
        armor[0] = Instantiate(_armor, playerSockets[2].transform.position, playerSockets[2].transform.rotation);
        armor[0].transform.parent = playerSockets[2].transform;
    }

    public void AssignArmor2(GameObject _armor)
    {
        
        GameObject.Destroy(GameObject.FindWithTag("Helmet"));
        armor[1] = Instantiate(_armor, playerSockets[3].transform.position, playerSockets[3].transform.rotation);
        armor[1].transform.parent = playerSockets[3].transform;
    }

    //public void AdjustStatsWithItem(Item item, bool equip)
    //{
    //    if (equip)
    //    {
    //        playerManager.playerMaxHealth += item.healthBonus;
    //        playerManager.playerDamage += item.damageBonus;
    //        playerManager.playerMaxEnergy += item.energyBonus;
    //        Debug.Log("Its workling here");
    //    }
    //    else
    //    {
    //        playerManager.playerMaxHealth -= item.healthBonus;
    //        playerManager.playerDamage -= item.damageBonus;
    //        playerManager.playerMaxEnergy -= item.energyBonus;
    //    }
    //}

}
