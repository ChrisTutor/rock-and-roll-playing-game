﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SongSelectionButtonControl : MonoBehaviour {



    public SongSelectionManager songSelectionManager;

    public void Awake()
    {
        songSelectionManager = GameObject.FindWithTag("GameController").GetComponent<SongSelectionManager>();
    }

    public void SaveSongName()
    {
        PlayerPrefs.SetString("NextSong", transform.GetChild(0).GetComponent<Text>().text);
        Debug.Log(PlayerPrefs.GetString("NextSong"));
        songSelectionManager.hasSelectedSong = true;
        SceneManager.LoadScene("Prototype Scene 02");
    }
}
