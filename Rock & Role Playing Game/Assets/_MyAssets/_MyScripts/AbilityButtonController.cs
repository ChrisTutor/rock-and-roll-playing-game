﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityButtonController : MonoBehaviour {

    EnergyController energyController;
    Button abilityButton;

    public void Awake()
    {
        energyController = GameObject.FindWithTag("Player").GetComponent<EnergyController>();
        abilityButton = GetComponent<Button>();
    }

    void Update()
    {
        if (energyController.energy < 300)
        {
            abilityButton.interactable = false;
        }
        else
        {
            abilityButton.interactable = true;
        }
	}
}
