﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(PreCalcSongData))]

public class SpawnManager : MonoBehaviour {

    GameObject[] m_spawnObjects;
    public GameObject[] m_enemySpawn;
    GameObject enemySpawn, enemySpawn2;

    int randomSpam;

    int beatCount, lowCount, midCount, highCount;

    float lowStrength, midStrength, highStrength;
    public float[] lowStrengthRec, midStrengthRec, highStrengthRec;
    
    public RhythmTool rhythmTool;
    public Vector3 startPosition, endPosition;
    public bool high, mid, low;
    public float debugLineOffset;
    public AnalysisData lowAnaData, midAnaData, highAnaData;
    private PreCalcSongData preCalc;


    private void Awake()
    {
        preCalc = GetComponent<PreCalcSongData>();
    }

    private void Start()
    {
        m_spawnObjects = new GameObject[3];
        m_spawnObjects[0] = GameObject.FindWithTag("Spawn0");
        m_spawnObjects[1] = GameObject.FindWithTag("Spawn1");
        m_spawnObjects[2] = GameObject.FindWithTag("Spawn2");
        //m_spawnObjects[3] = GameObject.FindWithTag("Spawn3");

        lowStrengthRec = midStrengthRec = highStrengthRec = new float[5];
        rhythmTool = GetComponent<RhythmTool>();

        lowCount = 0;
        midCount = 0;
        highCount = 0;
    }

    public void OnBeat(Beat beat)
    {
        beatCount += 1;

        #region Spawn gen 3 

        if(highStrength> midStrength && highStrength > lowStrength)
        {
            if (highStrengthRec[1] > preCalc.highOnsetAvg && highStrengthRec[2] > preCalc.highOnsetAvg && highStrengthRec[3] > preCalc.highOnsetAvg && highStrengthRec[4] > preCalc.highOnsetAvg)
            {
                enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
            }
            else if (highStrengthRec[1] > preCalc.highOnsetAvg && midStrengthRec[1] > preCalc.midOnsetAvg)
            {
                enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
                enemySpawn2 = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);

            }
            else
            {
                enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
            }
        }

        if (midStrength > highStrength && midStrength > lowStrength)
        {
            if (midStrengthRec[1] > preCalc.midOnsetAvg && midStrengthRec[2] > preCalc.midOnsetAvg && midStrengthRec[3] > preCalc.midOnsetAvg && midStrengthRec[4] > preCalc.midOnsetAvg)
            {
                enemySpawn = Instantiate(m_enemySpawn[4], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
            }
            else if (midStrengthRec[1] > preCalc.midOnsetAvg && lowStrengthRec[1] > preCalc.lowOnsetAvg)
            {
                enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
                enemySpawn2 = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
            }
            else
            {
                enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
            }
        }

        if(lowStrength > midStrength && lowStrength > highStrength)
        {
            if (lowStrengthRec[1] > preCalc.lowOnsetAvg && lowStrengthRec[2] > preCalc.lowOnsetAvg && lowStrengthRec[3] > preCalc.lowOnsetAvg && lowStrengthRec[4] > preCalc.lowOnsetAvg)
            {
                enemySpawn = Instantiate(m_enemySpawn[5], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
            }
            else if (lowStrengthRec[1] > preCalc.lowOnsetAvg && highStrengthRec[1] > preCalc.highOnsetAvg)
            {
                enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
                enemySpawn2 = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
            }
            else
            {
                enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
            }
        }
        #endregion

        #region Spawn gen 2
        ///////////////
        ////new spawn//
        ///////////////
        //if (lowCount > midCount && lowCount == highCount)
        //{
        //    enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //    enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
        //}
        //else if (lowCount > midCount && lowCount > highCount)
        //{
        //    if (midCount == highCount)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
        //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    }
        //    else
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
        //    }
        //}
        //else if (highCount > lowCount && highCount > midCount)
        //{
        //    if (lowCount == midCount)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    }
        //    else
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //    }
        //}
        //else if (midCount > lowCount)
        //{
        //    if (lowCount > highCount)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);

        //    }
        //    else if (midCount == highCount)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    }
        //    else if (lowCount == highCount)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    }
        //    else
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    }
        //}
        //else if (midCount > highCount)
        //{
        //    if (midCount == lowCount)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //        enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
        //    }
        //}
        #endregion

        #region Spawn Gen 1
        /////////////
        //old spawn//
        /////////////
        //if (high)
        //{
        //    if (mid == false && low == false)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);

        //    }
        //    else if (mid && !low)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    }
        //    else
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    }
        //}
        //else
        //{
        //    if (mid)
        //    {
        //        if (!low)
        //        {
        //            enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //        }
        //        else
        //        {
        //            enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //            enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //        }
        //    }
        //    else if (low)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    }
        //    else
        //    {
        //        randomSpam = Random.Range(1, 4);
        //        enemySpawn = Instantiate(m_enemySpawn[randomSpam], m_spawnObjects[randomSpam].transform.position, m_spawnObjects[randomSpam].transform.rotation);

        //    }
        //}
        #endregion

        //Vector3 debugStregthValues = new Vector3(highStrength, midStrength, lowStrength);
        //Debug.Log(debugStregthValues);

        highStrength = 0;
        midStrength = 0;
        lowStrength = 0;

        //Vector3 debugValues = new Vector3(highCount, midCount, lowCount);
        //Debug.Log(debugValues);
        
        highCount = 0;
        midCount = 0;
        lowCount = 0;

        
    }

    public void OnSubBeat(Beat beat, int beatType)
    {
        beatCount += 1;

        if (beatType == 2)
        {
            #region Spawn Gen 2 (subBeats)

            if (highStrength > midStrength && highStrength > lowStrength)
            {
                enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
            }

            if (midStrength > highStrength && midStrength > lowStrength)
            {
                enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
            }

            if (lowStrength > midStrength && lowStrength > highStrength)
            {
                enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);

            }
            #endregion

            #region Spawn Gen 1 (subBeats)
            //if (high)
            //{
            //    if (mid == false && low == false)
            //    {
            //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);

            //    }
            //    else if (mid && !low)
            //    {
            //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
            //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);

            //    }
            //    else
            //    {
            //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
            //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
            //    }
            //}
            //else
            //{
            //    if (mid)
            //    {
            //        if (!low)
            //        {
            //            enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
            //        }
            //        else
            //        {
            //            enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
            //            enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
            //        }
            //    }
            //    else if (low)
            //    {
            //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
            //    }
            //    else
            //    {
            //        enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);

            //    }
            //}

            #endregion
        }

        //if (beatType == 1)
        //{
        //    #region Spawn Gen 2 (subBeats)

        //    if (highStrength > midStrenght && highStrength > midStrenght)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //    }

        //    if (midStrenght > highStrength && midStrenght > lowStrenght)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    }

        //    if (lowStrenght > midStrenght && lowStrenght > highStrength)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);

        //    }
        //    #endregion

        //    #region Spawn Gen 1 (subBeats)
        //    //if (high)
        //    //{
        //    //    if (mid == false && low == false)
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);

        //    //    }
        //    //    else if (mid && !low)
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //    //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);

        //    //    }
        //    //    else
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //    //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    if (mid)
        //    //    {
        //    //        if (!low)
        //    //        {
        //    //            enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    //        }
        //    //        else
        //    //        {
        //    //            enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    //            enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    //        }
        //    //    }
        //    //    else if (low)
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    //    }
        //    //    else
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);

        //    //    }
        //    //}
        //        #endregion
        //}

        //if (beatType == 3)
        //{
        //    #region Spawn Gen 2 (subBeats)

        //    if (highStrength > midStrenght && highStrength > midStrenght)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //    }

        //    if (midStrenght > highStrength && midStrenght > lowStrenght)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    }

        //    if (lowStrenght > midStrenght && lowStrenght > highStrength)
        //    {
        //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);

        //    }
        //    #endregion

        //    #region Spawn Gen 1 (subBeats)
        //    //if (high)
        //    //{
        //    //    if (mid == false && low == false)
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);

        //    //    }
        //    //    else if (mid && !low)
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //    //        enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);

        //    //    }
        //    //    else
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
        //    //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    if (mid)
        //    //    {
        //    //        if (!low)
        //    //        {
        //    //            enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    //        }
        //    //        else
        //    //        {
        //    //            enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
        //    //            enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    //        }
        //    //    }
        //    //    else if (low)
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
        //    //    }
        //    //    else
        //    //    {
        //    //        enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);

        //    //    }
        //    //}

        //    #endregion
        //}
    }

    public void OnOnSet(OnsetType type, Onset onSet)
    {
        
        if (type == OnsetType.High)
        {
            highCount++;
            highStrength += onSet.strength;
            //GameObject enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
            high = true;

            highStrengthRec[4] = highStrengthRec[3];
            highStrengthRec[3] = highStrengthRec[2];
            highStrengthRec[2] = highStrengthRec[1];
            highStrengthRec[1] = highStrengthRec[0];
            highStrengthRec[0] = highStrength;
        }
        else
        {
            high = false;
        }

        if(type == OnsetType.Mid)
        {
            midCount++;
            midStrength = onSet.strength;
            //GameObject enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
            mid = true;

            midStrengthRec[4] = midStrengthRec[3];
            midStrengthRec[3] = midStrengthRec[2];
            midStrengthRec[2] = midStrengthRec[1];
            midStrengthRec[1] = midStrengthRec[0];
            midStrengthRec[0] = midStrength;
        }
        else
        {
            mid = false;
        }

        if(type == OnsetType.Low)
        {
            lowCount++;
            lowStrength = onSet.strength;
            //GameObject enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
            low = true;

            lowStrengthRec[4] = lowStrengthRec[3];
            lowStrengthRec[3] = lowStrengthRec[2];
            lowStrengthRec[2] = lowStrengthRec[1];
            lowStrengthRec[1] = lowStrengthRec[0];
            lowStrengthRec[0] = lowStrength;
        }
        else
        {
            low = false;
        }

    }

}
