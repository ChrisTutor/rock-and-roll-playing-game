﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BossUIManager : MonoBehaviour
{
    public Slider bossHealthBar;
    public BossStatManager bossStatManager;

    public void Awake()
    {
        bossHealthBar = GameObject.FindWithTag("BossHealthBar").GetComponent<Slider>();
        bossStatManager = GetComponent<BossStatManager>();
    }

    public void Start()
    {
        bossHealthBar.minValue = 0;
        bossHealthBar.maxValue = bossStatManager.bossMaxHealth;
    }

    public void Update()
    {
        bossHealthBar.value = bossStatManager.bossHealth;
    }
}
