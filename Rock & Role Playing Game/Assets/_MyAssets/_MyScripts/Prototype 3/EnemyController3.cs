﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController3 : MonoBehaviour {

    Vector3 m_startPos;
    Vector3 m_endPos;
    public float m_bpm = 80;
    public float t = 0;
    RhythmTool rhythmTool;


    //the death particl effect
    GameObject deathParticel;


    public void Awake()
    {
        deathParticel = Resources.Load("DeathParticle") as GameObject;
        
    }
    // Use this for initialization
    void Start()
    {

        m_startPos = transform.position;
        m_endPos = new Vector3(transform.position.x, transform.position.y, GameObject.FindWithTag("EndPoint").transform.position.z);
        m_bpm = 60;

    }

    // Update is called once per frame
    void Update()
    {

        t = m_bpm / 200; //gameManagerObj.GetComponent<RhythmEventProvider>().interpolation;
        transform.position = Vector3.MoveTowards(transform.position, m_endPos, t);
        //m_bpm = rhythmTool.bpm;

        if (transform.position == m_endPos)
        {
            Despawn();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.transform.tag == "Player")
        {
            Kill();
        }
    }

    public void Kill()
    {
        Instantiate(deathParticel, this.transform.position, this.transform.rotation);
        GameObject.Destroy(this.gameObject);
    }

    public void Despawn()
    {
        GameObject.Destroy(this.gameObject);
    }
}
