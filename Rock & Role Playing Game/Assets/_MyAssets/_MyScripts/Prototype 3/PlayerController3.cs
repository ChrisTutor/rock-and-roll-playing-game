﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController3 : MonoBehaviour {

    Vector3 targetPosition;
    Vector3 startingPosition;
    public float timeStep;

    public void AssignNewPlayerPosition(Vector2 position)
    {
        //timeStep = Time.time;
        startingPosition = transform.position;
        targetPosition = new Vector3(position.x, this.transform.position.y, position.y);
    }

    void Start()
    {
        targetPosition = this.transform.position;
    }

    void Update()
    {
        if (transform.position != targetPosition)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, targetPosition, timeStep);
        }
    }
}
