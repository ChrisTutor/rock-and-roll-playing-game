﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController3 : MonoBehaviour {

    GameObject player;

    void Awake()
    {
        player = GameObject.FindWithTag("Player");
    }

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnButtonPress);
    }

    void OnButtonPress()
    {
        player.GetComponent<PlayerController3>().AssignNewPlayerPosition(new Vector2(this.transform.position.x, this.transform.position.z));
    }
}
