﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour {

    public Slider loadingSlider;

    public void Awake()
    {
        loadingSlider = GameObject.Find("LoadingSlider").GetComponent<Slider>();
    }

    public void Update()
    {
        if(SceneManager.GetActiveScene().isLoaded)
        {
            LoadScene(1);
        } 
    }

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(LoadAsyncronously(sceneIndex));
    }

    IEnumerator LoadAsyncronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress /0.9f);
            Debug.Log(progress);
            loadingSlider.value = progress;
            yield return null;
        }
    }
    
        
}
