﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnbuncherManager : MonoBehaviour {

    EnemyUnbuncher front, back, side1, side2;
    bool areOn;

    public void Awake()
    {
        front = transform.GetChild(0).GetComponent<EnemyUnbuncher>();
        back = transform.GetChild(1).GetComponent<EnemyUnbuncher>();
        side1 = transform.GetChild(2).GetComponent<EnemyUnbuncher>();
        side2 = transform.GetChild(3).GetComponent<EnemyUnbuncher>();
    }

    public void Start()
    {
        BeingHit(false);
    }

    public void BeingHit(bool isOn)
    {
        front.gameObject.GetComponent<BoxCollider>().enabled = isOn;
        back.gameObject.GetComponent<BoxCollider>().enabled = isOn;
        side1.gameObject.GetComponent<BoxCollider>().enabled = isOn;
        side2.gameObject.GetComponent<BoxCollider>().enabled = isOn;
        areOn = isOn;
    }

    public bool IsClear()
    {
        if (front.isClear && back.isClear)
        {
            if (side1.isClear || side2.isClear)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
