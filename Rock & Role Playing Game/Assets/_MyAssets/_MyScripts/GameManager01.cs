﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager01 : MonoBehaviour {

    AudioClip song;
    RhythmTool rhythmTool;

    public void OnSongLoaded()
    {
        rhythmTool.Play();
    }

    public void Awake()
    {
        rhythmTool = GetComponent<RhythmTool>();
        song = Resources.Load(PlayerPrefs.GetString("NextSong")) as AudioClip;
    }

    public void Start()
    {
        rhythmTool = GetComponent<RhythmTool>();
        rhythmTool.NewSong(song);
        rhythmTool.SongLoaded += OnSongLoaded;
    }
}
