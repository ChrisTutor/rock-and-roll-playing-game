﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitZoneManager : MonoBehaviour {

    public bool m_green, m_red, m_yellow, m_blue;
    public GameObject m_greenHitZone, m_redHitZone, m_blueHitZone, m_yellowHitZone;
    public GameObject m_greenTarget, m_redTarget, m_yellowTarget, m_blueTarget; 


    private void Start()
    {
        m_greenHitZone = GameObject.FindWithTag("HitZone0");
        m_redHitZone = GameObject.FindWithTag("HitZone1");
        m_yellowHitZone = GameObject.FindWithTag("HitZone2");
        //m_blueHitZone = GameObject.FindWithTag("HitZone3");
    }

    private void Update()
    {
        m_green = m_greenHitZone.GetComponent<HitZoneControl>().m_isInZone;

        if(m_greenHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
        {
            m_greenTarget = m_greenHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
        }

        m_red = m_redHitZone.GetComponent<HitZoneControl>().m_isInZone;

        if(m_redHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
        {
            m_redTarget = m_redHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
        }

        m_yellow = m_yellowHitZone.GetComponent<HitZoneControl>().m_isInZone;

        if (m_yellowHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
        {
            m_yellowTarget = m_yellowHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
        }

        //m_blue = m_blueHitZone.GetComponent<HitZoneControl>().m_isInZone;

        //if(m_blueHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
        //{
        //    m_blueTarget = m_blueHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
        //}
    }

}
