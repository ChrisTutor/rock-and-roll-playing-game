﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnbuncher : MonoBehaviour {

    public bool isClear = true;
    public void Start()
    {
        isClear = true;
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject != transform.parent)
        {
            isClear = false;
        }

    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject != transform.parent)
        {
            isClear = true;
        }
    }

    //public void OnTriggerExit(Collider other)
    //{
    //    isClear = true;
    //}

    public void Update()
    {
        
    }
}
