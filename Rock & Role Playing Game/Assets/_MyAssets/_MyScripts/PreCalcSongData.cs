﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(RhythmTool))]

public class PreCalcSongData : MonoBehaviour {

    RhythmTool rhythmTool;
    private AnalysisData low, mid, high;
    private Beat beat;

    int lowCounter, midCounter, highCounter;
    int beatCounter;
    float averageBpm;
    float lowOnsetTotal, midOnsetTotal, highOnsetTotal;
    public float lowOnsetAvg, midOnsetAvg, highOnsetAvg;



    void Awake()
    {
        rhythmTool = GetComponent<RhythmTool>();
        rhythmTool.SongLoaded += PreCalc;
    }

    public void PreCalc()
    {
        low = rhythmTool.low;
        mid = rhythmTool.mid;
        high = rhythmTool.high;
        

        lowCounter = midCounter = highCounter = 0;

        for (int i = 0; i < rhythmTool.totalFrames; i++)
        {
            int frameIndex = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames);

            float lowOnset = low.GetOnset(frameIndex);
            float midOnset = mid.GetOnset(frameIndex);
            float highOnset = high.GetOnset(frameIndex);

            //if (rhythmTool.IsBeat(frameIndex) == 1)
            //{
            //    //beat = 
            //    beatCounter++;
            //}

            if (lowOnset > 0)
            {
                lowOnsetTotal += lowOnset;
                lowCounter++;
            }
            if (midOnset > 0)
            {
                midOnsetTotal += midOnset;
                midCounter++;
            }
            if (highOnset > 0)
            {
                highOnsetTotal += highOnset;
                highCounter++;
            }
        }

        lowOnsetAvg = lowOnsetTotal / lowCounter;
        Debug.Log(lowOnsetAvg);

        midOnsetAvg = midOnsetTotal / midCounter;
        Debug.Log(midOnsetAvg);

        highOnsetAvg = highOnsetTotal / highCounter;
        Debug.Log(highOnsetAvg);

        //float songLengthMin = / 60;
        //Debug.Log("TimeSeconds = " + rhythmTool.TimeSeconds() + " Min per song = " + songLengthMin);
        //averageBpm = beatCounter / songLengthMin;

        //Debug.Log("Total Beats: "+ beatCounter.ToString());
    }
}
