﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUserControl : MonoBehaviour {

    HitZoneManager hitZoneManager;
    Animator animator;
    ScoreManager scoreManager;

    PlayerManager playerHealthManager;

    

    private void Start()
    {
        
        hitZoneManager = GameObject.FindWithTag("HitZoneParent").GetComponent<HitZoneManager>();
        scoreManager = GameObject.FindWithTag("GameController").GetComponent<ScoreManager>();
        animator = GetComponent<Animator>();
        playerHealthManager = GetComponent<PlayerManager>();
    }

    private void FixedUpdate()
    {
        if(Input.GetButtonDown("Green"))
        {
            if (hitZoneManager.m_green)
            {
                if(hitZoneManager.m_greenTarget != null)
                {
                    hitZoneManager.m_greenTarget.GetComponent<EnemyController>().IsHit(playerHealthManager.playerDamage);
                    hitZoneManager.m_green = false;
                    //scoreManager.Hit();
                }

            }
        }

        if (Input.GetButtonDown("Red"))
        {
            if (hitZoneManager.m_red)
            {

                if(hitZoneManager.m_redTarget != null)
                {
                    hitZoneManager.m_redTarget.GetComponent<EnemyController>().IsHit(playerHealthManager.playerDamage);
                    hitZoneManager.m_red = false;
                    //scoreManager.Hit();
                }


            }
        }

        if (Input.GetButtonDown("Yellow"))
        {
            if(hitZoneManager.m_yellowTarget)
            {
                if (hitZoneManager.m_yellow)
                {
                    hitZoneManager.m_yellowTarget.GetComponent<EnemyController>().IsHit(playerHealthManager.playerDamage);
                    hitZoneManager.m_yellow = false;
                    //scoreManager.Hit();


                }
            }
            
        }

        if (Input.GetButtonDown("Blue"))
        {
            if (hitZoneManager.m_blue)
            {

                if(hitZoneManager.m_blueTarget != null)
                {
                    hitZoneManager.m_blueTarget.GetComponent<EnemyController>().IsHit(playerHealthManager.playerDamage);
                    hitZoneManager.m_blue = false;
                    //scoreManager.Hit();
                }
            }
        }
    }
}
