﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// AnalysisData
struct AnalysisData_t108342674;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.Dictionary`2<System.Int32,Onset>
struct Dictionary_2_t4035389440;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Analysis
struct  Analysis_t439488098  : public Il2CppObject
{
public:
	// AnalysisData Analysis::<analysisData>k__BackingField
	AnalysisData_t108342674 * ___U3CanalysisDataU3Ek__BackingField_0;
	// System.String Analysis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<System.Int32> Analysis::onsetIndices
	List_1_t1440998580 * ___onsetIndices_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,Onset> Analysis::_onsets
	Dictionary_2_t4035389440 * ____onsets_3;
	// System.Collections.Generic.List`1<System.Single> Analysis::_magnitude
	List_1_t1445631064 * ____magnitude_4;
	// System.Collections.Generic.List`1<System.Single> Analysis::_magnitudeSmooth
	List_1_t1445631064 * ____magnitudeSmooth_5;
	// System.Collections.Generic.List`1<System.Single> Analysis::_flux
	List_1_t1445631064 * ____flux_6;
	// System.Collections.Generic.List`1<System.Single> Analysis::_magnitudeAvg
	List_1_t1445631064 * ____magnitudeAvg_7;
	// System.Int32 Analysis::t1
	int32_t ___t1_8;
	// System.Int32 Analysis::t2
	int32_t ___t2_9;
	// System.Int32 Analysis::p1
	int32_t ___p1_10;
	// System.Int32 Analysis::p2
	int32_t ___p2_11;
	// System.Int32 Analysis::start
	int32_t ___start_12;
	// System.Int32 Analysis::end
	int32_t ___end_13;
	// System.Int32 Analysis::totalFrames
	int32_t ___totalFrames_14;

public:
	inline static int32_t get_offset_of_U3CanalysisDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___U3CanalysisDataU3Ek__BackingField_0)); }
	inline AnalysisData_t108342674 * get_U3CanalysisDataU3Ek__BackingField_0() const { return ___U3CanalysisDataU3Ek__BackingField_0; }
	inline AnalysisData_t108342674 ** get_address_of_U3CanalysisDataU3Ek__BackingField_0() { return &___U3CanalysisDataU3Ek__BackingField_0; }
	inline void set_U3CanalysisDataU3Ek__BackingField_0(AnalysisData_t108342674 * value)
	{
		___U3CanalysisDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanalysisDataU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_onsetIndices_2() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___onsetIndices_2)); }
	inline List_1_t1440998580 * get_onsetIndices_2() const { return ___onsetIndices_2; }
	inline List_1_t1440998580 ** get_address_of_onsetIndices_2() { return &___onsetIndices_2; }
	inline void set_onsetIndices_2(List_1_t1440998580 * value)
	{
		___onsetIndices_2 = value;
		Il2CppCodeGenWriteBarrier(&___onsetIndices_2, value);
	}

	inline static int32_t get_offset_of__onsets_3() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ____onsets_3)); }
	inline Dictionary_2_t4035389440 * get__onsets_3() const { return ____onsets_3; }
	inline Dictionary_2_t4035389440 ** get_address_of__onsets_3() { return &____onsets_3; }
	inline void set__onsets_3(Dictionary_2_t4035389440 * value)
	{
		____onsets_3 = value;
		Il2CppCodeGenWriteBarrier(&____onsets_3, value);
	}

	inline static int32_t get_offset_of__magnitude_4() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ____magnitude_4)); }
	inline List_1_t1445631064 * get__magnitude_4() const { return ____magnitude_4; }
	inline List_1_t1445631064 ** get_address_of__magnitude_4() { return &____magnitude_4; }
	inline void set__magnitude_4(List_1_t1445631064 * value)
	{
		____magnitude_4 = value;
		Il2CppCodeGenWriteBarrier(&____magnitude_4, value);
	}

	inline static int32_t get_offset_of__magnitudeSmooth_5() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ____magnitudeSmooth_5)); }
	inline List_1_t1445631064 * get__magnitudeSmooth_5() const { return ____magnitudeSmooth_5; }
	inline List_1_t1445631064 ** get_address_of__magnitudeSmooth_5() { return &____magnitudeSmooth_5; }
	inline void set__magnitudeSmooth_5(List_1_t1445631064 * value)
	{
		____magnitudeSmooth_5 = value;
		Il2CppCodeGenWriteBarrier(&____magnitudeSmooth_5, value);
	}

	inline static int32_t get_offset_of__flux_6() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ____flux_6)); }
	inline List_1_t1445631064 * get__flux_6() const { return ____flux_6; }
	inline List_1_t1445631064 ** get_address_of__flux_6() { return &____flux_6; }
	inline void set__flux_6(List_1_t1445631064 * value)
	{
		____flux_6 = value;
		Il2CppCodeGenWriteBarrier(&____flux_6, value);
	}

	inline static int32_t get_offset_of__magnitudeAvg_7() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ____magnitudeAvg_7)); }
	inline List_1_t1445631064 * get__magnitudeAvg_7() const { return ____magnitudeAvg_7; }
	inline List_1_t1445631064 ** get_address_of__magnitudeAvg_7() { return &____magnitudeAvg_7; }
	inline void set__magnitudeAvg_7(List_1_t1445631064 * value)
	{
		____magnitudeAvg_7 = value;
		Il2CppCodeGenWriteBarrier(&____magnitudeAvg_7, value);
	}

	inline static int32_t get_offset_of_t1_8() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___t1_8)); }
	inline int32_t get_t1_8() const { return ___t1_8; }
	inline int32_t* get_address_of_t1_8() { return &___t1_8; }
	inline void set_t1_8(int32_t value)
	{
		___t1_8 = value;
	}

	inline static int32_t get_offset_of_t2_9() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___t2_9)); }
	inline int32_t get_t2_9() const { return ___t2_9; }
	inline int32_t* get_address_of_t2_9() { return &___t2_9; }
	inline void set_t2_9(int32_t value)
	{
		___t2_9 = value;
	}

	inline static int32_t get_offset_of_p1_10() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___p1_10)); }
	inline int32_t get_p1_10() const { return ___p1_10; }
	inline int32_t* get_address_of_p1_10() { return &___p1_10; }
	inline void set_p1_10(int32_t value)
	{
		___p1_10 = value;
	}

	inline static int32_t get_offset_of_p2_11() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___p2_11)); }
	inline int32_t get_p2_11() const { return ___p2_11; }
	inline int32_t* get_address_of_p2_11() { return &___p2_11; }
	inline void set_p2_11(int32_t value)
	{
		___p2_11 = value;
	}

	inline static int32_t get_offset_of_start_12() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___start_12)); }
	inline int32_t get_start_12() const { return ___start_12; }
	inline int32_t* get_address_of_start_12() { return &___start_12; }
	inline void set_start_12(int32_t value)
	{
		___start_12 = value;
	}

	inline static int32_t get_offset_of_end_13() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___end_13)); }
	inline int32_t get_end_13() const { return ___end_13; }
	inline int32_t* get_address_of_end_13() { return &___end_13; }
	inline void set_end_13(int32_t value)
	{
		___end_13 = value;
	}

	inline static int32_t get_offset_of_totalFrames_14() { return static_cast<int32_t>(offsetof(Analysis_t439488098, ___totalFrames_14)); }
	inline int32_t get_totalFrames_14() const { return ___totalFrames_14; }
	inline int32_t* get_address_of_totalFrames_14() { return &___totalFrames_14; }
	inline void set_totalFrames_14(int32_t value)
	{
		___totalFrames_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
