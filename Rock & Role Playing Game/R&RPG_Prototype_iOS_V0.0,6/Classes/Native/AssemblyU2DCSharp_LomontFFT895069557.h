﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LomontFFT
struct  LomontFFT_t895069557  : public Il2CppObject
{
public:
	// System.Int32 LomontFFT::<A>k__BackingField
	int32_t ___U3CAU3Ek__BackingField_0;
	// System.Int32 LomontFFT::<B>k__BackingField
	int32_t ___U3CBU3Ek__BackingField_1;
	// System.Single[] LomontFFT::cosTable
	SingleU5BU5D_t577127397* ___cosTable_2;
	// System.Single[] LomontFFT::sinTable
	SingleU5BU5D_t577127397* ___sinTable_3;

public:
	inline static int32_t get_offset_of_U3CAU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LomontFFT_t895069557, ___U3CAU3Ek__BackingField_0)); }
	inline int32_t get_U3CAU3Ek__BackingField_0() const { return ___U3CAU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CAU3Ek__BackingField_0() { return &___U3CAU3Ek__BackingField_0; }
	inline void set_U3CAU3Ek__BackingField_0(int32_t value)
	{
		___U3CAU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CBU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LomontFFT_t895069557, ___U3CBU3Ek__BackingField_1)); }
	inline int32_t get_U3CBU3Ek__BackingField_1() const { return ___U3CBU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CBU3Ek__BackingField_1() { return &___U3CBU3Ek__BackingField_1; }
	inline void set_U3CBU3Ek__BackingField_1(int32_t value)
	{
		___U3CBU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_cosTable_2() { return static_cast<int32_t>(offsetof(LomontFFT_t895069557, ___cosTable_2)); }
	inline SingleU5BU5D_t577127397* get_cosTable_2() const { return ___cosTable_2; }
	inline SingleU5BU5D_t577127397** get_address_of_cosTable_2() { return &___cosTable_2; }
	inline void set_cosTable_2(SingleU5BU5D_t577127397* value)
	{
		___cosTable_2 = value;
		Il2CppCodeGenWriteBarrier(&___cosTable_2, value);
	}

	inline static int32_t get_offset_of_sinTable_3() { return static_cast<int32_t>(offsetof(LomontFFT_t895069557, ___sinTable_3)); }
	inline SingleU5BU5D_t577127397* get_sinTable_3() const { return ___sinTable_3; }
	inline SingleU5BU5D_t577127397** get_address_of_sinTable_3() { return &___sinTable_3; }
	inline void set_sinTable_3(SingleU5BU5D_t577127397* value)
	{
		___sinTable_3 = value;
		Il2CppCodeGenWriteBarrier(&___sinTable_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
