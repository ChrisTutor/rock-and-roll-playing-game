﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// GameManager
struct GameManager_t2252321495;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyController
struct  EnemyController_t2146768720  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 EnemyController::m_startPos
	Vector3_t2243707580  ___m_startPos_2;
	// UnityEngine.Vector3 EnemyController::m_endPos
	Vector3_t2243707580  ___m_endPos_3;
	// System.Single EnemyController::time
	float ___time_4;
	// UnityEngine.GameObject EnemyController::gameManagerObj
	GameObject_t1756533147 * ___gameManagerObj_5;
	// GameManager EnemyController::gameManager
	GameManager_t2252321495 * ___gameManager_6;

public:
	inline static int32_t get_offset_of_m_startPos_2() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___m_startPos_2)); }
	inline Vector3_t2243707580  get_m_startPos_2() const { return ___m_startPos_2; }
	inline Vector3_t2243707580 * get_address_of_m_startPos_2() { return &___m_startPos_2; }
	inline void set_m_startPos_2(Vector3_t2243707580  value)
	{
		___m_startPos_2 = value;
	}

	inline static int32_t get_offset_of_m_endPos_3() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___m_endPos_3)); }
	inline Vector3_t2243707580  get_m_endPos_3() const { return ___m_endPos_3; }
	inline Vector3_t2243707580 * get_address_of_m_endPos_3() { return &___m_endPos_3; }
	inline void set_m_endPos_3(Vector3_t2243707580  value)
	{
		___m_endPos_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_gameManagerObj_5() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___gameManagerObj_5)); }
	inline GameObject_t1756533147 * get_gameManagerObj_5() const { return ___gameManagerObj_5; }
	inline GameObject_t1756533147 ** get_address_of_gameManagerObj_5() { return &___gameManagerObj_5; }
	inline void set_gameManagerObj_5(GameObject_t1756533147 * value)
	{
		___gameManagerObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameManagerObj_5, value);
	}

	inline static int32_t get_offset_of_gameManager_6() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___gameManager_6)); }
	inline GameManager_t2252321495 * get_gameManager_6() const { return ___gameManager_6; }
	inline GameManager_t2252321495 ** get_address_of_gameManager_6() { return &___gameManager_6; }
	inline void set_gameManager_6(GameManager_t2252321495 * value)
	{
		___gameManager_6 = value;
		Il2CppCodeGenWriteBarrier(&___gameManager_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
