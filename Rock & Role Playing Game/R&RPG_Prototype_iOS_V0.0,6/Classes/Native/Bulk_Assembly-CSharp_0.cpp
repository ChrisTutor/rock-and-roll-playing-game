﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_Analysis439488098.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_AnalysisData108342674.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4035389440.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21792734662.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "AssemblyU2DCSharp_Onset732596509.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2262295624.h"
#include "AssemblyU2DCSharp_ReadOnlyDictionary_2_gen28079572.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_AttackButtonManager2228488667.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_HitZoneManager1308216902.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_BasicController2369989902.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_RhythmTool215962618.h"
#include "AssemblyU2DCSharp_Beat2695683572.h"
#include "AssemblyU2DCSharp_BeatTracker2801099156.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1703509207.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2257663140.h"
#include "AssemblyU2DCSharp_ReadOnlyDictionary_2_gen1991166635.h"
#include "AssemblyU2DCSharp_SongData3132760915.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23755821725.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4241422837.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_678743357.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3023533909.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2921398135.h"
#include "mscorlib_System_Threading_WaitCallback2798937288.h"
#include "AssemblyU2DCSharp_CameraMotor550925884.h"
#include "AssemblyU2DCSharp_DataController1918319064.h"
#include "AssemblyU2DCSharp_EnemyController2146768720.h"
#include "AssemblyU2DCSharp_EventsController2543309541.h"
#include "AssemblyU2DCSharp_RhythmEventProvider215006757.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_OnNewSong4169165798.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1195085273.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_RhythmEvent_2308618982.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_BeatEvent33541086.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4062269323.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_RhythmEvent_2633773566.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_SubBeatEvent2499915164.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen771031906.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_RhythmEvent_4179532911.h"
#include "AssemblyU2DCSharp_FailScreenButtonControl1753294869.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_PlayerHealthManager3067865410.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "AssemblyU2DCSharp_HitZoneControl2657704264.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_Line2729441502.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "AssemblyU2DCSharp_LomontFFT895069557.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Double4078015681.h"
#include "AssemblyU2DCSharp_OnsetType3680206797.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "AssemblyU2DCSharp_PlayerMotor2528789646.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_CollisionFlags4046947985.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "AssemblyU2DCSharp_PlayerUserControl2572554735.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_OnsetEvent1329939269.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_ChangeEvent1135638418.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_TimingUpdateE543213975.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_FrameChangedE800671821.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol400792449.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3879095185.h"
#include "mscorlib_System_Action_1_gen16806139.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_RhythmEvent_1701265135.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_RhythmEvent_1696632651.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_RhythmEvent_2688166119.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_RhythmEvent_3330968721.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_ReadOnlyDictionary_2_gen1371992995.h"
#include "AssemblyU2DCSharp_Segmenter2695296026.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4103576526.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen3077283191.h"
#include "AssemblyU2DCSharp_RhythmTool_U3CQueueNewSongU3Ec__1486555536.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3638306200.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3307160776.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "mscorlib_System_IO_Path41728875.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3772431102.h"
#include "AssemblyU2DCSharp_RhythmTool_U3CAsyncAnalyzeU3Ec__1906342971.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen1810603002.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2653229824.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen170329564.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen3456830328.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2734033587.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen174962048.h"
#include "UnityEngine_UnityEngine_FFTWindow2870052902.h"
#include "mscorlib_System_Threading_ParameterizedThreadStart2412552885.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Threading_Thread241561612.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1084335567.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23136648085.h"
#include "mscorlib_System_IO_FileMode236403845.h"
#include "mscorlib_System_IO_FileAccess4282042064.h"
#include "mscorlib_System_IO_FileShare3362491215.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1866979105.h"
#include "mscorlib_System_IO_FileStream1695958676.h"
#include "AssemblyU2DCSharp_SpawnManager4269630218.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_TileManager3422405329.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279.h"
#include "AssemblyU2DCSharp_Tutorial_1_Controller3740746359.h"
#include "AssemblyU2DCSharp_Util4006552276.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AssemblyU2DCSharp_VisualizerController119580760.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2098562634.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2587731426.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1301679762.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1633292308.h"

// Analysis
struct Analysis_t439488098;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Collections.Generic.Dictionary`2<System.Int32,Onset>
struct Dictionary_2_t4035389440;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// AnalysisData
struct AnalysisData_t108342674;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t2368636977;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>
struct ReadOnlyCollection_1_t2262295624;
// ReadOnlyDictionary`2<System.Int32,Onset>
struct ReadOnlyDictionary_2_t28079572;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Onset>>
struct IEnumerator_1_t3563225785;
// ReadOnlyDictionary`2<System.Int32,System.Object>
struct ReadOnlyDictionary_2_t1984932358;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t1225111275;
// Onset
struct Onset_t732596509;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.IDictionary`2<System.Int32,Onset>
struct IDictionary_2_t2034472861;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Object>
struct IDictionary_2_t3991325647;
// AttackButtonManager
struct AttackButtonManager_t2228488667;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// HitZoneManager
struct HitZoneManager_t1308216902;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t2455055323;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Object
struct Object_t1021602117;
// BasicController
struct BasicController_t2369989902;
// RhythmTool
struct RhythmTool_t215962618;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Action
struct Action_t3226471752;
// Beat
struct Beat_t2695683572;
// BeatTracker
struct BeatTracker_t2801099156;
// System.Collections.Generic.Dictionary`2<System.Int32,Beat>
struct Dictionary_2_t1703509207;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t2257663140;
// ReadOnlyDictionary`2<System.Int32,Beat>
struct ReadOnlyDictionary_2_t1991166635;
// System.Collections.Generic.IDictionary`2<System.Int32,Beat>
struct IDictionary_2_t3997559924;
// SongData
struct SongData_t3132760915;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t2364004493;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Beat>>
struct IEnumerator_1_t1231345552;
// System.Collections.Generic.Dictionary`2<System.Single,System.Int32>
struct Dictionary_2_t2921398135;
// System.Threading.WaitCallback
struct WaitCallback_t2798937288;
// System.Array
struct Il2CppArray;
// CameraMotor
struct CameraMotor_t550925884;
// DataController
struct DataController_t1918319064;
// EnemyController
struct EnemyController_t2146768720;
// EventsController
struct EventsController_t2543309541;
// RhythmEventProvider
struct RhythmEventProvider_t215006757;
// UnityEngine.Events.UnityAction`2<System.String,System.Int32>
struct UnityAction_2_t1195085273;
// UnityEngine.Events.UnityAction`2<System.Object,System.Int32>
struct UnityAction_2_t3167333435;
// RhythmEventProvider/RhythmEvent`2<System.String,System.Int32>
struct RhythmEvent_2_t308618982;
// RhythmEventProvider/RhythmEvent`2<System.Object,System.Int32>
struct RhythmEvent_2_t2280867144;
// UnityEngine.Events.UnityAction`1<Beat>
struct UnityAction_1_t4062269323;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// RhythmEventProvider/RhythmEvent`1<Beat>
struct RhythmEvent_1_t2633773566;
// RhythmEventProvider/RhythmEvent`1<System.Object>
struct RhythmEvent_1_t2627539289;
// UnityEngine.Events.UnityAction`2<Beat,System.Int32>
struct UnityAction_2_t771031906;
// RhythmEventProvider/RhythmEvent`2<Beat,System.Int32>
struct RhythmEvent_2_t4179532911;
// FailScreenButtonControl
struct FailScreenButtonControl_t1753294869;
// GameManager
struct GameManager_t2252321495;
// PlayerHealthManager
struct PlayerHealthManager_t3067865410;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// HitZoneControl
struct HitZoneControl_t2657704264;
// UnityEngine.Collider
struct Collider_t3497673348;
// Line
struct Line_t2729441502;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Material
struct Material_t193706927;
// LomontFFT
struct LomontFFT_t895069557;
// System.ArgumentException
struct ArgumentException_t3259014390;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// PlayerMotor
struct PlayerMotor_t2528789646;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// UnityEngine.Animation
struct Animation_t2068071072;
// PlayerUserControl
struct PlayerUserControl_t2572554735;
// UnityEngine.Animator
struct Animator_t69676727;
// RhythmEventProvider/BeatEvent
struct BeatEvent_t33541086;
// RhythmEventProvider/SubBeatEvent
struct SubBeatEvent_t2499915164;
// RhythmEventProvider/OnsetEvent
struct OnsetEvent_t1329939269;
// RhythmEventProvider/ChangeEvent
struct ChangeEvent_t1135638418;
// RhythmEventProvider/TimingUpdateEvent
struct TimingUpdateEvent_t543213975;
// RhythmEventProvider/FrameChangedEvent
struct FrameChangedEvent_t800671821;
// RhythmEventProvider/OnNewSong
struct OnNewSong_t4169165798;
// System.Collections.ObjectModel.ReadOnlyCollection`1<RhythmEventProvider>
struct ReadOnlyCollection_1_t400792449;
// System.Collections.Generic.List`1<RhythmEventProvider>
struct List_1_t3879095185;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2875234987;
// System.Action`1<RhythmEventProvider>
struct Action_1_t16806139;
// System.Delegate
struct Delegate_t3022476291;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// RhythmEventProvider/RhythmEvent`2<System.Int32,System.Single>
struct RhythmEvent_2_t1701265135;
// RhythmEventProvider/RhythmEvent`2<System.Int32,System.Int32>
struct RhythmEvent_2_t1696632651;
// RhythmEventProvider/RhythmEvent`2<OnsetType,Onset>
struct RhythmEvent_2_t688166119;
// RhythmEventProvider/RhythmEvent`2<OnsetType,System.Object>
struct RhythmEvent_2_t2645018905;
// RhythmEventProvider/RhythmEvent`4<System.Int32,System.Single,System.Single,System.Single>
struct RhythmEvent_4_t3330968721;
// ReadOnlyDictionary`2<System.Int32,System.Single>
struct ReadOnlyDictionary_2_t1371992995;
// Segmenter
struct Segmenter_t2695296026;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Collections.Generic.List`1<Analysis>
struct List_1_t4103576526;
// UnityEngine.Events.UnityEvent`2<System.String,System.Int32>
struct UnityEvent_2_t3077283191;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Int32>
struct UnityEvent_2_t754564057;
// System.Collections.Generic.IEnumerator`1<RhythmEventProvider>
struct IEnumerator_1_t1985497880;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// RhythmTool/<QueueNewSong>c__Iterator0
struct U3CQueueNewSongU3Ec__Iterator0_t1486555536;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.List`1<AnalysisData>
struct List_1_t3772431102;
// RhythmTool/<AsyncAnalyze>c__Iterator1
struct U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971;
// UnityEngine.Events.UnityEvent`4<System.Int32,System.Single,System.Single,System.Single>
struct UnityEvent_4_t1810603002;
// UnityEngine.Events.UnityEvent`2<Beat,System.Int32>
struct UnityEvent_2_t2653229824;
// UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>
struct UnityEvent_2_t170329564;
// UnityEngine.Events.UnityEvent`2<OnsetType,Onset>
struct UnityEvent_2_t3456830328;
// UnityEngine.Events.UnityEvent`2<OnsetType,System.Object>
struct UnityEvent_2_t1118715818;
// UnityEngine.Events.UnityEvent`1<Beat>
struct UnityEvent_1_t2734033587;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t2727799310;
// UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>
struct UnityEvent_2_t174962048;
// System.Threading.ParameterizedThreadStart
struct ParameterizedThreadStart_t2412552885;
// System.Threading.Thread
struct Thread_t241561612;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t1084335567;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Single>
struct IDictionary_2_t3378386284;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>
struct IEnumerator_1_t612171912;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t1866979105;
// System.IO.FileStream
struct FileStream_t1695958676;
// SpawnManager
struct SpawnManager_t4269630218;
// TileManager
struct TileManager_t3422405329;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// Tutorial_1_Controller
struct Tutorial_1_Controller_t3740746359;
// System.Exception
struct Exception_t1927440687;
// VisualizerController
struct VisualizerController_t119580760;
// System.Collections.Generic.List`1<Line>
struct List_1_t2098562634;
// UnityEngine.Events.UnityAction`2<System.Int32,System.Single>
struct UnityAction_2_t2587731426;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t1301679762;
extern Il2CppClass* List_1_t1445631064_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t4035389440_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern Il2CppClass* AnalysisData_t108342674_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1509370154_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4135957043_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m555649161_MethodInfo_var;
extern const uint32_t Analysis__ctor_m1571091587_MetadataUsageId;
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m3644677550_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m4110591713_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m4007066975_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m3607535064_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m620564420_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral888332695;
extern Il2CppCodeGenString* _stringLiteral2794641442;
extern const uint32_t Analysis_Init_m3996269154_MetadataUsageId;
extern Il2CppClass* IEnumerator_1_t3563225785_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_TrimExcess_m2205122117_MethodInfo_var;
extern const MethodInfo* ReadOnlyDictionary_2_GetEnumerator_m3517933108_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3879747900_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2281585953_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1340372924_MethodInfo_var;
extern const uint32_t Analysis_Init_m2824569801_MetadataUsageId;
extern const MethodInfo* Dictionary_2_TryGetValue_m3835210299_MethodInfo_var;
extern const uint32_t Analysis_GetOnset_m3617908717_MetadataUsageId;
extern Il2CppClass* Util_t4006552276_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_set_Item_m4206996116_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3254877171_MethodInfo_var;
extern const uint32_t Analysis_Analyze_m2787083839_MetadataUsageId;
extern const MethodInfo* Dictionary_2_ContainsKey_m1627758495_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m552409364_MethodInfo_var;
extern const uint32_t Analysis_DrawDebugLines_m2160884685_MetadataUsageId;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Analysis_Smooth_m199185592_MetadataUsageId;
extern const uint32_t Analysis_Smooth_m4230704111_MetadataUsageId;
extern Il2CppClass* Onset_t732596509_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m688682013_MethodInfo_var;
extern const uint32_t Analysis_FindPeaks_m2740314761_MetadataUsageId;
extern const MethodInfo* List_1_get_Count_m3406166720_MethodInfo_var;
extern const uint32_t Analysis_Threshold_m3563177457_MetadataUsageId;
extern const MethodInfo* List_1_IndexOf_m858031556_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m852068579_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const uint32_t Analysis_RankPeaks_m34459363_MetadataUsageId;
extern const uint32_t Analysis_Average_m3308932463_MetadataUsageId;
extern Il2CppClass* ReadOnlyDictionary_2_t28079572_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_AsReadOnly_m1653708817_MethodInfo_var;
extern const MethodInfo* ReadOnlyDictionary_2__ctor_m150260537_MethodInfo_var;
extern const uint32_t AnalysisData__ctor_m1090324430_MetadataUsageId;
extern const MethodInfo* ReadOnlyDictionary_2_TryGetValue_m2791401779_MethodInfo_var;
extern const uint32_t AnalysisData_GetOnset_m643039293_MetadataUsageId;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisHitZoneManager_t1308216902_m2080816239_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var;
extern const MethodInfo* AttackButtonManager_OnAttackPressed_m3799490329_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1397813543;
extern const uint32_t AttackButtonManager_Start_m1492590518_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510846499;
extern Il2CppCodeGenString* _stringLiteral3021629811;
extern Il2CppCodeGenString* _stringLiteral777220966;
extern Il2CppCodeGenString* _stringLiteral2395476974;
extern const uint32_t AttackButtonManager_OnAttackPressed_m3799490329_MetadataUsageId;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRhythmTool_t215962618_m966574361_MethodInfo_var;
extern const MethodInfo* BasicController_OnSongLoaded_m3706846700_MethodInfo_var;
extern const uint32_t BasicController_Start_m1102353557_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t1703509207_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadOnlyDictionary_2_t1991166635_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1598946593_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3978597214_MethodInfo_var;
extern const MethodInfo* List_1_AsReadOnly_m3503877813_MethodInfo_var;
extern const MethodInfo* ReadOnlyDictionary_2__ctor_m2336533874_MethodInfo_var;
extern const uint32_t BeatTracker__ctor_m1207605921_MetadataUsageId;
extern const MethodInfo* Dictionary_2_Clear_m1564562098_MethodInfo_var;
extern const uint32_t BeatTracker_Init_m1298165546_MetadataUsageId;
extern Il2CppClass* IEnumerator_1_t1231345552_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_AddRange_m2567809379_MethodInfo_var;
extern const MethodInfo* ReadOnlyDictionary_2_GetEnumerator_m885255707_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3096183613_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2369008986_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3808454219_MethodInfo_var;
extern const uint32_t BeatTracker_Init_m2105785558_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t2921398135_il2cpp_TypeInfo_var;
extern Il2CppClass* Beat_t2695683572_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1683620704_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2258219861_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m4174387577_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1122070824_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m2607921061_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1780001816_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m4065924530_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1983754276_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1733170775_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2838785900_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3884776761_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m797750959_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2354998391_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m909613959_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1568926478_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2364881746_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2527786909_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1062633493_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m3494432915_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m623660598_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4282865897_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1274756239_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_get_Count_m1734424457_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_get_Item_m925539044_MethodInfo_var;
extern const MethodInfo* List_1_Insert_m2673244951_MethodInfo_var;
extern const uint32_t BeatTracker_FillStart_m2215021468_MetadataUsageId;
extern const uint32_t BeatTracker_FillEnd_m1691323021_MetadataUsageId;
extern const uint32_t BeatTracker_AddBeat_m3465552806_MetadataUsageId;
extern Il2CppClass* WaitCallback_t2798937288_il2cpp_TypeInfo_var;
extern const MethodInfo* BeatTracker_U3CTrackBeatU3Em__0_m1598643413_MethodInfo_var;
extern const uint32_t BeatTracker_TrackBeat_m2149388341_MetadataUsageId;
extern const uint32_t BeatTracker_FindBeat_m212206812_MetadataUsageId;
extern const MethodInfo* Dictionary_2_ContainsKey_m604849294_MethodInfo_var;
extern const uint32_t BeatTracker_DrawDebugLines_m1071911214_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Count_m2326583579_MethodInfo_var;
extern const uint32_t BeatTracker_NextBeat_m3845220818_MetadataUsageId;
extern const uint32_t BeatTracker_PrevBeat_m277720998_MetadataUsageId;
extern const MethodInfo* List_1_BinarySearch_m4172392722_MethodInfo_var;
extern const uint32_t BeatTracker_NextBeatIndex_m2115259593_MetadataUsageId;
extern const uint32_t BeatTracker_PrevBeatIndex_m2127559407_MetadataUsageId;
extern const uint32_t BeatTracker_BeatTimer_m2155883739_MetadataUsageId;
extern const uint32_t BeatTracker_IsBeat_m3805475791_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t CameraMotor_Start_m1077771969_MetadataUsageId;
extern const uint32_t CameraMotor_Update_m300393638_MetadataUsageId;
extern const MethodInfo* DataController_OnSongLoaded_m440076366_MethodInfo_var;
extern const uint32_t DataController_Start_m1356344579_MetadataUsageId;
extern const MethodInfo* ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var;
extern const uint32_t DataController_Update_m2156759382_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral40213808;
extern const uint32_t EnemyController_Start_m2470974037_MetadataUsageId;
extern Il2CppClass* UnityAction_2_t1195085273_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t4062269323_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_2_t771031906_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRhythmEventProvider_t215006757_m163918678_MethodInfo_var;
extern const MethodInfo* EventsController_OnSongLoaded_m327293090_MethodInfo_var;
extern const MethodInfo* UnityAction_2__ctor_m3874320903_MethodInfo_var;
extern const MethodInfo* RhythmEvent_2_AddListener_m3749635587_MethodInfo_var;
extern const MethodInfo* EventsController_OnBeat_m1455163469_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3300750889_MethodInfo_var;
extern const MethodInfo* RhythmEvent_1_AddListener_m1356841763_MethodInfo_var;
extern const MethodInfo* EventsController_OnSubBeat_m1296718868_MethodInfo_var;
extern const MethodInfo* UnityAction_2__ctor_m3846259155_MethodInfo_var;
extern const MethodInfo* RhythmEvent_2_AddListener_m1848967427_MethodInfo_var;
extern const uint32_t EventsController_Start_m1558683108_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisPlayerHealthManager_t3067865410_m1535056983_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1362668110;
extern const uint32_t GameManager_Awake_m99497495_MetadataUsageId;
extern const MethodInfo* GameManager_OnSongLoaded_m1532214553_MethodInfo_var;
extern const uint32_t GameManager_Start_m2655388892_MetadataUsageId;
extern const uint32_t HitZoneControl_Awake_m2387973540_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2436044795;
extern Il2CppCodeGenString* _stringLiteral4002128736;
extern Il2CppCodeGenString* _stringLiteral1273245381;
extern Il2CppCodeGenString* _stringLiteral2839329322;
extern const uint32_t HitZoneManager_Start_m654590803_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var;
extern const uint32_t HitZoneManager_FixedUpdate_m1197673902_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t1268241104_m3460404950_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3855512561;
extern const uint32_t Line_Init_m3596432585_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2183092574;
extern Il2CppCodeGenString* _stringLiteral2444813273;
extern const uint32_t LomontFFT_FFT_m2236739420_MetadataUsageId;
extern const uint32_t LomontFFT_TableFFT_m1393901266_MetadataUsageId;
extern const uint32_t LomontFFT_RealFFT_m1443477460_MetadataUsageId;
extern const uint32_t LomontFFT_Initialize_m2055186573_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisSlider_t297367283_m33033319_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral975222662;
extern const uint32_t PlayerHealthManager_Awake_m2582082686_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisCharacterController_t4094781467_m1582798737_MethodInfo_var;
extern const uint32_t PlayerMotor_Start_m2017763775_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m2503703020_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral2309168105;
extern const uint32_t PlayerMotor_Update_m80195384_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const uint32_t PlayerUserControl_Start_m501980854_MetadataUsageId;
extern const uint32_t PlayerUserControl_FixedUpdate_m1847931561_MetadataUsageId;
extern Il2CppClass* BeatEvent_t33541086_il2cpp_TypeInfo_var;
extern Il2CppClass* SubBeatEvent_t2499915164_il2cpp_TypeInfo_var;
extern Il2CppClass* OnsetEvent_t1329939269_il2cpp_TypeInfo_var;
extern Il2CppClass* ChangeEvent_t1135638418_il2cpp_TypeInfo_var;
extern Il2CppClass* TimingUpdateEvent_t543213975_il2cpp_TypeInfo_var;
extern Il2CppClass* FrameChangedEvent_t800671821_il2cpp_TypeInfo_var;
extern Il2CppClass* OnNewSong_t4169165798_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityEvent_t408735097_il2cpp_TypeInfo_var;
extern const uint32_t RhythmEventProvider__ctor_m2933971684_MetadataUsageId;
extern Il2CppClass* RhythmEventProvider_t215006757_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_AsReadOnly_m3056134219_MethodInfo_var;
extern const uint32_t RhythmEventProvider_get_eventProviders_m1235811388_MetadataUsageId;
extern Il2CppClass* Action_1_t16806139_il2cpp_TypeInfo_var;
extern const uint32_t RhythmEventProvider_add_EventProviderEnabled_m2395562879_MetadataUsageId;
extern const uint32_t RhythmEventProvider_remove_EventProviderEnabled_m3999705428_MetadataUsageId;
extern const MethodInfo* List_1_Contains_m2342885972_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3390357842_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m4148756095_MethodInfo_var;
extern const uint32_t RhythmEventProvider_OnEnable_m13362604_MetadataUsageId;
extern const MethodInfo* List_1_Remove_m4083267577_MethodInfo_var;
extern const uint32_t RhythmEventProvider_OnDisable_m3828130085_MetadataUsageId;
extern Il2CppClass* List_1_t3879095185_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3143476342_MethodInfo_var;
extern const uint32_t RhythmEventProvider__cctor_m1114474335_MetadataUsageId;
extern const MethodInfo* RhythmEvent_1__ctor_m2502037646_MethodInfo_var;
extern const uint32_t BeatEvent__ctor_m1246752065_MetadataUsageId;
extern const MethodInfo* RhythmEvent_2__ctor_m460284861_MethodInfo_var;
extern const uint32_t ChangeEvent__ctor_m2572504441_MetadataUsageId;
extern const MethodInfo* RhythmEvent_2__ctor_m557224401_MethodInfo_var;
extern const uint32_t FrameChangedEvent__ctor_m790505990_MetadataUsageId;
extern const MethodInfo* RhythmEvent_2__ctor_m2299413610_MethodInfo_var;
extern const uint32_t OnNewSong__ctor_m1780728203_MetadataUsageId;
extern const MethodInfo* RhythmEvent_2__ctor_m1345214749_MethodInfo_var;
extern const uint32_t OnsetEvent__ctor_m1857777506_MetadataUsageId;
extern const MethodInfo* RhythmEvent_2__ctor_m2210944366_MethodInfo_var;
extern const uint32_t SubBeatEvent__ctor_m1401496825_MetadataUsageId;
extern const MethodInfo* RhythmEvent_4__ctor_m2850559519_MethodInfo_var;
extern const uint32_t TimingUpdateEvent__ctor_m1890207098_MetadataUsageId;
extern const uint32_t RhythmTool_add_SongLoaded_m1500519296_MetadataUsageId;
extern const uint32_t RhythmTool_remove_SongLoaded_m3258091535_MetadataUsageId;
extern const uint32_t RhythmTool_add_SongEnded_m3686190681_MetadataUsageId;
extern const uint32_t RhythmTool_remove_SongEnded_m951710700_MetadataUsageId;
extern const uint32_t RhythmTool_set_lead_m1732875513_MetadataUsageId;
extern Il2CppClass* List_1_t4103576526_il2cpp_TypeInfo_var;
extern Il2CppClass* Analysis_t439488098_il2cpp_TypeInfo_var;
extern Il2CppClass* BeatTracker_t2801099156_il2cpp_TypeInfo_var;
extern Il2CppClass* Segmenter_t2695296026_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m9134919_MethodInfo_var;
extern const MethodInfo* List_1_Add_m845626571_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1502598480;
extern Il2CppCodeGenString* _stringLiteral339799080;
extern Il2CppCodeGenString* _stringLiteral4217033790;
extern Il2CppCodeGenString* _stringLiteral4231481717;
extern const uint32_t RhythmTool_Init_m2742459395_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var;
extern const MethodInfo* RhythmTool_OnEventProviderEnabled_m1814537051_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2757687936_MethodInfo_var;
extern const uint32_t RhythmTool_Awake_m49992394_MetadataUsageId;
extern const MethodInfo* UnityEvent_2_Invoke_m2327814026_MethodInfo_var;
extern const uint32_t RhythmTool_InitializeEventProvider_m925349371_MetadataUsageId;
extern Il2CppClass* IEnumerator_1_t1985497880_il2cpp_TypeInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_GetEnumerator_m2483346076_MethodInfo_var;
extern const uint32_t RhythmTool_InitializeEventProviders_m198279347_MetadataUsageId;
extern Il2CppClass* U3CQueueNewSongU3Ec__Iterator0_t1486555536_il2cpp_TypeInfo_var;
extern const uint32_t RhythmTool_QueueNewSong_m891591844_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3071804604_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3316544882_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m391630496_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1317449898_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2880262412_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3892498626_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1403247792_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2097033946_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2395002637;
extern Il2CppCodeGenString* _stringLiteral2751490661;
extern const uint32_t RhythmTool_LoadNewSong_m1895013473_MetadataUsageId;
extern Il2CppClass* List_1_t3772431102_il2cpp_TypeInfo_var;
extern Il2CppClass* SongData_t3132760915_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2896249751_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3745327547_MethodInfo_var;
extern const uint32_t RhythmTool_EndOfAnalysis_m2036138409_MetadataUsageId;
extern const MethodInfo* ReadOnlyCollection_1_get_Item_m2127664468_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_get_Count_m3788110603_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2957948280;
extern const uint32_t RhythmTool_EndOfSong_m2843491136_MetadataUsageId;
extern const uint32_t RhythmTool_Update_m3028187676_MetadataUsageId;
extern const uint32_t RhythmTool_Analyze_m1399810877_MetadataUsageId;
extern Il2CppClass* U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971_il2cpp_TypeInfo_var;
extern const uint32_t RhythmTool_AsyncAnalyze_m1726189964_MetadataUsageId;
extern const uint32_t RhythmTool_BackGroundAnalyze_m3994437969_MetadataUsageId;
extern const MethodInfo* RhythmEvent_4_get_listenerCount_m3042521709_MethodInfo_var;
extern const MethodInfo* UnityEvent_4_Invoke_m1874634762_MethodInfo_var;
extern const uint32_t RhythmTool_PassData_m3430060328_MetadataUsageId;
extern const MethodInfo* UnityEvent_2_Invoke_m407873616_MethodInfo_var;
extern const uint32_t RhythmTool_PassSubBeat_m3729313650_MetadataUsageId;
extern const MethodInfo* UnityEvent_2_Invoke_m514975631_MethodInfo_var;
extern const MethodInfo* RhythmEvent_2_get_listenerCount_m405578019_MethodInfo_var;
extern const MethodInfo* UnityEvent_2_Invoke_m1980955963_MethodInfo_var;
extern const MethodInfo* RhythmEvent_1_get_listenerCount_m1237053972_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_Invoke_m4176776406_MethodInfo_var;
extern const MethodInfo* RhythmEvent_2_get_listenerCount_m3928554923_MethodInfo_var;
extern const MethodInfo* UnityEvent_2_Invoke_m3012962327_MethodInfo_var;
extern const uint32_t RhythmTool_PassEvents_m4272423917_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1851748119;
extern Il2CppCodeGenString* _stringLiteral2075151806;
extern const uint32_t RhythmTool_AddAnalysis_m2159689153_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3981451750;
extern const uint32_t RhythmTool_GetAnalysis_m368113886_MetadataUsageId;
extern const MethodInfo* List_1_get_Item_m3137246970_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2439640319_MethodInfo_var;
extern const uint32_t RhythmTool_DrawDebugLines_m2101946209_MetadataUsageId;
extern Il2CppClass* ParameterizedThreadStart_t2412552885_il2cpp_TypeInfo_var;
extern Il2CppClass* Thread_t241561612_il2cpp_TypeInfo_var;
extern const MethodInfo* RhythmTool_BackGroundAnalyze_m3994437969_MethodInfo_var;
extern const uint32_t U3CAsyncAnalyzeU3Ec__Iterator1_MoveNext_m3847191016_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsyncAnalyzeU3Ec__Iterator1_Reset_m4013296515_MetadataUsageId;
extern const uint32_t U3CQueueNewSongU3Ec__Iterator0_Reset_m4175747132_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t1084335567_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadOnlyDictionary_2_t1371992995_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3391555206_MethodInfo_var;
extern const MethodInfo* ReadOnlyDictionary_2__ctor_m855543715_MethodInfo_var;
extern const uint32_t Segmenter__ctor_m3661281169_MetadataUsageId;
extern const MethodInfo* Dictionary_2_Clear_m1652694321_MethodInfo_var;
extern const uint32_t Segmenter_Init_m1948752761_MetadataUsageId;
extern Il2CppClass* IEnumerator_1_t612171912_il2cpp_TypeInfo_var;
extern const MethodInfo* ReadOnlyDictionary_2_GetEnumerator_m4119707746_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2768754870_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2168417739_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4070564334_MethodInfo_var;
extern const uint32_t Segmenter_Init_m2037373012_MetadataUsageId;
extern const uint32_t Segmenter_DetectChanges_m3953497980_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Count_m3437212136_MethodInfo_var;
extern const uint32_t Segmenter_IsChange_m246302866_MetadataUsageId;
extern const uint32_t Segmenter_PrevChangeIndex_m843645_MetadataUsageId;
extern const uint32_t Segmenter_NextChangeIndex_m3584235607_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Item_m1874253002_MethodInfo_var;
extern const uint32_t Segmenter_PrevChange_m778242627_MetadataUsageId;
extern const uint32_t Segmenter_NextChange_m3516349633_MetadataUsageId;
extern Il2CppClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern Il2CppClass* FileStream_t1695958676_il2cpp_TypeInfo_var;
extern Il2CppClass* IFormatter_t936711909_il2cpp_TypeInfo_var;
extern const uint32_t SongData_Serialize_m1654775970_MetadataUsageId;
extern const uint32_t SongData_Deserialize_m3771651313_MetadataUsageId;
extern Il2CppClass* GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral859132627;
extern Il2CppCodeGenString* _stringLiteral2425216568;
extern Il2CppCodeGenString* _stringLiteral2021932041;
extern Il2CppCodeGenString* _stringLiteral3588015982;
extern const uint32_t SpawnManager_Start_m521168043_MetadataUsageId;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t SpawnManager_OnBeat_m2725267976_MetadataUsageId;
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const uint32_t TileManager_Start_m2619986134_MetadataUsageId;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const uint32_t TileManager_SpawnTile_m1752937928_MetadataUsageId;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m3404034275_MethodInfo_var;
extern const uint32_t TileManager_DeleteTile_m3895137793_MetadataUsageId;
extern const uint32_t Tutorial_1_Controller_Start_m1243183122_MetadataUsageId;
extern const uint32_t Tutorial_1_Controller_Update_m96272187_MetadataUsageId;
extern const uint32_t Util_GetSpectrum_m1376041373_MetadataUsageId;
extern const uint32_t Util_GetSpectrumMagnitude_m1549234393_MetadataUsageId;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral408410324;
extern const uint32_t Util_GetSpectrumMagnitude_m3600392938_MetadataUsageId;
extern const uint32_t Util_GetMono_m2743771466_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1531776908;
extern Il2CppCodeGenString* _stringLiteral2777144342;
extern Il2CppCodeGenString* _stringLiteral2157815886;
extern const uint32_t Util_GetMono_m2664450603_MetadataUsageId;
extern const uint32_t Util_UpsampleSingnal_m120385462_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2872387472;
extern const uint32_t Util_UpsampleSingnal_m3064131617_MetadataUsageId;
extern Il2CppClass* LomontFFT_t895069557_il2cpp_TypeInfo_var;
extern const uint32_t Util__cctor_m993639886_MetadataUsageId;
extern Il2CppClass* List_1_t2098562634_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_2_t2587731426_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2026045323_MethodInfo_var;
extern const MethodInfo* VisualizerController_OnBeat_m464411654_MethodInfo_var;
extern const MethodInfo* VisualizerController_OnChange_m2842847708_MethodInfo_var;
extern const MethodInfo* UnityAction_2__ctor_m4231767244_MethodInfo_var;
extern const MethodInfo* RhythmEvent_2_AddListener_m2211829322_MethodInfo_var;
extern const MethodInfo* VisualizerController_OnSongLoaded_m1911486583_MethodInfo_var;
extern const MethodInfo* VisualizerController_OnSongEnded_m337983393_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3361499332_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2419708395;
extern const uint32_t VisualizerController_Start_m125187779_MetadataUsageId;
extern const MethodInfo* List_1_get_Item_m3879122397_MethodInfo_var;
extern const uint32_t VisualizerController_NextSong_m1666774829_MetadataUsageId;
extern const uint32_t VisualizerController_Update_m2253024630_MetadataUsageId;
extern const MethodInfo* List_1_GetEnumerator_m3424352800_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2930845510_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1195348343_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1329402980_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3502228714_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m1874657968_MethodInfo_var;
extern const uint32_t VisualizerController_UpdateLines_m940043679_MetadataUsageId;
extern const uint32_t VisualizerController_OnBeat_m464411654_MetadataUsageId;
extern const uint32_t VisualizerController_OnChange_m2842847708_MetadataUsageId;
extern const uint32_t VisualizerController_OnOnset_m689097857_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisLine_t2729441502_m1643494813_MethodInfo_var;
extern const uint32_t VisualizerController_CreateLine_m3677149367_MetadataUsageId;
extern const MethodInfo* List_1_Clear_m2227676928_MethodInfo_var;
extern const uint32_t VisualizerController_ClearLines_m3213124107_MetadataUsageId;

// System.Single[]
struct SingleU5BU5D_t577127397  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GameObject_t1756533147 * m_Items[1];

public:
	inline GameObject_t1756533147 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1756533147 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
extern "C"  void List_1__ctor_m1509370154_gshared (List_1_t1445631064 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3111963761_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m555649161_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m3644677550_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Single>::Clear()
extern "C"  void List_1_Clear_m4110591713_gshared (List_1_t1445631064 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m899854001_gshared (Dictionary_2_t1697274930 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Single>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3607535064_gshared (List_1_t1445631064 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Single>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m620564420_gshared (List_1_t1445631064 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Single>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2205122117_gshared (List_1_t1445631064 * __this, const MethodInfo* method);
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ReadOnlyDictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyDictionary_2_GetEnumerator_m3797341439_gshared (ReadOnlyDictionary_2_t1984932358 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1537018582_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2897691047_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1296007576_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, Il2CppObject * p1, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m2140744741_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, Il2CppObject ** p1, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Single>::set_Item(System.Int32,!0)
extern "C"  void List_1_set_Item_m4206996116_gshared (List_1_t1445631064 * __this, int32_t p0, float p1, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
extern "C"  float List_1_get_Item_m3254877171_gshared (List_1_t1445631064 * __this, int32_t p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m313884773_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, const MethodInfo* method);
// !1 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(!0)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1573979506_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m688682013_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count()
extern "C"  int32_t List_1_get_Count_m3406166720_gshared (List_1_t1445631064 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(!0)
extern "C"  int32_t List_1_IndexOf_m858031556_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m852068579_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1921196075_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Single>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2262295624 * List_1_AsReadOnly_m1653708817_gshared (List_1_t1445631064 * __this, const MethodInfo* method);
// System.Void ReadOnlyDictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void ReadOnlyDictionary_2__ctor_m2856680850_gshared (ReadOnlyDictionary_2_t1984932358 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Boolean ReadOnlyDictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ReadOnlyDictionary_2_TryGetValue_m2829924768_gshared (ReadOnlyDictionary_2_t1984932358 * __this, int32_t p0, Il2CppObject ** p1, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1598946593_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2257663140 * List_1_AsReadOnly_m3503877813_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m2567809379_gshared (List_1_t1440998580 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m1683620704_gshared (Dictionary_2_t2921398135 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m4174387577_gshared (Dictionary_2_t2921398135 * __this, float p0, const MethodInfo* method);
// !1 System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::get_Item(!0)
extern "C"  int32_t Dictionary_2_get_Item_m1122070824_gshared (Dictionary_2_t2921398135 * __this, float p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m2607921061_gshared (Dictionary_2_t2921398135 * __this, float p0, int32_t p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1780001816_gshared (Dictionary_2_t2921398135 * __this, float p0, int32_t p1, const MethodInfo* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t4241422837  Dictionary_2_GetEnumerator_m4065924530_gshared (Dictionary_2_t2921398135 * __this, const MethodInfo* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t678743357  Enumerator_get_Current_m1983754276_gshared (Enumerator_t4241422837 * __this, const MethodInfo* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1733170775_gshared (KeyValuePair_2_t678743357 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Key()
extern "C"  float KeyValuePair_2_get_Key_m2838785900_gshared (KeyValuePair_2_t678743357 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3884776761_gshared (Enumerator_t4241422837 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m797750959_gshared (Enumerator_t4241422837 * __this, const MethodInfo* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3017299632  Dictionary_2_GetEnumerator_m3404768274_gshared (Dictionary_2_t1697274930 * __this, const MethodInfo* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2754383612_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2770956757_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2243145188_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t975728254  List_1_GetEnumerator_m2527786909_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1062633493_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(!0)
extern "C"  bool List_1_Remove_m3494432915_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m2771612799_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4282865897_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1274756239_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1734424457_gshared (ReadOnlyCollection_1_t2257663140 * __this, const MethodInfo* method);
// !0 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m925539044_gshared (ReadOnlyCollection_1_t2257663140 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,!0)
extern "C"  void List_1_Insert_m2673244951_gshared (List_1_t1440998580 * __this, int32_t p0, int32_t p1, const MethodInfo* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2168147420_gshared (Dictionary_2_t1697274930 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::BinarySearch(!0)
extern "C"  int32_t List_1_BinarySearch_m4172392722_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// !0 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>::get_Item(System.Int32)
extern "C"  float ReadOnlyCollection_1_get_Item_m210738684_gshared (ReadOnlyCollection_1_t2262295624 * __this, int32_t p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3450389068_gshared (UnityAction_2_t3167333435 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`2<System.Object,System.Int32>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void RhythmEvent_2_AddListener_m2984595847_gshared (RhythmEvent_2_t2280867144 * __this, UnityAction_2_t3167333435 * p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2836997866_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void RhythmEvent_1_AddListener_m2976521859_gshared (RhythmEvent_1_t2627539289 * __this, UnityAction_1_t4056035046 * p0, const MethodInfo* method);
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2875234987 * List_1_AsReadOnly_m2712896313_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C"  bool List_1_Contains_m1658838094_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C"  void Action_1_Invoke_m4180501989_gshared (Action_1_t2491248677 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`1<System.Object>::.ctor()
extern "C"  void RhythmEvent_1__ctor_m118991706_gshared (RhythmEvent_1_t2627539289 * __this, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`2<System.Int32,System.Single>::.ctor()
extern "C"  void RhythmEvent_2__ctor_m460284861_gshared (RhythmEvent_2_t1701265135 * __this, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`2<System.Int32,System.Int32>::.ctor()
extern "C"  void RhythmEvent_2__ctor_m557224401_gshared (RhythmEvent_2_t1696632651 * __this, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`2<System.Object,System.Int32>::.ctor()
extern "C"  void RhythmEvent_2__ctor_m2133560544_gshared (RhythmEvent_2_t2280867144 * __this, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`2<OnsetType,System.Object>::.ctor()
extern "C"  void RhythmEvent_2__ctor_m1704365748_gshared (RhythmEvent_2_t2645018905 * __this, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`4<System.Int32,System.Single,System.Single,System.Single>::.ctor()
extern "C"  void RhythmEvent_4__ctor_m2850559519_gshared (RhythmEvent_4_t3330968721 * __this, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Int32>::Invoke(!0,!1)
extern "C"  void UnityEvent_2_Invoke_m1340061553_gshared (UnityEvent_2_t754564057 * __this, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerator`1<!0> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2569117622_gshared (ReadOnlyCollection_1_t2875234987 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// !0 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_get_Item_m2664978718_gshared (ReadOnlyCollection_1_t2875234987 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2562379905_gshared (ReadOnlyCollection_1_t2875234987 * __this, const MethodInfo* method);
// System.Int32 RhythmEventProvider/RhythmEvent`4<System.Int32,System.Single,System.Single,System.Single>::get_listenerCount()
extern "C"  int32_t RhythmEvent_4_get_listenerCount_m3042521709_gshared (RhythmEvent_4_t3330968721 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`4<System.Int32,System.Single,System.Single,System.Single>::Invoke(!0,!1,!2,!3)
extern "C"  void UnityEvent_4_Invoke_m1874634762_gshared (UnityEvent_4_t1810603002 * __this, int32_t p0, float p1, float p2, float p3, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>::Invoke(!0,!1)
extern "C"  void UnityEvent_2_Invoke_m514975631_gshared (UnityEvent_2_t170329564 * __this, int32_t p0, int32_t p1, const MethodInfo* method);
// System.Int32 RhythmEventProvider/RhythmEvent`2<OnsetType,System.Object>::get_listenerCount()
extern "C"  int32_t RhythmEvent_2_get_listenerCount_m3434387278_gshared (RhythmEvent_2_t2645018905 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`2<OnsetType,System.Object>::Invoke(!0,!1)
extern "C"  void UnityEvent_2_Invoke_m3277818011_gshared (UnityEvent_2_t1118715818 * __this, int32_t p0, Il2CppObject * p1, const MethodInfo* method);
// System.Int32 RhythmEventProvider/RhythmEvent`1<System.Object>::get_listenerCount()
extern "C"  int32_t RhythmEvent_1_get_listenerCount_m946170288_gshared (RhythmEvent_1_t2627539289 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(!0)
extern "C"  void UnityEvent_1_Invoke_m838874366_gshared (UnityEvent_1_t2727799310 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Int32 RhythmEventProvider/RhythmEvent`2<System.Int32,System.Single>::get_listenerCount()
extern "C"  int32_t RhythmEvent_2_get_listenerCount_m3928554923_gshared (RhythmEvent_2_t1701265135 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>::Invoke(!0,!1)
extern "C"  void UnityEvent_2_Invoke_m3012962327_gshared (UnityEvent_2_t174962048 * __this, int32_t p0, float p1, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor()
extern "C"  void Dictionary_2__ctor_m3391555206_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
// System.Void ReadOnlyDictionary`2<System.Int32,System.Single>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void ReadOnlyDictionary_2__ctor_m855543715_gshared (ReadOnlyDictionary_2_t1371992995 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Clear()
extern "C"  void Dictionary_2_Clear_m1652694321_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ReadOnlyDictionary`2<System.Int32,System.Single>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyDictionary_2_GetEnumerator_m4119707746_gshared (ReadOnlyDictionary_2_t1371992995 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2768754870_gshared (KeyValuePair_2_t3136648085 * __this, const MethodInfo* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m2168417739_gshared (KeyValuePair_2_t3136648085 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m4070564334_gshared (Dictionary_2_t1084335567 * __this, int32_t p0, float p1, const MethodInfo* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3437212136_gshared (Dictionary_2_t1084335567 * __this, const MethodInfo* method);
// !1 System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::get_Item(!0)
extern "C"  float Dictionary_2_get_Item_m1874253002_gshared (Dictionary_2_t1084335567 * __this, int32_t p0, const MethodInfo* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3615096820_gshared (List_1_t2058570427 * __this, int32_t p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<System.Int32,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m4231767244_gshared (UnityAction_2_t2587731426 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void RhythmEventProvider/RhythmEvent`2<System.Int32,System.Single>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void RhythmEvent_2_AddListener_m2211829322_gshared (RhythmEvent_2_t1701265135 * __this, UnityAction_2_t2587731426 * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analysis::set_name(System.String)
extern "C"  void Analysis_set_name_m1665071749 (Analysis_t439488098 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
#define List_1__ctor_m1509370154(__this, method) ((  void (*) (List_1_t1445631064 *, const MethodInfo*))List_1__ctor_m1509370154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Onset>::.ctor(System.Int32)
#define Dictionary_2__ctor_m4135957043(__this, p0, method) ((  void (*) (Dictionary_2_t4035389440 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3111963761_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
#define List_1__ctor_m555649161(__this, p0, method) ((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1__ctor_m555649161_gshared)(__this, p0, method)
// System.Void AnalysisData::.ctor(System.String,System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.Dictionary`2<System.Int32,Onset>)
extern "C"  void AnalysisData__ctor_m1090324430 (AnalysisData_t108342674 * __this, String_t* ___name0, List_1_t1445631064 * ___magnitude1, List_1_t1445631064 * ___flux2, List_1_t1445631064 * ___magnitudeSmooth3, List_1_t1445631064 * ___magnitudeAvg4, Dictionary_2_t4035389440 * ___onsets5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analysis::set_analysisData(AnalysisData)
extern "C"  void Analysis_set_analysisData_m1435892088 (Analysis_t439488098 * __this, AnalysisData_t108342674 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
#define List_1_Clear_m3644677550(__this, method) ((  void (*) (List_1_t1440998580 *, const MethodInfo*))List_1_Clear_m3644677550_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Clear()
#define List_1_Clear_m4110591713(__this, method) ((  void (*) (List_1_t1445631064 *, const MethodInfo*))List_1_Clear_m4110591713_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Onset>::Clear()
#define Dictionary_2_Clear_m4007066975(__this, method) ((  void (*) (Dictionary_2_t4035389440 *, const MethodInfo*))Dictionary_2_Clear_m899854001_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3607535064(__this, p0, method) ((  void (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3607535064_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Single>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m620564420(__this, p0, method) ((  void (*) (List_1_t1445631064 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m620564420_gshared)(__this, p0, method)
// System.Int32 RhythmTool::get_fftWindowSize()
extern "C"  int32_t RhythmTool_get_fftWindowSize_m3317244415 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Analysis::get_name()
extern "C"  String_t* Analysis_get_name_m4149432526 (Analysis_t439488098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::get_magnitude()
extern "C"  ReadOnlyCollection_1_t2262295624 * AnalysisData_get_magnitude_m671389901 (AnalysisData_t108342674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::get_flux()
extern "C"  ReadOnlyCollection_1_t2262295624 * AnalysisData_get_flux_m3559386780 (AnalysisData_t108342674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::get_magnitudeSmooth()
extern "C"  ReadOnlyCollection_1_t2262295624 * AnalysisData_get_magnitudeSmooth_m1926036311 (AnalysisData_t108342674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::get_magnitudeAvg()
extern "C"  ReadOnlyCollection_1_t2262295624 * AnalysisData_get_magnitudeAvg_m3285528701 (AnalysisData_t108342674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Single>::TrimExcess()
#define List_1_TrimExcess_m2205122117(__this, method) ((  void (*) (List_1_t1445631064 *, const MethodInfo*))List_1_TrimExcess_m2205122117_gshared)(__this, method)
// ReadOnlyDictionary`2<System.Int32,Onset> AnalysisData::get_onsets()
extern "C"  ReadOnlyDictionary_2_t28079572 * AnalysisData_get_onsets_m2742507631 (AnalysisData_t108342674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ReadOnlyDictionary`2<System.Int32,Onset>::GetEnumerator()
#define ReadOnlyDictionary_2_GetEnumerator_m3517933108(__this, method) ((  Il2CppObject* (*) (ReadOnlyDictionary_2_t28079572 *, const MethodInfo*))ReadOnlyDictionary_2_GetEnumerator_m3797341439_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,Onset>::get_Key()
#define KeyValuePair_2_get_Key_m3879747900(__this, method) ((  int32_t (*) (KeyValuePair_2_t1792734662 *, const MethodInfo*))KeyValuePair_2_get_Key_m1537018582_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.Int32,Onset>::get_Value()
#define KeyValuePair_2_get_Value_m2281585953(__this, method) ((  Onset_t732596509 * (*) (KeyValuePair_2_t1792734662 *, const MethodInfo*))KeyValuePair_2_get_Value_m2897691047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Onset>::Add(!0,!1)
#define Dictionary_2_Add_m1340372924(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4035389440 *, int32_t, Onset_t732596509 *, const MethodInfo*))Dictionary_2_Add_m1296007576_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Onset>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3835210299(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t4035389440 *, int32_t, Onset_t732596509 **, const MethodInfo*))Dictionary_2_TryGetValue_m2140744741_gshared)(__this, p0, p1, method)
// System.Single Util::Sum(System.Single[],System.Int32,System.Int32)
extern "C"  float Util_Sum_m3399145285 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___input0, int32_t ___start1, int32_t ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Single>::set_Item(System.Int32,!0)
#define List_1_set_Item_m4206996116(__this, p0, p1, method) ((  void (*) (List_1_t1445631064 *, int32_t, float, const MethodInfo*))List_1_set_Item_m4206996116_gshared)(__this, p0, p1, method)
// System.Void Analysis::Smooth(System.Int32,System.Int32,System.Int32)
extern "C"  void Analysis_Smooth_m199185592 (Analysis_t439488098 * __this, int32_t ___index0, int32_t ___windowSize1, int32_t ___iterations2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analysis::Average(System.Int32)
extern "C"  void Analysis_Average_m3308932463 (Analysis_t439488098 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
#define List_1_get_Item_m3254877171(__this, p0, method) ((  float (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))List_1_get_Item_m3254877171_gshared)(__this, p0, method)
// System.Void Analysis::FindPeaks(System.Int32,System.Single,System.Int32)
extern "C"  void Analysis_FindPeaks_m2740314761 (Analysis_t439488098 * __this, int32_t ___index0, float ___thresholdMultiplier1, int32_t ___thresholdWindowSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analysis::RankPeaks(System.Int32,System.Int32)
extern "C"  void Analysis_RankPeaks_m34459363 (Analysis_t439488098 * __this, int32_t ___index0, int32_t ___windowSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2020392075  Color_get_red_m2410286591 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern "C"  void Debug_DrawLine_m3455422326 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, Color_t2020392075  p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2020392075  Color_get_black_m2650940523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_blue()
extern "C"  Color_t2020392075  Color_get_blue_m4180825090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Onset>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1627758495(__this, p0, method) ((  bool (*) (Dictionary_2_t4035389440 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m313884773_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,Onset>::get_Item(!0)
#define Dictionary_2_get_Item_m552409364(__this, p0, method) ((  Onset_t732596509 * (*) (Dictionary_2_t4035389440 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1573979506_gshared)(__this, p0, method)
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2020392075  Color_get_green_m2671273823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2020392075  Color_get_white_m3987539815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Max_m1875893177 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analysis::Smooth(System.Int32,System.Int32)
extern "C"  void Analysis_Smooth_m4230704111 (Analysis_t439488098 * __this, int32_t ___index0, int32_t ___windowSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Analysis::Threshold(System.Int32,System.Single,System.Int32)
extern "C"  float Analysis_Threshold_m3563177457 (Analysis_t439488098 * __this, int32_t ___index0, float ___multiplier1, int32_t ___windowSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Onset::.ctor(System.Int32,System.Single,System.Int32)
extern "C"  void Onset__ctor_m2770042849 (Onset_t732596509 * __this, int32_t ___index0, float ___strength1, int32_t ___rank2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m688682013(__this, p0, method) ((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_Add_m688682013_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count()
#define List_1_get_Count_m3406166720(__this, method) ((  int32_t (*) (List_1_t1445631064 *, const MethodInfo*))List_1_get_Count_m3406166720_gshared)(__this, method)
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m2906823867 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m2354025655 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(!0)
#define List_1_IndexOf_m858031556(__this, p0, method) ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_IndexOf_m858031556_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
#define List_1_get_Count_m852068579(__this, method) ((  int32_t (*) (List_1_t1440998580 *, const MethodInfo*))List_1_get_Count_m852068579_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
#define List_1_get_Item_m1921196075(__this, p0, method) ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_get_Item_m1921196075_gshared)(__this, p0, method)
// System.Void AnalysisData::set_name(System.String)
extern "C"  void AnalysisData_set_name_m2914210485 (AnalysisData_t108342674 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Single>::AsReadOnly()
#define List_1_AsReadOnly_m1653708817(__this, method) ((  ReadOnlyCollection_1_t2262295624 * (*) (List_1_t1445631064 *, const MethodInfo*))List_1_AsReadOnly_m1653708817_gshared)(__this, method)
// System.Void AnalysisData::set_magnitude(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>)
extern "C"  void AnalysisData_set_magnitude_m987943692 (AnalysisData_t108342674 * __this, ReadOnlyCollection_1_t2262295624 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalysisData::set_flux(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>)
extern "C"  void AnalysisData_set_flux_m1006935713 (AnalysisData_t108342674 * __this, ReadOnlyCollection_1_t2262295624 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalysisData::set_magnitudeSmooth(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>)
extern "C"  void AnalysisData_set_magnitudeSmooth_m1803049594 (AnalysisData_t108342674 * __this, ReadOnlyCollection_1_t2262295624 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalysisData::set_magnitudeAvg(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>)
extern "C"  void AnalysisData_set_magnitudeAvg_m3935231414 (AnalysisData_t108342674 * __this, ReadOnlyCollection_1_t2262295624 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadOnlyDictionary`2<System.Int32,Onset>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define ReadOnlyDictionary_2__ctor_m150260537(__this, p0, method) ((  void (*) (ReadOnlyDictionary_2_t28079572 *, Il2CppObject*, const MethodInfo*))ReadOnlyDictionary_2__ctor_m2856680850_gshared)(__this, p0, method)
// System.Void AnalysisData::set_onsets(ReadOnlyDictionary`2<System.Int32,Onset>)
extern "C"  void AnalysisData_set_onsets_m1908993738 (AnalysisData_t108342674 * __this, ReadOnlyDictionary_2_t28079572 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReadOnlyDictionary`2<System.Int32,Onset>::TryGetValue(TKey,TValue&)
#define ReadOnlyDictionary_2_TryGetValue_m2791401779(__this, p0, p1, method) ((  bool (*) (ReadOnlyDictionary_2_t28079572 *, int32_t, Onset_t732596509 **, const MethodInfo*))ReadOnlyDictionary_2_TryGetValue_m2829924768_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindWithTag_m1929006324 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<HitZoneManager>()
#define GameObject_GetComponent_TisHitZoneManager_t1308216902_m2080816239(__this, method) ((  HitZoneManager_t1308216902 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2872111280_m3412601438(__this, method) ((  Button_t2872111280 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C"  ButtonClickedEvent_t2455055323 * Button_get_onClick_m1595880935 (Button_t2872111280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m2649891629 (UnityAction_t4025899511 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m1596810379 (UnityEvent_t408735097 * __this, UnityAction_t4025899511 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m357168014 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<RhythmTool>()
#define Component_GetComponent_TisRhythmTool_t215962618_m966574361(__this, method) ((  RhythmTool_t215962618 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void RhythmTool::NewSong(UnityEngine.AudioClip)
extern "C"  void RhythmTool_NewSong_m2509631375 (RhythmTool_t215962618 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2606471964 (Action_t3226471752 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::add_SongLoaded(System.Action)
extern "C"  void RhythmTool_add_SongLoaded_m1500519296 (RhythmTool_t215962618 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::Play()
extern "C"  void RhythmTool_Play_m2237616365 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::DrawDebugLines()
extern "C"  void RhythmTool_DrawDebugLines_m2101946209 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m1598946593(__this, method) ((  void (*) (List_1_t1440998580 *, const MethodInfo*))List_1__ctor_m1598946593_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Beat>::.ctor(System.Int32)
#define Dictionary_2__ctor_m3978597214(__this, p0, method) ((  void (*) (Dictionary_2_t1703509207 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3111963761_gshared)(__this, p0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
#define List_1_AsReadOnly_m3503877813(__this, method) ((  ReadOnlyCollection_1_t2257663140 * (*) (List_1_t1440998580 *, const MethodInfo*))List_1_AsReadOnly_m3503877813_gshared)(__this, method)
// System.Void BeatTracker::set_beatIndices(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>)
extern "C"  void BeatTracker_set_beatIndices_m645906055 (BeatTracker_t2801099156 * __this, ReadOnlyCollection_1_t2257663140 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadOnlyDictionary`2<System.Int32,Beat>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define ReadOnlyDictionary_2__ctor_m2336533874(__this, p0, method) ((  void (*) (ReadOnlyDictionary_2_t1991166635 *, Il2CppObject*, const MethodInfo*))ReadOnlyDictionary_2__ctor_m2856680850_gshared)(__this, p0, method)
// System.Void BeatTracker::set_beats(ReadOnlyDictionary`2<System.Int32,Beat>)
extern "C"  void BeatTracker_set_beats_m2173293630 (BeatTracker_t2801099156 * __this, ReadOnlyDictionary_2_t1991166635 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Beat>::Clear()
#define Dictionary_2_Clear_m1564562098(__this, method) ((  void (*) (Dictionary_2_t1703509207 *, const MethodInfo*))Dictionary_2_Clear_m899854001_gshared)(__this, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32> BeatTracker::get_beatIndices()
extern "C"  ReadOnlyCollection_1_t2257663140 * BeatTracker_get_beatIndices_m1207551596 (BeatTracker_t2801099156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m2567809379(__this, p0, method) ((  void (*) (List_1_t1440998580 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2567809379_gshared)(__this, p0, method)
// ReadOnlyDictionary`2<System.Int32,Beat> BeatTracker::get_beats()
extern "C"  ReadOnlyDictionary_2_t1991166635 * BeatTracker_get_beats_m3284239415 (BeatTracker_t2801099156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ReadOnlyDictionary`2<System.Int32,Beat>::GetEnumerator()
#define ReadOnlyDictionary_2_GetEnumerator_m885255707(__this, method) ((  Il2CppObject* (*) (ReadOnlyDictionary_2_t1991166635 *, const MethodInfo*))ReadOnlyDictionary_2_GetEnumerator_m3797341439_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,Beat>::get_Key()
#define KeyValuePair_2_get_Key_m3096183613(__this, method) ((  int32_t (*) (KeyValuePair_2_t3755821725 *, const MethodInfo*))KeyValuePair_2_get_Key_m1537018582_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.Int32,Beat>::get_Value()
#define KeyValuePair_2_get_Value_m2369008986(__this, method) ((  Beat_t2695683572 * (*) (KeyValuePair_2_t3755821725 *, const MethodInfo*))KeyValuePair_2_get_Value_m2897691047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Beat>::Add(!0,!1)
#define Dictionary_2_Add_m3808454219(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1703509207 *, int32_t, Beat_t2695683572 *, const MethodInfo*))Dictionary_2_Add_m1296007576_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::.ctor()
#define Dictionary_2__ctor_m1683620704(__this, method) ((  void (*) (Dictionary_2_t2921398135 *, const MethodInfo*))Dictionary_2__ctor_m1683620704_gshared)(__this, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,Beat>::get_Item(!0)
#define Dictionary_2_get_Item_m2258219861(__this, p0, method) ((  Beat_t2695683572 * (*) (Dictionary_2_t1703509207 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1573979506_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m4174387577(__this, p0, method) ((  bool (*) (Dictionary_2_t2921398135 *, float, const MethodInfo*))Dictionary_2_ContainsKey_m4174387577_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::get_Item(!0)
#define Dictionary_2_get_Item_m1122070824(__this, p0, method) ((  int32_t (*) (Dictionary_2_t2921398135 *, float, const MethodInfo*))Dictionary_2_get_Item_m1122070824_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m2607921061(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2921398135 *, float, int32_t, const MethodInfo*))Dictionary_2_set_Item_m2607921061_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::Add(!0,!1)
#define Dictionary_2_Add_m1780001816(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2921398135 *, float, int32_t, const MethodInfo*))Dictionary_2_Add_m1780001816_gshared)(__this, p0, p1, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Single,System.Int32>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m4065924530(__this, method) ((  Enumerator_t4241422837  (*) (Dictionary_2_t2921398135 *, const MethodInfo*))Dictionary_2_GetEnumerator_m4065924530_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Int32>::get_Current()
#define Enumerator_get_Current_m1983754276(__this, method) ((  KeyValuePair_2_t678743357  (*) (Enumerator_t4241422837 *, const MethodInfo*))Enumerator_get_Current_m1983754276_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m1733170775(__this, method) ((  int32_t (*) (KeyValuePair_2_t678743357 *, const MethodInfo*))KeyValuePair_2_get_Value_m1733170775_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m2838785900(__this, method) ((  float (*) (KeyValuePair_2_t678743357 *, const MethodInfo*))KeyValuePair_2_get_Key_m2838785900_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3884776761(__this, method) ((  bool (*) (Enumerator_t4241422837 *, const MethodInfo*))Enumerator_MoveNext_m3884776761_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Int32>::Dispose()
#define Enumerator_Dispose_m797750959(__this, method) ((  void (*) (Enumerator_t4241422837 *, const MethodInfo*))Enumerator_Dispose_m797750959_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,Beat>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2354998391(__this, method) ((  Enumerator_t3023533909  (*) (Dictionary_2_t1703509207 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3404768274_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Beat>::get_Current()
#define Enumerator_get_Current_m909613959(__this, method) ((  KeyValuePair_2_t3755821725  (*) (Enumerator_t3023533909 *, const MethodInfo*))Enumerator_get_Current_m2754383612_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Beat>::MoveNext()
#define Enumerator_MoveNext_m1568926478(__this, method) ((  bool (*) (Enumerator_t3023533909 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Beat>::Dispose()
#define Enumerator_Dispose_m2364881746(__this, method) ((  void (*) (Enumerator_t3023533909 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
#define List_1_GetEnumerator_m2527786909(__this, method) ((  Enumerator_t975728254  (*) (List_1_t1440998580 *, const MethodInfo*))List_1_GetEnumerator_m2527786909_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m1062633493(__this, method) ((  int32_t (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_get_Current_m1062633493_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(!0)
#define List_1_Remove_m3494432915(__this, p0, method) ((  bool (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_Remove_m3494432915_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Beat>::Remove(!0)
#define Dictionary_2_Remove_m623660598(__this, p0, method) ((  bool (*) (Dictionary_2_t1703509207 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2771612799_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m4282865897(__this, method) ((  bool (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_MoveNext_m4282865897_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m1274756239(__this, method) ((  void (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_Dispose_m1274756239_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1734424457(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2257663140 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1734424457_gshared)(__this, method)
// !0 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m925539044(__this, p0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2257663140 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m925539044_gshared)(__this, p0, method)
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m2927198556 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,!0)
#define List_1_Insert_m2673244951(__this, p0, p1, method) ((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m2673244951_gshared)(__this, p0, p1, method)
// System.Void Beat::.ctor(System.Single,System.Single,System.Int32)
extern "C"  void Beat__ctor_m206855294 (Beat_t2695683572 * __this, float ___length0, float ___bpm1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeatTracker::AddBeat(System.Int32,System.Single)
extern "C"  void BeatTracker_AddBeat_m3465552806 (BeatTracker_t2801099156 * __this, int32_t ___index0, float ___beatLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void WaitCallback__ctor_m1513386157 (WaitCallback_t2798937288 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ThreadPool::QueueUserWorkItem(System.Threading.WaitCallback,System.Object)
extern "C"  bool ThreadPool_QueueUserWorkItem_m2209660682 (Il2CppObject * __this /* static, unused */, WaitCallback_t2798937288 * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::Smooth(System.Single[],System.Int32)
extern "C"  void Util_Smooth_m3641735451 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___signal0, int32_t ___windowSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
extern "C"  void Array_Clear_m782967417 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::UpsampleSingnal(System.Single[],System.Single[],System.Int32)
extern "C"  void Util_UpsampleSingnal_m3064131617 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___signal0, SingleU5BU5D_t577127397* ___upsampledSignal1, int32_t ___multiplier2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BeatTracker::RepetitionScore(System.Single[],System.Int32)
extern "C"  float BeatTracker_RepetitionScore_m1324094558 (BeatTracker_t2801099156 * __this, SingleU5BU5D_t577127397* ___signal0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BeatTracker::GapScore(System.Single[],System.Int32)
extern "C"  float BeatTracker_GapScore_m397149463 (BeatTracker_t2801099156 * __this, SingleU5BU5D_t577127397* ___signal0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m2564622569 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BeatTracker::OffsetScore(System.Single[],System.Int32,System.Int32)
extern "C"  float BeatTracker_OffsetScore_m442017893 (BeatTracker_t2801099156 * __this, SingleU5BU5D_t577127397* ___signal0, int32_t ___offset1, int32_t ___gap2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Beat>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m604849294(__this, p0, method) ((  bool (*) (Dictionary_2_t1703509207 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m313884773_gshared)(__this, p0, method)
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2243707580  Vector3_get_up_m2725403797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Debug_DrawLine_m2961609580 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_gray()
extern "C"  Color_t2020392075  Color_get_gray_m1396712533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C"  Color_t2020392075  Color_get_yellow_m3741935494 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Beat>::get_Count()
#define Dictionary_2_get_Count_m2326583579(__this, method) ((  int32_t (*) (Dictionary_2_t1703509207 *, const MethodInfo*))Dictionary_2_get_Count_m2168147420_gshared)(__this, method)
// System.Int32 BeatTracker::NextBeatIndex(System.Int32)
extern "C"  int32_t BeatTracker_NextBeatIndex_m2115259593 (BeatTracker_t2801099156 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BeatTracker::PrevBeatIndex(System.Int32)
extern "C"  int32_t BeatTracker_PrevBeatIndex_m2127559407 (BeatTracker_t2801099156 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Int32>::BinarySearch(!0)
#define List_1_BinarySearch_m4172392722(__this, p0, method) ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_BinarySearch_m4172392722_gshared)(__this, p0, method)
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m3542052159 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeatTracker::FindBeat()
extern "C"  void BeatTracker_FindBeat_m212206812 (BeatTracker_t2801099156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindGameObjectWithTag_m829057129 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m2407545601 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_Lerp_m2935648359 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m3314153180 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnalysisData RhythmTool::get_low()
extern "C"  AnalysisData_t108342674 * RhythmTool_get_low_m2159614661 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RhythmTool::get_songLoaded()
extern "C"  bool RhythmTool_get_songLoaded_m3458667938 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmTool::get_currentFrame()
extern "C"  int32_t RhythmTool_get_currentFrame_m867144816 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmTool::get_totalFrames()
extern "C"  int32_t RhythmTool_get_totalFrames_m68629944 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RhythmTool::get_interpolation()
extern "C"  float RhythmTool_get_interpolation_m2645903814 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m210738684(__this, p0, method) ((  float (*) (ReadOnlyCollection_1_t2262295624 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m210738684_gshared)(__this, p0, method)
// Onset AnalysisData::GetOnset(System.Int32)
extern "C"  Onset_t732596509 * AnalysisData_GetOnset_m643039293 (AnalysisData_t108342674 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Onset::op_Implicit(Onset)
extern "C"  float Onset_op_Implicit_m4069753098 (Il2CppObject * __this /* static, unused */, Onset_t732596509 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmTool::IsBeat(System.Int32)
extern "C"  int32_t RhythmTool_IsBeat_m3576622446 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RhythmTool::IsChange(System.Int32)
extern "C"  bool RhythmTool_IsChange_m1817224686 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RhythmTool::NextChange(System.Int32)
extern "C"  float RhythmTool_NextChange_m3596269439 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_MoveTowards_m1358638081 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<RhythmEventProvider>()
#define Component_GetComponent_TisRhythmEventProvider_t215006757_m163918678(__this, method) ((  RhythmEventProvider_t215006757 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityAction`2<System.String,System.Int32>::.ctor(System.Object,System.IntPtr)
#define UnityAction_2__ctor_m3874320903(__this, p0, p1, method) ((  void (*) (UnityAction_2_t1195085273 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_2__ctor_m3450389068_gshared)(__this, p0, p1, method)
// System.Void RhythmEventProvider/RhythmEvent`2<System.String,System.Int32>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
#define RhythmEvent_2_AddListener_m3749635587(__this, p0, method) ((  void (*) (RhythmEvent_2_t308618982 *, UnityAction_2_t1195085273 *, const MethodInfo*))RhythmEvent_2_AddListener_m2984595847_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityAction`1<Beat>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m3300750889(__this, p0, p1, method) ((  void (*) (UnityAction_1_t4062269323 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m2836997866_gshared)(__this, p0, p1, method)
// System.Void RhythmEventProvider/RhythmEvent`1<Beat>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define RhythmEvent_1_AddListener_m1356841763(__this, p0, method) ((  void (*) (RhythmEvent_1_t2633773566 *, UnityAction_1_t4062269323 *, const MethodInfo*))RhythmEvent_1_AddListener_m2976521859_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityAction`2<Beat,System.Int32>::.ctor(System.Object,System.IntPtr)
#define UnityAction_2__ctor_m3846259155(__this, p0, p1, method) ((  void (*) (UnityAction_2_t771031906 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_2__ctor_m3450389068_gshared)(__this, p0, p1, method)
// System.Void RhythmEventProvider/RhythmEvent`2<Beat,System.Int32>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
#define RhythmEvent_2_AddListener_m1848967427(__this, p0, method) ((  void (*) (RhythmEvent_2_t4179532911 *, UnityAction_2_t771031906 *, const MethodInfo*))RhythmEvent_2_AddListener_m2984595847_gshared)(__this, p0, method)
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
extern "C"  Vector3_t2243707580  Random_get_insideUnitSphere_m4012327022 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2325460848 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m2964039490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m745914591 (Scene_t1684909666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1619949821 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m3885595876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<PlayerHealthManager>()
#define GameObject_GetComponent_TisPlayerHealthManager_t3067865410_m1535056983(__this, method) ((  PlayerHealthManager_t3067865410 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t209405766_m195193039(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void RhythmTool::Stop()
extern "C"  void RhythmTool_Stop_m1172477109 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1796096907 (Behaviour_t955675639 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C"  int32_t GameObject_get_layer_m725607808 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<HitZoneControl>()
#define GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(__this, method) ((  HitZoneControl_t2657704264 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Line::set_index(System.Int32)
extern "C"  void Line_set_index_m719778531 (Line_t2729441502 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t1268241104_m3460404950(__this, method) ((  MeshRenderer_t1268241104 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t2020392075  Color_Lerp_m3323752807 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, Color_t2020392075  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t193706927 * Renderer_get_material_m2553789785 (Renderer_t257310565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m650857509 (Material_t193706927 * __this, String_t* p0, Color_t2020392075  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LomontFFT::set_A(System.Int32)
extern "C"  void LomontFFT_set_A_m3461982773 (LomontFFT_t895069557 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LomontFFT::set_B(System.Int32)
extern "C"  void LomontFFT_set_B_m1642579824 (LomontFFT_t895069557 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m2000667605 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m3739475201 (ArgumentException_t3259014390 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LomontFFT::Reverse(System.Single[],System.Int32)
extern "C"  void LomontFFT_Reverse_m2434779414 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___data0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LomontFFT::get_B()
extern "C"  int32_t LomontFFT_get_B_m513422853 (LomontFFT_t895069557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LomontFFT::Scale(System.Single[],System.Int32,System.Boolean)
extern "C"  void LomontFFT_Scale_m1113003897 (LomontFFT_t895069557 * __this, SingleU5BU5D_t577127397* ___data0, int32_t ___n1, bool ___forward2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LomontFFT::Initialize(System.Int32)
extern "C"  void LomontFFT_Initialize_m2055186573 (LomontFFT_t895069557 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LomontFFT::TableFFT(System.Single[],System.Boolean)
extern "C"  void LomontFFT_TableFFT_m1393901266 (LomontFFT_t895069557 * __this, SingleU5BU5D_t577127397* ___data0, bool ___forward1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LomontFFT::get_A()
extern "C"  int32_t LomontFFT_get_A_m513422754 (LomontFFT_t895069557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Slider>()
#define GameObject_GetComponent_TisSlider_t297367283_m33033319(__this, method) ((  Slider_t297367283 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.UI.Slider::set_minValue(System.Single)
extern "C"  void Slider_set_minValue_m1484509981 (Slider_t297367283 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Slider::set_maxValue(System.Single)
extern "C"  void Slider_set_maxValue_m2951480075 (Slider_t297367283 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
#define Component_GetComponent_TisCharacterController_t4094781467_m1582798737(__this, method) ((  CharacterController_t4094781467 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2216684562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2243707580  Vector3_get_forward_m1201659139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C"  int32_t CharacterController_Move_m3456882757 (CharacterController_t4094781467 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
extern "C"  bool CharacterController_get_isGrounded_m2594228107 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m4133353720 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t2068071072_m2503703020(__this, method) ((  Animation_t2068071072 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C"  bool Animation_Play_m976361057 (Animation_t2068071072 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m2792523731 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmEventProvider/BeatEvent::.ctor()
extern "C"  void BeatEvent__ctor_m1246752065 (BeatEvent_t33541086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmEventProvider/SubBeatEvent::.ctor()
extern "C"  void SubBeatEvent__ctor_m1401496825 (SubBeatEvent_t2499915164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmEventProvider/OnsetEvent::.ctor()
extern "C"  void OnsetEvent__ctor_m1857777506 (OnsetEvent_t1329939269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmEventProvider/ChangeEvent::.ctor()
extern "C"  void ChangeEvent__ctor_m2572504441 (ChangeEvent_t1135638418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmEventProvider/TimingUpdateEvent::.ctor()
extern "C"  void TimingUpdateEvent__ctor_m1890207098 (TimingUpdateEvent_t543213975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmEventProvider/FrameChangedEvent::.ctor()
extern "C"  void FrameChangedEvent__ctor_m790505990 (FrameChangedEvent_t800671821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmEventProvider/OnNewSong::.ctor()
extern "C"  void OnNewSong__ctor_m1780728203 (OnNewSong_t4169165798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern "C"  void UnityEvent__ctor_m588741179 (UnityEvent_t408735097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<RhythmEventProvider>::AsReadOnly()
#define List_1_AsReadOnly_m3056134219(__this, method) ((  ReadOnlyCollection_1_t400792449 * (*) (List_1_t3879095185 *, const MethodInfo*))List_1_AsReadOnly_m2712896313_gshared)(__this, method)
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<RhythmEventProvider>::Contains(!0)
#define List_1_Contains_m2342885972(__this, p0, method) ((  bool (*) (List_1_t3879095185 *, RhythmEventProvider_t215006757 *, const MethodInfo*))List_1_Contains_m1658838094_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<RhythmEventProvider>::Add(!0)
#define List_1_Add_m3390357842(__this, p0, method) ((  void (*) (List_1_t3879095185 *, RhythmEventProvider_t215006757 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void System.Action`1<RhythmEventProvider>::Invoke(!0)
#define Action_1_Invoke_m4148756095(__this, p0, method) ((  void (*) (Action_1_t16806139 *, RhythmEventProvider_t215006757 *, const MethodInfo*))Action_1_Invoke_m4180501989_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<RhythmEventProvider>::Remove(!0)
#define List_1_Remove_m4083267577(__this, p0, method) ((  bool (*) (List_1_t3879095185 *, RhythmEventProvider_t215006757 *, const MethodInfo*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<RhythmEventProvider>::.ctor()
#define List_1__ctor_m3143476342(__this, method) ((  void (*) (List_1_t3879095185 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void RhythmEventProvider/RhythmEvent`1<Beat>::.ctor()
#define RhythmEvent_1__ctor_m2502037646(__this, method) ((  void (*) (RhythmEvent_1_t2633773566 *, const MethodInfo*))RhythmEvent_1__ctor_m118991706_gshared)(__this, method)
// System.Void RhythmEventProvider/RhythmEvent`2<System.Int32,System.Single>::.ctor()
#define RhythmEvent_2__ctor_m460284861(__this, method) ((  void (*) (RhythmEvent_2_t1701265135 *, const MethodInfo*))RhythmEvent_2__ctor_m460284861_gshared)(__this, method)
// System.Void RhythmEventProvider/RhythmEvent`2<System.Int32,System.Int32>::.ctor()
#define RhythmEvent_2__ctor_m557224401(__this, method) ((  void (*) (RhythmEvent_2_t1696632651 *, const MethodInfo*))RhythmEvent_2__ctor_m557224401_gshared)(__this, method)
// System.Void RhythmEventProvider/RhythmEvent`2<System.String,System.Int32>::.ctor()
#define RhythmEvent_2__ctor_m2299413610(__this, method) ((  void (*) (RhythmEvent_2_t308618982 *, const MethodInfo*))RhythmEvent_2__ctor_m2133560544_gshared)(__this, method)
// System.Void RhythmEventProvider/RhythmEvent`2<OnsetType,Onset>::.ctor()
#define RhythmEvent_2__ctor_m1345214749(__this, method) ((  void (*) (RhythmEvent_2_t688166119 *, const MethodInfo*))RhythmEvent_2__ctor_m1704365748_gshared)(__this, method)
// System.Void RhythmEventProvider/RhythmEvent`2<Beat,System.Int32>::.ctor()
#define RhythmEvent_2__ctor_m2210944366(__this, method) ((  void (*) (RhythmEvent_2_t4179532911 *, const MethodInfo*))RhythmEvent_2__ctor_m2133560544_gshared)(__this, method)
// System.Void RhythmEventProvider/RhythmEvent`4<System.Int32,System.Single,System.Single,System.Single>::.ctor()
#define RhythmEvent_4__ctor_m2850559519(__this, method) ((  void (*) (RhythmEvent_4_t3330968721 *, const MethodInfo*))RhythmEvent_4__ctor_m2850559519_gshared)(__this, method)
// ReadOnlyDictionary`2<System.Int32,System.Single> Segmenter::get_changes()
extern "C"  ReadOnlyDictionary_2_t1371992995 * Segmenter_get_changes_m1846267788 (Segmenter_t2695296026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RhythmTool::get_analysisDone()
extern "C"  bool RhythmTool_get_analysisDone_m3570634808 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnalysisData Analysis::get_analysisData()
extern "C"  AnalysisData_t108342674 * Analysis_get_analysisData_m3591218099 (Analysis_t439488098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioSource::get_volume()
extern "C"  float AudioSource_get_volume_m66289169 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C"  void AudioSource_set_volume_m2777308722 (AudioSource_t1135106623 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioSource::get_pitch()
extern "C"  float AudioSource_get_pitch_m4220572439 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C"  void AudioSource_set_pitch_m3064416458 (AudioSource_t1135106623 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C"  bool AudioSource_get_isPlaying_m3677592677 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Analysis>::.ctor()
#define List_1__ctor_m9134919(__this, method) ((  void (*) (List_1_t4103576526 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void Analysis::.ctor(System.Int32,System.Int32,System.String)
extern "C"  void Analysis__ctor_m1571091587 (Analysis_t439488098 * __this, int32_t ___start0, int32_t ___end1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Analysis>::Add(!0)
#define List_1_Add_m845626571(__this, p0, method) ((  void (*) (List_1_t4103576526 *, Analysis_t439488098 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void BeatTracker::.ctor()
extern "C"  void BeatTracker__ctor_m1207605921 (BeatTracker_t2801099156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnalysisData RhythmTool::get_all()
extern "C"  AnalysisData_t108342674 * RhythmTool_get_all_m1284823694 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Segmenter::.ctor(AnalysisData)
extern "C"  void Segmenter__ctor_m3661281169 (Segmenter_t2695296026 * __this, AnalysisData_t108342674 * ___analysis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_songLoaded(System.Boolean)
extern "C"  void RhythmTool_set_songLoaded_m1649989549 (RhythmTool_t215962618 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::Init()
extern "C"  void RhythmTool_Init_m2742459395 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, method) ((  AudioSource_t1135106623 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void System.Action`1<RhythmEventProvider>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2757687936(__this, p0, p1, method) ((  void (*) (Action_1_t16806139 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m584977596_gshared)(__this, p0, p1, method)
// System.Void RhythmEventProvider::add_EventProviderEnabled(System.Action`1<RhythmEventProvider>)
extern "C"  void RhythmEventProvider_add_EventProviderEnabled_m2395562879 (Il2CppObject * __this /* static, unused */, Action_1_t16806139 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::InitializeEventProvider(RhythmEventProvider)
extern "C"  void RhythmTool_InitializeEventProvider_m925349371 (RhythmTool_t215962618 * __this, RhythmEventProvider_t215006757 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C"  AudioClip_t1932558630 * AudioSource_get_clip_m2127996365 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`2<System.String,System.Int32>::Invoke(!0,!1)
#define UnityEvent_2_Invoke_m2327814026(__this, p0, p1, method) ((  void (*) (UnityEvent_2_t3077283191 *, String_t*, int32_t, const MethodInfo*))UnityEvent_2_Invoke_m1340061553_gshared)(__this, p0, p1, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<RhythmEventProvider> RhythmEventProvider::get_eventProviders()
extern "C"  ReadOnlyCollection_1_t400792449 * RhythmEventProvider_get_eventProviders_m1235811388 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<!0> System.Collections.ObjectModel.ReadOnlyCollection`1<RhythmEventProvider>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m2483346076(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t400792449 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2569117622_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m1668572632 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RhythmTool::QueueNewSong(UnityEngine.AudioClip)
extern "C"  Il2CppObject * RhythmTool_QueueNewSong_m891591844 (RhythmTool_t215962618 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool/<QueueNewSong>c__Iterator0::.ctor()
extern "C"  void U3CQueueNewSongU3Ec__Iterator0__ctor_m4124132409 (U3CQueueNewSongU3Ec__Iterator0_t1486555536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m3452679614 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m738814682 (AudioSource_t1135106623 * __this, AudioClip_t1932558630 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioClip::get_channels()
extern "C"  int32_t AudioClip_get_channels_m211770176 (AudioClip_t1932558630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioClip::get_samples()
extern "C"  int32_t AudioClip_get_samples_m3690111759 (AudioClip_t1932558630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmTool::get_frameSpacing()
extern "C"  int32_t RhythmTool_get_frameSpacing_m936531512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_totalFrames(System.Int32)
extern "C"  void RhythmTool_set_totalFrames_m509422205 (RhythmTool_t215962618 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioClip::get_frequency()
extern "C"  int32_t AudioClip_get_frequency_m237362468 (AudioClip_t1932558630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_frameLength(System.Single)
extern "C"  void RhythmTool_set_frameLength_m595084212 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Analysis>::GetEnumerator()
#define List_1_GetEnumerator_m3071804604(__this, method) ((  Enumerator_t3638306200  (*) (List_1_t4103576526 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Analysis>::get_Current()
#define Enumerator_get_Current_m3316544882(__this, method) ((  Analysis_t439488098 * (*) (Enumerator_t3638306200 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Void Analysis::Init(System.Int32)
extern "C"  void Analysis_Init_m3996269154 (Analysis_t439488098 * __this, int32_t ___totalFrames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<Analysis>::MoveNext()
#define Enumerator_MoveNext_m391630496(__this, method) ((  bool (*) (Enumerator_t3638306200 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Analysis>::Dispose()
#define Enumerator_Dispose_m1317449898(__this, method) ((  void (*) (Enumerator_t3638306200 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Single RhythmTool::get_frameLength()
extern "C"  float RhythmTool_get_frameLength_m2593270439 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeatTracker::Init(System.Single)
extern "C"  void BeatTracker_Init_m1298165546 (BeatTracker_t2801099156 * __this, float ___frameLength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Segmenter::Init()
extern "C"  void Segmenter_Init_m1948752761 (Segmenter_t2695296026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_currentFrame(System.Int32)
extern "C"  void RhythmTool_set_currentFrame_m2370147509 (RhythmTool_t215962618 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_lastFrame(System.Int32)
extern "C"  void RhythmTool_set_lastFrame_m3646349138 (RhythmTool_t215962618 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_currentSample(System.Single)
extern "C"  void RhythmTool_set_currentSample_m1718109522 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_analysisDone(System.Boolean)
extern "C"  void RhythmTool_set_analysisDone_m701052397 (RhythmTool_t215962618 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RhythmTool::AsyncAnalyze(System.Int32)
extern "C"  Il2CppObject * RhythmTool_AsyncAnalyze_m1726189964 (RhythmTool_t215962618 * __this, int32_t ___frames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C"  String_t* Application_get_persistentDataPath_m3129298355 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.File::Exists(System.String)
extern "C"  bool File_Exists_m1685968367 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SongData SongData::Deserialize(System.String)
extern "C"  SongData_t3132760915 * SongData_Deserialize_m3771651313 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<AnalysisData>::GetEnumerator()
#define List_1_GetEnumerator_m2880262412(__this, method) ((  Enumerator_t3307160776  (*) (List_1_t3772431102 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<AnalysisData>::get_Current()
#define Enumerator_get_Current_m3892498626(__this, method) ((  AnalysisData_t108342674 * (*) (Enumerator_t3307160776 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.String AnalysisData::get_name()
extern "C"  String_t* AnalysisData_get_name_m3184943038 (AnalysisData_t108342674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analysis::Init(AnalysisData)
extern "C"  void Analysis_Init_m2824569801 (Analysis_t439488098 * __this, AnalysisData_t108342674 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<AnalysisData>::MoveNext()
#define Enumerator_MoveNext_m1403247792(__this, method) ((  bool (*) (Enumerator_t3307160776 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AnalysisData>::Dispose()
#define Enumerator_Dispose_m2097033946(__this, method) ((  void (*) (Enumerator_t3307160776 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void BeatTracker::Init(SongData)
extern "C"  void BeatTracker_Init_m2105785558 (BeatTracker_t2801099156 * __this, SongData_t3132760915 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Segmenter::Init(SongData)
extern "C"  void Segmenter_Init_m2037373012 (Segmenter_t2695296026 * __this, SongData_t3132760915 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::InitializeEventProviders()
extern "C"  void RhythmTool_InitializeEventProviders_m198279347 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m3801112262 (Action_t3226471752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m3997572739 (GameObject_t1756533147 * __this, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeatTracker::FillEnd()
extern "C"  void BeatTracker_FillEnd_m1691323021 (BeatTracker_t2801099156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<AnalysisData>::.ctor()
#define List_1__ctor_m2896249751(__this, method) ((  void (*) (List_1_t3772431102 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AnalysisData>::Add(!0)
#define List_1_Add_m3745327547(__this, p0, method) ((  void (*) (List_1_t3772431102 *, AnalysisData_t108342674 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void SongData::.ctor(System.Collections.Generic.List`1<AnalysisData>,System.String,System.Int32,BeatTracker,Segmenter)
extern "C"  void SongData__ctor_m676028367 (SongData_t3132760915 * __this, List_1_t3772431102 * ___analyses0, String_t* ___name1, int32_t ___length2, BeatTracker_t2801099156 * ___beatTracker3, Segmenter_t2695296026 * ___segmenter4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SongData::Serialize()
extern "C"  void SongData_Serialize_m1654775970 (SongData_t3132760915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.ObjectModel.ReadOnlyCollection`1<RhythmEventProvider>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m2127664468(__this, p0, method) ((  RhythmEventProvider_t215006757 * (*) (ReadOnlyCollection_1_t400792449 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2664978718_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m4163344491 (UnityEvent_t408735097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RhythmEventProvider>::get_Count()
#define ReadOnlyCollection_1_get_Count_m3788110603(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t400792449 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RhythmTool::get_isPlaying()
extern "C"  bool RhythmTool_get_isPlaying_m2081879838 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RhythmTool::get_currentSample()
extern "C"  float RhythmTool_get_currentSample_m2446084809 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m4281640537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioSource::get_timeSamples()
extern "C"  int32_t AudioSource_get_timeSamples_m1719074425 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_interpolation(System.Single)
extern "C"  void RhythmTool_set_interpolation_m2276852035 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::EndOfSong()
extern "C"  void RhythmTool_EndOfSong_m2843491136 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Beat RhythmTool::NextBeat(System.Int32)
extern "C"  Beat_t2695683572 * RhythmTool_NextBeat_m782067910 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_beatLength(System.Single)
extern "C"  void RhythmTool_set_beatLength_m1126195059 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::set_bpm(System.Single)
extern "C"  void RhythmTool_set_bpm_m3815049600 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::Analyze()
extern "C"  void RhythmTool_Analyze_m1399810877 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::PassData()
extern "C"  void RhythmTool_PassData_m3430060328 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmTool::get_lastFrame()
extern "C"  int32_t RhythmTool_get_lastFrame_m3065909813 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::EndOfAnalysis()
extern "C"  void RhythmTool_EndOfAnalysis_m2036138409 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
extern "C"  bool AudioClip_GetData_m1645657273 (AudioClip_t1932558630 * __this, SingleU5BU5D_t577127397* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::GetMono(System.Single[],System.Single[],System.Int32)
extern "C"  void Util_GetMono_m2664450603 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___samples0, SingleU5BU5D_t577127397* ___mono1, int32_t ___channels2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::GetSpectrum(System.Single[])
extern "C"  void Util_GetSpectrum_m1376041373 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___samples0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::GetSpectrumMagnitude(System.Single[],System.Single[])
extern "C"  void Util_GetSpectrumMagnitude_m3600392938 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___spectrum0, SingleU5BU5D_t577127397* ___spectrumMagnitude1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analysis::Analyze(System.Single[],System.Int32)
extern "C"  void Analysis_Analyze_m2787083839 (Analysis_t439488098 * __this, SingleU5BU5D_t577127397* ___spectrum0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeatTracker::TrackBeat(System.Single)
extern "C"  void BeatTracker_TrackBeat_m2149388341 (BeatTracker_t2801099156 * __this, float ___sample0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Segmenter::DetectChanges(System.Int32)
extern "C"  void Segmenter_DetectChanges_m3953497980 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool/<AsyncAnalyze>c__Iterator1::.ctor()
extern "C"  void U3CAsyncAnalyzeU3Ec__Iterator1__ctor_m1537308272 (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::PassSubBeat(RhythmEventProvider,System.Single)
extern "C"  void RhythmTool_PassSubBeat_m3729313650 (RhythmTool_t215962618 * __this, RhythmEventProvider_t215006757 * ___r0, float ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::PassEvents(RhythmEventProvider,System.Int32)
extern "C"  void RhythmTool_PassEvents_m4272423917 (RhythmTool_t215962618 * __this, RhythmEventProvider_t215006757 * ___r0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmEventProvider/RhythmEvent`4<System.Int32,System.Single,System.Single,System.Single>::get_listenerCount()
#define RhythmEvent_4_get_listenerCount_m3042521709(__this, method) ((  int32_t (*) (RhythmEvent_4_t3330968721 *, const MethodInfo*))RhythmEvent_4_get_listenerCount_m3042521709_gshared)(__this, method)
// System.Single RhythmTool::BeatTimer(System.Single)
extern "C"  float RhythmTool_BeatTimer_m3542768051 (RhythmTool_t215962618 * __this, float ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`4<System.Int32,System.Single,System.Single,System.Single>::Invoke(!0,!1,!2,!3)
#define UnityEvent_4_Invoke_m1874634762(__this, p0, p1, p2, p3, method) ((  void (*) (UnityEvent_4_t1810603002 *, int32_t, float, float, float, const MethodInfo*))UnityEvent_4_Invoke_m1874634762_gshared)(__this, p0, p1, p2, p3, method)
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C"  int32_t Mathf_CeilToInt_m2672598779 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Beat RhythmTool::PrevBeat(System.Int32)
extern "C"  Beat_t2695683572 * RhythmTool_PrevBeat_m2126131030 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`2<Beat,System.Int32>::Invoke(!0,!1)
#define UnityEvent_2_Invoke_m407873616(__this, p0, p1, method) ((  void (*) (UnityEvent_2_t2653229824 *, Beat_t2695683572 *, int32_t, const MethodInfo*))UnityEvent_2_Invoke_m1340061553_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>::Invoke(!0,!1)
#define UnityEvent_2_Invoke_m514975631(__this, p0, p1, method) ((  void (*) (UnityEvent_2_t170329564 *, int32_t, int32_t, const MethodInfo*))UnityEvent_2_Invoke_m514975631_gshared)(__this, p0, p1, method)
// System.Int32 RhythmEventProvider/RhythmEvent`2<OnsetType,Onset>::get_listenerCount()
#define RhythmEvent_2_get_listenerCount_m405578019(__this, method) ((  int32_t (*) (RhythmEvent_2_t688166119 *, const MethodInfo*))RhythmEvent_2_get_listenerCount_m3434387278_gshared)(__this, method)
// Onset Analysis::GetOnset(System.Int32)
extern "C"  Onset_t732596509 * Analysis_GetOnset_m3617908717 (Analysis_t439488098 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Onset::op_GreaterThan(Onset,System.Single)
extern "C"  bool Onset_op_GreaterThan_m1430579383 (Il2CppObject * __this /* static, unused */, Onset_t732596509 * ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`2<OnsetType,Onset>::Invoke(!0,!1)
#define UnityEvent_2_Invoke_m1980955963(__this, p0, p1, method) ((  void (*) (UnityEvent_2_t3456830328 *, int32_t, Onset_t732596509 *, const MethodInfo*))UnityEvent_2_Invoke_m3277818011_gshared)(__this, p0, p1, method)
// System.Int32 RhythmEventProvider/RhythmEvent`1<Beat>::get_listenerCount()
#define RhythmEvent_1_get_listenerCount_m1237053972(__this, method) ((  int32_t (*) (RhythmEvent_1_t2633773566 *, const MethodInfo*))RhythmEvent_1_get_listenerCount_m946170288_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<Beat>::Invoke(!0)
#define UnityEvent_1_Invoke_m4176776406(__this, p0, method) ((  void (*) (UnityEvent_1_t2734033587 *, Beat_t2695683572 *, const MethodInfo*))UnityEvent_1_Invoke_m838874366_gshared)(__this, p0, method)
// System.Int32 RhythmEventProvider/RhythmEvent`2<System.Int32,System.Single>::get_listenerCount()
#define RhythmEvent_2_get_listenerCount_m3928554923(__this, method) ((  int32_t (*) (RhythmEvent_2_t1701265135 *, const MethodInfo*))RhythmEvent_2_get_listenerCount_m3928554923_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Int32,System.Single>::Invoke(!0,!1)
#define UnityEvent_2_Invoke_m3012962327(__this, p0, p1, method) ((  void (*) (UnityEvent_2_t174962048 *, int32_t, float, const MethodInfo*))UnityEvent_2_Invoke_m3012962327_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m353744792 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Pause()
extern "C"  void AudioSource_Pause_m71375470 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::UnPause()
extern "C"  void AudioSource_UnPause_m1911402783 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RhythmTool::TimeSeconds(System.Int32)
extern "C"  float RhythmTool_TimeSeconds_m1495203528 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::GetSpectrumData(System.Single[],System.Int32,UnityEngine.FFTWindow)
extern "C"  void AudioSource_GetSpectrumData_m4116675176 (AudioSource_t1135106623 * __this, SingleU5BU5D_t577127397* p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m2503577968 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m2474583393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<Analysis>::get_Item(System.Int32)
#define List_1_get_Item_m3137246970(__this, p0, method) ((  Analysis_t439488098 * (*) (List_1_t4103576526 *, int32_t, const MethodInfo*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Void Analysis::DrawDebugLines(System.Int32,System.Int32)
extern "C"  void Analysis_DrawDebugLines_m2160884685 (Analysis_t439488098 * __this, int32_t ___index0, int32_t ___h1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Analysis>::get_Count()
#define List_1_get_Count_m2439640319(__this, method) ((  int32_t (*) (List_1_t4103576526 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Void BeatTracker::DrawDebugLines(System.Int32)
extern "C"  void BeatTracker_DrawDebugLines_m1071911214 (BeatTracker_t2801099156 * __this, int32_t ___currentFrame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Beat BeatTracker::NextBeat(System.Int32)
extern "C"  Beat_t2695683572 * BeatTracker_NextBeat_m3845220818 (BeatTracker_t2801099156 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Beat BeatTracker::PrevBeat(System.Int32)
extern "C"  Beat_t2695683572 * BeatTracker_PrevBeat_m277720998 (BeatTracker_t2801099156 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmTool::NextBeatIndex(System.Int32)
extern "C"  int32_t RhythmTool_NextBeatIndex_m2622800185 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmTool::PrevBeatIndex(System.Int32)
extern "C"  int32_t RhythmTool_PrevBeatIndex_m2822132327 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BeatTracker::BeatTimer(System.Single)
extern "C"  float BeatTracker_BeatTimer_m2155883739 (BeatTracker_t2801099156 * __this, float ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BeatTracker::IsBeat(System.Int32,System.Int32)
extern "C"  int32_t BeatTracker_IsBeat_m3805475791 (BeatTracker_t2801099156 * __this, int32_t ___index0, int32_t ___min1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 RhythmTool::IsBeat(System.Int32,System.Int32)
extern "C"  int32_t RhythmTool_IsBeat_m2985775255 (RhythmTool_t215962618 * __this, int32_t ___index0, int32_t ___min1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Segmenter::IsChange(System.Int32)
extern "C"  bool Segmenter_IsChange_m246302866 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Segmenter::PrevChangeIndex(System.Int32)
extern "C"  int32_t Segmenter_PrevChangeIndex_m843645 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Segmenter::NextChangeIndex(System.Int32)
extern "C"  int32_t Segmenter_NextChangeIndex_m3584235607 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Segmenter::PrevChange(System.Int32)
extern "C"  float Segmenter_PrevChange_m778242627 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Segmenter::NextChange(System.Int32)
extern "C"  float Segmenter_NextChange_m3516349633 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ParameterizedThreadStart::.ctor(System.Object,System.IntPtr)
extern "C"  void ParameterizedThreadStart__ctor_m1215446210 (ParameterizedThreadStart_t2412552885 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::.ctor(System.Threading.ParameterizedThreadStart)
extern "C"  void Thread__ctor_m583758171 (Thread_t241561612 * __this, ParameterizedThreadStart_t2412552885 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Start(System.Object)
extern "C"  void Thread_Start_m2652746659 (Thread_t241561612 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::get_IsAlive()
extern "C"  bool Thread_get_IsAlive_m4169372557 (Thread_t241561612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RhythmTool::get_calculateTempo()
extern "C"  bool RhythmTool_get_calculateTempo_m3725479207 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BeatTracker::FillStart()
extern "C"  void BeatTracker_FillStart_m2215021468 (BeatTracker_t2801099156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RhythmTool::LoadNewSong(UnityEngine.AudioClip)
extern "C"  void RhythmTool_LoadNewSong_m1895013473 (RhythmTool_t215962618 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::.ctor()
#define Dictionary_2__ctor_m3391555206(__this, method) ((  void (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2__ctor_m3391555206_gshared)(__this, method)
// System.Void ReadOnlyDictionary`2<System.Int32,System.Single>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define ReadOnlyDictionary_2__ctor_m855543715(__this, p0, method) ((  void (*) (ReadOnlyDictionary_2_t1371992995 *, Il2CppObject*, const MethodInfo*))ReadOnlyDictionary_2__ctor_m855543715_gshared)(__this, p0, method)
// System.Void Segmenter::set_changes(ReadOnlyDictionary`2<System.Int32,System.Single>)
extern "C"  void Segmenter_set_changes_m1458862885 (Segmenter_t2695296026 * __this, ReadOnlyDictionary_2_t1371992995 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Segmenter::set_changeIndices(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>)
extern "C"  void Segmenter_set_changeIndices_m3118712765 (Segmenter_t2695296026 * __this, ReadOnlyCollection_1_t2257663140 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Clear()
#define Dictionary_2_Clear_m1652694321(__this, method) ((  void (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_Clear_m1652694321_gshared)(__this, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32> Segmenter::get_changeIndices()
extern "C"  ReadOnlyCollection_1_t2257663140 * Segmenter_get_changeIndices_m4266597926 (Segmenter_t2695296026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ReadOnlyDictionary`2<System.Int32,System.Single>::GetEnumerator()
#define ReadOnlyDictionary_2_GetEnumerator_m4119707746(__this, method) ((  Il2CppObject* (*) (ReadOnlyDictionary_2_t1371992995 *, const MethodInfo*))ReadOnlyDictionary_2_GetEnumerator_m4119707746_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m2768754870(__this, method) ((  int32_t (*) (KeyValuePair_2_t3136648085 *, const MethodInfo*))KeyValuePair_2_get_Key_m2768754870_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m2168417739(__this, method) ((  float (*) (KeyValuePair_2_t3136648085 *, const MethodInfo*))KeyValuePair_2_get_Value_m2168417739_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::Add(!0,!1)
#define Dictionary_2_Add_m4070564334(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1084335567 *, int32_t, float, const MethodInfo*))Dictionary_2_Add_m4070564334_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::get_Count()
#define Dictionary_2_get_Count_m3437212136(__this, method) ((  int32_t (*) (Dictionary_2_t1084335567 *, const MethodInfo*))Dictionary_2_get_Count_m3437212136_gshared)(__this, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,System.Single>::get_Item(!0)
#define Dictionary_2_get_Item_m1874253002(__this, p0, method) ((  float (*) (Dictionary_2_t1084335567 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1874253002_gshared)(__this, p0, method)
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern "C"  void BinaryFormatter__ctor_m4171832002 (BinaryFormatter_t1866979105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare)
extern "C"  void FileStream__ctor_m3699774824 (FileStream_t1695958676 * __this, String_t* p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
#define List_1__ctor_m704351054(__this, method) ((  void (*) (List_1_t1125654279 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void TileManager::SpawnTile(System.Int32)
extern "C"  void TileManager_SpawnTile_m1752937928 (TileManager_t3422405329 * __this, int32_t ___prefabIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TileManager::DeleteTile()
extern "C"  void TileManager_DeleteTile_m3895137793 (TileManager_t3422405329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TileManager::RandomPrefabIndex()
extern "C"  int32_t TileManager_RandomPrefabIndex_m124292481 (TileManager_t3422405329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m4124909910 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
#define List_1_Add_m3441471442(__this, p0, method) ((  void (*) (List_1_t1125654279 *, GameObject_t1756533147 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m939767277(__this, p0, method) ((  GameObject_t1756533147 * (*) (List_1_t1125654279 *, int32_t, const MethodInfo*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3404034275(__this, p0, method) ((  void (*) (List_1_t1125654279 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, p0, method)
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LomontFFT::RealFFT(System.Single[],System.Boolean)
extern "C"  void LomontFFT_RealFFT_m1443477460 (LomontFFT_t895069557 * __this, SingleU5BU5D_t577127397* ___data0, bool ___forward1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m485833136 (Exception_t1927440687 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m1686556575 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LomontFFT::.ctor()
extern "C"  void LomontFFT__ctor_m1683549620 (LomontFFT_t895069557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C"  void Application_set_runInBackground_m3543179741 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Line>::.ctor()
#define List_1__ctor_m2026045323(__this, method) ((  void (*) (List_1_t2098562634 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityAction`2<System.Int32,System.Single>::.ctor(System.Object,System.IntPtr)
#define UnityAction_2__ctor_m4231767244(__this, p0, p1, method) ((  void (*) (UnityAction_2_t2587731426 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_2__ctor_m4231767244_gshared)(__this, p0, p1, method)
// System.Void RhythmEventProvider/RhythmEvent`2<System.Int32,System.Single>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
#define RhythmEvent_2_AddListener_m2211829322(__this, p0, method) ((  void (*) (RhythmEvent_2_t1701265135 *, UnityAction_2_t2587731426 *, const MethodInfo*))RhythmEvent_2_AddListener_m2211829322_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::get_Count()
#define List_1_get_Count_m3361499332(__this, method) ((  int32_t (*) (List_1_t1301679762 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Void VisualizerController::NextSong()
extern "C"  void VisualizerController_NextSong_m1666774829 (VisualizerController_t119580760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisualizerController::ClearLines()
extern "C"  void VisualizerController_ClearLines_m3213124107 (VisualizerController_t119580760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.AudioClip>::get_Item(System.Int32)
#define List_1_get_Item_m3879122397(__this, p0, method) ((  AudioClip_t1932558630 * (*) (List_1_t1301679762 *, int32_t, const MethodInfo*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m1771960377 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C"  bool Input_GetKey_m3849524999 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisualizerController::UpdateLines()
extern "C"  void VisualizerController_UpdateLines_m940043679 (VisualizerController_t119580760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RhythmTool::get_bpm()
extern "C"  float RhythmTool_get_bpm_m154604281 (RhythmTool_t215962618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString()
extern "C"  String_t* Single_ToString_m1813392066 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Line>::GetEnumerator()
#define List_1_GetEnumerator_m3424352800(__this, method) ((  Enumerator_t1633292308  (*) (List_1_t2098562634 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Line>::get_Current()
#define Enumerator_get_Current_m2930845510(__this, method) ((  Line_t2729441502 * (*) (Enumerator_t1633292308 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Int32 Line::get_index()
extern "C"  int32_t Line_get_index_m1915203340 (Line_t2729441502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Line>::Add(!0)
#define List_1_Add_m1195348343(__this, p0, method) ((  void (*) (List_1_t2098562634 *, Line_t2729441502 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Line>::MoveNext()
#define Enumerator_MoveNext_m1329402980(__this, method) ((  bool (*) (Enumerator_t1633292308 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Line>::Dispose()
#define Enumerator_Dispose_m3502228714(__this, method) ((  void (*) (Enumerator_t1633292308 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Line>::Remove(!0)
#define List_1_Remove_m1874657968(__this, p0, method) ((  bool (*) (List_1_t2098562634 *, Line_t2729441502 *, const MethodInfo*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// Line VisualizerController::CreateLine(System.Int32,UnityEngine.Color,System.Single,System.Single)
extern "C"  Line_t2729441502 * VisualizerController_CreateLine_m3677149367 (VisualizerController_t119580760 * __this, int32_t ___index0, Color_t2020392075  ___color1, float ___opacity2, float ___yPosition3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Onset::op_LessThan(Onset,System.Single)
extern "C"  bool Onset_op_LessThan_m1075825226 (Il2CppObject * __this /* static, unused */, Onset_t732596509 * ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_magenta()
extern "C"  Color_t2020392075  Color_get_magenta_m3193089961 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Line>()
#define GameObject_GetComponent_TisLine_t2729441502_m1643494813(__this, method) ((  Line_t2729441502 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void Line::Init(UnityEngine.Color,System.Single,System.Int32)
extern "C"  void Line_Init_m3596432585 (Line_t2729441502 * __this, Color_t2020392075  ___color0, float ___opacity1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Line>::Clear()
#define List_1_Clear_m2227676928(__this, method) ((  void (*) (List_1_t2098562634 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Analysis::.ctor(System.Int32,System.Int32,System.String)
extern "C"  void Analysis__ctor_m1571091587 (Analysis_t439488098 * __this, int32_t ___start0, int32_t ___end1, String_t* ___name2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis__ctor_m1571091587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private int t1 = 0;
		__this->set_t1_8(0);
		// private int t2 = 0;
		__this->set_t2_9(0);
		// private int p1 = 0;
		__this->set_p1_10(0);
		// private int p2 = 0;
		__this->set_p2_11(0);
		// public Analysis(int start, int end, string name)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// this.name = name;
		String_t* L_0 = ___name2;
		// this.name = name;
		Analysis_set_name_m1665071749(__this, L_0, /*hidden argument*/NULL);
		// this.start = start;
		int32_t L_1 = ___start0;
		__this->set_start_12(L_1);
		// this.end = end;
		int32_t L_2 = ___end1;
		__this->set_end_13(L_2);
		// _magnitude = new List<float>();
		// _magnitude = new List<float>();
		List_1_t1445631064 * L_3 = (List_1_t1445631064 *)il2cpp_codegen_object_new(List_1_t1445631064_il2cpp_TypeInfo_var);
		List_1__ctor_m1509370154(L_3, /*hidden argument*/List_1__ctor_m1509370154_MethodInfo_var);
		__this->set__magnitude_4(L_3);
		// _flux = new List<float>();
		// _flux = new List<float>();
		List_1_t1445631064 * L_4 = (List_1_t1445631064 *)il2cpp_codegen_object_new(List_1_t1445631064_il2cpp_TypeInfo_var);
		List_1__ctor_m1509370154(L_4, /*hidden argument*/List_1__ctor_m1509370154_MethodInfo_var);
		__this->set__flux_6(L_4);
		// _magnitudeSmooth = new List<float>();
		// _magnitudeSmooth = new List<float>();
		List_1_t1445631064 * L_5 = (List_1_t1445631064 *)il2cpp_codegen_object_new(List_1_t1445631064_il2cpp_TypeInfo_var);
		List_1__ctor_m1509370154(L_5, /*hidden argument*/List_1__ctor_m1509370154_MethodInfo_var);
		__this->set__magnitudeSmooth_5(L_5);
		// _magnitudeAvg = new List<float>();
		// _magnitudeAvg = new List<float>();
		List_1_t1445631064 * L_6 = (List_1_t1445631064 *)il2cpp_codegen_object_new(List_1_t1445631064_il2cpp_TypeInfo_var);
		List_1__ctor_m1509370154(L_6, /*hidden argument*/List_1__ctor_m1509370154_MethodInfo_var);
		__this->set__magnitudeAvg_7(L_6);
		// _onsets = new Dictionary<int, Onset>(3000);
		// _onsets = new Dictionary<int, Onset>(3000);
		Dictionary_2_t4035389440 * L_7 = (Dictionary_2_t4035389440 *)il2cpp_codegen_object_new(Dictionary_2_t4035389440_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4135957043(L_7, ((int32_t)3000), /*hidden argument*/Dictionary_2__ctor_m4135957043_MethodInfo_var);
		__this->set__onsets_3(L_7);
		// onsetIndices = new List<int>(3000);
		// onsetIndices = new List<int>(3000);
		List_1_t1440998580 * L_8 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m555649161(L_8, ((int32_t)3000), /*hidden argument*/List_1__ctor_m555649161_MethodInfo_var);
		__this->set_onsetIndices_2(L_8);
		// analysisData = new AnalysisData(name, _magnitude, _flux, _magnitudeSmooth, _magnitudeAvg, _onsets);
		String_t* L_9 = ___name2;
		List_1_t1445631064 * L_10 = __this->get__magnitude_4();
		List_1_t1445631064 * L_11 = __this->get__flux_6();
		List_1_t1445631064 * L_12 = __this->get__magnitudeSmooth_5();
		List_1_t1445631064 * L_13 = __this->get__magnitudeAvg_7();
		Dictionary_2_t4035389440 * L_14 = __this->get__onsets_3();
		// analysisData = new AnalysisData(name, _magnitude, _flux, _magnitudeSmooth, _magnitudeAvg, _onsets);
		AnalysisData_t108342674 * L_15 = (AnalysisData_t108342674 *)il2cpp_codegen_object_new(AnalysisData_t108342674_il2cpp_TypeInfo_var);
		AnalysisData__ctor_m1090324430(L_15, L_9, L_10, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		// analysisData = new AnalysisData(name, _magnitude, _flux, _magnitudeSmooth, _magnitudeAvg, _onsets);
		Analysis_set_analysisData_m1435892088(__this, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// AnalysisData Analysis::get_analysisData()
extern "C"  AnalysisData_t108342674 * Analysis_get_analysisData_m3591218099 (Analysis_t439488098 * __this, const MethodInfo* method)
{
	AnalysisData_t108342674 * V_0 = NULL;
	{
		// public AnalysisData analysisData { get; private set; }
		AnalysisData_t108342674 * L_0 = __this->get_U3CanalysisDataU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		AnalysisData_t108342674 * L_1 = V_0;
		return L_1;
	}
}
// System.Void Analysis::set_analysisData(AnalysisData)
extern "C"  void Analysis_set_analysisData_m1435892088 (Analysis_t439488098 * __this, AnalysisData_t108342674 * ___value0, const MethodInfo* method)
{
	{
		// public AnalysisData analysisData { get; private set; }
		AnalysisData_t108342674 * L_0 = ___value0;
		__this->set_U3CanalysisDataU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String Analysis::get_name()
extern "C"  String_t* Analysis_get_name_m4149432526 (Analysis_t439488098 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		// public string name { get; private set; }
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void Analysis::set_name(System.String)
extern "C"  void Analysis_set_name_m1665071749 (Analysis_t439488098 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		// public string name { get; private set; }
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void Analysis::Init(System.Int32)
extern "C"  void Analysis_Init_m3996269154 (Analysis_t439488098 * __this, int32_t ___totalFrames0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_Init_m3996269154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// this.totalFrames = totalFrames;
		int32_t L_0 = ___totalFrames0;
		__this->set_totalFrames_14(L_0);
		// onsetIndices.Clear();
		List_1_t1440998580 * L_1 = __this->get_onsetIndices_2();
		// onsetIndices.Clear();
		NullCheck(L_1);
		List_1_Clear_m3644677550(L_1, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		// _magnitude.Clear();
		List_1_t1445631064 * L_2 = __this->get__magnitude_4();
		// _magnitude.Clear();
		NullCheck(L_2);
		List_1_Clear_m4110591713(L_2, /*hidden argument*/List_1_Clear_m4110591713_MethodInfo_var);
		// _flux.Clear();
		List_1_t1445631064 * L_3 = __this->get__flux_6();
		// _flux.Clear();
		NullCheck(L_3);
		List_1_Clear_m4110591713(L_3, /*hidden argument*/List_1_Clear_m4110591713_MethodInfo_var);
		// _magnitudeSmooth.Clear();
		List_1_t1445631064 * L_4 = __this->get__magnitudeSmooth_5();
		// _magnitudeSmooth.Clear();
		NullCheck(L_4);
		List_1_Clear_m4110591713(L_4, /*hidden argument*/List_1_Clear_m4110591713_MethodInfo_var);
		// _magnitudeAvg.Clear();
		List_1_t1445631064 * L_5 = __this->get__magnitudeAvg_7();
		// _magnitudeAvg.Clear();
		NullCheck(L_5);
		List_1_Clear_m4110591713(L_5, /*hidden argument*/List_1_Clear_m4110591713_MethodInfo_var);
		// _onsets.Clear();
		Dictionary_2_t4035389440 * L_6 = __this->get__onsets_3();
		// _onsets.Clear();
		NullCheck(L_6);
		Dictionary_2_Clear_m4007066975(L_6, /*hidden argument*/Dictionary_2_Clear_m4007066975_MethodInfo_var);
		// _magnitude.Capacity = totalFrames;
		List_1_t1445631064 * L_7 = __this->get__magnitude_4();
		int32_t L_8 = ___totalFrames0;
		// _magnitude.Capacity = totalFrames;
		NullCheck(L_7);
		List_1_set_Capacity_m3607535064(L_7, L_8, /*hidden argument*/List_1_set_Capacity_m3607535064_MethodInfo_var);
		// _flux.Capacity = totalFrames;
		List_1_t1445631064 * L_9 = __this->get__flux_6();
		int32_t L_10 = ___totalFrames0;
		// _flux.Capacity = totalFrames;
		NullCheck(L_9);
		List_1_set_Capacity_m3607535064(L_9, L_10, /*hidden argument*/List_1_set_Capacity_m3607535064_MethodInfo_var);
		// _magnitudeSmooth.Capacity = totalFrames;
		List_1_t1445631064 * L_11 = __this->get__magnitudeSmooth_5();
		int32_t L_12 = ___totalFrames0;
		// _magnitudeSmooth.Capacity = totalFrames;
		NullCheck(L_11);
		List_1_set_Capacity_m3607535064(L_11, L_12, /*hidden argument*/List_1_set_Capacity_m3607535064_MethodInfo_var);
		// _magnitudeAvg.Capacity = totalFrames;
		List_1_t1445631064 * L_13 = __this->get__magnitudeAvg_7();
		int32_t L_14 = ___totalFrames0;
		// _magnitudeAvg.Capacity = totalFrames;
		NullCheck(L_13);
		List_1_set_Capacity_m3607535064(L_13, L_14, /*hidden argument*/List_1_set_Capacity_m3607535064_MethodInfo_var);
		// _magnitude.AddRange(new float[totalFrames]);
		List_1_t1445631064 * L_15 = __this->get__magnitude_4();
		int32_t L_16 = ___totalFrames0;
		// _magnitude.AddRange(new float[totalFrames]);
		NullCheck(L_15);
		List_1_AddRange_m620564420(L_15, (Il2CppObject*)(Il2CppObject*)((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_16)), /*hidden argument*/List_1_AddRange_m620564420_MethodInfo_var);
		// _flux.AddRange(new float[totalFrames]);
		List_1_t1445631064 * L_17 = __this->get__flux_6();
		int32_t L_18 = ___totalFrames0;
		// _flux.AddRange(new float[totalFrames]);
		NullCheck(L_17);
		List_1_AddRange_m620564420(L_17, (Il2CppObject*)(Il2CppObject*)((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_18)), /*hidden argument*/List_1_AddRange_m620564420_MethodInfo_var);
		// _magnitudeSmooth.AddRange(new float[totalFrames]);
		List_1_t1445631064 * L_19 = __this->get__magnitudeSmooth_5();
		int32_t L_20 = ___totalFrames0;
		// _magnitudeSmooth.AddRange(new float[totalFrames]);
		NullCheck(L_19);
		List_1_AddRange_m620564420(L_19, (Il2CppObject*)(Il2CppObject*)((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_20)), /*hidden argument*/List_1_AddRange_m620564420_MethodInfo_var);
		// _magnitudeAvg.AddRange(new float[totalFrames]);
		List_1_t1445631064 * L_21 = __this->get__magnitudeAvg_7();
		int32_t L_22 = ___totalFrames0;
		// _magnitudeAvg.AddRange(new float[totalFrames]);
		NullCheck(L_21);
		List_1_AddRange_m620564420(L_21, (Il2CppObject*)(Il2CppObject*)((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_22)), /*hidden argument*/List_1_AddRange_m620564420_MethodInfo_var);
		// t1 = 0;
		__this->set_t1_8(0);
		// t2 = 0;
		__this->set_t2_9(0);
		// p1 = 0;
		__this->set_p1_10(0);
		// p2 = 0;
		__this->set_p2_11(0);
		// int spectrumSize = (RhythmTool.fftWindowSize / 2);
		int32_t L_23 = RhythmTool_get_fftWindowSize_m3317244415(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_23/(int32_t)2));
		// if (end < start || start < 0 || end < 0 || start >= spectrumSize || end > spectrumSize)
		int32_t L_24 = __this->get_end_13();
		int32_t L_25 = __this->get_start_12();
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_26 = __this->get_start_12();
		if ((((int32_t)L_26) < ((int32_t)0)))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_27 = __this->get_end_13();
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_28 = __this->get_start_12();
		int32_t L_29 = V_0;
		if ((((int32_t)L_28) >= ((int32_t)L_29)))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_30 = __this->get_end_13();
		int32_t L_31 = V_0;
		if ((((int32_t)L_30) <= ((int32_t)L_31)))
		{
			goto IL_013d;
		}
	}

IL_0123:
	{
		// Debug.LogError("Invalid range for analysis " + name + ". Range must be within fftWindowSize and start cannot come after end.");
		// Debug.LogError("Invalid range for analysis " + name + ". Range must be within fftWindowSize and start cannot come after end.");
		String_t* L_32 = Analysis_get_name_m4149432526(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral888332695, L_32, _stringLiteral2794641442, /*hidden argument*/NULL);
		// Debug.LogError("Invalid range for analysis " + name + ". Range must be within fftWindowSize and start cannot come after end.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_013d:
	{
		// }
		return;
	}
}
// System.Void Analysis::Init(AnalysisData)
extern "C"  void Analysis_Init_m2824569801 (Analysis_t439488098 * __this, AnalysisData_t108342674 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_Init_m2824569801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1792734662  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// onsetIndices.Clear();
		List_1_t1440998580 * L_0 = __this->get_onsetIndices_2();
		// onsetIndices.Clear();
		NullCheck(L_0);
		List_1_Clear_m3644677550(L_0, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		// _magnitude.Clear();
		List_1_t1445631064 * L_1 = __this->get__magnitude_4();
		// _magnitude.Clear();
		NullCheck(L_1);
		List_1_Clear_m4110591713(L_1, /*hidden argument*/List_1_Clear_m4110591713_MethodInfo_var);
		// _flux.Clear();
		List_1_t1445631064 * L_2 = __this->get__flux_6();
		// _flux.Clear();
		NullCheck(L_2);
		List_1_Clear_m4110591713(L_2, /*hidden argument*/List_1_Clear_m4110591713_MethodInfo_var);
		// _magnitudeSmooth.Clear();
		List_1_t1445631064 * L_3 = __this->get__magnitudeSmooth_5();
		// _magnitudeSmooth.Clear();
		NullCheck(L_3);
		List_1_Clear_m4110591713(L_3, /*hidden argument*/List_1_Clear_m4110591713_MethodInfo_var);
		// _magnitudeAvg.Clear();
		List_1_t1445631064 * L_4 = __this->get__magnitudeAvg_7();
		// _magnitudeAvg.Clear();
		NullCheck(L_4);
		List_1_Clear_m4110591713(L_4, /*hidden argument*/List_1_Clear_m4110591713_MethodInfo_var);
		// _onsets.Clear();
		Dictionary_2_t4035389440 * L_5 = __this->get__onsets_3();
		// _onsets.Clear();
		NullCheck(L_5);
		Dictionary_2_Clear_m4007066975(L_5, /*hidden argument*/Dictionary_2_Clear_m4007066975_MethodInfo_var);
		// _magnitude.AddRange(data.magnitude);
		List_1_t1445631064 * L_6 = __this->get__magnitude_4();
		AnalysisData_t108342674 * L_7 = ___data0;
		// _magnitude.AddRange(data.magnitude);
		NullCheck(L_7);
		ReadOnlyCollection_1_t2262295624 * L_8 = AnalysisData_get_magnitude_m671389901(L_7, /*hidden argument*/NULL);
		// _magnitude.AddRange(data.magnitude);
		NullCheck(L_6);
		List_1_AddRange_m620564420(L_6, L_8, /*hidden argument*/List_1_AddRange_m620564420_MethodInfo_var);
		// _flux.AddRange(data.flux);
		List_1_t1445631064 * L_9 = __this->get__flux_6();
		AnalysisData_t108342674 * L_10 = ___data0;
		// _flux.AddRange(data.flux);
		NullCheck(L_10);
		ReadOnlyCollection_1_t2262295624 * L_11 = AnalysisData_get_flux_m3559386780(L_10, /*hidden argument*/NULL);
		// _flux.AddRange(data.flux);
		NullCheck(L_9);
		List_1_AddRange_m620564420(L_9, L_11, /*hidden argument*/List_1_AddRange_m620564420_MethodInfo_var);
		// _magnitudeSmooth.AddRange(data.magnitudeSmooth);
		List_1_t1445631064 * L_12 = __this->get__magnitudeSmooth_5();
		AnalysisData_t108342674 * L_13 = ___data0;
		// _magnitudeSmooth.AddRange(data.magnitudeSmooth);
		NullCheck(L_13);
		ReadOnlyCollection_1_t2262295624 * L_14 = AnalysisData_get_magnitudeSmooth_m1926036311(L_13, /*hidden argument*/NULL);
		// _magnitudeSmooth.AddRange(data.magnitudeSmooth);
		NullCheck(L_12);
		List_1_AddRange_m620564420(L_12, L_14, /*hidden argument*/List_1_AddRange_m620564420_MethodInfo_var);
		// _magnitudeAvg.AddRange(data.magnitudeAvg);
		List_1_t1445631064 * L_15 = __this->get__magnitudeAvg_7();
		AnalysisData_t108342674 * L_16 = ___data0;
		// _magnitudeAvg.AddRange(data.magnitudeAvg);
		NullCheck(L_16);
		ReadOnlyCollection_1_t2262295624 * L_17 = AnalysisData_get_magnitudeAvg_m3285528701(L_16, /*hidden argument*/NULL);
		// _magnitudeAvg.AddRange(data.magnitudeAvg);
		NullCheck(L_15);
		List_1_AddRange_m620564420(L_15, L_17, /*hidden argument*/List_1_AddRange_m620564420_MethodInfo_var);
		// _magnitude.TrimExcess();
		List_1_t1445631064 * L_18 = __this->get__magnitude_4();
		// _magnitude.TrimExcess();
		NullCheck(L_18);
		List_1_TrimExcess_m2205122117(L_18, /*hidden argument*/List_1_TrimExcess_m2205122117_MethodInfo_var);
		// _flux.TrimExcess();
		List_1_t1445631064 * L_19 = __this->get__flux_6();
		// _flux.TrimExcess();
		NullCheck(L_19);
		List_1_TrimExcess_m2205122117(L_19, /*hidden argument*/List_1_TrimExcess_m2205122117_MethodInfo_var);
		// _magnitudeSmooth.TrimExcess();
		List_1_t1445631064 * L_20 = __this->get__magnitudeSmooth_5();
		// _magnitudeSmooth.TrimExcess();
		NullCheck(L_20);
		List_1_TrimExcess_m2205122117(L_20, /*hidden argument*/List_1_TrimExcess_m2205122117_MethodInfo_var);
		// _magnitudeAvg.TrimExcess();
		List_1_t1445631064 * L_21 = __this->get__magnitudeAvg_7();
		// _magnitudeAvg.TrimExcess();
		NullCheck(L_21);
		List_1_TrimExcess_m2205122117(L_21, /*hidden argument*/List_1_TrimExcess_m2205122117_MethodInfo_var);
		// foreach(KeyValuePair<int, Onset> onset in data.onsets)
		AnalysisData_t108342674 * L_22 = ___data0;
		// foreach(KeyValuePair<int, Onset> onset in data.onsets)
		NullCheck(L_22);
		ReadOnlyDictionary_2_t28079572 * L_23 = AnalysisData_get_onsets_m2742507631(L_22, /*hidden argument*/NULL);
		// foreach(KeyValuePair<int, Onset> onset in data.onsets)
		NullCheck(L_23);
		Il2CppObject* L_24 = ReadOnlyDictionary_2_GetEnumerator_m3517933108(L_23, /*hidden argument*/ReadOnlyDictionary_2_GetEnumerator_m3517933108_MethodInfo_var);
		V_1 = L_24;
	}

IL_00c0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e7;
		}

IL_00c5:
		{
			// foreach(KeyValuePair<int, Onset> onset in data.onsets)
			Il2CppObject* L_25 = V_1;
			// foreach(KeyValuePair<int, Onset> onset in data.onsets)
			NullCheck(L_25);
			KeyValuePair_2_t1792734662  L_26 = InterfaceFuncInvoker0< KeyValuePair_2_t1792734662  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Onset>>::get_Current() */, IEnumerator_1_t3563225785_il2cpp_TypeInfo_var, L_25);
			V_0 = L_26;
			// _onsets.Add(onset.Key, onset.Value);
			Dictionary_2_t4035389440 * L_27 = __this->get__onsets_3();
			// _onsets.Add(onset.Key, onset.Value);
			int32_t L_28 = KeyValuePair_2_get_Key_m3879747900((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m3879747900_MethodInfo_var);
			// _onsets.Add(onset.Key, onset.Value);
			Onset_t732596509 * L_29 = KeyValuePair_2_get_Value_m2281585953((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m2281585953_MethodInfo_var);
			// _onsets.Add(onset.Key, onset.Value);
			NullCheck(L_27);
			Dictionary_2_Add_m1340372924(L_27, L_28, L_29, /*hidden argument*/Dictionary_2_Add_m1340372924_MethodInfo_var);
		}

IL_00e7:
		{
			Il2CppObject* L_30 = V_1;
			// foreach(KeyValuePair<int, Onset> onset in data.onsets)
			NullCheck(L_30);
			bool L_31 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_30);
			if (L_31)
			{
				goto IL_00c5;
			}
		}

IL_00f2:
		{
			IL2CPP_LEAVE(0x104, FINALLY_00f7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_32 = V_1;
			if (!L_32)
			{
				goto IL_0103;
			}
		}

IL_00fd:
		{
			Il2CppObject* L_33 = V_1;
			// foreach(KeyValuePair<int, Onset> onset in data.onsets)
			NullCheck(L_33);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_33);
		}

IL_0103:
		{
			IL2CPP_END_FINALLY(247)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0x104, IL_0104)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0104:
	{
		// }
		return;
	}
}
// Onset Analysis::GetOnset(System.Int32)
extern "C"  Onset_t732596509 * Analysis_GetOnset_m3617908717 (Analysis_t439488098 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_GetOnset_m3617908717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Onset_t732596509 * V_0 = NULL;
	Onset_t732596509 * V_1 = NULL;
	{
		// _onsets.TryGetValue(index, out o);
		Dictionary_2_t4035389440 * L_0 = __this->get__onsets_3();
		int32_t L_1 = ___index0;
		// _onsets.TryGetValue(index, out o);
		NullCheck(L_0);
		Dictionary_2_TryGetValue_m3835210299(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3835210299_MethodInfo_var);
		// return o;
		Onset_t732596509 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		// }
		Onset_t732596509 * L_3 = V_1;
		return L_3;
	}
}
// System.Void Analysis::Analyze(System.Single[],System.Int32)
extern "C"  void Analysis_Analyze_m2787083839 (Analysis_t439488098 * __this, SingleU5BU5D_t577127397* ___spectrum0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_Analyze_m2787083839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _magnitude[index] = Util.Sum(spectrum, start, end);
		List_1_t1445631064 * L_0 = __this->get__magnitude_4();
		int32_t L_1 = ___index1;
		SingleU5BU5D_t577127397* L_2 = ___spectrum0;
		int32_t L_3 = __this->get_start_12();
		int32_t L_4 = __this->get_end_13();
		// _magnitude[index] = Util.Sum(spectrum, start, end);
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		float L_5 = Util_Sum_m3399145285(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		// _magnitude[index] = Util.Sum(spectrum, start, end);
		NullCheck(L_0);
		List_1_set_Item_m4206996116(L_0, L_1, L_5, /*hidden argument*/List_1_set_Item_m4206996116_MethodInfo_var);
		// Smooth(index, 10, 5);
		int32_t L_6 = ___index1;
		// Smooth(index, 10, 5);
		Analysis_Smooth_m199185592(__this, L_6, ((int32_t)10), 5, /*hidden argument*/NULL);
		// Average(index);
		int32_t L_7 = ___index1;
		// Average(index);
		Analysis_Average_m3308932463(__this, L_7, /*hidden argument*/NULL);
		// if (index > 1)
		int32_t L_8 = ___index1;
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_005e;
		}
	}
	{
		// _flux[index] = (_magnitude[index] - _magnitude[index - 1]);
		List_1_t1445631064 * L_9 = __this->get__flux_6();
		int32_t L_10 = ___index1;
		List_1_t1445631064 * L_11 = __this->get__magnitude_4();
		int32_t L_12 = ___index1;
		// _flux[index] = (_magnitude[index] - _magnitude[index - 1]);
		NullCheck(L_11);
		float L_13 = List_1_get_Item_m3254877171(L_11, L_12, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_14 = __this->get__magnitude_4();
		int32_t L_15 = ___index1;
		// _flux[index] = (_magnitude[index] - _magnitude[index - 1]);
		NullCheck(L_14);
		float L_16 = List_1_get_Item_m3254877171(L_14, ((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		// _flux[index] = (_magnitude[index] - _magnitude[index - 1]);
		NullCheck(L_9);
		List_1_set_Item_m4206996116(L_9, L_10, ((float)((float)L_13-(float)L_16)), /*hidden argument*/List_1_set_Item_m4206996116_MethodInfo_var);
	}

IL_005e:
	{
		// FindPeaks(index, 1.9f, 12);
		int32_t L_17 = ___index1;
		// FindPeaks(index, 1.9f, 12);
		Analysis_FindPeaks_m2740314761(__this, L_17, (1.9f), ((int32_t)12), /*hidden argument*/NULL);
		// RankPeaks(index - 12, 50);
		int32_t L_18 = ___index1;
		// RankPeaks(index - 12, 50);
		Analysis_RankPeaks_m34459363(__this, ((int32_t)((int32_t)L_18-(int32_t)((int32_t)12))), ((int32_t)50), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Analysis::DrawDebugLines(System.Int32,System.Int32)
extern "C"  void Analysis_DrawDebugLines_m2160884685 (Analysis_t439488098 * __this, int32_t ___index0, int32_t ___h1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_DrawDebugLines_m2160884685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Onset_t732596509 * V_3 = NULL;
	{
		// for (int i = 0; i < 299; i++)
		V_0 = 0;
		goto IL_020c;
	}

IL_0008:
	{
		// if (i + 1 + index > totalFrames - 1)
		int32_t L_0 = V_0;
		int32_t L_1 = ___index0;
		int32_t L_2 = __this->get_totalFrames_14();
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))+(int32_t)L_1))) <= ((int32_t)((int32_t)((int32_t)L_2-(int32_t)1)))))
		{
			goto IL_0020;
		}
	}
	{
		// break;
		goto IL_0217;
	}

IL_0020:
	{
		// Vector3 s = new Vector3(i, _magnitude[i + index] + h * 100, 0);
		int32_t L_3 = V_0;
		List_1_t1445631064 * L_4 = __this->get__magnitude_4();
		int32_t L_5 = V_0;
		int32_t L_6 = ___index0;
		// Vector3 s = new Vector3(i, _magnitude[i + index] + h * 100, 0);
		NullCheck(L_4);
		float L_7 = List_1_get_Item_m3254877171(L_4, ((int32_t)((int32_t)L_5+(int32_t)L_6)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_8 = ___h1;
		// Vector3 s = new Vector3(i, _magnitude[i + index] + h * 100, 0);
		Vector3__ctor_m2638739322((&V_1), (((float)((float)L_3))), ((float)((float)L_7+(float)(((float)((float)((int32_t)((int32_t)L_8*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// Vector3 e = new Vector3(i + 1, _magnitude[i + 1 + index] + h * 100, 0);
		int32_t L_9 = V_0;
		List_1_t1445631064 * L_10 = __this->get__magnitude_4();
		int32_t L_11 = V_0;
		int32_t L_12 = ___index0;
		// Vector3 e = new Vector3(i + 1, _magnitude[i + 1 + index] + h * 100, 0);
		NullCheck(L_10);
		float L_13 = List_1_get_Item_m3254877171(L_10, ((int32_t)((int32_t)((int32_t)((int32_t)L_11+(int32_t)1))+(int32_t)L_12)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_14 = ___h1;
		// Vector3 e = new Vector3(i + 1, _magnitude[i + 1 + index] + h * 100, 0);
		Vector3__ctor_m2638739322((&V_2), (((float)((float)((int32_t)((int32_t)L_9+(int32_t)1))))), ((float)((float)L_13+(float)(((float)((float)((int32_t)((int32_t)L_14*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.red);
		Vector3_t2243707580  L_15 = V_1;
		Vector3_t2243707580  L_16 = V_2;
		// Debug.DrawLine(s, e, Color.red);
		Color_t2020392075  L_17 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.red);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		// s = new Vector3(i, _magnitudeSmooth[i + index] + h * 100, 0);
		int32_t L_18 = V_0;
		List_1_t1445631064 * L_19 = __this->get__magnitudeSmooth_5();
		int32_t L_20 = V_0;
		int32_t L_21 = ___index0;
		// s = new Vector3(i, _magnitudeSmooth[i + index] + h * 100, 0);
		NullCheck(L_19);
		float L_22 = List_1_get_Item_m3254877171(L_19, ((int32_t)((int32_t)L_20+(int32_t)L_21)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_23 = ___h1;
		// s = new Vector3(i, _magnitudeSmooth[i + index] + h * 100, 0);
		Vector3__ctor_m2638739322((&V_1), (((float)((float)L_18))), ((float)((float)L_22+(float)(((float)((float)((int32_t)((int32_t)L_23*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// e = new Vector3(i + 1, _magnitudeSmooth[i + 1 + index] + h * 100, 0);
		int32_t L_24 = V_0;
		List_1_t1445631064 * L_25 = __this->get__magnitudeSmooth_5();
		int32_t L_26 = V_0;
		int32_t L_27 = ___index0;
		// e = new Vector3(i + 1, _magnitudeSmooth[i + 1 + index] + h * 100, 0);
		NullCheck(L_25);
		float L_28 = List_1_get_Item_m3254877171(L_25, ((int32_t)((int32_t)((int32_t)((int32_t)L_26+(int32_t)1))+(int32_t)L_27)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_29 = ___h1;
		// e = new Vector3(i + 1, _magnitudeSmooth[i + 1 + index] + h * 100, 0);
		Vector3__ctor_m2638739322((&V_2), (((float)((float)((int32_t)((int32_t)L_24+(int32_t)1))))), ((float)((float)L_28+(float)(((float)((float)((int32_t)((int32_t)L_29*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.red);
		Vector3_t2243707580  L_30 = V_1;
		Vector3_t2243707580  L_31 = V_2;
		// Debug.DrawLine(s, e, Color.red);
		Color_t2020392075  L_32 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.red);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_30, L_31, L_32, /*hidden argument*/NULL);
		// s = new Vector3(i, _magnitudeAvg[i + index] + h * 100, 0);
		int32_t L_33 = V_0;
		List_1_t1445631064 * L_34 = __this->get__magnitudeAvg_7();
		int32_t L_35 = V_0;
		int32_t L_36 = ___index0;
		// s = new Vector3(i, _magnitudeAvg[i + index] + h * 100, 0);
		NullCheck(L_34);
		float L_37 = List_1_get_Item_m3254877171(L_34, ((int32_t)((int32_t)L_35+(int32_t)L_36)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_38 = ___h1;
		// s = new Vector3(i, _magnitudeAvg[i + index] + h * 100, 0);
		Vector3__ctor_m2638739322((&V_1), (((float)((float)L_33))), ((float)((float)L_37+(float)(((float)((float)((int32_t)((int32_t)L_38*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// e = new Vector3(i + 1, _magnitudeAvg[i + 1 + index] + h * 100, 0);
		int32_t L_39 = V_0;
		List_1_t1445631064 * L_40 = __this->get__magnitudeAvg_7();
		int32_t L_41 = V_0;
		int32_t L_42 = ___index0;
		// e = new Vector3(i + 1, _magnitudeAvg[i + 1 + index] + h * 100, 0);
		NullCheck(L_40);
		float L_43 = List_1_get_Item_m3254877171(L_40, ((int32_t)((int32_t)((int32_t)((int32_t)L_41+(int32_t)1))+(int32_t)L_42)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_44 = ___h1;
		// e = new Vector3(i + 1, _magnitudeAvg[i + 1 + index] + h * 100, 0);
		Vector3__ctor_m2638739322((&V_2), (((float)((float)((int32_t)((int32_t)L_39+(int32_t)1))))), ((float)((float)L_43+(float)(((float)((float)((int32_t)((int32_t)L_44*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.black);
		Vector3_t2243707580  L_45 = V_1;
		Vector3_t2243707580  L_46 = V_2;
		// Debug.DrawLine(s, e, Color.black);
		Color_t2020392075  L_47 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.black);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_45, L_46, L_47, /*hidden argument*/NULL);
		// s = new Vector3(i, _flux[i + index] + h * 100, 0);
		int32_t L_48 = V_0;
		List_1_t1445631064 * L_49 = __this->get__flux_6();
		int32_t L_50 = V_0;
		int32_t L_51 = ___index0;
		// s = new Vector3(i, _flux[i + index] + h * 100, 0);
		NullCheck(L_49);
		float L_52 = List_1_get_Item_m3254877171(L_49, ((int32_t)((int32_t)L_50+(int32_t)L_51)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_53 = ___h1;
		// s = new Vector3(i, _flux[i + index] + h * 100, 0);
		Vector3__ctor_m2638739322((&V_1), (((float)((float)L_48))), ((float)((float)L_52+(float)(((float)((float)((int32_t)((int32_t)L_53*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// e = new Vector3(i + 1, _flux[i + 1 + index] + h * 100, 0);
		int32_t L_54 = V_0;
		List_1_t1445631064 * L_55 = __this->get__flux_6();
		int32_t L_56 = V_0;
		int32_t L_57 = ___index0;
		// e = new Vector3(i + 1, _flux[i + 1 + index] + h * 100, 0);
		NullCheck(L_55);
		float L_58 = List_1_get_Item_m3254877171(L_55, ((int32_t)((int32_t)((int32_t)((int32_t)L_56+(int32_t)1))+(int32_t)L_57)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_59 = ___h1;
		// e = new Vector3(i + 1, _flux[i + 1 + index] + h * 100, 0);
		Vector3__ctor_m2638739322((&V_2), (((float)((float)((int32_t)((int32_t)L_54+(int32_t)1))))), ((float)((float)L_58+(float)(((float)((float)((int32_t)((int32_t)L_59*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.blue);
		Vector3_t2243707580  L_60 = V_1;
		Vector3_t2243707580  L_61 = V_2;
		// Debug.DrawLine(s, e, Color.blue);
		Color_t2020392075  L_62 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.blue);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_60, L_61, L_62, /*hidden argument*/NULL);
		// if (_onsets.ContainsKey(i + index))
		Dictionary_2_t4035389440 * L_63 = __this->get__onsets_3();
		int32_t L_64 = V_0;
		int32_t L_65 = ___index0;
		// if (_onsets.ContainsKey(i + index))
		NullCheck(L_63);
		bool L_66 = Dictionary_2_ContainsKey_m1627758495(L_63, ((int32_t)((int32_t)L_64+(int32_t)L_65)), /*hidden argument*/Dictionary_2_ContainsKey_m1627758495_MethodInfo_var);
		if (!L_66)
		{
			goto IL_0207;
		}
	}
	{
		// Onset o = _onsets[i + index];
		Dictionary_2_t4035389440 * L_67 = __this->get__onsets_3();
		int32_t L_68 = V_0;
		int32_t L_69 = ___index0;
		// Onset o = _onsets[i + index];
		NullCheck(L_67);
		Onset_t732596509 * L_70 = Dictionary_2_get_Item_m552409364(L_67, ((int32_t)((int32_t)L_68+(int32_t)L_69)), /*hidden argument*/Dictionary_2_get_Item_m552409364_MethodInfo_var);
		V_3 = L_70;
		// s = new Vector3(i, h * 100, -1);
		int32_t L_71 = V_0;
		int32_t L_72 = ___h1;
		// s = new Vector3(i, h * 100, -1);
		Vector3__ctor_m2638739322((&V_1), (((float)((float)L_71))), (((float)((float)((int32_t)((int32_t)L_72*(int32_t)((int32_t)100)))))), (-1.0f), /*hidden argument*/NULL);
		// e = new Vector3(i, o.strength + h * 100, -1);
		int32_t L_73 = V_0;
		Onset_t732596509 * L_74 = V_3;
		NullCheck(L_74);
		float L_75 = L_74->get_strength_1();
		int32_t L_76 = ___h1;
		// e = new Vector3(i, o.strength + h * 100, -1);
		Vector3__ctor_m2638739322((&V_2), (((float)((float)L_73))), ((float)((float)L_75+(float)(((float)((float)((int32_t)((int32_t)L_76*(int32_t)((int32_t)100)))))))), (-1.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.green);
		Vector3_t2243707580  L_77 = V_1;
		Vector3_t2243707580  L_78 = V_2;
		// Debug.DrawLine(s, e, Color.green);
		Color_t2020392075  L_79 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.green);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_77, L_78, L_79, /*hidden argument*/NULL);
		// s = new Vector3(i, h * 100, 0);
		int32_t L_80 = V_0;
		int32_t L_81 = ___h1;
		// s = new Vector3(i, h * 100, 0);
		Vector3__ctor_m2638739322((&V_1), (((float)((float)L_80))), (((float)((float)((int32_t)((int32_t)L_81*(int32_t)((int32_t)100)))))), (0.0f), /*hidden argument*/NULL);
		// e = new Vector3(i, -o.rank + h * 100, 0);
		int32_t L_82 = V_0;
		Onset_t732596509 * L_83 = V_3;
		NullCheck(L_83);
		int32_t L_84 = L_83->get_rank_2();
		int32_t L_85 = ___h1;
		// e = new Vector3(i, -o.rank + h * 100, 0);
		Vector3__ctor_m2638739322((&V_2), (((float)((float)L_82))), (((float)((float)((int32_t)((int32_t)((-L_84))+(int32_t)((int32_t)((int32_t)L_85*(int32_t)((int32_t)100)))))))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.white);
		Vector3_t2243707580  L_86 = V_1;
		Vector3_t2243707580  L_87 = V_2;
		// Debug.DrawLine(s, e, Color.white);
		Color_t2020392075  L_88 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(s, e, Color.white);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_86, L_87, L_88, /*hidden argument*/NULL);
	}

IL_0207:
	{
		// for (int i = 0; i < 299; i++)
		int32_t L_89 = V_0;
		V_0 = ((int32_t)((int32_t)L_89+(int32_t)1));
	}

IL_020c:
	{
		// for (int i = 0; i < 299; i++)
		int32_t L_90 = V_0;
		if ((((int32_t)L_90) < ((int32_t)((int32_t)299))))
		{
			goto IL_0008;
		}
	}

IL_0217:
	{
		// }
		return;
	}
}
// System.Void Analysis::Smooth(System.Int32,System.Int32,System.Int32)
extern "C"  void Analysis_Smooth_m199185592 (Analysis_t439488098 * __this, int32_t ___index0, int32_t ___windowSize1, int32_t ___iterations2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_Smooth_m199185592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// _magnitudeSmooth[index] = _magnitude[index];
		List_1_t1445631064 * L_0 = __this->get__magnitudeSmooth_5();
		int32_t L_1 = ___index0;
		List_1_t1445631064 * L_2 = __this->get__magnitude_4();
		int32_t L_3 = ___index0;
		// _magnitudeSmooth[index] = _magnitude[index];
		NullCheck(L_2);
		float L_4 = List_1_get_Item_m3254877171(L_2, L_3, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		// _magnitudeSmooth[index] = _magnitude[index];
		NullCheck(L_0);
		List_1_set_Item_m4206996116(L_0, L_1, L_4, /*hidden argument*/List_1_set_Item_m4206996116_MethodInfo_var);
		// for (int i = 1; i < iterations + 1; i++)
		V_0 = 1;
		goto IL_003c;
	}

IL_0020:
	{
		// int iterationIndex = Mathf.Max(0, index - i * (windowSize / 2));
		int32_t L_5 = ___index0;
		int32_t L_6 = V_0;
		int32_t L_7 = ___windowSize1;
		// int iterationIndex = Mathf.Max(0, index - i * (windowSize / 2));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_8 = Mathf_Max_m1875893177(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_5-(int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)((int32_t)L_7/(int32_t)2)))))), /*hidden argument*/NULL);
		V_1 = L_8;
		// Smooth(iterationIndex, windowSize);
		int32_t L_9 = V_1;
		int32_t L_10 = ___windowSize1;
		// Smooth(iterationIndex, windowSize);
		Analysis_Smooth_m4230704111(__this, L_9, L_10, /*hidden argument*/NULL);
		// for (int i = 1; i < iterations + 1; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003c:
	{
		// for (int i = 1; i < iterations + 1; i++)
		int32_t L_12 = V_0;
		int32_t L_13 = ___iterations2;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)((int32_t)L_13+(int32_t)1)))))
		{
			goto IL_0020;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Analysis::Smooth(System.Int32,System.Int32)
extern "C"  void Analysis_Smooth_m4230704111 (Analysis_t439488098 * __this, int32_t ___index0, int32_t ___windowSize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_Smooth_m4230704111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// float average = 0;
		V_0 = (0.0f);
		// for (int i = index - (windowSize / 2); i < index + (windowSize / 2); i++)
		int32_t L_0 = ___index0;
		int32_t L_1 = ___windowSize1;
		V_1 = ((int32_t)((int32_t)L_0-(int32_t)((int32_t)((int32_t)L_1/(int32_t)2))));
		goto IL_003a;
	}

IL_0012:
	{
		// if (i > 0 && i < totalFrames)
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_3 = V_1;
		int32_t L_4 = __this->get_totalFrames_14();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		// average += _magnitudeSmooth[i];
		float L_5 = V_0;
		List_1_t1445631064 * L_6 = __this->get__magnitudeSmooth_5();
		int32_t L_7 = V_1;
		// average += _magnitudeSmooth[i];
		NullCheck(L_6);
		float L_8 = List_1_get_Item_m3254877171(L_6, L_7, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		V_0 = ((float)((float)L_5+(float)L_8));
	}

IL_0035:
	{
		// for (int i = index - (windowSize / 2); i < index + (windowSize / 2); i++)
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003a:
	{
		// for (int i = index - (windowSize / 2); i < index + (windowSize / 2); i++)
		int32_t L_10 = V_1;
		int32_t L_11 = ___index0;
		int32_t L_12 = ___windowSize1;
		if ((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)L_11+(int32_t)((int32_t)((int32_t)L_12/(int32_t)2)))))))
		{
			goto IL_0012;
		}
	}
	{
		// _magnitudeSmooth[index] = average / windowSize;
		List_1_t1445631064 * L_13 = __this->get__magnitudeSmooth_5();
		int32_t L_14 = ___index0;
		float L_15 = V_0;
		int32_t L_16 = ___windowSize1;
		// _magnitudeSmooth[index] = average / windowSize;
		NullCheck(L_13);
		List_1_set_Item_m4206996116(L_13, L_14, ((float)((float)L_15/(float)(((float)((float)L_16))))), /*hidden argument*/List_1_set_Item_m4206996116_MethodInfo_var);
		// }
		return;
	}
}
// System.Void Analysis::FindPeaks(System.Int32,System.Single,System.Int32)
extern "C"  void Analysis_FindPeaks_m2740314761 (Analysis_t439488098 * __this, int32_t ___index0, float ___thresholdMultiplier1, int32_t ___thresholdWindowSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_FindPeaks_m2740314761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	Onset_t732596509 * V_2 = NULL;
	{
		// int offset = Mathf.Max(index - (thresholdWindowSize / 2) - 1, 0);
		int32_t L_0 = ___index0;
		int32_t L_1 = ___thresholdWindowSize2;
		// int offset = Mathf.Max(index - (thresholdWindowSize / 2) - 1, 0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m1875893177(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)L_0-(int32_t)((int32_t)((int32_t)L_1/(int32_t)2))))-(int32_t)1)), 0, /*hidden argument*/NULL);
		V_0 = L_2;
		// float threshold = Threshold(offset, thresholdMultiplier, thresholdWindowSize);
		int32_t L_3 = V_0;
		float L_4 = ___thresholdMultiplier1;
		int32_t L_5 = ___thresholdWindowSize2;
		// float threshold = Threshold(offset, thresholdMultiplier, thresholdWindowSize);
		float L_6 = Analysis_Threshold_m3563177457(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// if (_flux[offset] > threshold && _flux[offset] > _flux[offset + 1] && _flux[offset] > _flux[offset - 1])
		List_1_t1445631064 * L_7 = __this->get__flux_6();
		int32_t L_8 = V_0;
		// if (_flux[offset] > threshold && _flux[offset] > _flux[offset + 1] && _flux[offset] > _flux[offset - 1])
		NullCheck(L_7);
		float L_9 = List_1_get_Item_m3254877171(L_7, L_8, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		float L_10 = V_1;
		if ((!(((float)L_9) > ((float)L_10))))
		{
			goto IL_0098;
		}
	}
	{
		List_1_t1445631064 * L_11 = __this->get__flux_6();
		int32_t L_12 = V_0;
		// if (_flux[offset] > threshold && _flux[offset] > _flux[offset + 1] && _flux[offset] > _flux[offset - 1])
		NullCheck(L_11);
		float L_13 = List_1_get_Item_m3254877171(L_11, L_12, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_14 = __this->get__flux_6();
		int32_t L_15 = V_0;
		// if (_flux[offset] > threshold && _flux[offset] > _flux[offset + 1] && _flux[offset] > _flux[offset - 1])
		NullCheck(L_14);
		float L_16 = List_1_get_Item_m3254877171(L_14, ((int32_t)((int32_t)L_15+(int32_t)1)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		if ((!(((float)L_13) > ((float)L_16))))
		{
			goto IL_0098;
		}
	}
	{
		List_1_t1445631064 * L_17 = __this->get__flux_6();
		int32_t L_18 = V_0;
		// if (_flux[offset] > threshold && _flux[offset] > _flux[offset + 1] && _flux[offset] > _flux[offset - 1])
		NullCheck(L_17);
		float L_19 = List_1_get_Item_m3254877171(L_17, L_18, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_20 = __this->get__flux_6();
		int32_t L_21 = V_0;
		// if (_flux[offset] > threshold && _flux[offset] > _flux[offset + 1] && _flux[offset] > _flux[offset - 1])
		NullCheck(L_20);
		float L_22 = List_1_get_Item_m3254877171(L_20, ((int32_t)((int32_t)L_21-(int32_t)1)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		if ((!(((float)L_19) > ((float)L_22))))
		{
			goto IL_0098;
		}
	}
	{
		// Onset o = new Onset(offset, _flux[offset], 0);
		int32_t L_23 = V_0;
		List_1_t1445631064 * L_24 = __this->get__flux_6();
		int32_t L_25 = V_0;
		// Onset o = new Onset(offset, _flux[offset], 0);
		NullCheck(L_24);
		float L_26 = List_1_get_Item_m3254877171(L_24, L_25, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		// Onset o = new Onset(offset, _flux[offset], 0);
		Onset_t732596509 * L_27 = (Onset_t732596509 *)il2cpp_codegen_object_new(Onset_t732596509_il2cpp_TypeInfo_var);
		Onset__ctor_m2770042849(L_27, L_23, L_26, 0, /*hidden argument*/NULL);
		V_2 = L_27;
		// _onsets.Add(offset, o);
		Dictionary_2_t4035389440 * L_28 = __this->get__onsets_3();
		int32_t L_29 = V_0;
		Onset_t732596509 * L_30 = V_2;
		// _onsets.Add(offset, o);
		NullCheck(L_28);
		Dictionary_2_Add_m1340372924(L_28, L_29, L_30, /*hidden argument*/Dictionary_2_Add_m1340372924_MethodInfo_var);
		// onsetIndices.Add(offset);
		List_1_t1440998580 * L_31 = __this->get_onsetIndices_2();
		int32_t L_32 = V_0;
		// onsetIndices.Add(offset);
		NullCheck(L_31);
		List_1_Add_m688682013(L_31, L_32, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
	}

IL_0098:
	{
		// }
		return;
	}
}
// System.Single Analysis::Threshold(System.Int32,System.Single,System.Int32)
extern "C"  float Analysis_Threshold_m3563177457 (Analysis_t439488098 * __this, int32_t ___index0, float ___multiplier1, int32_t ___windowSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_Threshold_m3563177457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	{
		// int start = Mathf.Max(0, index - windowSize/2);
		int32_t L_0 = ___index0;
		int32_t L_1 = ___windowSize2;
		// int start = Mathf.Max(0, index - windowSize/2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m1875893177(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_0-(int32_t)((int32_t)((int32_t)L_1/(int32_t)2)))), /*hidden argument*/NULL);
		V_0 = L_2;
		// int end = Mathf.Min(_flux.Count - 1, index + windowSize/2);
		List_1_t1445631064 * L_3 = __this->get__flux_6();
		// int end = Mathf.Min(_flux.Count - 1, index + windowSize/2);
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m3406166720(L_3, /*hidden argument*/List_1_get_Count_m3406166720_MethodInfo_var);
		int32_t L_5 = ___index0;
		int32_t L_6 = ___windowSize2;
		// int end = Mathf.Min(_flux.Count - 1, index + windowSize/2);
		int32_t L_7 = Mathf_Min_m2906823867(NULL /*static, unused*/, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5+(int32_t)((int32_t)((int32_t)L_6/(int32_t)2)))), /*hidden argument*/NULL);
		V_1 = L_7;
		// float mean = 0;
		V_2 = (0.0f);
		// for (int i = start; i <= end; i++)
		int32_t L_8 = V_0;
		V_3 = L_8;
		goto IL_004a;
	}

IL_0032:
	{
		// mean += Mathf.Abs(_flux[i]);
		float L_9 = V_2;
		List_1_t1445631064 * L_10 = __this->get__flux_6();
		int32_t L_11 = V_3;
		// mean += Mathf.Abs(_flux[i]);
		NullCheck(L_10);
		float L_12 = List_1_get_Item_m3254877171(L_10, L_11, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		// mean += Mathf.Abs(_flux[i]);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_13 = fabsf(L_12);
		V_2 = ((float)((float)L_9+(float)L_13));
		// for (int i = start; i <= end; i++)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004a:
	{
		// for (int i = start; i <= end; i++)
		int32_t L_15 = V_3;
		int32_t L_16 = V_1;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_0032;
		}
	}
	{
		// mean /= (end - start);
		float L_17 = V_2;
		int32_t L_18 = V_1;
		int32_t L_19 = V_0;
		V_2 = ((float)((float)L_17/(float)(((float)((float)((int32_t)((int32_t)L_18-(int32_t)L_19)))))));
		// return Mathf.Clamp(mean * multiplier, 3, 70);
		float L_20 = V_2;
		float L_21 = ___multiplier1;
		// return Mathf.Clamp(mean * multiplier, 3, 70);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_20*(float)L_21)), (3.0f), (70.0f), /*hidden argument*/NULL);
		V_4 = L_22;
		goto IL_0071;
	}

IL_0071:
	{
		// }
		float L_23 = V_4;
		return L_23;
	}
}
// System.Void Analysis::RankPeaks(System.Int32,System.Int32)
extern "C"  void Analysis_RankPeaks_m34459363 (Analysis_t439488098 * __this, int32_t ___index0, int32_t ___windowSize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_RankPeaks_m34459363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		// int offset = Mathf.Max(0, index - windowSize);
		int32_t L_0 = ___index0;
		int32_t L_1 = ___windowSize1;
		// int offset = Mathf.Max(0, index - windowSize);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m1875893177(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_0-(int32_t)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		// if (!_onsets.ContainsKey(offset))
		Dictionary_2_t4035389440 * L_3 = __this->get__onsets_3();
		int32_t L_4 = V_0;
		// if (!_onsets.ContainsKey(offset))
		NullCheck(L_3);
		bool L_5 = Dictionary_2_ContainsKey_m1627758495(L_3, L_4, /*hidden argument*/Dictionary_2_ContainsKey_m1627758495_MethodInfo_var);
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		// return;
		goto IL_0110;
	}

IL_0021:
	{
		// int onsetIndex = onsetIndices.IndexOf(offset);
		List_1_t1440998580 * L_6 = __this->get_onsetIndices_2();
		int32_t L_7 = V_0;
		// int onsetIndex = onsetIndices.IndexOf(offset);
		NullCheck(L_6);
		int32_t L_8 = List_1_IndexOf_m858031556(L_6, L_7, /*hidden argument*/List_1_IndexOf_m858031556_MethodInfo_var);
		V_1 = L_8;
		// int rank = onsetIndices.Count - onsetIndex;
		List_1_t1440998580 * L_9 = __this->get_onsetIndices_2();
		// int rank = onsetIndices.Count - onsetIndex;
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m852068579(L_9, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		int32_t L_11 = V_1;
		V_2 = ((int32_t)((int32_t)L_10-(int32_t)L_11));
		// for(int i = 5; i>0; i--)
		V_3 = 5;
		goto IL_00f7;
	}

IL_0043:
	{
		// if(onsetIndex-i>0&&onsetIndex+i<onsetIndices.Count)
		int32_t L_12 = V_1;
		int32_t L_13 = V_3;
		if ((((int32_t)((int32_t)((int32_t)L_12-(int32_t)L_13))) <= ((int32_t)0)))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_14 = V_1;
		int32_t L_15 = V_3;
		List_1_t1440998580 * L_16 = __this->get_onsetIndices_2();
		// if(onsetIndex-i>0&&onsetIndex+i<onsetIndices.Count)
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m852068579(L_16, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)((int32_t)((int32_t)L_14+(int32_t)L_15))) >= ((int32_t)L_17)))
		{
			goto IL_00f2;
		}
	}
	{
		// float c = _flux[offset];
		List_1_t1445631064 * L_18 = __this->get__flux_6();
		int32_t L_19 = V_0;
		// float c = _flux[offset];
		NullCheck(L_18);
		float L_20 = List_1_get_Item_m3254877171(L_18, L_19, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		V_4 = L_20;
		// float p = _flux[onsetIndices[onsetIndex - i]];
		List_1_t1445631064 * L_21 = __this->get__flux_6();
		List_1_t1440998580 * L_22 = __this->get_onsetIndices_2();
		int32_t L_23 = V_1;
		int32_t L_24 = V_3;
		// float p = _flux[onsetIndices[onsetIndex - i]];
		NullCheck(L_22);
		int32_t L_25 = List_1_get_Item_m1921196075(L_22, ((int32_t)((int32_t)L_23-(int32_t)L_24)), /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		// float p = _flux[onsetIndices[onsetIndex - i]];
		NullCheck(L_21);
		float L_26 = List_1_get_Item_m3254877171(L_21, L_25, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		V_5 = L_26;
		// float n = _flux[onsetIndices[onsetIndex + i]];
		List_1_t1445631064 * L_27 = __this->get__flux_6();
		List_1_t1440998580 * L_28 = __this->get_onsetIndices_2();
		int32_t L_29 = V_1;
		int32_t L_30 = V_3;
		// float n = _flux[onsetIndices[onsetIndex + i]];
		NullCheck(L_28);
		int32_t L_31 = List_1_get_Item_m1921196075(L_28, ((int32_t)((int32_t)L_29+(int32_t)L_30)), /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		// float n = _flux[onsetIndices[onsetIndex + i]];
		NullCheck(L_27);
		float L_32 = List_1_get_Item_m3254877171(L_27, L_31, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		V_6 = L_32;
		// if(c>p && c>n)
		float L_33 = V_4;
		float L_34 = V_5;
		if ((!(((float)L_33) > ((float)L_34))))
		{
			goto IL_00bd;
		}
	}
	{
		float L_35 = V_4;
		float L_36 = V_6;
		if ((!(((float)L_35) > ((float)L_36))))
		{
			goto IL_00bd;
		}
	}
	{
		// rank = 6-i;
		int32_t L_37 = V_3;
		V_2 = ((int32_t)((int32_t)6-(int32_t)L_37));
	}

IL_00bd:
	{
		// if (onsetIndices[onsetIndex - i] < offset - windowSize / 2 && onsetIndices[onsetIndex + i] > offset + windowSize / 2)
		List_1_t1440998580 * L_38 = __this->get_onsetIndices_2();
		int32_t L_39 = V_1;
		int32_t L_40 = V_3;
		// if (onsetIndices[onsetIndex - i] < offset - windowSize / 2 && onsetIndices[onsetIndex + i] > offset + windowSize / 2)
		NullCheck(L_38);
		int32_t L_41 = List_1_get_Item_m1921196075(L_38, ((int32_t)((int32_t)L_39-(int32_t)L_40)), /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		int32_t L_42 = V_0;
		int32_t L_43 = ___windowSize1;
		if ((((int32_t)L_41) >= ((int32_t)((int32_t)((int32_t)L_42-(int32_t)((int32_t)((int32_t)L_43/(int32_t)2)))))))
		{
			goto IL_00f1;
		}
	}
	{
		List_1_t1440998580 * L_44 = __this->get_onsetIndices_2();
		int32_t L_45 = V_1;
		int32_t L_46 = V_3;
		// if (onsetIndices[onsetIndex - i] < offset - windowSize / 2 && onsetIndices[onsetIndex + i] > offset + windowSize / 2)
		NullCheck(L_44);
		int32_t L_47 = List_1_get_Item_m1921196075(L_44, ((int32_t)((int32_t)L_45+(int32_t)L_46)), /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		int32_t L_48 = V_0;
		int32_t L_49 = ___windowSize1;
		if ((((int32_t)L_47) <= ((int32_t)((int32_t)((int32_t)L_48+(int32_t)((int32_t)((int32_t)L_49/(int32_t)2)))))))
		{
			goto IL_00f1;
		}
	}
	{
		// rank = 6-i;
		int32_t L_50 = V_3;
		V_2 = ((int32_t)((int32_t)6-(int32_t)L_50));
	}

IL_00f1:
	{
	}

IL_00f2:
	{
		// for(int i = 5; i>0; i--)
		int32_t L_51 = V_3;
		V_3 = ((int32_t)((int32_t)L_51-(int32_t)1));
	}

IL_00f7:
	{
		// for(int i = 5; i>0; i--)
		int32_t L_52 = V_3;
		if ((((int32_t)L_52) > ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
		// _onsets[offset].rank = rank;
		Dictionary_2_t4035389440 * L_53 = __this->get__onsets_3();
		int32_t L_54 = V_0;
		// _onsets[offset].rank = rank;
		NullCheck(L_53);
		Onset_t732596509 * L_55 = Dictionary_2_get_Item_m552409364(L_53, L_54, /*hidden argument*/Dictionary_2_get_Item_m552409364_MethodInfo_var);
		int32_t L_56 = V_2;
		NullCheck(L_55);
		L_55->set_rank_2(L_56);
	}

IL_0110:
	{
		// }
		return;
	}
}
// System.Void Analysis::Average(System.Int32)
extern "C"  void Analysis_Average_m3308932463 (Analysis_t439488098 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Analysis_Average_m3308932463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	List_1_t1445631064 * V_8 = NULL;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	{
		// if (index == totalFrames - 1)
		int32_t L_0 = ___index0;
		int32_t L_1 = __this->get_totalFrames_14();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)((int32_t)L_1-(int32_t)1))))))
		{
			goto IL_001f;
		}
	}
	{
		// t2 = index;
		int32_t L_2 = ___index0;
		__this->set_t2_9(L_2);
		// p2 = index;
		int32_t L_3 = ___index0;
		__this->set_p2_11(L_3);
	}

IL_001f:
	{
		// int offset = Mathf.Max(index - 100, 1);
		int32_t L_4 = ___index0;
		// int offset = Mathf.Max(index - 100, 1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m1875893177(NULL /*static, unused*/, ((int32_t)((int32_t)L_4-(int32_t)((int32_t)100))), 1, /*hidden argument*/NULL);
		V_0 = L_5;
		// if (_magnitudeSmooth[offset] < _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] < _magnitudeSmooth[offset + 1])
		List_1_t1445631064 * L_6 = __this->get__magnitudeSmooth_5();
		int32_t L_7 = V_0;
		// if (_magnitudeSmooth[offset] < _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] < _magnitudeSmooth[offset + 1])
		NullCheck(L_6);
		float L_8 = List_1_get_Item_m3254877171(L_6, L_7, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_9 = __this->get__magnitudeSmooth_5();
		int32_t L_10 = V_0;
		// if (_magnitudeSmooth[offset] < _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] < _magnitudeSmooth[offset + 1])
		NullCheck(L_9);
		float L_11 = List_1_get_Item_m3254877171(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		if ((!(((float)L_8) < ((float)L_11))))
		{
			goto IL_007d;
		}
	}
	{
		List_1_t1445631064 * L_12 = __this->get__magnitudeSmooth_5();
		int32_t L_13 = V_0;
		// if (_magnitudeSmooth[offset] < _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] < _magnitudeSmooth[offset + 1])
		NullCheck(L_12);
		float L_14 = List_1_get_Item_m3254877171(L_12, L_13, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_15 = __this->get__magnitudeSmooth_5();
		int32_t L_16 = V_0;
		// if (_magnitudeSmooth[offset] < _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] < _magnitudeSmooth[offset + 1])
		NullCheck(L_15);
		float L_17 = List_1_get_Item_m3254877171(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		if ((!(((float)L_14) < ((float)L_17))))
		{
			goto IL_007d;
		}
	}
	{
		// t1 = t2;
		int32_t L_18 = __this->get_t2_9();
		__this->set_t1_8(L_18);
		// t2 = offset;
		int32_t L_19 = V_0;
		__this->set_t2_9(L_19);
	}

IL_007d:
	{
		// if (_magnitudeSmooth[offset] > _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] > _magnitudeSmooth[offset + 1])
		List_1_t1445631064 * L_20 = __this->get__magnitudeSmooth_5();
		int32_t L_21 = V_0;
		// if (_magnitudeSmooth[offset] > _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] > _magnitudeSmooth[offset + 1])
		NullCheck(L_20);
		float L_22 = List_1_get_Item_m3254877171(L_20, L_21, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_23 = __this->get__magnitudeSmooth_5();
		int32_t L_24 = V_0;
		// if (_magnitudeSmooth[offset] > _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] > _magnitudeSmooth[offset + 1])
		NullCheck(L_23);
		float L_25 = List_1_get_Item_m3254877171(L_23, ((int32_t)((int32_t)L_24-(int32_t)1)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		if ((!(((float)L_22) > ((float)L_25))))
		{
			goto IL_00d0;
		}
	}
	{
		List_1_t1445631064 * L_26 = __this->get__magnitudeSmooth_5();
		int32_t L_27 = V_0;
		// if (_magnitudeSmooth[offset] > _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] > _magnitudeSmooth[offset + 1])
		NullCheck(L_26);
		float L_28 = List_1_get_Item_m3254877171(L_26, L_27, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_29 = __this->get__magnitudeSmooth_5();
		int32_t L_30 = V_0;
		// if (_magnitudeSmooth[offset] > _magnitudeSmooth[offset - 1] && _magnitudeSmooth[offset] > _magnitudeSmooth[offset + 1])
		NullCheck(L_29);
		float L_31 = List_1_get_Item_m3254877171(L_29, ((int32_t)((int32_t)L_30+(int32_t)1)), /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		if ((!(((float)L_28) > ((float)L_31))))
		{
			goto IL_00d0;
		}
	}
	{
		// p1 = p2;
		int32_t L_32 = __this->get_p2_11();
		__this->set_p1_10(L_32);
		// p2 = offset;
		int32_t L_33 = V_0;
		__this->set_p2_11(L_33);
	}

IL_00d0:
	{
		// if (t1 != t2)
		int32_t L_34 = __this->get_t1_8();
		int32_t L_35 = __this->get_t2_9();
		if ((((int32_t)L_34) == ((int32_t)L_35)))
		{
			goto IL_0204;
		}
	}
	{
		// if (t2 < p2)
		int32_t L_36 = __this->get_t2_9();
		int32_t L_37 = __this->get_p2_11();
		if ((((int32_t)L_36) >= ((int32_t)L_37)))
		{
			goto IL_0203;
		}
	}
	{
		// int nt = t2 - t1;
		int32_t L_38 = __this->get_t2_9();
		int32_t L_39 = __this->get_t1_8();
		V_1 = ((int32_t)((int32_t)L_38-(int32_t)L_39));
		// int np = p2 - p1;
		int32_t L_40 = __this->get_p2_11();
		int32_t L_41 = __this->get_p1_10();
		V_2 = ((int32_t)((int32_t)L_40-(int32_t)L_41));
		// float ft = (_magnitudeSmooth[t2] - _magnitudeSmooth[t1]) / nt;
		List_1_t1445631064 * L_42 = __this->get__magnitudeSmooth_5();
		int32_t L_43 = __this->get_t2_9();
		// float ft = (_magnitudeSmooth[t2] - _magnitudeSmooth[t1]) / nt;
		NullCheck(L_42);
		float L_44 = List_1_get_Item_m3254877171(L_42, L_43, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_45 = __this->get__magnitudeSmooth_5();
		int32_t L_46 = __this->get_t1_8();
		// float ft = (_magnitudeSmooth[t2] - _magnitudeSmooth[t1]) / nt;
		NullCheck(L_45);
		float L_47 = List_1_get_Item_m3254877171(L_45, L_46, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_48 = V_1;
		V_3 = ((float)((float)((float)((float)L_44-(float)L_47))/(float)(((float)((float)L_48)))));
		// float fp = (_magnitudeSmooth[p2] - _magnitudeSmooth[p1]) / np;
		List_1_t1445631064 * L_49 = __this->get__magnitudeSmooth_5();
		int32_t L_50 = __this->get_p2_11();
		// float fp = (_magnitudeSmooth[p2] - _magnitudeSmooth[p1]) / np;
		NullCheck(L_49);
		float L_51 = List_1_get_Item_m3254877171(L_49, L_50, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_52 = __this->get__magnitudeSmooth_5();
		int32_t L_53 = __this->get_p1_10();
		// float fp = (_magnitudeSmooth[p2] - _magnitudeSmooth[p1]) / np;
		NullCheck(L_52);
		float L_54 = List_1_get_Item_m3254877171(L_52, L_53, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_55 = V_2;
		V_4 = ((float)((float)((float)((float)L_51-(float)L_54))/(float)(((float)((float)L_55)))));
		// for (int i = p1; i < t2; i++)
		int32_t L_56 = __this->get_p1_10();
		V_5 = L_56;
		goto IL_01e9;
	}

IL_016c:
	{
		// int ti = i - t1;
		int32_t L_57 = V_5;
		int32_t L_58 = __this->get_t1_8();
		V_6 = ((int32_t)((int32_t)L_57-(int32_t)L_58));
		// int pi = i - p1;
		int32_t L_59 = V_5;
		int32_t L_60 = __this->get_p1_10();
		V_7 = ((int32_t)((int32_t)L_59-(int32_t)L_60));
		// _magnitudeAvg[i] = (_magnitudeSmooth[t1] + (ft * ti)) + (_magnitudeSmooth[p1] + (fp * pi));
		List_1_t1445631064 * L_61 = __this->get__magnitudeAvg_7();
		int32_t L_62 = V_5;
		List_1_t1445631064 * L_63 = __this->get__magnitudeSmooth_5();
		int32_t L_64 = __this->get_t1_8();
		// _magnitudeAvg[i] = (_magnitudeSmooth[t1] + (ft * ti)) + (_magnitudeSmooth[p1] + (fp * pi));
		NullCheck(L_63);
		float L_65 = List_1_get_Item_m3254877171(L_63, L_64, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		float L_66 = V_3;
		int32_t L_67 = V_6;
		List_1_t1445631064 * L_68 = __this->get__magnitudeSmooth_5();
		int32_t L_69 = __this->get_p1_10();
		// _magnitudeAvg[i] = (_magnitudeSmooth[t1] + (ft * ti)) + (_magnitudeSmooth[p1] + (fp * pi));
		NullCheck(L_68);
		float L_70 = List_1_get_Item_m3254877171(L_68, L_69, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		float L_71 = V_4;
		int32_t L_72 = V_7;
		// _magnitudeAvg[i] = (_magnitudeSmooth[t1] + (ft * ti)) + (_magnitudeSmooth[p1] + (fp * pi));
		NullCheck(L_61);
		List_1_set_Item_m4206996116(L_61, L_62, ((float)((float)((float)((float)L_65+(float)((float)((float)L_66*(float)(((float)((float)L_67)))))))+(float)((float)((float)L_70+(float)((float)((float)L_71*(float)(((float)((float)L_72))))))))), /*hidden argument*/List_1_set_Item_m4206996116_MethodInfo_var);
		// _magnitudeAvg[i] *= .5f;
		List_1_t1445631064 * L_73 = __this->get__magnitudeAvg_7();
		List_1_t1445631064 * L_74 = L_73;
		V_8 = L_74;
		int32_t L_75 = V_5;
		int32_t L_76 = L_75;
		V_9 = L_76;
		List_1_t1445631064 * L_77 = V_8;
		int32_t L_78 = V_9;
		// _magnitudeAvg[i] *= .5f;
		NullCheck(L_77);
		float L_79 = List_1_get_Item_m3254877171(L_77, L_78, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		// _magnitudeAvg[i] *= .5f;
		NullCheck(L_74);
		List_1_set_Item_m4206996116(L_74, L_76, ((float)((float)L_79*(float)(0.5f))), /*hidden argument*/List_1_set_Item_m4206996116_MethodInfo_var);
		// for (int i = p1; i < t2; i++)
		int32_t L_80 = V_5;
		V_5 = ((int32_t)((int32_t)L_80+(int32_t)1));
	}

IL_01e9:
	{
		// for (int i = p1; i < t2; i++)
		int32_t L_81 = V_5;
		int32_t L_82 = __this->get_t2_9();
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_016c;
		}
	}
	{
		// t1 = t2;
		int32_t L_83 = __this->get_t2_9();
		__this->set_t1_8(L_83);
	}

IL_0203:
	{
	}

IL_0204:
	{
		// if (p1 != p2)
		int32_t L_84 = __this->get_p1_10();
		int32_t L_85 = __this->get_p2_11();
		if ((((int32_t)L_84) == ((int32_t)L_85)))
		{
			goto IL_033e;
		}
	}
	{
		// if (p2 < t2)
		int32_t L_86 = __this->get_p2_11();
		int32_t L_87 = __this->get_t2_9();
		if ((((int32_t)L_86) >= ((int32_t)L_87)))
		{
			goto IL_033d;
		}
	}
	{
		// int nt = t2 - t1;
		int32_t L_88 = __this->get_t2_9();
		int32_t L_89 = __this->get_t1_8();
		V_10 = ((int32_t)((int32_t)L_88-(int32_t)L_89));
		// int np = p2 - p1;
		int32_t L_90 = __this->get_p2_11();
		int32_t L_91 = __this->get_p1_10();
		V_11 = ((int32_t)((int32_t)L_90-(int32_t)L_91));
		// float ft = (_magnitudeSmooth[t2] - _magnitudeSmooth[t1]) / nt;
		List_1_t1445631064 * L_92 = __this->get__magnitudeSmooth_5();
		int32_t L_93 = __this->get_t2_9();
		// float ft = (_magnitudeSmooth[t2] - _magnitudeSmooth[t1]) / nt;
		NullCheck(L_92);
		float L_94 = List_1_get_Item_m3254877171(L_92, L_93, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_95 = __this->get__magnitudeSmooth_5();
		int32_t L_96 = __this->get_t1_8();
		// float ft = (_magnitudeSmooth[t2] - _magnitudeSmooth[t1]) / nt;
		NullCheck(L_95);
		float L_97 = List_1_get_Item_m3254877171(L_95, L_96, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_98 = V_10;
		V_12 = ((float)((float)((float)((float)L_94-(float)L_97))/(float)(((float)((float)L_98)))));
		// float fp = (_magnitudeSmooth[p2] - _magnitudeSmooth[p1]) / np;
		List_1_t1445631064 * L_99 = __this->get__magnitudeSmooth_5();
		int32_t L_100 = __this->get_p2_11();
		// float fp = (_magnitudeSmooth[p2] - _magnitudeSmooth[p1]) / np;
		NullCheck(L_99);
		float L_101 = List_1_get_Item_m3254877171(L_99, L_100, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		List_1_t1445631064 * L_102 = __this->get__magnitudeSmooth_5();
		int32_t L_103 = __this->get_p1_10();
		// float fp = (_magnitudeSmooth[p2] - _magnitudeSmooth[p1]) / np;
		NullCheck(L_102);
		float L_104 = List_1_get_Item_m3254877171(L_102, L_103, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		int32_t L_105 = V_11;
		V_13 = ((float)((float)((float)((float)L_101-(float)L_104))/(float)(((float)((float)L_105)))));
		// for (int i = t1; i < p2; i++)
		int32_t L_106 = __this->get_t1_8();
		V_14 = L_106;
		goto IL_0323;
	}

IL_02a5:
	{
		// int ti = i - t1;
		int32_t L_107 = V_14;
		int32_t L_108 = __this->get_t1_8();
		V_15 = ((int32_t)((int32_t)L_107-(int32_t)L_108));
		// int pi = i - p1;
		int32_t L_109 = V_14;
		int32_t L_110 = __this->get_p1_10();
		V_16 = ((int32_t)((int32_t)L_109-(int32_t)L_110));
		// _magnitudeAvg[i] = (_magnitudeSmooth[t1] + (ft * ti)) + (_magnitudeSmooth[p1] + (fp * pi));
		List_1_t1445631064 * L_111 = __this->get__magnitudeAvg_7();
		int32_t L_112 = V_14;
		List_1_t1445631064 * L_113 = __this->get__magnitudeSmooth_5();
		int32_t L_114 = __this->get_t1_8();
		// _magnitudeAvg[i] = (_magnitudeSmooth[t1] + (ft * ti)) + (_magnitudeSmooth[p1] + (fp * pi));
		NullCheck(L_113);
		float L_115 = List_1_get_Item_m3254877171(L_113, L_114, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		float L_116 = V_12;
		int32_t L_117 = V_15;
		List_1_t1445631064 * L_118 = __this->get__magnitudeSmooth_5();
		int32_t L_119 = __this->get_p1_10();
		// _magnitudeAvg[i] = (_magnitudeSmooth[t1] + (ft * ti)) + (_magnitudeSmooth[p1] + (fp * pi));
		NullCheck(L_118);
		float L_120 = List_1_get_Item_m3254877171(L_118, L_119, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		float L_121 = V_13;
		int32_t L_122 = V_16;
		// _magnitudeAvg[i] = (_magnitudeSmooth[t1] + (ft * ti)) + (_magnitudeSmooth[p1] + (fp * pi));
		NullCheck(L_111);
		List_1_set_Item_m4206996116(L_111, L_112, ((float)((float)((float)((float)L_115+(float)((float)((float)L_116*(float)(((float)((float)L_117)))))))+(float)((float)((float)L_120+(float)((float)((float)L_121*(float)(((float)((float)L_122))))))))), /*hidden argument*/List_1_set_Item_m4206996116_MethodInfo_var);
		// _magnitudeAvg[i] *= .5f;
		List_1_t1445631064 * L_123 = __this->get__magnitudeAvg_7();
		List_1_t1445631064 * L_124 = L_123;
		V_8 = L_124;
		int32_t L_125 = V_14;
		int32_t L_126 = L_125;
		V_17 = L_126;
		List_1_t1445631064 * L_127 = V_8;
		int32_t L_128 = V_17;
		// _magnitudeAvg[i] *= .5f;
		NullCheck(L_127);
		float L_129 = List_1_get_Item_m3254877171(L_127, L_128, /*hidden argument*/List_1_get_Item_m3254877171_MethodInfo_var);
		// _magnitudeAvg[i] *= .5f;
		NullCheck(L_124);
		List_1_set_Item_m4206996116(L_124, L_126, ((float)((float)L_129*(float)(0.5f))), /*hidden argument*/List_1_set_Item_m4206996116_MethodInfo_var);
		// for (int i = t1; i < p2; i++)
		int32_t L_130 = V_14;
		V_14 = ((int32_t)((int32_t)L_130+(int32_t)1));
	}

IL_0323:
	{
		// for (int i = t1; i < p2; i++)
		int32_t L_131 = V_14;
		int32_t L_132 = __this->get_p2_11();
		if ((((int32_t)L_131) < ((int32_t)L_132)))
		{
			goto IL_02a5;
		}
	}
	{
		// p1 = p2;
		int32_t L_133 = __this->get_p2_11();
		__this->set_p1_10(L_133);
	}

IL_033d:
	{
	}

IL_033e:
	{
		// }
		return;
	}
}
// System.Void AnalysisData::.ctor(System.String,System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.Dictionary`2<System.Int32,Onset>)
extern "C"  void AnalysisData__ctor_m1090324430 (AnalysisData_t108342674 * __this, String_t* ___name0, List_1_t1445631064 * ___magnitude1, List_1_t1445631064 * ___flux2, List_1_t1445631064 * ___magnitudeSmooth3, List_1_t1445631064 * ___magnitudeAvg4, Dictionary_2_t4035389440 * ___onsets5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalysisData__ctor_m1090324430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public AnalysisData(string name, List<float> magnitude, List<float> flux, List<float> magnitudeSmooth, List<float> magnitudeAvg, Dictionary<int, Onset> onsets)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// this.name = name;
		String_t* L_0 = ___name0;
		// this.name = name;
		AnalysisData_set_name_m2914210485(__this, L_0, /*hidden argument*/NULL);
		// this.magnitude = magnitude.AsReadOnly();
		List_1_t1445631064 * L_1 = ___magnitude1;
		// this.magnitude = magnitude.AsReadOnly();
		NullCheck(L_1);
		ReadOnlyCollection_1_t2262295624 * L_2 = List_1_AsReadOnly_m1653708817(L_1, /*hidden argument*/List_1_AsReadOnly_m1653708817_MethodInfo_var);
		// this.magnitude = magnitude.AsReadOnly();
		AnalysisData_set_magnitude_m987943692(__this, L_2, /*hidden argument*/NULL);
		// this.flux = flux.AsReadOnly();
		List_1_t1445631064 * L_3 = ___flux2;
		// this.flux = flux.AsReadOnly();
		NullCheck(L_3);
		ReadOnlyCollection_1_t2262295624 * L_4 = List_1_AsReadOnly_m1653708817(L_3, /*hidden argument*/List_1_AsReadOnly_m1653708817_MethodInfo_var);
		// this.flux = flux.AsReadOnly();
		AnalysisData_set_flux_m1006935713(__this, L_4, /*hidden argument*/NULL);
		// this.magnitudeSmooth = magnitudeSmooth.AsReadOnly();
		List_1_t1445631064 * L_5 = ___magnitudeSmooth3;
		// this.magnitudeSmooth = magnitudeSmooth.AsReadOnly();
		NullCheck(L_5);
		ReadOnlyCollection_1_t2262295624 * L_6 = List_1_AsReadOnly_m1653708817(L_5, /*hidden argument*/List_1_AsReadOnly_m1653708817_MethodInfo_var);
		// this.magnitudeSmooth = magnitudeSmooth.AsReadOnly();
		AnalysisData_set_magnitudeSmooth_m1803049594(__this, L_6, /*hidden argument*/NULL);
		// this.magnitudeAvg = magnitudeAvg.AsReadOnly();
		List_1_t1445631064 * L_7 = ___magnitudeAvg4;
		// this.magnitudeAvg = magnitudeAvg.AsReadOnly();
		NullCheck(L_7);
		ReadOnlyCollection_1_t2262295624 * L_8 = List_1_AsReadOnly_m1653708817(L_7, /*hidden argument*/List_1_AsReadOnly_m1653708817_MethodInfo_var);
		// this.magnitudeAvg = magnitudeAvg.AsReadOnly();
		AnalysisData_set_magnitudeAvg_m3935231414(__this, L_8, /*hidden argument*/NULL);
		// this.onsets = new ReadOnlyDictionary<int, Onset>(onsets);
		Dictionary_2_t4035389440 * L_9 = ___onsets5;
		// this.onsets = new ReadOnlyDictionary<int, Onset>(onsets);
		ReadOnlyDictionary_2_t28079572 * L_10 = (ReadOnlyDictionary_2_t28079572 *)il2cpp_codegen_object_new(ReadOnlyDictionary_2_t28079572_il2cpp_TypeInfo_var);
		ReadOnlyDictionary_2__ctor_m150260537(L_10, L_9, /*hidden argument*/ReadOnlyDictionary_2__ctor_m150260537_MethodInfo_var);
		// this.onsets = new ReadOnlyDictionary<int, Onset>(onsets);
		AnalysisData_set_onsets_m1908993738(__this, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String AnalysisData::get_name()
extern "C"  String_t* AnalysisData_get_name_m3184943038 (AnalysisData_t108342674 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		// public string name { get; private set; }
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void AnalysisData::set_name(System.String)
extern "C"  void AnalysisData_set_name_m2914210485 (AnalysisData_t108342674 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		// public string name { get; private set; }
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::get_magnitude()
extern "C"  ReadOnlyCollection_1_t2262295624 * AnalysisData_get_magnitude_m671389901 (AnalysisData_t108342674 * __this, const MethodInfo* method)
{
	ReadOnlyCollection_1_t2262295624 * V_0 = NULL;
	{
		// public ReadOnlyCollection<float> magnitude { get; private set; }
		ReadOnlyCollection_1_t2262295624 * L_0 = __this->get_U3CmagnitudeU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyCollection_1_t2262295624 * L_1 = V_0;
		return L_1;
	}
}
// System.Void AnalysisData::set_magnitude(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>)
extern "C"  void AnalysisData_set_magnitude_m987943692 (AnalysisData_t108342674 * __this, ReadOnlyCollection_1_t2262295624 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyCollection<float> magnitude { get; private set; }
		ReadOnlyCollection_1_t2262295624 * L_0 = ___value0;
		__this->set_U3CmagnitudeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::get_flux()
extern "C"  ReadOnlyCollection_1_t2262295624 * AnalysisData_get_flux_m3559386780 (AnalysisData_t108342674 * __this, const MethodInfo* method)
{
	ReadOnlyCollection_1_t2262295624 * V_0 = NULL;
	{
		// public ReadOnlyCollection<float> flux { get; private set; }
		ReadOnlyCollection_1_t2262295624 * L_0 = __this->get_U3CfluxU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyCollection_1_t2262295624 * L_1 = V_0;
		return L_1;
	}
}
// System.Void AnalysisData::set_flux(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>)
extern "C"  void AnalysisData_set_flux_m1006935713 (AnalysisData_t108342674 * __this, ReadOnlyCollection_1_t2262295624 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyCollection<float> flux { get; private set; }
		ReadOnlyCollection_1_t2262295624 * L_0 = ___value0;
		__this->set_U3CfluxU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::get_magnitudeSmooth()
extern "C"  ReadOnlyCollection_1_t2262295624 * AnalysisData_get_magnitudeSmooth_m1926036311 (AnalysisData_t108342674 * __this, const MethodInfo* method)
{
	ReadOnlyCollection_1_t2262295624 * V_0 = NULL;
	{
		// public ReadOnlyCollection<float> magnitudeSmooth { get; private set; }
		ReadOnlyCollection_1_t2262295624 * L_0 = __this->get_U3CmagnitudeSmoothU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyCollection_1_t2262295624 * L_1 = V_0;
		return L_1;
	}
}
// System.Void AnalysisData::set_magnitudeSmooth(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>)
extern "C"  void AnalysisData_set_magnitudeSmooth_m1803049594 (AnalysisData_t108342674 * __this, ReadOnlyCollection_1_t2262295624 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyCollection<float> magnitudeSmooth { get; private set; }
		ReadOnlyCollection_1_t2262295624 * L_0 = ___value0;
		__this->set_U3CmagnitudeSmoothU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::get_magnitudeAvg()
extern "C"  ReadOnlyCollection_1_t2262295624 * AnalysisData_get_magnitudeAvg_m3285528701 (AnalysisData_t108342674 * __this, const MethodInfo* method)
{
	ReadOnlyCollection_1_t2262295624 * V_0 = NULL;
	{
		// public ReadOnlyCollection<float> magnitudeAvg { get; private set; }
		ReadOnlyCollection_1_t2262295624 * L_0 = __this->get_U3CmagnitudeAvgU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyCollection_1_t2262295624 * L_1 = V_0;
		return L_1;
	}
}
// System.Void AnalysisData::set_magnitudeAvg(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>)
extern "C"  void AnalysisData_set_magnitudeAvg_m3935231414 (AnalysisData_t108342674 * __this, ReadOnlyCollection_1_t2262295624 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyCollection<float> magnitudeAvg { get; private set; }
		ReadOnlyCollection_1_t2262295624 * L_0 = ___value0;
		__this->set_U3CmagnitudeAvgU3Ek__BackingField_4(L_0);
		return;
	}
}
// ReadOnlyDictionary`2<System.Int32,Onset> AnalysisData::get_onsets()
extern "C"  ReadOnlyDictionary_2_t28079572 * AnalysisData_get_onsets_m2742507631 (AnalysisData_t108342674 * __this, const MethodInfo* method)
{
	ReadOnlyDictionary_2_t28079572 * V_0 = NULL;
	{
		// public ReadOnlyDictionary<int, Onset> onsets { get; private set; }
		ReadOnlyDictionary_2_t28079572 * L_0 = __this->get_U3ConsetsU3Ek__BackingField_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyDictionary_2_t28079572 * L_1 = V_0;
		return L_1;
	}
}
// System.Void AnalysisData::set_onsets(ReadOnlyDictionary`2<System.Int32,Onset>)
extern "C"  void AnalysisData_set_onsets_m1908993738 (AnalysisData_t108342674 * __this, ReadOnlyDictionary_2_t28079572 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyDictionary<int, Onset> onsets { get; private set; }
		ReadOnlyDictionary_2_t28079572 * L_0 = ___value0;
		__this->set_U3ConsetsU3Ek__BackingField_5(L_0);
		return;
	}
}
// Onset AnalysisData::GetOnset(System.Int32)
extern "C"  Onset_t732596509 * AnalysisData_GetOnset_m643039293 (AnalysisData_t108342674 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalysisData_GetOnset_m643039293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Onset_t732596509 * V_0 = NULL;
	Onset_t732596509 * V_1 = NULL;
	{
		// onsets.TryGetValue(index, out o);
		// onsets.TryGetValue(index, out o);
		ReadOnlyDictionary_2_t28079572 * L_0 = AnalysisData_get_onsets_m2742507631(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		// onsets.TryGetValue(index, out o);
		NullCheck(L_0);
		ReadOnlyDictionary_2_TryGetValue_m2791401779(L_0, L_1, (&V_0), /*hidden argument*/ReadOnlyDictionary_2_TryGetValue_m2791401779_MethodInfo_var);
		// return o;
		Onset_t732596509 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		// }
		Onset_t732596509 * L_3 = V_1;
		return L_3;
	}
}
// System.Void AttackButtonManager::.ctor()
extern "C"  void AttackButtonManager__ctor_m3265656322 (AttackButtonManager_t2228488667 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AttackButtonManager::Start()
extern "C"  void AttackButtonManager_Start_m1492590518 (AttackButtonManager_t2228488667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttackButtonManager_Start_m1492590518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hitZoneManager = GameObject.FindWithTag("HitZoneParent").GetComponent<HitZoneManager>();
		// hitZoneManager = GameObject.FindWithTag("HitZoneParent").GetComponent<HitZoneManager>();
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1397813543, /*hidden argument*/NULL);
		// hitZoneManager = GameObject.FindWithTag("HitZoneParent").GetComponent<HitZoneManager>();
		NullCheck(L_0);
		HitZoneManager_t1308216902 * L_1 = GameObject_GetComponent_TisHitZoneManager_t1308216902_m2080816239(L_0, /*hidden argument*/GameObject_GetComponent_TisHitZoneManager_t1308216902_m2080816239_MethodInfo_var);
		__this->set_hitZoneManager_2(L_1);
		// button = GetComponent<Button>();
		// button = GetComponent<Button>();
		Button_t2872111280 * L_2 = Component_GetComponent_TisButton_t2872111280_m3412601438(__this, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var);
		__this->set_button_3(L_2);
		// button.onClick.AddListener(OnAttackPressed);
		Button_t2872111280 * L_3 = __this->get_button_3();
		// button.onClick.AddListener(OnAttackPressed);
		NullCheck(L_3);
		ButtonClickedEvent_t2455055323 * L_4 = Button_get_onClick_m1595880935(L_3, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)AttackButtonManager_OnAttackPressed_m3799490329_MethodInfo_var);
		UnityAction_t4025899511 * L_6 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_6, __this, L_5, /*hidden argument*/NULL);
		// button.onClick.AddListener(OnAttackPressed);
		NullCheck(L_4);
		UnityEvent_AddListener_m1596810379(L_4, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AttackButtonManager::OnAttackPressed()
extern "C"  void AttackButtonManager_OnAttackPressed_m3799490329 (AttackButtonManager_t2228488667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttackButtonManager_OnAttackPressed_m3799490329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (this.transform.tag == "Green")
		// if (this.transform.tag == "Green")
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// if (this.transform.tag == "Green")
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m357168014(L_0, /*hidden argument*/NULL);
		// if (this.transform.tag == "Green")
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		// if (hitZoneManager.m_green)
		HitZoneManager_t1308216902 * L_3 = __this->get_hitZoneManager_2();
		NullCheck(L_3);
		bool L_4 = L_3->get_m_green_2();
		if (!L_4)
		{
			goto IL_004a;
		}
	}
	{
		// GameManager.Destroy(hitZoneManager.m_greenTarget);
		HitZoneManager_t1308216902 * L_5 = __this->get_hitZoneManager_2();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_m_greenTarget_10();
		// GameManager.Destroy(hitZoneManager.m_greenTarget);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		// hitZoneManager.m_green = false;
		HitZoneManager_t1308216902 * L_7 = __this->get_hitZoneManager_2();
		NullCheck(L_7);
		L_7->set_m_green_2((bool)0);
	}

IL_004a:
	{
	}

IL_004b:
	{
		// if (this.transform.tag == "Red")
		// if (this.transform.tag == "Red")
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// if (this.transform.tag == "Red")
		NullCheck(L_8);
		String_t* L_9 = Component_get_tag_m357168014(L_8, /*hidden argument*/NULL);
		// if (this.transform.tag == "Red")
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0095;
		}
	}
	{
		// if (hitZoneManager.m_red)
		HitZoneManager_t1308216902 * L_11 = __this->get_hitZoneManager_2();
		NullCheck(L_11);
		bool L_12 = L_11->get_m_red_3();
		if (!L_12)
		{
			goto IL_0094;
		}
	}
	{
		// GameManager.Destroy(hitZoneManager.m_redTarget);
		HitZoneManager_t1308216902 * L_13 = __this->get_hitZoneManager_2();
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = L_13->get_m_redTarget_11();
		// GameManager.Destroy(hitZoneManager.m_redTarget);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		// hitZoneManager.m_red = false;
		HitZoneManager_t1308216902 * L_15 = __this->get_hitZoneManager_2();
		NullCheck(L_15);
		L_15->set_m_red_3((bool)0);
	}

IL_0094:
	{
	}

IL_0095:
	{
		// if (this.transform.tag == "Yellow")
		// if (this.transform.tag == "Yellow")
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// if (this.transform.tag == "Yellow")
		NullCheck(L_16);
		String_t* L_17 = Component_get_tag_m357168014(L_16, /*hidden argument*/NULL);
		// if (this.transform.tag == "Yellow")
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00df;
		}
	}
	{
		// if (hitZoneManager.m_yellow)
		HitZoneManager_t1308216902 * L_19 = __this->get_hitZoneManager_2();
		NullCheck(L_19);
		bool L_20 = L_19->get_m_yellow_4();
		if (!L_20)
		{
			goto IL_00de;
		}
	}
	{
		// GameManager.Destroy(hitZoneManager.m_yellowTarget);
		HitZoneManager_t1308216902 * L_21 = __this->get_hitZoneManager_2();
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = L_21->get_m_yellowTarget_12();
		// GameManager.Destroy(hitZoneManager.m_yellowTarget);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		// hitZoneManager.m_yellow = false;
		HitZoneManager_t1308216902 * L_23 = __this->get_hitZoneManager_2();
		NullCheck(L_23);
		L_23->set_m_yellow_4((bool)0);
	}

IL_00de:
	{
	}

IL_00df:
	{
		// if (this.transform.tag == "Blue")
		// if (this.transform.tag == "Blue")
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// if (this.transform.tag == "Blue")
		NullCheck(L_24);
		String_t* L_25 = Component_get_tag_m357168014(L_24, /*hidden argument*/NULL);
		// if (this.transform.tag == "Blue")
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_25, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0129;
		}
	}
	{
		// if (hitZoneManager.m_blue)
		HitZoneManager_t1308216902 * L_27 = __this->get_hitZoneManager_2();
		NullCheck(L_27);
		bool L_28 = L_27->get_m_blue_5();
		if (!L_28)
		{
			goto IL_0128;
		}
	}
	{
		// GameManager.Destroy(hitZoneManager.m_blueTarget);
		HitZoneManager_t1308216902 * L_29 = __this->get_hitZoneManager_2();
		NullCheck(L_29);
		GameObject_t1756533147 * L_30 = L_29->get_m_blueTarget_13();
		// GameManager.Destroy(hitZoneManager.m_blueTarget);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		// hitZoneManager.m_blue = false;
		HitZoneManager_t1308216902 * L_31 = __this->get_hitZoneManager_2();
		NullCheck(L_31);
		L_31->set_m_blue_5((bool)0);
	}

IL_0128:
	{
	}

IL_0129:
	{
		// }
		return;
	}
}
// System.Void BasicController::.ctor()
extern "C"  void BasicController__ctor_m3153565989 (BasicController_t2369989902 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BasicController::Start()
extern "C"  void BasicController_Start_m1102353557 (BasicController_t2369989902 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BasicController_Start_m1102353557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// rhythmTool = GetComponent<RhythmTool>();
		// rhythmTool = GetComponent<RhythmTool>();
		RhythmTool_t215962618 * L_0 = Component_GetComponent_TisRhythmTool_t215962618_m966574361(__this, /*hidden argument*/Component_GetComponent_TisRhythmTool_t215962618_m966574361_MethodInfo_var);
		__this->set_rhythmTool_2(L_0);
		// rhythmTool.NewSong(audioClip);
		RhythmTool_t215962618 * L_1 = __this->get_rhythmTool_2();
		AudioClip_t1932558630 * L_2 = __this->get_audioClip_3();
		// rhythmTool.NewSong(audioClip);
		NullCheck(L_1);
		RhythmTool_NewSong_m2509631375(L_1, L_2, /*hidden argument*/NULL);
		// rhythmTool.SongLoaded += OnSongLoaded;
		RhythmTool_t215962618 * L_3 = __this->get_rhythmTool_2();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)BasicController_OnSongLoaded_m3706846700_MethodInfo_var);
		Action_t3226471752 * L_5 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_5, __this, L_4, /*hidden argument*/NULL);
		// rhythmTool.SongLoaded += OnSongLoaded;
		NullCheck(L_3);
		RhythmTool_add_SongLoaded_m1500519296(L_3, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BasicController::OnSongLoaded()
extern "C"  void BasicController_OnSongLoaded_m3706846700 (BasicController_t2369989902 * __this, const MethodInfo* method)
{
	{
		// rhythmTool.Play ();
		RhythmTool_t215962618 * L_0 = __this->get_rhythmTool_2();
		// rhythmTool.Play ();
		NullCheck(L_0);
		RhythmTool_Play_m2237616365(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BasicController::Update()
extern "C"  void BasicController_Update_m1265671600 (BasicController_t2369989902 * __this, const MethodInfo* method)
{
	{
		// rhythmTool.DrawDebugLines ();
		RhythmTool_t215962618 * L_0 = __this->get_rhythmTool_2();
		// rhythmTool.DrawDebugLines ();
		NullCheck(L_0);
		RhythmTool_DrawDebugLines_m2101946209(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Beat::.ctor(System.Single,System.Single,System.Int32)
extern "C"  void Beat__ctor_m206855294 (Beat_t2695683572 * __this, float ___length0, float ___bpm1, int32_t ___index2, const MethodInfo* method)
{
	{
		// public Beat(float length, float bpm, int index)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// this.length=length;
		float L_0 = ___length0;
		__this->set_length_0(L_0);
		// this.bpm=bpm;
		float L_1 = ___bpm1;
		__this->set_bpm_1(L_1);
		// this.index=index;
		int32_t L_2 = ___index2;
		__this->set_index_2(L_2);
		// }
		return;
	}
}
// System.Void BeatTracker::.ctor()
extern "C"  void BeatTracker__ctor_m1207605921 (BeatTracker_t2801099156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker__ctor_m1207605921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private List<int> peaks = new List<int>();
		// private List<int> peaks = new List<int>();
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_0, /*hidden argument*/List_1__ctor_m1598946593_MethodInfo_var);
		__this->set_peaks_6(L_0);
		// private int beatTrackInterval = 20;
		__this->set_beatTrackInterval_13(((int32_t)20));
		// private int index = 0;
		__this->set_index_14(0);
		// public BeatTracker()
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// _beatIndices = new List<int>(3000);
		// _beatIndices = new List<int>(3000);
		List_1_t1440998580 * L_1 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m555649161(L_1, ((int32_t)3000), /*hidden argument*/List_1__ctor_m555649161_MethodInfo_var);
		__this->set__beatIndices_15(L_1);
		// _beats = new Dictionary<int, Beat>(3000);
		// _beats = new Dictionary<int, Beat>(3000);
		Dictionary_2_t1703509207 * L_2 = (Dictionary_2_t1703509207 *)il2cpp_codegen_object_new(Dictionary_2_t1703509207_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3978597214(L_2, ((int32_t)3000), /*hidden argument*/Dictionary_2__ctor_m3978597214_MethodInfo_var);
		__this->set__beats_16(L_2);
		// beatIndices = _beatIndices.AsReadOnly();
		List_1_t1440998580 * L_3 = __this->get__beatIndices_15();
		// beatIndices = _beatIndices.AsReadOnly();
		NullCheck(L_3);
		ReadOnlyCollection_1_t2257663140 * L_4 = List_1_AsReadOnly_m3503877813(L_3, /*hidden argument*/List_1_AsReadOnly_m3503877813_MethodInfo_var);
		// beatIndices = _beatIndices.AsReadOnly();
		BeatTracker_set_beatIndices_m645906055(__this, L_4, /*hidden argument*/NULL);
		// beats = new ReadOnlyDictionary<int, Beat>(_beats);
		Dictionary_2_t1703509207 * L_5 = __this->get__beats_16();
		// beats = new ReadOnlyDictionary<int, Beat>(_beats);
		ReadOnlyDictionary_2_t1991166635 * L_6 = (ReadOnlyDictionary_2_t1991166635 *)il2cpp_codegen_object_new(ReadOnlyDictionary_2_t1991166635_il2cpp_TypeInfo_var);
		ReadOnlyDictionary_2__ctor_m2336533874(L_6, L_5, /*hidden argument*/ReadOnlyDictionary_2__ctor_m2336533874_MethodInfo_var);
		// beats = new ReadOnlyDictionary<int, Beat>(_beats);
		BeatTracker_set_beats_m2173293630(__this, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32> BeatTracker::get_beatIndices()
extern "C"  ReadOnlyCollection_1_t2257663140 * BeatTracker_get_beatIndices_m1207551596 (BeatTracker_t2801099156 * __this, const MethodInfo* method)
{
	ReadOnlyCollection_1_t2257663140 * V_0 = NULL;
	{
		// public ReadOnlyCollection<int> beatIndices { get; private set; }
		ReadOnlyCollection_1_t2257663140 * L_0 = __this->get_U3CbeatIndicesU3Ek__BackingField_17();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyCollection_1_t2257663140 * L_1 = V_0;
		return L_1;
	}
}
// System.Void BeatTracker::set_beatIndices(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>)
extern "C"  void BeatTracker_set_beatIndices_m645906055 (BeatTracker_t2801099156 * __this, ReadOnlyCollection_1_t2257663140 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyCollection<int> beatIndices { get; private set; }
		ReadOnlyCollection_1_t2257663140 * L_0 = ___value0;
		__this->set_U3CbeatIndicesU3Ek__BackingField_17(L_0);
		return;
	}
}
// ReadOnlyDictionary`2<System.Int32,Beat> BeatTracker::get_beats()
extern "C"  ReadOnlyDictionary_2_t1991166635 * BeatTracker_get_beats_m3284239415 (BeatTracker_t2801099156 * __this, const MethodInfo* method)
{
	ReadOnlyDictionary_2_t1991166635 * V_0 = NULL;
	{
		// public ReadOnlyDictionary<int, Beat> beats { get; private set; }
		ReadOnlyDictionary_2_t1991166635 * L_0 = __this->get_U3CbeatsU3Ek__BackingField_18();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyDictionary_2_t1991166635 * L_1 = V_0;
		return L_1;
	}
}
// System.Void BeatTracker::set_beats(ReadOnlyDictionary`2<System.Int32,Beat>)
extern "C"  void BeatTracker_set_beats_m2173293630 (BeatTracker_t2801099156 * __this, ReadOnlyDictionary_2_t1991166635 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyDictionary<int, Beat> beats { get; private set; }
		ReadOnlyDictionary_2_t1991166635 * L_0 = ___value0;
		__this->set_U3CbeatsU3Ek__BackingField_18(L_0);
		return;
	}
}
// System.Void BeatTracker::Init(System.Single)
extern "C"  void BeatTracker_Init_m1298165546 (BeatTracker_t2801099156 * __this, float ___frameLength0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_Init_m1298165546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.frameLength = frameLength;
		float L_0 = ___frameLength0;
		__this->set_frameLength_12(L_0);
		// beatHistogram = new float[91];
		__this->set_beatHistogram_7(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)91))));
		// signalBuffer = new float[310];
		__this->set_signalBuffer_0(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)310))));
		// repetitionScore = new float[600];
		__this->set_repetitionScore_3(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)600))));
		// currentSignal = new float[signalBuffer.Length];
		SingleU5BU5D_t577127397* L_1 = __this->get_signalBuffer_0();
		NullCheck(L_1);
		__this->set_currentSignal_1(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))));
		// upsampledSignal = new float[currentSignal.Length * 4];
		SingleU5BU5D_t577127397* L_2 = __this->get_currentSignal_1();
		NullCheck(L_2);
		__this->set_upsampledSignal_2(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))*(int32_t)4)))));
		// gapScore = new float[180];
		__this->set_gapScore_4(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)180))));
		// offsetScore = new float[100];
		__this->set_offsetScore_5(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100))));
		// sync = 0;
		__this->set_sync_11((0.0f));
		// index = 0;
		__this->set_index_14(0);
		// syncOld = 0;
		__this->set_syncOld_10((0.0f));
		// bestRepetition = 0;
		__this->set_bestRepetition_8(0);
		// currentBeatLength = 0;
		__this->set_currentBeatLength_9(0);
		// _beatIndices.Clear();
		List_1_t1440998580 * L_3 = __this->get__beatIndices_15();
		// _beatIndices.Clear();
		NullCheck(L_3);
		List_1_Clear_m3644677550(L_3, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		// _beats.Clear();
		Dictionary_2_t1703509207 * L_4 = __this->get__beats_16();
		// _beats.Clear();
		NullCheck(L_4);
		Dictionary_2_Clear_m1564562098(L_4, /*hidden argument*/Dictionary_2_Clear_m1564562098_MethodInfo_var);
		// }
		return;
	}
}
// System.Void BeatTracker::Init(SongData)
extern "C"  void BeatTracker_Init_m2105785558 (BeatTracker_t2801099156 * __this, SongData_t3132760915 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_Init_m2105785558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3755821725  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// _beatIndices.Clear();
		List_1_t1440998580 * L_0 = __this->get__beatIndices_15();
		// _beatIndices.Clear();
		NullCheck(L_0);
		List_1_Clear_m3644677550(L_0, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		// _beatIndices.AddRange(data.beatTracker.beatIndices);
		List_1_t1440998580 * L_1 = __this->get__beatIndices_15();
		SongData_t3132760915 * L_2 = ___data0;
		NullCheck(L_2);
		BeatTracker_t2801099156 * L_3 = L_2->get_beatTracker_1();
		// _beatIndices.AddRange(data.beatTracker.beatIndices);
		NullCheck(L_3);
		ReadOnlyCollection_1_t2257663140 * L_4 = BeatTracker_get_beatIndices_m1207551596(L_3, /*hidden argument*/NULL);
		// _beatIndices.AddRange(data.beatTracker.beatIndices);
		NullCheck(L_1);
		List_1_AddRange_m2567809379(L_1, L_4, /*hidden argument*/List_1_AddRange_m2567809379_MethodInfo_var);
		// _beats.Clear();
		Dictionary_2_t1703509207 * L_5 = __this->get__beats_16();
		// _beats.Clear();
		NullCheck(L_5);
		Dictionary_2_Clear_m1564562098(L_5, /*hidden argument*/Dictionary_2_Clear_m1564562098_MethodInfo_var);
		// foreach (KeyValuePair<int, Beat> beat in data.beatTracker.beats)
		SongData_t3132760915 * L_6 = ___data0;
		NullCheck(L_6);
		BeatTracker_t2801099156 * L_7 = L_6->get_beatTracker_1();
		// foreach (KeyValuePair<int, Beat> beat in data.beatTracker.beats)
		NullCheck(L_7);
		ReadOnlyDictionary_2_t1991166635 * L_8 = BeatTracker_get_beats_m3284239415(L_7, /*hidden argument*/NULL);
		// foreach (KeyValuePair<int, Beat> beat in data.beatTracker.beats)
		NullCheck(L_8);
		Il2CppObject* L_9 = ReadOnlyDictionary_2_GetEnumerator_m885255707(L_8, /*hidden argument*/ReadOnlyDictionary_2_GetEnumerator_m885255707_MethodInfo_var);
		V_1 = L_9;
	}

IL_003f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0066;
		}

IL_0044:
		{
			// foreach (KeyValuePair<int, Beat> beat in data.beatTracker.beats)
			Il2CppObject* L_10 = V_1;
			// foreach (KeyValuePair<int, Beat> beat in data.beatTracker.beats)
			NullCheck(L_10);
			KeyValuePair_2_t3755821725  L_11 = InterfaceFuncInvoker0< KeyValuePair_2_t3755821725  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Beat>>::get_Current() */, IEnumerator_1_t1231345552_il2cpp_TypeInfo_var, L_10);
			V_0 = L_11;
			// _beats.Add(beat.Key, beat.Value);
			Dictionary_2_t1703509207 * L_12 = __this->get__beats_16();
			// _beats.Add(beat.Key, beat.Value);
			int32_t L_13 = KeyValuePair_2_get_Key_m3096183613((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m3096183613_MethodInfo_var);
			// _beats.Add(beat.Key, beat.Value);
			Beat_t2695683572 * L_14 = KeyValuePair_2_get_Value_m2369008986((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m2369008986_MethodInfo_var);
			// _beats.Add(beat.Key, beat.Value);
			NullCheck(L_12);
			Dictionary_2_Add_m3808454219(L_12, L_13, L_14, /*hidden argument*/Dictionary_2_Add_m3808454219_MethodInfo_var);
		}

IL_0066:
		{
			Il2CppObject* L_15 = V_1;
			// foreach (KeyValuePair<int, Beat> beat in data.beatTracker.beats)
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0044;
			}
		}

IL_0071:
		{
			IL2CPP_LEAVE(0x83, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_1;
			if (!L_17)
			{
				goto IL_0082;
			}
		}

IL_007c:
		{
			Il2CppObject* L_18 = V_1;
			// foreach (KeyValuePair<int, Beat> beat in data.beatTracker.beats)
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_18);
		}

IL_0082:
		{
			IL2CPP_END_FINALLY(118)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x83, IL_0083)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0083:
	{
		// }
		return;
	}
}
// System.Void BeatTracker::FillStart()
extern "C"  void BeatTracker_FillStart_m2215021468 (BeatTracker_t2801099156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_FillStart_m2215021468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t2921398135 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	Dictionary_2_t2921398135 * V_5 = NULL;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	float V_8 = 0.0f;
	KeyValuePair_2_t678743357  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Enumerator_t4241422837  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	int32_t V_12 = 0;
	List_1_t1440998580 * V_13 = NULL;
	KeyValuePair_2_t3755821725  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Enumerator_t3023533909  V_15;
	memset(&V_15, 0, sizeof(V_15));
	int32_t V_16 = 0;
	Enumerator_t975728254  V_17;
	memset(&V_17, 0, sizeof(V_17));
	float V_18 = 0.0f;
	int32_t V_19 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// if (_beatIndices.Count < 10)
		List_1_t1440998580 * L_0 = __this->get__beatIndices_15();
		// if (_beatIndices.Count < 10)
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m852068579(L_0, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0018;
		}
	}
	{
		// return;
		goto IL_029c;
	}

IL_0018:
	{
		// Dictionary<float, int> lengths = new Dictionary<float, int>();
		Dictionary_2_t2921398135 * L_2 = (Dictionary_2_t2921398135 *)il2cpp_codegen_object_new(Dictionary_2_t2921398135_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1683620704(L_2, /*hidden argument*/Dictionary_2__ctor_m1683620704_MethodInfo_var);
		V_0 = L_2;
		// int max = Mathf.Min(20, _beatIndices.Count);
		List_1_t1440998580 * L_3 = __this->get__beatIndices_15();
		// int max = Mathf.Min(20, _beatIndices.Count);
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m852068579(L_3, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		// int max = Mathf.Min(20, _beatIndices.Count);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Min_m2906823867(NULL /*static, unused*/, ((int32_t)20), L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		// for (int i = 0; i < max; i++)
		V_2 = 0;
		goto IL_0092;
	}

IL_0038:
	{
		// int ii = _beatIndices[i];
		List_1_t1440998580 * L_6 = __this->get__beatIndices_15();
		int32_t L_7 = V_2;
		// int ii = _beatIndices[i];
		NullCheck(L_6);
		int32_t L_8 = List_1_get_Item_m1921196075(L_6, L_7, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_3 = L_8;
		// float length = _beats[ii].length;
		Dictionary_2_t1703509207 * L_9 = __this->get__beats_16();
		int32_t L_10 = V_3;
		// float length = _beats[ii].length;
		NullCheck(L_9);
		Beat_t2695683572 * L_11 = Dictionary_2_get_Item_m2258219861(L_9, L_10, /*hidden argument*/Dictionary_2_get_Item_m2258219861_MethodInfo_var);
		NullCheck(L_11);
		float L_12 = L_11->get_length_0();
		V_4 = L_12;
		// if (lengths.ContainsKey(length))
		Dictionary_2_t2921398135 * L_13 = V_0;
		float L_14 = V_4;
		// if (lengths.ContainsKey(length))
		NullCheck(L_13);
		bool L_15 = Dictionary_2_ContainsKey_m4174387577(L_13, L_14, /*hidden argument*/Dictionary_2_ContainsKey_m4174387577_MethodInfo_var);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		// lengths[length]++;
		Dictionary_2_t2921398135 * L_16 = V_0;
		Dictionary_2_t2921398135 * L_17 = L_16;
		V_5 = L_17;
		float L_18 = V_4;
		float L_19 = L_18;
		V_6 = L_19;
		Dictionary_2_t2921398135 * L_20 = V_5;
		float L_21 = V_6;
		// lengths[length]++;
		NullCheck(L_20);
		int32_t L_22 = Dictionary_2_get_Item_m1122070824(L_20, L_21, /*hidden argument*/Dictionary_2_get_Item_m1122070824_MethodInfo_var);
		// lengths[length]++;
		NullCheck(L_17);
		Dictionary_2_set_Item_m2607921061(L_17, L_19, ((int32_t)((int32_t)L_22+(int32_t)1)), /*hidden argument*/Dictionary_2_set_Item_m2607921061_MethodInfo_var);
		goto IL_008d;
	}

IL_0084:
	{
		// lengths.Add(length, 1);
		Dictionary_2_t2921398135 * L_23 = V_0;
		float L_24 = V_4;
		// lengths.Add(length, 1);
		NullCheck(L_23);
		Dictionary_2_Add_m1780001816(L_23, L_24, 1, /*hidden argument*/Dictionary_2_Add_m1780001816_MethodInfo_var);
	}

IL_008d:
	{
		// for (int i = 0; i < max; i++)
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0092:
	{
		// for (int i = 0; i < max; i++)
		int32_t L_26 = V_2;
		int32_t L_27 = V_1;
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0038;
		}
	}
	{
		// int beatCount = 0;
		V_7 = 0;
		// float beatLength = 0;
		V_8 = (0.0f);
		// foreach (KeyValuePair<float, int> l in lengths)
		Dictionary_2_t2921398135 * L_28 = V_0;
		// foreach (KeyValuePair<float, int> l in lengths)
		NullCheck(L_28);
		Enumerator_t4241422837  L_29 = Dictionary_2_GetEnumerator_m4065924530(L_28, /*hidden argument*/Dictionary_2_GetEnumerator_m4065924530_MethodInfo_var);
		V_10 = L_29;
	}

IL_00ac:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00de;
		}

IL_00b1:
		{
			// foreach (KeyValuePair<float, int> l in lengths)
			// foreach (KeyValuePair<float, int> l in lengths)
			KeyValuePair_2_t678743357  L_30 = Enumerator_get_Current_m1983754276((&V_10), /*hidden argument*/Enumerator_get_Current_m1983754276_MethodInfo_var);
			V_9 = L_30;
			// if (l.Value > beatCount)
			// if (l.Value > beatCount)
			int32_t L_31 = KeyValuePair_2_get_Value_m1733170775((&V_9), /*hidden argument*/KeyValuePair_2_get_Value_m1733170775_MethodInfo_var);
			int32_t L_32 = V_7;
			if ((((int32_t)L_31) <= ((int32_t)L_32)))
			{
				goto IL_00dd;
			}
		}

IL_00c9:
		{
			// beatCount = l.Value;
			// beatCount = l.Value;
			int32_t L_33 = KeyValuePair_2_get_Value_m1733170775((&V_9), /*hidden argument*/KeyValuePair_2_get_Value_m1733170775_MethodInfo_var);
			V_7 = L_33;
			// beatLength = l.Key;
			// beatLength = l.Key;
			float L_34 = KeyValuePair_2_get_Key_m2838785900((&V_9), /*hidden argument*/KeyValuePair_2_get_Key_m2838785900_MethodInfo_var);
			V_8 = L_34;
		}

IL_00dd:
		{
		}

IL_00de:
		{
			// foreach (KeyValuePair<float, int> l in lengths)
			bool L_35 = Enumerator_MoveNext_m3884776761((&V_10), /*hidden argument*/Enumerator_MoveNext_m3884776761_MethodInfo_var);
			if (L_35)
			{
				goto IL_00b1;
			}
		}

IL_00ea:
		{
			IL2CPP_LEAVE(0xFD, FINALLY_00ef);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ef;
	}

FINALLY_00ef:
	{ // begin finally (depth: 1)
		// foreach (KeyValuePair<float, int> l in lengths)
		Enumerator_Dispose_m797750959((&V_10), /*hidden argument*/Enumerator_Dispose_m797750959_MethodInfo_var);
		IL2CPP_END_FINALLY(239)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(239)
	{
		IL2CPP_JUMP_TBL(0xFD, IL_00fd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00fd:
	{
		// if (beatLength < 1)
		float L_36 = V_8;
		if ((!(((float)L_36) < ((float)(1.0f)))))
		{
			goto IL_010e;
		}
	}
	{
		// return;
		goto IL_029c;
	}

IL_010e:
	{
		// float firstIndex = _beatIndices[0];
		List_1_t1440998580 * L_37 = __this->get__beatIndices_15();
		// float firstIndex = _beatIndices[0];
		NullCheck(L_37);
		int32_t L_38 = List_1_get_Item_m1921196075(L_37, 0, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_11 = (((float)((float)L_38)));
		// for (int i = 0; i < max; i++)
		V_12 = 0;
		goto IL_0167;
	}

IL_0125:
	{
		// if (_beats[_beatIndices[i]].length == beatLength)
		Dictionary_2_t1703509207 * L_39 = __this->get__beats_16();
		List_1_t1440998580 * L_40 = __this->get__beatIndices_15();
		int32_t L_41 = V_12;
		// if (_beats[_beatIndices[i]].length == beatLength)
		NullCheck(L_40);
		int32_t L_42 = List_1_get_Item_m1921196075(L_40, L_41, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		// if (_beats[_beatIndices[i]].length == beatLength)
		NullCheck(L_39);
		Beat_t2695683572 * L_43 = Dictionary_2_get_Item_m2258219861(L_39, L_42, /*hidden argument*/Dictionary_2_get_Item_m2258219861_MethodInfo_var);
		NullCheck(L_43);
		float L_44 = L_43->get_length_0();
		float L_45 = V_8;
		if ((!(((float)L_44) == ((float)L_45))))
		{
			goto IL_0160;
		}
	}
	{
		// firstIndex = _beatIndices[i];
		List_1_t1440998580 * L_46 = __this->get__beatIndices_15();
		int32_t L_47 = V_12;
		// firstIndex = _beatIndices[i];
		NullCheck(L_46);
		int32_t L_48 = List_1_get_Item_m1921196075(L_46, L_47, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_11 = (((float)((float)L_48)));
		// break;
		goto IL_016f;
	}

IL_0160:
	{
		// for (int i = 0; i < max; i++)
		int32_t L_49 = V_12;
		V_12 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_0167:
	{
		// for (int i = 0; i < max; i++)
		int32_t L_50 = V_12;
		int32_t L_51 = V_1;
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_0125;
		}
	}

IL_016f:
	{
		// List<int> toRemove = new List<int>();
		List_1_t1440998580 * L_52 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_52, /*hidden argument*/List_1__ctor_m1598946593_MethodInfo_var);
		V_13 = L_52;
		// foreach(KeyValuePair<int, Beat> k in _beats)
		Dictionary_2_t1703509207 * L_53 = __this->get__beats_16();
		// foreach(KeyValuePair<int, Beat> k in _beats)
		NullCheck(L_53);
		Enumerator_t3023533909  L_54 = Dictionary_2_GetEnumerator_m2354998391(L_53, /*hidden argument*/Dictionary_2_GetEnumerator_m2354998391_MethodInfo_var);
		V_15 = L_54;
	}

IL_0184:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01b8;
		}

IL_0189:
		{
			// foreach(KeyValuePair<int, Beat> k in _beats)
			// foreach(KeyValuePair<int, Beat> k in _beats)
			KeyValuePair_2_t3755821725  L_55 = Enumerator_get_Current_m909613959((&V_15), /*hidden argument*/Enumerator_get_Current_m909613959_MethodInfo_var);
			V_14 = L_55;
			// if(k.Value.index < firstIndex)
			// if(k.Value.index < firstIndex)
			Beat_t2695683572 * L_56 = KeyValuePair_2_get_Value_m2369008986((&V_14), /*hidden argument*/KeyValuePair_2_get_Value_m2369008986_MethodInfo_var);
			NullCheck(L_56);
			int32_t L_57 = L_56->get_index_2();
			float L_58 = V_11;
			if ((!(((float)(((float)((float)L_57)))) < ((float)L_58))))
			{
				goto IL_01b7;
			}
		}

IL_01a7:
		{
			// toRemove.Add(k.Key);
			List_1_t1440998580 * L_59 = V_13;
			// toRemove.Add(k.Key);
			int32_t L_60 = KeyValuePair_2_get_Key_m3096183613((&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m3096183613_MethodInfo_var);
			// toRemove.Add(k.Key);
			NullCheck(L_59);
			List_1_Add_m688682013(L_59, L_60, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
		}

IL_01b7:
		{
		}

IL_01b8:
		{
			// foreach(KeyValuePair<int, Beat> k in _beats)
			bool L_61 = Enumerator_MoveNext_m1568926478((&V_15), /*hidden argument*/Enumerator_MoveNext_m1568926478_MethodInfo_var);
			if (L_61)
			{
				goto IL_0189;
			}
		}

IL_01c4:
		{
			IL2CPP_LEAVE(0x1D7, FINALLY_01c9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01c9;
	}

FINALLY_01c9:
	{ // begin finally (depth: 1)
		// foreach(KeyValuePair<int, Beat> k in _beats)
		Enumerator_Dispose_m2364881746((&V_15), /*hidden argument*/Enumerator_Dispose_m2364881746_MethodInfo_var);
		IL2CPP_END_FINALLY(457)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(457)
	{
		IL2CPP_JUMP_TBL(0x1D7, IL_01d7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01d7:
	{
		// foreach(int k in toRemove)
		List_1_t1440998580 * L_62 = V_13;
		// foreach(int k in toRemove)
		NullCheck(L_62);
		Enumerator_t975728254  L_63 = List_1_GetEnumerator_m2527786909(L_62, /*hidden argument*/List_1_GetEnumerator_m2527786909_MethodInfo_var);
		V_17 = L_63;
	}

IL_01e1:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020d;
		}

IL_01e6:
		{
			// foreach(int k in toRemove)
			// foreach(int k in toRemove)
			int32_t L_64 = Enumerator_get_Current_m1062633493((&V_17), /*hidden argument*/Enumerator_get_Current_m1062633493_MethodInfo_var);
			V_16 = L_64;
			// _beatIndices.Remove(k);
			List_1_t1440998580 * L_65 = __this->get__beatIndices_15();
			int32_t L_66 = V_16;
			// _beatIndices.Remove(k);
			NullCheck(L_65);
			List_1_Remove_m3494432915(L_65, L_66, /*hidden argument*/List_1_Remove_m3494432915_MethodInfo_var);
			// _beats.Remove(k);
			Dictionary_2_t1703509207 * L_67 = __this->get__beats_16();
			int32_t L_68 = V_16;
			// _beats.Remove(k);
			NullCheck(L_67);
			Dictionary_2_Remove_m623660598(L_67, L_68, /*hidden argument*/Dictionary_2_Remove_m623660598_MethodInfo_var);
		}

IL_020d:
		{
			// foreach(int k in toRemove)
			bool L_69 = Enumerator_MoveNext_m4282865897((&V_17), /*hidden argument*/Enumerator_MoveNext_m4282865897_MethodInfo_var);
			if (L_69)
			{
				goto IL_01e6;
			}
		}

IL_0219:
		{
			IL2CPP_LEAVE(0x22C, FINALLY_021e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_021e;
	}

FINALLY_021e:
	{ // begin finally (depth: 1)
		// foreach(int k in toRemove)
		Enumerator_Dispose_m1274756239((&V_17), /*hidden argument*/Enumerator_Dispose_m1274756239_MethodInfo_var);
		IL2CPP_END_FINALLY(542)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(542)
	{
		IL2CPP_JUMP_TBL(0x22C, IL_022c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_022c:
	{
		// float bpm = _beats[beatIndices[beatIndices.Count-1]].bpm;
		Dictionary_2_t1703509207 * L_70 = __this->get__beats_16();
		// float bpm = _beats[beatIndices[beatIndices.Count-1]].bpm;
		ReadOnlyCollection_1_t2257663140 * L_71 = BeatTracker_get_beatIndices_m1207551596(__this, /*hidden argument*/NULL);
		// float bpm = _beats[beatIndices[beatIndices.Count-1]].bpm;
		ReadOnlyCollection_1_t2257663140 * L_72 = BeatTracker_get_beatIndices_m1207551596(__this, /*hidden argument*/NULL);
		// float bpm = _beats[beatIndices[beatIndices.Count-1]].bpm;
		NullCheck(L_72);
		int32_t L_73 = ReadOnlyCollection_1_get_Count_m1734424457(L_72, /*hidden argument*/ReadOnlyCollection_1_get_Count_m1734424457_MethodInfo_var);
		// float bpm = _beats[beatIndices[beatIndices.Count-1]].bpm;
		NullCheck(L_71);
		int32_t L_74 = ReadOnlyCollection_1_get_Item_m925539044(L_71, ((int32_t)((int32_t)L_73-(int32_t)1)), /*hidden argument*/ReadOnlyCollection_1_get_Item_m925539044_MethodInfo_var);
		// float bpm = _beats[beatIndices[beatIndices.Count-1]].bpm;
		NullCheck(L_70);
		Beat_t2695683572 * L_75 = Dictionary_2_get_Item_m2258219861(L_70, L_74, /*hidden argument*/Dictionary_2_get_Item_m2258219861_MethodInfo_var);
		NullCheck(L_75);
		float L_76 = L_75->get_bpm_1();
		V_18 = L_76;
		// while(firstIndex > beatLength)
		goto IL_0293;
	}

IL_025b:
	{
		// firstIndex -= beatLength;
		float L_77 = V_11;
		float L_78 = V_8;
		V_11 = ((float)((float)L_77-(float)L_78));
		// int index = Mathf.RoundToInt(firstIndex);
		float L_79 = V_11;
		// int index = Mathf.RoundToInt(firstIndex);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_80 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		V_19 = L_80;
		// _beatIndices.Insert(0, index);
		List_1_t1440998580 * L_81 = __this->get__beatIndices_15();
		int32_t L_82 = V_19;
		// _beatIndices.Insert(0, index);
		NullCheck(L_81);
		List_1_Insert_m2673244951(L_81, 0, L_82, /*hidden argument*/List_1_Insert_m2673244951_MethodInfo_var);
		// _beats.Add(index, new Beat(beatLength, bpm, index));
		Dictionary_2_t1703509207 * L_83 = __this->get__beats_16();
		int32_t L_84 = V_19;
		float L_85 = V_8;
		float L_86 = V_18;
		int32_t L_87 = V_19;
		// _beats.Add(index, new Beat(beatLength, bpm, index));
		Beat_t2695683572 * L_88 = (Beat_t2695683572 *)il2cpp_codegen_object_new(Beat_t2695683572_il2cpp_TypeInfo_var);
		Beat__ctor_m206855294(L_88, L_85, L_86, L_87, /*hidden argument*/NULL);
		// _beats.Add(index, new Beat(beatLength, bpm, index));
		NullCheck(L_83);
		Dictionary_2_Add_m3808454219(L_83, L_84, L_88, /*hidden argument*/Dictionary_2_Add_m3808454219_MethodInfo_var);
	}

IL_0293:
	{
		// while(firstIndex > beatLength)
		float L_89 = V_11;
		float L_90 = V_8;
		if ((((float)L_89) > ((float)L_90)))
		{
			goto IL_025b;
		}
	}

IL_029c:
	{
		// }
		return;
	}
}
// System.Void BeatTracker::FillEnd()
extern "C"  void BeatTracker_FillEnd_m1691323021 (BeatTracker_t2801099156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_FillEnd_m1691323021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t2921398135 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Dictionary_2_t2921398135 * V_4 = NULL;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	KeyValuePair_2_t678743357  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Enumerator_t4241422837  V_9;
	memset(&V_9, 0, sizeof(V_9));
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// if (_beatIndices.Count < 40)
		List_1_t1440998580 * L_0 = __this->get__beatIndices_15();
		// if (_beatIndices.Count < 40)
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m852068579(L_0, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)40))))
		{
			goto IL_0018;
		}
	}
	{
		// return;
		goto IL_0156;
	}

IL_0018:
	{
		// Dictionary<float, int> lengths = new Dictionary<float, int>();
		Dictionary_2_t2921398135 * L_2 = (Dictionary_2_t2921398135 *)il2cpp_codegen_object_new(Dictionary_2_t2921398135_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1683620704(L_2, /*hidden argument*/Dictionary_2__ctor_m1683620704_MethodInfo_var);
		V_0 = L_2;
		// for (int i = _beatIndices.Count - 30; i < _beatIndices.Count; i++)
		List_1_t1440998580 * L_3 = __this->get__beatIndices_15();
		// for (int i = _beatIndices.Count - 30; i < _beatIndices.Count; i++)
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m852068579(L_3, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		V_1 = ((int32_t)((int32_t)L_4-(int32_t)((int32_t)30)));
		goto IL_0088;
	}

IL_0032:
	{
		// int ii = _beatIndices[i];
		List_1_t1440998580 * L_5 = __this->get__beatIndices_15();
		int32_t L_6 = V_1;
		// int ii = _beatIndices[i];
		NullCheck(L_5);
		int32_t L_7 = List_1_get_Item_m1921196075(L_5, L_6, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_2 = L_7;
		// float length = _beats[ii].length;
		Dictionary_2_t1703509207 * L_8 = __this->get__beats_16();
		int32_t L_9 = V_2;
		// float length = _beats[ii].length;
		NullCheck(L_8);
		Beat_t2695683572 * L_10 = Dictionary_2_get_Item_m2258219861(L_8, L_9, /*hidden argument*/Dictionary_2_get_Item_m2258219861_MethodInfo_var);
		NullCheck(L_10);
		float L_11 = L_10->get_length_0();
		V_3 = L_11;
		// if (lengths.ContainsKey(length))
		Dictionary_2_t2921398135 * L_12 = V_0;
		float L_13 = V_3;
		// if (lengths.ContainsKey(length))
		NullCheck(L_12);
		bool L_14 = Dictionary_2_ContainsKey_m4174387577(L_12, L_13, /*hidden argument*/Dictionary_2_ContainsKey_m4174387577_MethodInfo_var);
		if (!L_14)
		{
			goto IL_007b;
		}
	}
	{
		// lengths[length]++;
		Dictionary_2_t2921398135 * L_15 = V_0;
		Dictionary_2_t2921398135 * L_16 = L_15;
		V_4 = L_16;
		float L_17 = V_3;
		float L_18 = L_17;
		V_5 = L_18;
		Dictionary_2_t2921398135 * L_19 = V_4;
		float L_20 = V_5;
		// lengths[length]++;
		NullCheck(L_19);
		int32_t L_21 = Dictionary_2_get_Item_m1122070824(L_19, L_20, /*hidden argument*/Dictionary_2_get_Item_m1122070824_MethodInfo_var);
		// lengths[length]++;
		NullCheck(L_16);
		Dictionary_2_set_Item_m2607921061(L_16, L_18, ((int32_t)((int32_t)L_21+(int32_t)1)), /*hidden argument*/Dictionary_2_set_Item_m2607921061_MethodInfo_var);
		goto IL_0083;
	}

IL_007b:
	{
		// lengths.Add(length, 1);
		Dictionary_2_t2921398135 * L_22 = V_0;
		float L_23 = V_3;
		// lengths.Add(length, 1);
		NullCheck(L_22);
		Dictionary_2_Add_m1780001816(L_22, L_23, 1, /*hidden argument*/Dictionary_2_Add_m1780001816_MethodInfo_var);
	}

IL_0083:
	{
		// for (int i = _beatIndices.Count - 30; i < _beatIndices.Count; i++)
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0088:
	{
		// for (int i = _beatIndices.Count - 30; i < _beatIndices.Count; i++)
		int32_t L_25 = V_1;
		List_1_t1440998580 * L_26 = __this->get__beatIndices_15();
		// for (int i = _beatIndices.Count - 30; i < _beatIndices.Count; i++)
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m852068579(L_26, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_0032;
		}
	}
	{
		// int beatCount = 0;
		V_6 = 0;
		// float beatLength = 0;
		V_7 = (0.0f);
		// foreach (KeyValuePair<float, int> l in lengths)
		Dictionary_2_t2921398135 * L_28 = V_0;
		// foreach (KeyValuePair<float, int> l in lengths)
		NullCheck(L_28);
		Enumerator_t4241422837  L_29 = Dictionary_2_GetEnumerator_m4065924530(L_28, /*hidden argument*/Dictionary_2_GetEnumerator_m4065924530_MethodInfo_var);
		V_9 = L_29;
	}

IL_00ac:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00de;
		}

IL_00b1:
		{
			// foreach (KeyValuePair<float, int> l in lengths)
			// foreach (KeyValuePair<float, int> l in lengths)
			KeyValuePair_2_t678743357  L_30 = Enumerator_get_Current_m1983754276((&V_9), /*hidden argument*/Enumerator_get_Current_m1983754276_MethodInfo_var);
			V_8 = L_30;
			// if (l.Value > beatCount)
			// if (l.Value > beatCount)
			int32_t L_31 = KeyValuePair_2_get_Value_m1733170775((&V_8), /*hidden argument*/KeyValuePair_2_get_Value_m1733170775_MethodInfo_var);
			int32_t L_32 = V_6;
			if ((((int32_t)L_31) <= ((int32_t)L_32)))
			{
				goto IL_00dd;
			}
		}

IL_00c9:
		{
			// beatCount = l.Value;
			// beatCount = l.Value;
			int32_t L_33 = KeyValuePair_2_get_Value_m1733170775((&V_8), /*hidden argument*/KeyValuePair_2_get_Value_m1733170775_MethodInfo_var);
			V_6 = L_33;
			// beatLength = l.Key;
			// beatLength = l.Key;
			float L_34 = KeyValuePair_2_get_Key_m2838785900((&V_8), /*hidden argument*/KeyValuePair_2_get_Key_m2838785900_MethodInfo_var);
			V_7 = L_34;
		}

IL_00dd:
		{
		}

IL_00de:
		{
			// foreach (KeyValuePair<float, int> l in lengths)
			bool L_35 = Enumerator_MoveNext_m3884776761((&V_9), /*hidden argument*/Enumerator_MoveNext_m3884776761_MethodInfo_var);
			if (L_35)
			{
				goto IL_00b1;
			}
		}

IL_00ea:
		{
			IL2CPP_LEAVE(0xFD, FINALLY_00ef);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ef;
	}

FINALLY_00ef:
	{ // begin finally (depth: 1)
		// foreach (KeyValuePair<float, int> l in lengths)
		Enumerator_Dispose_m797750959((&V_9), /*hidden argument*/Enumerator_Dispose_m797750959_MethodInfo_var);
		IL2CPP_END_FINALLY(239)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(239)
	{
		IL2CPP_JUMP_TBL(0xFD, IL_00fd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00fd:
	{
		// int lastBeat = _beatIndices[_beatIndices.Count - 1];
		List_1_t1440998580 * L_36 = __this->get__beatIndices_15();
		List_1_t1440998580 * L_37 = __this->get__beatIndices_15();
		// int lastBeat = _beatIndices[_beatIndices.Count - 1];
		NullCheck(L_37);
		int32_t L_38 = List_1_get_Count_m852068579(L_37, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		// int lastBeat = _beatIndices[_beatIndices.Count - 1];
		NullCheck(L_36);
		int32_t L_39 = List_1_get_Item_m1921196075(L_36, ((int32_t)((int32_t)L_38-(int32_t)1)), /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_10 = L_39;
		// int numFill = (int)((index - lastBeat) / beatLength);
		int32_t L_40 = __this->get_index_14();
		int32_t L_41 = V_10;
		float L_42 = V_7;
		V_11 = (((int32_t)((int32_t)((float)((float)(((float)((float)((int32_t)((int32_t)L_40-(int32_t)L_41)))))/(float)L_42)))));
		// for (int i = 1; i < numFill; i++)
		V_12 = 1;
		goto IL_014d;
	}

IL_012f:
	{
		// AddBeat(lastBeat + Mathf.RoundToInt(i* beatLength), beatLength);
		int32_t L_43 = V_10;
		int32_t L_44 = V_12;
		float L_45 = V_7;
		// AddBeat(lastBeat + Mathf.RoundToInt(i* beatLength), beatLength);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_46 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_44)))*(float)L_45)), /*hidden argument*/NULL);
		float L_47 = V_7;
		// AddBeat(lastBeat + Mathf.RoundToInt(i* beatLength), beatLength);
		BeatTracker_AddBeat_m3465552806(__this, ((int32_t)((int32_t)L_43+(int32_t)L_46)), L_47, /*hidden argument*/NULL);
		// for (int i = 1; i < numFill; i++)
		int32_t L_48 = V_12;
		V_12 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_014d:
	{
		// for (int i = 1; i < numFill; i++)
		int32_t L_49 = V_12;
		int32_t L_50 = V_11;
		if ((((int32_t)L_49) < ((int32_t)L_50)))
		{
			goto IL_012f;
		}
	}

IL_0156:
	{
		// }
		return;
	}
}
// System.Void BeatTracker::AddBeat(System.Int32,System.Single)
extern "C"  void BeatTracker_AddBeat_m3465552806 (BeatTracker_t2801099156 * __this, int32_t ___index0, float ___beatLength1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_AddBeat_m3465552806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Beat_t2695683572 * V_3 = NULL;
	{
		// _beatIndices.Add(index);
		List_1_t1440998580 * L_0 = __this->get__beatIndices_15();
		int32_t L_1 = ___index0;
		// _beatIndices.Add(index);
		NullCheck(L_0);
		List_1_Add_m688682013(L_0, L_1, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
		// float currentBPM = 0;
		V_0 = (0.0f);
		// int previousBeats = 0;
		V_1 = 0;
		// for (int i = Mathf.Max(0, _beatIndices.Count - 20); i < _beatIndices.Count-1; i++)
		List_1_t1440998580 * L_2 = __this->get__beatIndices_15();
		// for (int i = Mathf.Max(0, _beatIndices.Count - 20); i < _beatIndices.Count-1; i++)
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m852068579(L_2, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		// for (int i = Mathf.Max(0, _beatIndices.Count - 20); i < _beatIndices.Count-1; i++)
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Max_m1875893177(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_3-(int32_t)((int32_t)20))), /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0058;
	}

IL_002f:
	{
		// currentBPM += _beatIndices[i + 1] - _beatIndices[i];
		float L_5 = V_0;
		List_1_t1440998580 * L_6 = __this->get__beatIndices_15();
		int32_t L_7 = V_2;
		// currentBPM += _beatIndices[i + 1] - _beatIndices[i];
		NullCheck(L_6);
		int32_t L_8 = List_1_get_Item_m1921196075(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		List_1_t1440998580 * L_9 = __this->get__beatIndices_15();
		int32_t L_10 = V_2;
		// currentBPM += _beatIndices[i + 1] - _beatIndices[i];
		NullCheck(L_9);
		int32_t L_11 = List_1_get_Item_m1921196075(L_9, L_10, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_0 = ((float)((float)L_5+(float)(((float)((float)((int32_t)((int32_t)L_8-(int32_t)L_11)))))));
		// previousBeats++;
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
		// for (int i = Mathf.Max(0, _beatIndices.Count - 20); i < _beatIndices.Count-1; i++)
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0058:
	{
		// for (int i = Mathf.Max(0, _beatIndices.Count - 20); i < _beatIndices.Count-1; i++)
		int32_t L_14 = V_2;
		List_1_t1440998580 * L_15 = __this->get__beatIndices_15();
		// for (int i = Mathf.Max(0, _beatIndices.Count - 20); i < _beatIndices.Count-1; i++)
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m852068579(L_15, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_14) < ((int32_t)((int32_t)((int32_t)L_16-(int32_t)1)))))
		{
			goto IL_002f;
		}
	}
	{
		// if (previousBeats == 0)
		int32_t L_17 = V_1;
		if (L_17)
		{
			goto IL_0078;
		}
	}
	{
		// currentBPM = beatLength;
		float L_18 = ___beatLength1;
		V_0 = L_18;
		goto IL_007d;
	}

IL_0078:
	{
		// currentBPM /= previousBeats;
		float L_19 = V_0;
		int32_t L_20 = V_1;
		V_0 = ((float)((float)L_19/(float)(((float)((float)L_20)))));
	}

IL_007d:
	{
		// currentBPM = (60f / (frameLength * currentBPM));
		float L_21 = __this->get_frameLength_12();
		float L_22 = V_0;
		V_0 = ((float)((float)(60.0f)/(float)((float)((float)L_21*(float)L_22))));
		// currentBPM = Mathf.Round(currentBPM);
		float L_23 = V_0;
		// currentBPM = Mathf.Round(currentBPM);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_24 = bankers_roundf(L_23);
		V_0 = L_24;
		// currentBPM = Mathf.Clamp(currentBPM, 0, 199);
		float L_25 = V_0;
		// currentBPM = Mathf.Clamp(currentBPM, 0, 199);
		float L_26 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_25, (0.0f), (199.0f), /*hidden argument*/NULL);
		V_0 = L_26;
		// Beat b = new Beat(beatLength, currentBPM, index);
		float L_27 = ___beatLength1;
		float L_28 = V_0;
		int32_t L_29 = ___index0;
		// Beat b = new Beat(beatLength, currentBPM, index);
		Beat_t2695683572 * L_30 = (Beat_t2695683572 *)il2cpp_codegen_object_new(Beat_t2695683572_il2cpp_TypeInfo_var);
		Beat__ctor_m206855294(L_30, L_27, L_28, L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		// _beats.Add(index, b);
		Dictionary_2_t1703509207 * L_31 = __this->get__beats_16();
		int32_t L_32 = ___index0;
		Beat_t2695683572 * L_33 = V_3;
		// _beats.Add(index, b);
		NullCheck(L_31);
		Dictionary_2_Add_m3808454219(L_31, L_32, L_33, /*hidden argument*/Dictionary_2_Add_m3808454219_MethodInfo_var);
		// }
		return;
	}
}
// System.Void BeatTracker::TrackBeat(System.Single)
extern "C"  void BeatTracker_TrackBeat_m2149388341 (BeatTracker_t2801099156 * __this, float ___sample0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_TrackBeat_m2149388341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// signalBuffer[index % signalBuffer.Length] = sample;
		SingleU5BU5D_t577127397* L_0 = __this->get_signalBuffer_0();
		int32_t L_1 = __this->get_index_14();
		SingleU5BU5D_t577127397* L_2 = __this->get_signalBuffer_0();
		NullCheck(L_2);
		float L_3 = ___sample0;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_1%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))), (float)L_3);
		// if (index > currentSignal.Length && index % beatTrackInterval == 0)
		int32_t L_4 = __this->get_index_14();
		SingleU5BU5D_t577127397* L_5 = __this->get_currentSignal_1();
		NullCheck(L_5);
		if ((((int32_t)L_4) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))))
		{
			goto IL_009b;
		}
	}
	{
		int32_t L_6 = __this->get_index_14();
		int32_t L_7 = __this->get_beatTrackInterval_13();
		if (((int32_t)((int32_t)L_6%(int32_t)L_7)))
		{
			goto IL_009b;
		}
	}
	{
		// for (int i = 0; i < currentSignal.Length; i++)
		V_0 = 0;
		goto IL_006d;
	}

IL_0045:
	{
		// currentSignal[i] = signalBuffer[(i + index + 1) % signalBuffer.Length];
		SingleU5BU5D_t577127397* L_8 = __this->get_currentSignal_1();
		int32_t L_9 = V_0;
		SingleU5BU5D_t577127397* L_10 = __this->get_signalBuffer_0();
		int32_t L_11 = V_0;
		int32_t L_12 = __this->get_index_14();
		SingleU5BU5D_t577127397* L_13 = __this->get_signalBuffer_0();
		NullCheck(L_13);
		NullCheck(L_10);
		int32_t L_14 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11+(int32_t)L_12))+(int32_t)1))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))));
		float L_15 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (float)L_15);
		// for (int i = 0; i < currentSignal.Length; i++)
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_006d:
	{
		// for (int i = 0; i < currentSignal.Length; i++)
		int32_t L_17 = V_0;
		SingleU5BU5D_t577127397* L_18 = __this->get_currentSignal_1();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0045;
		}
	}
	{
		// syncOld = sync;
		float L_19 = __this->get_sync_11();
		__this->set_syncOld_10(L_19);
		// ThreadPool.QueueUserWorkItem(o => FindBeat(), null);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)BeatTracker_U3CTrackBeatU3Em__0_m1598643413_MethodInfo_var);
		WaitCallback_t2798937288 * L_21 = (WaitCallback_t2798937288 *)il2cpp_codegen_object_new(WaitCallback_t2798937288_il2cpp_TypeInfo_var);
		WaitCallback__ctor_m1513386157(L_21, __this, L_20, /*hidden argument*/NULL);
		// ThreadPool.QueueUserWorkItem(o => FindBeat(), null);
		ThreadPool_QueueUserWorkItem_m2209660682(NULL /*static, unused*/, L_21, NULL, /*hidden argument*/NULL);
	}

IL_009b:
	{
		// if (currentBeatLength > 5)
		int32_t L_22 = __this->get_currentBeatLength_9();
		if ((((int32_t)L_22) <= ((int32_t)5)))
		{
			goto IL_0114;
		}
	}
	{
		// if (currentBeatLength > 20)
		int32_t L_23 = __this->get_currentBeatLength_9();
		if ((((int32_t)L_23) <= ((int32_t)((int32_t)20))))
		{
			goto IL_0101;
		}
	}
	{
		// if (sync > currentBeatLength)
		float L_24 = __this->get_sync_11();
		int32_t L_25 = __this->get_currentBeatLength_9();
		if ((!(((float)L_24) > ((float)(((float)((float)L_25)))))))
		{
			goto IL_0100;
		}
	}
	{
		// sync -= currentBeatLength;
		float L_26 = __this->get_sync_11();
		int32_t L_27 = __this->get_currentBeatLength_9();
		__this->set_sync_11(((float)((float)L_26-(float)(((float)((float)L_27))))));
		// AddBeat(index - currentSignal.Length, currentBeatLength / 4f);
		int32_t L_28 = __this->get_index_14();
		SingleU5BU5D_t577127397* L_29 = __this->get_currentSignal_1();
		NullCheck(L_29);
		int32_t L_30 = __this->get_currentBeatLength_9();
		// AddBeat(index - currentSignal.Length, currentBeatLength / 4f);
		BeatTracker_AddBeat_m3465552806(__this, ((int32_t)((int32_t)L_28-(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))), ((float)((float)(((float)((float)L_30)))/(float)(4.0f))), /*hidden argument*/NULL);
	}

IL_0100:
	{
	}

IL_0101:
	{
		// sync += 4;
		float L_31 = __this->get_sync_11();
		__this->set_sync_11(((float)((float)L_31+(float)(4.0f))));
	}

IL_0114:
	{
		// index++;
		int32_t L_32 = __this->get_index_14();
		__this->set_index_14(((int32_t)((int32_t)L_32+(int32_t)1)));
		// }
		return;
	}
}
// System.Void BeatTracker::FindBeat()
extern "C"  void BeatTracker_FindBeat_m212206812 (BeatTracker_t2801099156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_FindBeat_m212206812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	float V_20 = 0.0f;
	{
		// Util.Smooth(currentSignal, 4);
		SingleU5BU5D_t577127397* L_0 = __this->get_currentSignal_1();
		// Util.Smooth(currentSignal, 4);
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		Util_Smooth_m3641735451(NULL /*static, unused*/, L_0, 4, /*hidden argument*/NULL);
		// Util.Smooth(currentSignal, 4);
		SingleU5BU5D_t577127397* L_1 = __this->get_currentSignal_1();
		// Util.Smooth(currentSignal, 4);
		Util_Smooth_m3641735451(NULL /*static, unused*/, L_1, 4, /*hidden argument*/NULL);
		// Array.Clear(currentSignal, 0, 5);
		SingleU5BU5D_t577127397* L_2 = __this->get_currentSignal_1();
		// Array.Clear(currentSignal, 0, 5);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, 0, 5, /*hidden argument*/NULL);
		// Array.Clear(currentSignal, currentSignal.Length - 5, 5);
		SingleU5BU5D_t577127397* L_3 = __this->get_currentSignal_1();
		SingleU5BU5D_t577127397* L_4 = __this->get_currentSignal_1();
		NullCheck(L_4);
		// Array.Clear(currentSignal, currentSignal.Length - 5, 5);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))-(int32_t)5)), 5, /*hidden argument*/NULL);
		// Util.UpsampleSingnal(currentSignal, upsampledSignal, 4);
		SingleU5BU5D_t577127397* L_5 = __this->get_currentSignal_1();
		SingleU5BU5D_t577127397* L_6 = __this->get_upsampledSignal_2();
		// Util.UpsampleSingnal(currentSignal, upsampledSignal, 4);
		Util_UpsampleSingnal_m3064131617(NULL /*static, unused*/, L_5, L_6, 4, /*hidden argument*/NULL);
		// for (int i = 0; i < repetitionScore.Length; i++)
		V_0 = 0;
		goto IL_006e;
	}

IL_0055:
	{
		// repetitionScore[i] = RepetitionScore(upsampledSignal, i);
		SingleU5BU5D_t577127397* L_7 = __this->get_repetitionScore_3();
		int32_t L_8 = V_0;
		SingleU5BU5D_t577127397* L_9 = __this->get_upsampledSignal_2();
		int32_t L_10 = V_0;
		// repetitionScore[i] = RepetitionScore(upsampledSignal, i);
		float L_11 = BeatTracker_RepetitionScore_m1324094558(__this, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (float)L_11);
		// for (int i = 0; i < repetitionScore.Length; i++)
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_006e:
	{
		// for (int i = 0; i < repetitionScore.Length; i++)
		int32_t L_13 = V_0;
		SingleU5BU5D_t577127397* L_14 = __this->get_repetitionScore_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0055;
		}
	}
	{
		// bestRepetition = 40;
		__this->set_bestRepetition_8(((int32_t)40));
		// for (int i = 40; i < repetitionScore.Length - 5; i++)
		V_1 = ((int32_t)40);
		goto IL_00b3;
	}

IL_008c:
	{
		// if (repetitionScore[i] > repetitionScore[bestRepetition])
		SingleU5BU5D_t577127397* L_15 = __this->get_repetitionScore_3();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		float L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		SingleU5BU5D_t577127397* L_19 = __this->get_repetitionScore_3();
		int32_t L_20 = __this->get_bestRepetition_8();
		NullCheck(L_19);
		int32_t L_21 = L_20;
		float L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		if ((!(((float)L_18) > ((float)L_22))))
		{
			goto IL_00ae;
		}
	}
	{
		// bestRepetition = i;
		int32_t L_23 = V_1;
		__this->set_bestRepetition_8(L_23);
	}

IL_00ae:
	{
		// for (int i = 40; i < repetitionScore.Length - 5; i++)
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00b3:
	{
		// for (int i = 40; i < repetitionScore.Length - 5; i++)
		int32_t L_25 = V_1;
		SingleU5BU5D_t577127397* L_26 = __this->get_repetitionScore_3();
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))-(int32_t)5)))))
		{
			goto IL_008c;
		}
	}
	{
		// float fr = repetitionScore[bestRepetition];
		SingleU5BU5D_t577127397* L_27 = __this->get_repetitionScore_3();
		int32_t L_28 = __this->get_bestRepetition_8();
		NullCheck(L_27);
		int32_t L_29 = L_28;
		float L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		V_2 = L_30;
		// for (int i = 0; i < repetitionScore.Length; i++)
		V_3 = 0;
		goto IL_00f0;
	}

IL_00d8:
	{
		// repetitionScore[i] = repetitionScore[i] / fr;
		SingleU5BU5D_t577127397* L_31 = __this->get_repetitionScore_3();
		int32_t L_32 = V_3;
		SingleU5BU5D_t577127397* L_33 = __this->get_repetitionScore_3();
		int32_t L_34 = V_3;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		float L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		float L_37 = V_2;
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (float)((float)((float)L_36/(float)L_37)));
		// for (int i = 0; i < repetitionScore.Length; i++)
		int32_t L_38 = V_3;
		V_3 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00f0:
	{
		// for (int i = 0; i < repetitionScore.Length; i++)
		int32_t L_39 = V_3;
		SingleU5BU5D_t577127397* L_40 = __this->get_repetitionScore_3();
		NullCheck(L_40);
		if ((((int32_t)L_39) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_40)->max_length)))))))
		{
			goto IL_00d8;
		}
	}
	{
		// for (int i = 45; i < gapScore.Length; i++)
		V_4 = ((int32_t)45);
		goto IL_0126;
	}

IL_0107:
	{
		// gapScore[i] = GapScore(repetitionScore, i);
		SingleU5BU5D_t577127397* L_41 = __this->get_gapScore_4();
		int32_t L_42 = V_4;
		SingleU5BU5D_t577127397* L_43 = __this->get_repetitionScore_3();
		int32_t L_44 = V_4;
		// gapScore[i] = GapScore(repetitionScore, i);
		float L_45 = BeatTracker_GapScore_m397149463(__this, L_43, L_44, /*hidden argument*/NULL);
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(L_42), (float)L_45);
		// for (int i = 45; i < gapScore.Length; i++)
		int32_t L_46 = V_4;
		V_4 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_0126:
	{
		// for (int i = 45; i < gapScore.Length; i++)
		int32_t L_47 = V_4;
		SingleU5BU5D_t577127397* L_48 = __this->get_gapScore_4();
		NullCheck(L_48);
		if ((((int32_t)L_47) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_48)->max_length)))))))
		{
			goto IL_0107;
		}
	}
	{
		// int bestGapScore = 45;
		V_5 = ((int32_t)45);
		// for (int i = 45; i < gapScore.Length - 1; i++)
		V_6 = ((int32_t)45);
		goto IL_0165;
	}

IL_0142:
	{
		// if (gapScore[i] > gapScore[bestGapScore])
		SingleU5BU5D_t577127397* L_49 = __this->get_gapScore_4();
		int32_t L_50 = V_6;
		NullCheck(L_49);
		int32_t L_51 = L_50;
		float L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		SingleU5BU5D_t577127397* L_53 = __this->get_gapScore_4();
		int32_t L_54 = V_5;
		NullCheck(L_53);
		int32_t L_55 = L_54;
		float L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		if ((!(((float)L_52) > ((float)L_56))))
		{
			goto IL_015e;
		}
	}
	{
		// bestGapScore = i;
		int32_t L_57 = V_6;
		V_5 = L_57;
	}

IL_015e:
	{
		// for (int i = 45; i < gapScore.Length - 1; i++)
		int32_t L_58 = V_6;
		V_6 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_0165:
	{
		// for (int i = 45; i < gapScore.Length - 1; i++)
		int32_t L_59 = V_6;
		SingleU5BU5D_t577127397* L_60 = __this->get_gapScore_4();
		NullCheck(L_60);
		if ((((int32_t)L_59) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_60)->max_length))))-(int32_t)1)))))
		{
			goto IL_0142;
		}
	}
	{
		// peaks.Clear();
		List_1_t1440998580 * L_61 = __this->get_peaks_6();
		// peaks.Clear();
		NullCheck(L_61);
		List_1_Clear_m3644677550(L_61, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		// for (int i = 0; i < repetitionScore.Length - 5; i++)
		V_7 = 0;
		goto IL_028f;
	}

IL_0189:
	{
		// if (i + 1 < repetitionScore.Length && i - 1 >= 0)
		int32_t L_62 = V_7;
		SingleU5BU5D_t577127397* L_63 = __this->get_repetitionScore_3();
		NullCheck(L_63);
		if ((((int32_t)((int32_t)((int32_t)L_62+(int32_t)1))) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length)))))))
		{
			goto IL_0288;
		}
	}
	{
		int32_t L_64 = V_7;
		if ((((int32_t)((int32_t)((int32_t)L_64-(int32_t)1))) < ((int32_t)0)))
		{
			goto IL_0288;
		}
	}
	{
		// int t = i;
		int32_t L_65 = V_7;
		V_8 = L_65;
		// while (t < 45)
		goto IL_01b5;
	}

IL_01af:
	{
		// t *= 2;
		int32_t L_66 = V_8;
		V_8 = ((int32_t)((int32_t)L_66*(int32_t)2));
	}

IL_01b5:
	{
		// while (t < 45)
		int32_t L_67 = V_8;
		if ((((int32_t)L_67) < ((int32_t)((int32_t)45))))
		{
			goto IL_01af;
		}
	}
	{
		// while (t > gapScore.Length - 1)
		goto IL_01c9;
	}

IL_01c3:
	{
		// t /= 2;
		int32_t L_68 = V_8;
		V_8 = ((int32_t)((int32_t)L_68/(int32_t)2));
	}

IL_01c9:
	{
		// while (t > gapScore.Length - 1)
		int32_t L_69 = V_8;
		SingleU5BU5D_t577127397* L_70 = __this->get_gapScore_4();
		NullCheck(L_70);
		if ((((int32_t)L_69) > ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_70)->max_length))))-(int32_t)1)))))
		{
			goto IL_01c3;
		}
	}
	{
		// if (gapScore[t / 2] > gapScore[t])
		SingleU5BU5D_t577127397* L_71 = __this->get_gapScore_4();
		int32_t L_72 = V_8;
		NullCheck(L_71);
		int32_t L_73 = ((int32_t)((int32_t)L_72/(int32_t)2));
		float L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		SingleU5BU5D_t577127397* L_75 = __this->get_gapScore_4();
		int32_t L_76 = V_8;
		NullCheck(L_75);
		int32_t L_77 = L_76;
		float L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		if ((!(((float)L_74) > ((float)L_78))))
		{
			goto IL_01f9;
		}
	}
	{
		// t /= 2;
		int32_t L_79 = V_8;
		V_8 = ((int32_t)((int32_t)L_79/(int32_t)2));
	}

IL_01f9:
	{
		// float w = repetitionScore[i] * Mathf.Max(0, gapScore[t]);
		SingleU5BU5D_t577127397* L_80 = __this->get_repetitionScore_3();
		int32_t L_81 = V_7;
		NullCheck(L_80);
		int32_t L_82 = L_81;
		float L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		SingleU5BU5D_t577127397* L_84 = __this->get_gapScore_4();
		int32_t L_85 = V_8;
		NullCheck(L_84);
		int32_t L_86 = L_85;
		float L_87 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		// float w = repetitionScore[i] * Mathf.Max(0, gapScore[t]);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_88 = Mathf_Max_m2564622569(NULL /*static, unused*/, (0.0f), L_87, /*hidden argument*/NULL);
		V_9 = ((float)((float)L_83*(float)L_88));
		// float threshold = .425f;
		V_10 = (0.425f);
		// if (beatHistogram[currentBeatLength] < 7)
		SingleU5BU5D_t577127397* L_89 = __this->get_beatHistogram_7();
		int32_t L_90 = __this->get_currentBeatLength_9();
		NullCheck(L_89);
		int32_t L_91 = L_90;
		float L_92 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		if ((!(((float)L_92) < ((float)(7.0f)))))
		{
			goto IL_023d;
		}
	}
	{
		// threshold = .25f;
		V_10 = (0.25f);
	}

IL_023d:
	{
		// if (repetitionScore[i] > repetitionScore[i + 1] && repetitionScore[i] > repetitionScore[i - 1] && w > threshold) //.9f
		SingleU5BU5D_t577127397* L_93 = __this->get_repetitionScore_3();
		int32_t L_94 = V_7;
		NullCheck(L_93);
		int32_t L_95 = L_94;
		float L_96 = (L_93)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		SingleU5BU5D_t577127397* L_97 = __this->get_repetitionScore_3();
		int32_t L_98 = V_7;
		NullCheck(L_97);
		int32_t L_99 = ((int32_t)((int32_t)L_98+(int32_t)1));
		float L_100 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		if ((!(((float)L_96) > ((float)L_100))))
		{
			goto IL_0287;
		}
	}
	{
		SingleU5BU5D_t577127397* L_101 = __this->get_repetitionScore_3();
		int32_t L_102 = V_7;
		NullCheck(L_101);
		int32_t L_103 = L_102;
		float L_104 = (L_101)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		SingleU5BU5D_t577127397* L_105 = __this->get_repetitionScore_3();
		int32_t L_106 = V_7;
		NullCheck(L_105);
		int32_t L_107 = ((int32_t)((int32_t)L_106-(int32_t)1));
		float L_108 = (L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_107));
		if ((!(((float)L_104) > ((float)L_108))))
		{
			goto IL_0287;
		}
	}
	{
		float L_109 = V_9;
		float L_110 = V_10;
		if ((!(((float)L_109) > ((float)L_110))))
		{
			goto IL_0287;
		}
	}
	{
		// peaks.Add(i);
		List_1_t1440998580 * L_111 = __this->get_peaks_6();
		int32_t L_112 = V_7;
		// peaks.Add(i);
		NullCheck(L_111);
		List_1_Add_m688682013(L_111, L_112, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
	}

IL_0287:
	{
	}

IL_0288:
	{
		// for (int i = 0; i < repetitionScore.Length - 5; i++)
		int32_t L_113 = V_7;
		V_7 = ((int32_t)((int32_t)L_113+(int32_t)1));
	}

IL_028f:
	{
		// for (int i = 0; i < repetitionScore.Length - 5; i++)
		int32_t L_114 = V_7;
		SingleU5BU5D_t577127397* L_115 = __this->get_repetitionScore_3();
		NullCheck(L_115);
		if ((((int32_t)L_114) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_115)->max_length))))-(int32_t)5)))))
		{
			goto IL_0189;
		}
	}
	{
		// if(peaks.Count == 0)
		List_1_t1440998580 * L_116 = __this->get_peaks_6();
		// if(peaks.Count == 0)
		NullCheck(L_116);
		int32_t L_117 = List_1_get_Count_m852068579(L_116, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if (L_117)
		{
			goto IL_0320;
		}
	}
	{
		// if (repetitionScore[bestRepetition / 2] > .75f)
		SingleU5BU5D_t577127397* L_118 = __this->get_repetitionScore_3();
		int32_t L_119 = __this->get_bestRepetition_8();
		NullCheck(L_118);
		int32_t L_120 = ((int32_t)((int32_t)L_119/(int32_t)2));
		float L_121 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_120));
		if ((!(((float)L_121) > ((float)(0.75f)))))
		{
			goto IL_02e0;
		}
	}
	{
		// peaks.Add(bestRepetition);
		List_1_t1440998580 * L_122 = __this->get_peaks_6();
		int32_t L_123 = __this->get_bestRepetition_8();
		// peaks.Add(bestRepetition);
		NullCheck(L_122);
		List_1_Add_m688682013(L_122, L_123, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
		goto IL_031f;
	}

IL_02e0:
	{
		// else if (bestRepetition * 2 < repetitionScore.Length)
		int32_t L_124 = __this->get_bestRepetition_8();
		SingleU5BU5D_t577127397* L_125 = __this->get_repetitionScore_3();
		NullCheck(L_125);
		if ((((int32_t)((int32_t)((int32_t)L_124*(int32_t)2))) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_125)->max_length)))))))
		{
			goto IL_031f;
		}
	}
	{
		// if (repetitionScore[bestRepetition * 2] > .75f)
		SingleU5BU5D_t577127397* L_126 = __this->get_repetitionScore_3();
		int32_t L_127 = __this->get_bestRepetition_8();
		NullCheck(L_126);
		int32_t L_128 = ((int32_t)((int32_t)L_127*(int32_t)2));
		float L_129 = (L_126)->GetAt(static_cast<il2cpp_array_size_t>(L_128));
		if ((!(((float)L_129) > ((float)(0.75f)))))
		{
			goto IL_031f;
		}
	}
	{
		// peaks.Add(bestRepetition);
		List_1_t1440998580 * L_130 = __this->get_peaks_6();
		int32_t L_131 = __this->get_bestRepetition_8();
		// peaks.Add(bestRepetition);
		NullCheck(L_130);
		List_1_Add_m688682013(L_130, L_131, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
	}

IL_031f:
	{
	}

IL_0320:
	{
		// if (peaks.Count == 0 && currentBeatLength <= 5)
		List_1_t1440998580 * L_132 = __this->get_peaks_6();
		// if (peaks.Count == 0 && currentBeatLength <= 5)
		NullCheck(L_132);
		int32_t L_133 = List_1_get_Count_m852068579(L_132, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if (L_133)
		{
			goto IL_0377;
		}
	}
	{
		int32_t L_134 = __this->get_currentBeatLength_9();
		if ((((int32_t)L_134) > ((int32_t)5)))
		{
			goto IL_0377;
		}
	}
	{
		// int maxPeak = bestGapScore;
		int32_t L_135 = V_5;
		V_11 = L_135;
		// while (maxPeak < 90)
		goto IL_034c;
	}

IL_0346:
	{
		// maxPeak *= 2;
		int32_t L_136 = V_11;
		V_11 = ((int32_t)((int32_t)L_136*(int32_t)2));
	}

IL_034c:
	{
		// while (maxPeak < 90)
		int32_t L_137 = V_11;
		if ((((int32_t)L_137) < ((int32_t)((int32_t)90))))
		{
			goto IL_0346;
		}
	}
	{
		// while (maxPeak > 90)
		goto IL_0360;
	}

IL_035a:
	{
		// maxPeak /= 2;
		int32_t L_138 = V_11;
		V_11 = ((int32_t)((int32_t)L_138/(int32_t)2));
	}

IL_0360:
	{
		// while (maxPeak > 90)
		int32_t L_139 = V_11;
		if ((((int32_t)L_139) > ((int32_t)((int32_t)90))))
		{
			goto IL_035a;
		}
	}
	{
		// peaks.Add(maxPeak);
		List_1_t1440998580 * L_140 = __this->get_peaks_6();
		int32_t L_141 = V_11;
		// peaks.Add(maxPeak);
		NullCheck(L_140);
		List_1_Add_m688682013(L_140, L_141, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
	}

IL_0377:
	{
		// peaks.Insert(0, 0);
		List_1_t1440998580 * L_142 = __this->get_peaks_6();
		// peaks.Insert(0, 0);
		NullCheck(L_142);
		List_1_Insert_m2673244951(L_142, 0, 0, /*hidden argument*/List_1_Insert_m2673244951_MethodInfo_var);
		// for (int i = 0; i < peaks.Count; i++)
		V_12 = 0;
		goto IL_0415;
	}

IL_038c:
	{
		// if (i + 1 < peaks.Count)
		int32_t L_143 = V_12;
		List_1_t1440998580 * L_144 = __this->get_peaks_6();
		// if (i + 1 < peaks.Count)
		NullCheck(L_144);
		int32_t L_145 = List_1_get_Count_m852068579(L_144, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)((int32_t)((int32_t)L_143+(int32_t)1))) >= ((int32_t)L_145)))
		{
			goto IL_040e;
		}
	}
	{
		// int gap = peaks[i + 1] - peaks[i];
		List_1_t1440998580 * L_146 = __this->get_peaks_6();
		int32_t L_147 = V_12;
		// int gap = peaks[i + 1] - peaks[i];
		NullCheck(L_146);
		int32_t L_148 = List_1_get_Item_m1921196075(L_146, ((int32_t)((int32_t)L_147+(int32_t)1)), /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		List_1_t1440998580 * L_149 = __this->get_peaks_6();
		int32_t L_150 = V_12;
		// int gap = peaks[i + 1] - peaks[i];
		NullCheck(L_149);
		int32_t L_151 = List_1_get_Item_m1921196075(L_149, L_150, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_13 = ((int32_t)((int32_t)L_148-(int32_t)L_151));
		// if (gap > 3)
		int32_t L_152 = V_13;
		if ((((int32_t)L_152) <= ((int32_t)3)))
		{
			goto IL_040d;
		}
	}
	{
		// while (gap < 45) //45
		goto IL_03d7;
	}

IL_03cf:
	{
		// gap *= 2;
		int32_t L_153 = V_13;
		V_13 = ((int32_t)((int32_t)L_153*(int32_t)2));
	}

IL_03d7:
	{
		// while (gap < 45) //45
		int32_t L_154 = V_13;
		if ((((int32_t)L_154) < ((int32_t)((int32_t)45))))
		{
			goto IL_03cf;
		}
	}
	{
		// while (gap > 90) //90
		goto IL_03ed;
	}

IL_03e5:
	{
		// gap /= 2;
		int32_t L_155 = V_13;
		V_13 = ((int32_t)((int32_t)L_155/(int32_t)2));
	}

IL_03ed:
	{
		// while (gap > 90) //90
		int32_t L_156 = V_13;
		if ((((int32_t)L_156) > ((int32_t)((int32_t)90))))
		{
			goto IL_03e5;
		}
	}
	{
		// beatHistogram[gap]++;
		SingleU5BU5D_t577127397* L_157 = __this->get_beatHistogram_7();
		int32_t L_158 = V_13;
		NullCheck(L_157);
		float* L_159 = ((L_157)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_158)));
		*((float*)(L_159)) = (float)((float)((float)(*((float*)L_159))+(float)(1.0f)));
	}

IL_040d:
	{
	}

IL_040e:
	{
		// for (int i = 0; i < peaks.Count; i++)
		int32_t L_160 = V_12;
		V_12 = ((int32_t)((int32_t)L_160+(int32_t)1));
	}

IL_0415:
	{
		// for (int i = 0; i < peaks.Count; i++)
		int32_t L_161 = V_12;
		List_1_t1440998580 * L_162 = __this->get_peaks_6();
		// for (int i = 0; i < peaks.Count; i++)
		NullCheck(L_162);
		int32_t L_163 = List_1_get_Count_m852068579(L_162, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_161) < ((int32_t)L_163)))
		{
			goto IL_038c;
		}
	}
	{
		// currentBeatLength = 1;
		__this->set_currentBeatLength_9(1);
		// for (int i = 1; i < beatHistogram.Length; i++)
		V_14 = 1;
		goto IL_0461;
	}

IL_0436:
	{
		// if (beatHistogram[i] > beatHistogram[currentBeatLength])
		SingleU5BU5D_t577127397* L_164 = __this->get_beatHistogram_7();
		int32_t L_165 = V_14;
		NullCheck(L_164);
		int32_t L_166 = L_165;
		float L_167 = (L_164)->GetAt(static_cast<il2cpp_array_size_t>(L_166));
		SingleU5BU5D_t577127397* L_168 = __this->get_beatHistogram_7();
		int32_t L_169 = __this->get_currentBeatLength_9();
		NullCheck(L_168);
		int32_t L_170 = L_169;
		float L_171 = (L_168)->GetAt(static_cast<il2cpp_array_size_t>(L_170));
		if ((!(((float)L_167) > ((float)L_171))))
		{
			goto IL_045a;
		}
	}
	{
		// currentBeatLength = i;
		int32_t L_172 = V_14;
		__this->set_currentBeatLength_9(L_172);
	}

IL_045a:
	{
		// for (int i = 1; i < beatHistogram.Length; i++)
		int32_t L_173 = V_14;
		V_14 = ((int32_t)((int32_t)L_173+(int32_t)1));
	}

IL_0461:
	{
		// for (int i = 1; i < beatHistogram.Length; i++)
		int32_t L_174 = V_14;
		SingleU5BU5D_t577127397* L_175 = __this->get_beatHistogram_7();
		NullCheck(L_175);
		if ((((int32_t)L_174) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_175)->max_length)))))))
		{
			goto IL_0436;
		}
	}
	{
		// if (beatHistogram[currentBeatLength] > 15)
		SingleU5BU5D_t577127397* L_176 = __this->get_beatHistogram_7();
		int32_t L_177 = __this->get_currentBeatLength_9();
		NullCheck(L_176);
		int32_t L_178 = L_177;
		float L_179 = (L_176)->GetAt(static_cast<il2cpp_array_size_t>(L_178));
		if ((!(((float)L_179) > ((float)(15.0f)))))
		{
			goto IL_04cf;
		}
	}
	{
		// for (int i = 0; i < beatHistogram.Length; i++)
		V_15 = 0;
		goto IL_04bf;
	}

IL_0490:
	{
		// beatHistogram[i] = Mathf.Clamp(beatHistogram[i] - 7, 0, 7);
		SingleU5BU5D_t577127397* L_180 = __this->get_beatHistogram_7();
		int32_t L_181 = V_15;
		SingleU5BU5D_t577127397* L_182 = __this->get_beatHistogram_7();
		int32_t L_183 = V_15;
		NullCheck(L_182);
		int32_t L_184 = L_183;
		float L_185 = (L_182)->GetAt(static_cast<il2cpp_array_size_t>(L_184));
		// beatHistogram[i] = Mathf.Clamp(beatHistogram[i] - 7, 0, 7);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_186 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_185-(float)(7.0f))), (0.0f), (7.0f), /*hidden argument*/NULL);
		NullCheck(L_180);
		(L_180)->SetAt(static_cast<il2cpp_array_size_t>(L_181), (float)L_186);
		// for (int i = 0; i < beatHistogram.Length; i++)
		int32_t L_187 = V_15;
		V_15 = ((int32_t)((int32_t)L_187+(int32_t)1));
	}

IL_04bf:
	{
		// for (int i = 0; i < beatHistogram.Length; i++)
		int32_t L_188 = V_15;
		SingleU5BU5D_t577127397* L_189 = __this->get_beatHistogram_7();
		NullCheck(L_189);
		if ((((int32_t)L_188) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_189)->max_length)))))))
		{
			goto IL_0490;
		}
	}
	{
	}

IL_04cf:
	{
		// for (int i = 0; i < offsetScore.Length; i++)
		V_16 = 0;
		goto IL_04fc;
	}

IL_04d7:
	{
		// offsetScore[i] = OffsetScore(upsampledSignal, i, currentBeatLength);
		SingleU5BU5D_t577127397* L_190 = __this->get_offsetScore_5();
		int32_t L_191 = V_16;
		SingleU5BU5D_t577127397* L_192 = __this->get_upsampledSignal_2();
		int32_t L_193 = V_16;
		int32_t L_194 = __this->get_currentBeatLength_9();
		// offsetScore[i] = OffsetScore(upsampledSignal, i, currentBeatLength);
		float L_195 = BeatTracker_OffsetScore_m442017893(__this, L_192, L_193, L_194, /*hidden argument*/NULL);
		NullCheck(L_190);
		(L_190)->SetAt(static_cast<il2cpp_array_size_t>(L_191), (float)L_195);
		// for (int i = 0; i < offsetScore.Length; i++)
		int32_t L_196 = V_16;
		V_16 = ((int32_t)((int32_t)L_196+(int32_t)1));
	}

IL_04fc:
	{
		// for (int i = 0; i < offsetScore.Length; i++)
		int32_t L_197 = V_16;
		SingleU5BU5D_t577127397* L_198 = __this->get_offsetScore_5();
		NullCheck(L_198);
		if ((((int32_t)L_197) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_198)->max_length)))))))
		{
			goto IL_04d7;
		}
	}
	{
		// int offset = 0;
		V_17 = 0;
		// for (int i = 1; i < offsetScore.Length - 1; i++)
		V_18 = 1;
		goto IL_056d;
	}

IL_0516:
	{
		// if (offsetScore[i] > offsetScore[offset] && offsetScore[i] > offsetScore[i + 1] && offsetScore[i] > offsetScore[i - 1])
		SingleU5BU5D_t577127397* L_199 = __this->get_offsetScore_5();
		int32_t L_200 = V_18;
		NullCheck(L_199);
		int32_t L_201 = L_200;
		float L_202 = (L_199)->GetAt(static_cast<il2cpp_array_size_t>(L_201));
		SingleU5BU5D_t577127397* L_203 = __this->get_offsetScore_5();
		int32_t L_204 = V_17;
		NullCheck(L_203);
		int32_t L_205 = L_204;
		float L_206 = (L_203)->GetAt(static_cast<il2cpp_array_size_t>(L_205));
		if ((!(((float)L_202) > ((float)L_206))))
		{
			goto IL_0566;
		}
	}
	{
		SingleU5BU5D_t577127397* L_207 = __this->get_offsetScore_5();
		int32_t L_208 = V_18;
		NullCheck(L_207);
		int32_t L_209 = L_208;
		float L_210 = (L_207)->GetAt(static_cast<il2cpp_array_size_t>(L_209));
		SingleU5BU5D_t577127397* L_211 = __this->get_offsetScore_5();
		int32_t L_212 = V_18;
		NullCheck(L_211);
		int32_t L_213 = ((int32_t)((int32_t)L_212+(int32_t)1));
		float L_214 = (L_211)->GetAt(static_cast<il2cpp_array_size_t>(L_213));
		if ((!(((float)L_210) > ((float)L_214))))
		{
			goto IL_0566;
		}
	}
	{
		SingleU5BU5D_t577127397* L_215 = __this->get_offsetScore_5();
		int32_t L_216 = V_18;
		NullCheck(L_215);
		int32_t L_217 = L_216;
		float L_218 = (L_215)->GetAt(static_cast<il2cpp_array_size_t>(L_217));
		SingleU5BU5D_t577127397* L_219 = __this->get_offsetScore_5();
		int32_t L_220 = V_18;
		NullCheck(L_219);
		int32_t L_221 = ((int32_t)((int32_t)L_220-(int32_t)1));
		float L_222 = (L_219)->GetAt(static_cast<il2cpp_array_size_t>(L_221));
		if ((!(((float)L_218) > ((float)L_222))))
		{
			goto IL_0566;
		}
	}
	{
		// offset = i;
		int32_t L_223 = V_18;
		V_17 = L_223;
	}

IL_0566:
	{
		// for (int i = 1; i < offsetScore.Length - 1; i++)
		int32_t L_224 = V_18;
		V_18 = ((int32_t)((int32_t)L_224+(int32_t)1));
	}

IL_056d:
	{
		// for (int i = 1; i < offsetScore.Length - 1; i++)
		int32_t L_225 = V_18;
		SingleU5BU5D_t577127397* L_226 = __this->get_offsetScore_5();
		NullCheck(L_226);
		if ((((int32_t)L_225) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_226)->max_length))))-(int32_t)1)))))
		{
			goto IL_0516;
		}
	}
	{
		// int s = 0;
		V_19 = 0;
		// if (offset < offsetScore.Length - 1)
		int32_t L_227 = V_17;
		SingleU5BU5D_t577127397* L_228 = __this->get_offsetScore_5();
		NullCheck(L_228);
		if ((((int32_t)L_227) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_228)->max_length))))-(int32_t)1)))))
		{
			goto IL_05d4;
		}
	}
	{
		// offset = offset % currentBeatLength;
		int32_t L_229 = V_17;
		int32_t L_230 = __this->get_currentBeatLength_9();
		V_17 = ((int32_t)((int32_t)L_229%(int32_t)L_230));
		// s = offset;
		int32_t L_231 = V_17;
		V_19 = L_231;
		// if (currentBeatLength > 0)
		int32_t L_232 = __this->get_currentBeatLength_9();
		if ((((int32_t)L_232) <= ((int32_t)0)))
		{
			goto IL_05c8;
		}
	}
	{
		// while (s < 0)
		goto IL_05bf;
	}

IL_05b4:
	{
		// s += currentBeatLength;
		int32_t L_233 = V_19;
		int32_t L_234 = __this->get_currentBeatLength_9();
		V_19 = ((int32_t)((int32_t)L_233+(int32_t)L_234));
	}

IL_05bf:
	{
		// while (s < 0)
		int32_t L_235 = V_19;
		if ((((int32_t)L_235) < ((int32_t)0)))
		{
			goto IL_05b4;
		}
	}
	{
	}

IL_05c8:
	{
		// s = currentBeatLength - s;
		int32_t L_236 = __this->get_currentBeatLength_9();
		int32_t L_237 = V_19;
		V_19 = ((int32_t)((int32_t)L_236-(int32_t)L_237));
	}

IL_05d4:
	{
		// float d = s - syncOld;
		int32_t L_238 = V_19;
		float L_239 = __this->get_syncOld_10();
		V_20 = ((float)((float)(((float)((float)L_238)))-(float)L_239));
		// if (Mathf.Abs(d) > currentBeatLength / 2f)
		float L_240 = V_20;
		// if (Mathf.Abs(d) > currentBeatLength / 2f)
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_241 = fabsf(L_240);
		int32_t L_242 = __this->get_currentBeatLength_9();
		if ((!(((float)L_241) > ((float)((float)((float)(((float)((float)L_242)))/(float)(2.0f)))))))
		{
			goto IL_0635;
		}
	}
	{
		// if (d > 0)
		float L_243 = V_20;
		if ((!(((float)L_243) > ((float)(0.0f)))))
		{
			goto IL_061d;
		}
	}
	{
		// sync += -2;
		float L_244 = __this->get_sync_11();
		__this->set_sync_11(((float)((float)L_244+(float)(-2.0f))));
		goto IL_062f;
	}

IL_061d:
	{
		// sync += 2;
		float L_245 = __this->get_sync_11();
		__this->set_sync_11(((float)((float)L_245+(float)(2.0f))));
	}

IL_062f:
	{
		goto IL_0673;
	}

IL_0635:
	{
		// if (d > 2)
		float L_246 = V_20;
		if ((!(((float)L_246) > ((float)(2.0f)))))
		{
			goto IL_0654;
		}
	}
	{
		// sync += 2;
		float L_247 = __this->get_sync_11();
		__this->set_sync_11(((float)((float)L_247+(float)(2.0f))));
	}

IL_0654:
	{
		// if (d < -2)
		float L_248 = V_20;
		if ((!(((float)L_248) < ((float)(-2.0f)))))
		{
			goto IL_0672;
		}
	}
	{
		// sync += -2;
		float L_249 = __this->get_sync_11();
		__this->set_sync_11(((float)((float)L_249+(float)(-2.0f))));
	}

IL_0672:
	{
	}

IL_0673:
	{
		// }
		return;
	}
}
// System.Single BeatTracker::RepetitionScore(System.Single[],System.Int32)
extern "C"  float BeatTracker_RepetitionScore_m1324094558 (BeatTracker_t2801099156 * __this, SingleU5BU5D_t577127397* ___signal0, int32_t ___offset1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		// float score = 0;
		V_0 = (0.0f);
		// for (int i = 0; i < signal.Length - offset; i++)
		V_1 = 0;
		goto IL_0020;
	}

IL_000e:
	{
		// score += (signal[i] * signal[i + offset]);
		float L_0 = V_0;
		SingleU5BU5D_t577127397* L_1 = ___signal0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		float L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		SingleU5BU5D_t577127397* L_5 = ___signal0;
		int32_t L_6 = V_1;
		int32_t L_7 = ___offset1;
		NullCheck(L_5);
		int32_t L_8 = ((int32_t)((int32_t)L_6+(int32_t)L_7));
		float L_9 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_0 = ((float)((float)L_0+(float)((float)((float)L_4*(float)L_9))));
		// for (int i = 0; i < signal.Length - offset; i++)
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0020:
	{
		// for (int i = 0; i < signal.Length - offset; i++)
		int32_t L_11 = V_1;
		SingleU5BU5D_t577127397* L_12 = ___signal0;
		NullCheck(L_12);
		int32_t L_13 = ___offset1;
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))-(int32_t)L_13)))))
		{
			goto IL_000e;
		}
	}
	{
		// return (score / (signal.Length - offset)) * 10;
		float L_14 = V_0;
		SingleU5BU5D_t577127397* L_15 = ___signal0;
		NullCheck(L_15);
		int32_t L_16 = ___offset1;
		V_2 = ((float)((float)((float)((float)L_14/(float)(((float)((float)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))-(int32_t)L_16)))))))*(float)(10.0f)));
		goto IL_003f;
	}

IL_003f:
	{
		// }
		float L_17 = V_2;
		return L_17;
	}
}
// System.Single BeatTracker::GapScore(System.Single[],System.Int32)
extern "C"  float BeatTracker_GapScore_m397149463 (BeatTracker_t2801099156 * __this, SingleU5BU5D_t577127397* ___signal0, int32_t ___offset1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	{
		// float e = 0;
		V_0 = (0.0f);
		// int i = 1;
		V_1 = 1;
		// int count = 0;
		V_2 = 0;
		// while (offset * i < signal.Length)
		goto IL_0022;
	}

IL_0010:
	{
		// e += signal[i * offset];
		float L_0 = V_0;
		SingleU5BU5D_t577127397* L_1 = ___signal0;
		int32_t L_2 = V_1;
		int32_t L_3 = ___offset1;
		NullCheck(L_1);
		int32_t L_4 = ((int32_t)((int32_t)L_2*(int32_t)L_3));
		float L_5 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = ((float)((float)L_0+(float)L_5));
		// i++;
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
		// count++;
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0022:
	{
		// while (offset * i < signal.Length)
		int32_t L_8 = ___offset1;
		int32_t L_9 = V_1;
		SingleU5BU5D_t577127397* L_10 = ___signal0;
		NullCheck(L_10);
		if ((((int32_t)((int32_t)((int32_t)L_8*(int32_t)L_9))) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		// if (count == 0)
		int32_t L_11 = V_2;
		if (L_11)
		{
			goto IL_003a;
		}
	}
	{
		// return e;
		float L_12 = V_0;
		V_3 = L_12;
		goto IL_0044;
	}

IL_003a:
	{
		// return e / count;
		float L_13 = V_0;
		int32_t L_14 = V_2;
		V_3 = ((float)((float)L_13/(float)(((float)((float)L_14)))));
		goto IL_0044;
	}

IL_0044:
	{
		// }
		float L_15 = V_3;
		return L_15;
	}
}
// System.Single BeatTracker::OffsetScore(System.Single[],System.Int32,System.Int32)
extern "C"  float BeatTracker_OffsetScore_m442017893 (BeatTracker_t2801099156 * __this, SingleU5BU5D_t577127397* ___signal0, int32_t ___offset1, int32_t ___gap2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		// float score = 0;
		V_0 = (0.0f);
		// for (int i = 0; i < signal.Length - offset; i++)
		V_1 = 0;
		goto IL_0024;
	}

IL_000e:
	{
		// if ((i) % gap == 0)
		int32_t L_0 = V_1;
		int32_t L_1 = ___gap2;
		if (((int32_t)((int32_t)L_0%(int32_t)L_1)))
		{
			goto IL_001f;
		}
	}
	{
		// score += (signal[i + offset]);
		float L_2 = V_0;
		SingleU5BU5D_t577127397* L_3 = ___signal0;
		int32_t L_4 = V_1;
		int32_t L_5 = ___offset1;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)((int32_t)L_4+(int32_t)L_5));
		float L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = ((float)((float)L_2+(float)L_7));
	}

IL_001f:
	{
		// for (int i = 0; i < signal.Length - offset; i++)
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0024:
	{
		// for (int i = 0; i < signal.Length - offset; i++)
		int32_t L_9 = V_1;
		SingleU5BU5D_t577127397* L_10 = ___signal0;
		NullCheck(L_10);
		int32_t L_11 = ___offset1;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))-(int32_t)L_11)))))
		{
			goto IL_000e;
		}
	}
	{
		// score /= (signal.Length - offset) / gap;
		float L_12 = V_0;
		SingleU5BU5D_t577127397* L_13 = ___signal0;
		NullCheck(L_13);
		int32_t L_14 = ___offset1;
		int32_t L_15 = ___gap2;
		V_0 = ((float)((float)L_12/(float)(((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))-(int32_t)L_14))/(int32_t)L_15)))))));
		// return score;
		float L_16 = V_0;
		V_2 = L_16;
		goto IL_0041;
	}

IL_0041:
	{
		// }
		float L_17 = V_2;
		return L_17;
	}
}
// System.Void BeatTracker::DrawDebugLines(System.Int32)
extern "C"  void BeatTracker_DrawDebugLines_m1071911214 (BeatTracker_t2801099156 * __this, int32_t ___currentFrame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_DrawDebugLines_m1071911214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	int32_t V_13 = 0;
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	int32_t V_16 = 0;
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	int32_t V_19 = 0;
	Enumerator_t975728254  V_20;
	memset(&V_20, 0, sizeof(V_20));
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// for (int i = currentFrame; i < currentFrame + 300; i++)
		int32_t L_0 = ___currentFrame0;
		V_0 = L_0;
		goto IL_0051;
	}

IL_0008:
	{
		// if (_beats.ContainsKey(i))
		Dictionary_2_t1703509207 * L_1 = __this->get__beats_16();
		int32_t L_2 = V_0;
		// if (_beats.ContainsKey(i))
		NullCheck(L_1);
		bool L_3 = Dictionary_2_ContainsKey_m604849294(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m604849294_MethodInfo_var);
		if (!L_3)
		{
			goto IL_004c;
		}
	}
	{
		// Debug.DrawLine(new Vector3(i - currentFrame, 0, -1), new Vector3(i - currentFrame, 400, 1), Color.black);
		int32_t L_4 = V_0;
		int32_t L_5 = ___currentFrame0;
		// Debug.DrawLine(new Vector3(i - currentFrame, 0, -1), new Vector3(i - currentFrame, 400, 1), Color.black);
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, (((float)((float)((int32_t)((int32_t)L_4-(int32_t)L_5))))), (0.0f), (-1.0f), /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		int32_t L_8 = ___currentFrame0;
		// Debug.DrawLine(new Vector3(i - currentFrame, 0, -1), new Vector3(i - currentFrame, 400, 1), Color.black);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, (((float)((float)((int32_t)((int32_t)L_7-(int32_t)L_8))))), (400.0f), (1.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(new Vector3(i - currentFrame, 0, -1), new Vector3(i - currentFrame, 400, 1), Color.black);
		Color_t2020392075  L_10 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(new Vector3(i - currentFrame, 0, -1), new Vector3(i - currentFrame, 400, 1), Color.black);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_6, L_9, L_10, /*hidden argument*/NULL);
	}

IL_004c:
	{
		// for (int i = currentFrame; i < currentFrame + 300; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0051:
	{
		// for (int i = currentFrame; i < currentFrame + 300; i++)
		int32_t L_12 = V_0;
		int32_t L_13 = ___currentFrame0;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)((int32_t)L_13+(int32_t)((int32_t)300))))))
		{
			goto IL_0008;
		}
	}
	{
		// for (int i = 0; i < beatHistogram.Length; i++)
		V_1 = 0;
		goto IL_00d0;
	}

IL_0065:
	{
		// Vector3 sv = new Vector3(i * -1, 0, 0);
		int32_t L_14 = V_1;
		// Vector3 sv = new Vector3(i * -1, 0, 0);
		Vector3__ctor_m2638739322((&V_2), (((float)((float)((int32_t)((int32_t)L_14*(int32_t)(-1)))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		// Vector3 ev = new Vector3(i * -1, beatHistogram[i], 0);
		int32_t L_15 = V_1;
		SingleU5BU5D_t577127397* L_16 = __this->get_beatHistogram_7();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		float L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		// Vector3 ev = new Vector3(i * -1, beatHistogram[i], 0);
		Vector3__ctor_m2638739322((&V_3), (((float)((float)((int32_t)((int32_t)L_15*(int32_t)(-1)))))), L_19, (0.0f), /*hidden argument*/NULL);
		// sv += Vector3.up * 100;
		Vector3_t2243707580  L_20 = V_2;
		// sv += Vector3.up * 100;
		Vector3_t2243707580  L_21 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		// sv += Vector3.up * 100;
		Vector3_t2243707580  L_22 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_21, (100.0f), /*hidden argument*/NULL);
		// sv += Vector3.up * 100;
		Vector3_t2243707580  L_23 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		// ev += Vector3.up * 100;
		Vector3_t2243707580  L_24 = V_3;
		// ev += Vector3.up * 100;
		Vector3_t2243707580  L_25 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		// ev += Vector3.up * 100;
		Vector3_t2243707580  L_26 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_25, (100.0f), /*hidden argument*/NULL);
		// ev += Vector3.up * 100;
		Vector3_t2243707580  L_27 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		// Debug.DrawLine(sv, ev, Color.blue);
		Vector3_t2243707580  L_28 = V_2;
		Vector3_t2243707580  L_29 = V_3;
		// Debug.DrawLine(sv, ev, Color.blue);
		Color_t2020392075  L_30 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(sv, ev, Color.blue);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_28, L_29, L_30, /*hidden argument*/NULL);
		// for (int i = 0; i < beatHistogram.Length; i++)
		int32_t L_31 = V_1;
		V_1 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_00d0:
	{
		// for (int i = 0; i < beatHistogram.Length; i++)
		int32_t L_32 = V_1;
		SingleU5BU5D_t577127397* L_33 = __this->get_beatHistogram_7();
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_0065;
		}
	}
	{
		// for (int i = 0; i < signalBuffer.Length - 1; i++)
		V_4 = 0;
		goto IL_0130;
	}

IL_00e6:
	{
		// Vector3 start = new Vector3(i, signalBuffer[i], 0);
		int32_t L_34 = V_4;
		SingleU5BU5D_t577127397* L_35 = __this->get_signalBuffer_0();
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		float L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		// Vector3 start = new Vector3(i, signalBuffer[i], 0);
		Vector3__ctor_m2638739322((&V_5), (((float)((float)L_34))), L_38, (0.0f), /*hidden argument*/NULL);
		// Vector3 end = new Vector3((i + 1), signalBuffer[i + 1], 0);
		int32_t L_39 = V_4;
		SingleU5BU5D_t577127397* L_40 = __this->get_signalBuffer_0();
		int32_t L_41 = V_4;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		float L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		// Vector3 end = new Vector3((i + 1), signalBuffer[i + 1], 0);
		Vector3__ctor_m2638739322((&V_6), (((float)((float)((int32_t)((int32_t)L_39+(int32_t)1))))), L_43, (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.black);
		Vector3_t2243707580  L_44 = V_5;
		Vector3_t2243707580  L_45 = V_6;
		// Debug.DrawLine(start, end, Color.black);
		Color_t2020392075  L_46 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.black);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_44, L_45, L_46, /*hidden argument*/NULL);
		// for (int i = 0; i < signalBuffer.Length - 1; i++)
		int32_t L_47 = V_4;
		V_4 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0130:
	{
		// for (int i = 0; i < signalBuffer.Length - 1; i++)
		int32_t L_48 = V_4;
		SingleU5BU5D_t577127397* L_49 = __this->get_signalBuffer_0();
		NullCheck(L_49);
		if ((((int32_t)L_48) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_49)->max_length))))-(int32_t)1)))))
		{
			goto IL_00e6;
		}
	}
	{
		// for (int i = 0; i < currentSignal.Length - 1; i++)
		V_7 = 0;
		goto IL_019a;
	}

IL_0149:
	{
		// Vector3 start = new Vector3(i, currentSignal[i] - 200, 0);
		int32_t L_50 = V_7;
		SingleU5BU5D_t577127397* L_51 = __this->get_currentSignal_1();
		int32_t L_52 = V_7;
		NullCheck(L_51);
		int32_t L_53 = L_52;
		float L_54 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		// Vector3 start = new Vector3(i, currentSignal[i] - 200, 0);
		Vector3__ctor_m2638739322((&V_8), (((float)((float)L_50))), ((float)((float)L_54-(float)(200.0f))), (0.0f), /*hidden argument*/NULL);
		// Vector3 end = new Vector3((i + 1), currentSignal[i + 1] - 200, 0);
		int32_t L_55 = V_7;
		SingleU5BU5D_t577127397* L_56 = __this->get_currentSignal_1();
		int32_t L_57 = V_7;
		NullCheck(L_56);
		int32_t L_58 = ((int32_t)((int32_t)L_57+(int32_t)1));
		float L_59 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		// Vector3 end = new Vector3((i + 1), currentSignal[i + 1] - 200, 0);
		Vector3__ctor_m2638739322((&V_9), (((float)((float)((int32_t)((int32_t)L_55+(int32_t)1))))), ((float)((float)L_59-(float)(200.0f))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start, end);
		Vector3_t2243707580  L_60 = V_8;
		Vector3_t2243707580  L_61 = V_9;
		// Debug.DrawLine(start, end);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m2961609580(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
		// for (int i = 0; i < currentSignal.Length - 1; i++)
		int32_t L_62 = V_7;
		V_7 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_019a:
	{
		// for (int i = 0; i < currentSignal.Length - 1; i++)
		int32_t L_63 = V_7;
		SingleU5BU5D_t577127397* L_64 = __this->get_currentSignal_1();
		NullCheck(L_64);
		if ((((int32_t)L_63) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_64)->max_length))))-(int32_t)1)))))
		{
			goto IL_0149;
		}
	}
	{
		// for (int i = 0; i < repetitionScore.Length - 1; i++)
		V_10 = 0;
		goto IL_0215;
	}

IL_01b3:
	{
		// Vector3 start = new Vector3(i, repetitionScore[i] * 10 - 100, 0);
		int32_t L_65 = V_10;
		SingleU5BU5D_t577127397* L_66 = __this->get_repetitionScore_3();
		int32_t L_67 = V_10;
		NullCheck(L_66);
		int32_t L_68 = L_67;
		float L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		// Vector3 start = new Vector3(i, repetitionScore[i] * 10 - 100, 0);
		Vector3__ctor_m2638739322((&V_11), (((float)((float)L_65))), ((float)((float)((float)((float)L_69*(float)(10.0f)))-(float)(100.0f))), (0.0f), /*hidden argument*/NULL);
		// Vector3 end = new Vector3((i + 1), repetitionScore[i + 1] * 10 - 100, 0);
		int32_t L_70 = V_10;
		SingleU5BU5D_t577127397* L_71 = __this->get_repetitionScore_3();
		int32_t L_72 = V_10;
		NullCheck(L_71);
		int32_t L_73 = ((int32_t)((int32_t)L_72+(int32_t)1));
		float L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		// Vector3 end = new Vector3((i + 1), repetitionScore[i + 1] * 10 - 100, 0);
		Vector3__ctor_m2638739322((&V_12), (((float)((float)((int32_t)((int32_t)L_70+(int32_t)1))))), ((float)((float)((float)((float)L_74*(float)(10.0f)))-(float)(100.0f))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.gray);
		Vector3_t2243707580  L_75 = V_11;
		Vector3_t2243707580  L_76 = V_12;
		// Debug.DrawLine(start, end, Color.gray);
		Color_t2020392075  L_77 = Color_get_gray_m1396712533(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.gray);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_75, L_76, L_77, /*hidden argument*/NULL);
		// for (int i = 0; i < repetitionScore.Length - 1; i++)
		int32_t L_78 = V_10;
		V_10 = ((int32_t)((int32_t)L_78+(int32_t)1));
	}

IL_0215:
	{
		// for (int i = 0; i < repetitionScore.Length - 1; i++)
		int32_t L_79 = V_10;
		SingleU5BU5D_t577127397* L_80 = __this->get_repetitionScore_3();
		NullCheck(L_80);
		if ((((int32_t)L_79) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_80)->max_length))))-(int32_t)1)))))
		{
			goto IL_01b3;
		}
	}
	{
		// for (int i = 0; i < gapScore.Length - 1; i++)
		V_13 = 0;
		goto IL_0290;
	}

IL_022e:
	{
		// Vector3 start = new Vector3(i, gapScore[i] * 10 - 100, 0);
		int32_t L_81 = V_13;
		SingleU5BU5D_t577127397* L_82 = __this->get_gapScore_4();
		int32_t L_83 = V_13;
		NullCheck(L_82);
		int32_t L_84 = L_83;
		float L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		// Vector3 start = new Vector3(i, gapScore[i] * 10 - 100, 0);
		Vector3__ctor_m2638739322((&V_14), (((float)((float)L_81))), ((float)((float)((float)((float)L_85*(float)(10.0f)))-(float)(100.0f))), (0.0f), /*hidden argument*/NULL);
		// Vector3 end = new Vector3((i + 1), gapScore[i + 1] * 10 - 100, 0);
		int32_t L_86 = V_13;
		SingleU5BU5D_t577127397* L_87 = __this->get_gapScore_4();
		int32_t L_88 = V_13;
		NullCheck(L_87);
		int32_t L_89 = ((int32_t)((int32_t)L_88+(int32_t)1));
		float L_90 = (L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		// Vector3 end = new Vector3((i + 1), gapScore[i + 1] * 10 - 100, 0);
		Vector3__ctor_m2638739322((&V_15), (((float)((float)((int32_t)((int32_t)L_86+(int32_t)1))))), ((float)((float)((float)((float)L_90*(float)(10.0f)))-(float)(100.0f))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.black);
		Vector3_t2243707580  L_91 = V_14;
		Vector3_t2243707580  L_92 = V_15;
		// Debug.DrawLine(start, end, Color.black);
		Color_t2020392075  L_93 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.black);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_91, L_92, L_93, /*hidden argument*/NULL);
		// for (int i = 0; i < gapScore.Length - 1; i++)
		int32_t L_94 = V_13;
		V_13 = ((int32_t)((int32_t)L_94+(int32_t)1));
	}

IL_0290:
	{
		// for (int i = 0; i < gapScore.Length - 1; i++)
		int32_t L_95 = V_13;
		SingleU5BU5D_t577127397* L_96 = __this->get_gapScore_4();
		NullCheck(L_96);
		if ((((int32_t)L_95) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_96)->max_length))))-(int32_t)1)))))
		{
			goto IL_022e;
		}
	}
	{
		// for (int i = 0; i < offsetScore.Length - 1; i++)
		V_16 = 0;
		goto IL_02ff;
	}

IL_02a9:
	{
		// Vector3 start = new Vector3(i, offsetScore[i] - 150, 0);
		int32_t L_97 = V_16;
		SingleU5BU5D_t577127397* L_98 = __this->get_offsetScore_5();
		int32_t L_99 = V_16;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		float L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		// Vector3 start = new Vector3(i, offsetScore[i] - 150, 0);
		Vector3__ctor_m2638739322((&V_17), (((float)((float)L_97))), ((float)((float)L_101-(float)(150.0f))), (0.0f), /*hidden argument*/NULL);
		// Vector3 end = new Vector3((i + 1), offsetScore[i + 1] - 150, 0);
		int32_t L_102 = V_16;
		SingleU5BU5D_t577127397* L_103 = __this->get_offsetScore_5();
		int32_t L_104 = V_16;
		NullCheck(L_103);
		int32_t L_105 = ((int32_t)((int32_t)L_104+(int32_t)1));
		float L_106 = (L_103)->GetAt(static_cast<il2cpp_array_size_t>(L_105));
		// Vector3 end = new Vector3((i + 1), offsetScore[i + 1] - 150, 0);
		Vector3__ctor_m2638739322((&V_18), (((float)((float)((int32_t)((int32_t)L_102+(int32_t)1))))), ((float)((float)L_106-(float)(150.0f))), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.red);
		Vector3_t2243707580  L_107 = V_17;
		Vector3_t2243707580  L_108 = V_18;
		// Debug.DrawLine(start, end, Color.red);
		Color_t2020392075  L_109 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.red);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_107, L_108, L_109, /*hidden argument*/NULL);
		// for (int i = 0; i < offsetScore.Length - 1; i++)
		int32_t L_110 = V_16;
		V_16 = ((int32_t)((int32_t)L_110+(int32_t)1));
	}

IL_02ff:
	{
		// for (int i = 0; i < offsetScore.Length - 1; i++)
		int32_t L_111 = V_16;
		SingleU5BU5D_t577127397* L_112 = __this->get_offsetScore_5();
		NullCheck(L_112);
		if ((((int32_t)L_111) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_112)->max_length))))-(int32_t)1)))))
		{
			goto IL_02a9;
		}
	}
	{
		// foreach (int p in peaks)
		List_1_t1440998580 * L_113 = __this->get_peaks_6();
		// foreach (int p in peaks)
		NullCheck(L_113);
		Enumerator_t975728254  L_114 = List_1_GetEnumerator_m2527786909(L_113, /*hidden argument*/List_1_GetEnumerator_m2527786909_MethodInfo_var);
		V_20 = L_114;
	}

IL_031e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03be;
		}

IL_0323:
		{
			// foreach (int p in peaks)
			// foreach (int p in peaks)
			int32_t L_115 = Enumerator_get_Current_m1062633493((&V_20), /*hidden argument*/Enumerator_get_Current_m1062633493_MethodInfo_var);
			V_19 = L_115;
			// Debug.DrawLine(new Vector3(p, -100, 0), new Vector3(p, -50, 0));
			int32_t L_116 = V_19;
			// Debug.DrawLine(new Vector3(p, -100, 0), new Vector3(p, -50, 0));
			Vector3_t2243707580  L_117;
			memset(&L_117, 0, sizeof(L_117));
			Vector3__ctor_m2638739322(&L_117, (((float)((float)L_116))), (-100.0f), (0.0f), /*hidden argument*/NULL);
			int32_t L_118 = V_19;
			// Debug.DrawLine(new Vector3(p, -100, 0), new Vector3(p, -50, 0));
			Vector3_t2243707580  L_119;
			memset(&L_119, 0, sizeof(L_119));
			Vector3__ctor_m2638739322(&L_119, (((float)((float)L_118))), (-50.0f), (0.0f), /*hidden argument*/NULL);
			// Debug.DrawLine(new Vector3(p, -100, 0), new Vector3(p, -50, 0));
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_DrawLine_m2961609580(NULL /*static, unused*/, L_117, L_119, /*hidden argument*/NULL);
			// int t = p;
			int32_t L_120 = V_19;
			V_21 = L_120;
			// if (t > 0)
			int32_t L_121 = V_21;
			if ((((int32_t)L_121) <= ((int32_t)0)))
			{
				goto IL_038f;
			}
		}

IL_0362:
		{
			// while (t < 45)
			goto IL_036e;
		}

IL_0368:
		{
			// t *= 2;
			int32_t L_122 = V_21;
			V_21 = ((int32_t)((int32_t)L_122*(int32_t)2));
		}

IL_036e:
		{
			// while (t < 45)
			int32_t L_123 = V_21;
			if ((((int32_t)L_123) < ((int32_t)((int32_t)45))))
			{
				goto IL_0368;
			}
		}

IL_0377:
		{
			// while (t > 149)
			goto IL_0382;
		}

IL_037c:
		{
			// t /= 2;
			int32_t L_124 = V_21;
			V_21 = ((int32_t)((int32_t)L_124/(int32_t)2));
		}

IL_0382:
		{
			// while (t > 149)
			int32_t L_125 = V_21;
			if ((((int32_t)L_125) > ((int32_t)((int32_t)149))))
			{
				goto IL_037c;
			}
		}

IL_038e:
		{
		}

IL_038f:
		{
			// Debug.DrawLine(new Vector3(t, -100, 0), new Vector3(t, -90, 0), Color.black);
			int32_t L_126 = V_21;
			// Debug.DrawLine(new Vector3(t, -100, 0), new Vector3(t, -90, 0), Color.black);
			Vector3_t2243707580  L_127;
			memset(&L_127, 0, sizeof(L_127));
			Vector3__ctor_m2638739322(&L_127, (((float)((float)L_126))), (-100.0f), (0.0f), /*hidden argument*/NULL);
			int32_t L_128 = V_21;
			// Debug.DrawLine(new Vector3(t, -100, 0), new Vector3(t, -90, 0), Color.black);
			Vector3_t2243707580  L_129;
			memset(&L_129, 0, sizeof(L_129));
			Vector3__ctor_m2638739322(&L_129, (((float)((float)L_128))), (-90.0f), (0.0f), /*hidden argument*/NULL);
			// Debug.DrawLine(new Vector3(t, -100, 0), new Vector3(t, -90, 0), Color.black);
			Color_t2020392075  L_130 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
			// Debug.DrawLine(new Vector3(t, -100, 0), new Vector3(t, -90, 0), Color.black);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_127, L_129, L_130, /*hidden argument*/NULL);
		}

IL_03be:
		{
			// foreach (int p in peaks)
			bool L_131 = Enumerator_MoveNext_m4282865897((&V_20), /*hidden argument*/Enumerator_MoveNext_m4282865897_MethodInfo_var);
			if (L_131)
			{
				goto IL_0323;
			}
		}

IL_03ca:
		{
			IL2CPP_LEAVE(0x3DD, FINALLY_03cf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_03cf;
	}

FINALLY_03cf:
	{ // begin finally (depth: 1)
		// foreach (int p in peaks)
		Enumerator_Dispose_m1274756239((&V_20), /*hidden argument*/Enumerator_Dispose_m1274756239_MethodInfo_var);
		IL2CPP_END_FINALLY(975)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(975)
	{
		IL2CPP_JUMP_TBL(0x3DD, IL_03dd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_03dd:
	{
		// Debug.DrawLine(new Vector3(bestRepetition, -100, 0), new Vector3(bestRepetition, -70, 0), Color.yellow);
		int32_t L_132 = __this->get_bestRepetition_8();
		// Debug.DrawLine(new Vector3(bestRepetition, -100, 0), new Vector3(bestRepetition, -70, 0), Color.yellow);
		Vector3_t2243707580  L_133;
		memset(&L_133, 0, sizeof(L_133));
		Vector3__ctor_m2638739322(&L_133, (((float)((float)L_132))), (-100.0f), (0.0f), /*hidden argument*/NULL);
		int32_t L_134 = __this->get_bestRepetition_8();
		// Debug.DrawLine(new Vector3(bestRepetition, -100, 0), new Vector3(bestRepetition, -70, 0), Color.yellow);
		Vector3_t2243707580  L_135;
		memset(&L_135, 0, sizeof(L_135));
		Vector3__ctor_m2638739322(&L_135, (((float)((float)L_134))), (-70.0f), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(new Vector3(bestRepetition, -100, 0), new Vector3(bestRepetition, -70, 0), Color.yellow);
		Color_t2020392075  L_136 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(new Vector3(bestRepetition, -100, 0), new Vector3(bestRepetition, -70, 0), Color.yellow);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_133, L_135, L_136, /*hidden argument*/NULL);
		// int b = bestRepetition;
		int32_t L_137 = __this->get_bestRepetition_8();
		V_22 = L_137;
		// if (b > 0)
		int32_t L_138 = V_22;
		if ((((int32_t)L_138) <= ((int32_t)0)))
		{
			goto IL_0455;
		}
	}
	{
		// while (b < 45)
		goto IL_042f;
	}

IL_0429:
	{
		// b *= 2;
		int32_t L_139 = V_22;
		V_22 = ((int32_t)((int32_t)L_139*(int32_t)2));
	}

IL_042f:
	{
		// while (b < 45)
		int32_t L_140 = V_22;
		if ((((int32_t)L_140) < ((int32_t)((int32_t)45))))
		{
			goto IL_0429;
		}
	}
	{
		// while (b > gapScore.Length - 1)
		goto IL_0443;
	}

IL_043d:
	{
		// b /= 2;
		int32_t L_141 = V_22;
		V_22 = ((int32_t)((int32_t)L_141/(int32_t)2));
	}

IL_0443:
	{
		// while (b > gapScore.Length - 1)
		int32_t L_142 = V_22;
		SingleU5BU5D_t577127397* L_143 = __this->get_gapScore_4();
		NullCheck(L_143);
		if ((((int32_t)L_142) > ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_143)->max_length))))-(int32_t)1)))))
		{
			goto IL_043d;
		}
	}
	{
	}

IL_0455:
	{
		// Debug.DrawLine(new Vector3(b, -100, 0), new Vector3(b, -80, 0), Color.yellow);
		int32_t L_144 = V_22;
		// Debug.DrawLine(new Vector3(b, -100, 0), new Vector3(b, -80, 0), Color.yellow);
		Vector3_t2243707580  L_145;
		memset(&L_145, 0, sizeof(L_145));
		Vector3__ctor_m2638739322(&L_145, (((float)((float)L_144))), (-100.0f), (0.0f), /*hidden argument*/NULL);
		int32_t L_146 = V_22;
		// Debug.DrawLine(new Vector3(b, -100, 0), new Vector3(b, -80, 0), Color.yellow);
		Vector3_t2243707580  L_147;
		memset(&L_147, 0, sizeof(L_147));
		Vector3__ctor_m2638739322(&L_147, (((float)((float)L_146))), (-80.0f), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(new Vector3(b, -100, 0), new Vector3(b, -80, 0), Color.yellow);
		Color_t2020392075  L_148 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(new Vector3(b, -100, 0), new Vector3(b, -80, 0), Color.yellow);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_145, L_147, L_148, /*hidden argument*/NULL);
		// }
		return;
	}
}
// Beat BeatTracker::NextBeat(System.Int32)
extern "C"  Beat_t2695683572 * BeatTracker_NextBeat_m3845220818 (BeatTracker_t2801099156 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_NextBeat_m3845220818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Beat_t2695683572 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (_beats.Count == 0)
		Dictionary_2_t1703509207 * L_0 = __this->get__beats_16();
		// if (_beats.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m2326583579(L_0, /*hidden argument*/Dictionary_2_get_Count_m2326583579_MethodInfo_var);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		// return new Beat(0, 0, 0);
		// return new Beat(0, 0, 0);
		Beat_t2695683572 * L_2 = (Beat_t2695683572 *)il2cpp_codegen_object_new(Beat_t2695683572_il2cpp_TypeInfo_var);
		Beat__ctor_m206855294(L_2, (0.0f), (0.0f), 0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0041;
	}

IL_0027:
	{
		// int nextBeat = NextBeatIndex(index);
		int32_t L_3 = ___index0;
		// int nextBeat = NextBeatIndex(index);
		int32_t L_4 = BeatTracker_NextBeatIndex_m2115259593(__this, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// return _beats[nextBeat];
		Dictionary_2_t1703509207 * L_5 = __this->get__beats_16();
		int32_t L_6 = V_1;
		// return _beats[nextBeat];
		NullCheck(L_5);
		Beat_t2695683572 * L_7 = Dictionary_2_get_Item_m2258219861(L_5, L_6, /*hidden argument*/Dictionary_2_get_Item_m2258219861_MethodInfo_var);
		V_0 = L_7;
		goto IL_0041;
	}

IL_0041:
	{
		// }
		Beat_t2695683572 * L_8 = V_0;
		return L_8;
	}
}
// Beat BeatTracker::PrevBeat(System.Int32)
extern "C"  Beat_t2695683572 * BeatTracker_PrevBeat_m277720998 (BeatTracker_t2801099156 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_PrevBeat_m277720998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Beat_t2695683572 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (_beats.Count == 0)
		Dictionary_2_t1703509207 * L_0 = __this->get__beats_16();
		// if (_beats.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m2326583579(L_0, /*hidden argument*/Dictionary_2_get_Count_m2326583579_MethodInfo_var);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		// return new Beat(0, 0, 0);
		// return new Beat(0, 0, 0);
		Beat_t2695683572 * L_2 = (Beat_t2695683572 *)il2cpp_codegen_object_new(Beat_t2695683572_il2cpp_TypeInfo_var);
		Beat__ctor_m206855294(L_2, (0.0f), (0.0f), 0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0041;
	}

IL_0027:
	{
		// int prevBeat = PrevBeatIndex(index);
		int32_t L_3 = ___index0;
		// int prevBeat = PrevBeatIndex(index);
		int32_t L_4 = BeatTracker_PrevBeatIndex_m2127559407(__this, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// return _beats[prevBeat];
		Dictionary_2_t1703509207 * L_5 = __this->get__beats_16();
		int32_t L_6 = V_1;
		// return _beats[prevBeat];
		NullCheck(L_5);
		Beat_t2695683572 * L_7 = Dictionary_2_get_Item_m2258219861(L_5, L_6, /*hidden argument*/Dictionary_2_get_Item_m2258219861_MethodInfo_var);
		V_0 = L_7;
		goto IL_0041;
	}

IL_0041:
	{
		// }
		Beat_t2695683572 * L_8 = V_0;
		return L_8;
	}
}
// System.Int32 BeatTracker::NextBeatIndex(System.Int32)
extern "C"  int32_t BeatTracker_NextBeatIndex_m2115259593 (BeatTracker_t2801099156 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_NextBeatIndex_m2115259593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// if (_beatIndices.Count == 0)
		List_1_t1440998580 * L_0 = __this->get__beatIndices_15();
		// if (_beatIndices.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m852068579(L_0, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return 0;
		V_0 = 0;
		goto IL_0057;
	}

IL_0018:
	{
		// int nextBeat = _beatIndices.BinarySearch(index);
		List_1_t1440998580 * L_2 = __this->get__beatIndices_15();
		int32_t L_3 = ___index0;
		// int nextBeat = _beatIndices.BinarySearch(index);
		NullCheck(L_2);
		int32_t L_4 = List_1_BinarySearch_m4172392722(L_2, L_3, /*hidden argument*/List_1_BinarySearch_m4172392722_MethodInfo_var);
		V_1 = L_4;
		// nextBeat = Mathf.Max(nextBeat, ~nextBeat);
		int32_t L_5 = V_1;
		int32_t L_6 = V_1;
		// nextBeat = Mathf.Max(nextBeat, ~nextBeat);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_Max_m1875893177(NULL /*static, unused*/, L_5, ((~L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		// nextBeat = Mathf.Clamp(nextBeat, 0, _beatIndices.Count - 1);
		int32_t L_8 = V_1;
		List_1_t1440998580 * L_9 = __this->get__beatIndices_15();
		// nextBeat = Mathf.Clamp(nextBeat, 0, _beatIndices.Count - 1);
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m852068579(L_9, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		// nextBeat = Mathf.Clamp(nextBeat, 0, _beatIndices.Count - 1);
		int32_t L_11 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, L_8, 0, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_11;
		// nextBeat = _beatIndices[nextBeat];
		List_1_t1440998580 * L_12 = __this->get__beatIndices_15();
		int32_t L_13 = V_1;
		// nextBeat = _beatIndices[nextBeat];
		NullCheck(L_12);
		int32_t L_14 = List_1_get_Item_m1921196075(L_12, L_13, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_1 = L_14;
		// return nextBeat;
		int32_t L_15 = V_1;
		V_0 = L_15;
		goto IL_0057;
	}

IL_0057:
	{
		// }
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.Int32 BeatTracker::PrevBeatIndex(System.Int32)
extern "C"  int32_t BeatTracker_PrevBeatIndex_m2127559407 (BeatTracker_t2801099156 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_PrevBeatIndex_m2127559407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// if (_beatIndices.Count == 0)
		List_1_t1440998580 * L_0 = __this->get__beatIndices_15();
		// if (_beatIndices.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m852068579(L_0, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return 0;
		V_0 = 0;
		goto IL_0059;
	}

IL_0018:
	{
		// int prevBeat = _beatIndices.BinarySearch(index);
		List_1_t1440998580 * L_2 = __this->get__beatIndices_15();
		int32_t L_3 = ___index0;
		// int prevBeat = _beatIndices.BinarySearch(index);
		NullCheck(L_2);
		int32_t L_4 = List_1_BinarySearch_m4172392722(L_2, L_3, /*hidden argument*/List_1_BinarySearch_m4172392722_MethodInfo_var);
		V_1 = L_4;
		// prevBeat = Mathf.Max(prevBeat, ~prevBeat);
		int32_t L_5 = V_1;
		int32_t L_6 = V_1;
		// prevBeat = Mathf.Max(prevBeat, ~prevBeat);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_Max_m1875893177(NULL /*static, unused*/, L_5, ((~L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		// prevBeat = Mathf.Clamp(prevBeat - 1, 0, _beatIndices.Count - 1);
		int32_t L_8 = V_1;
		List_1_t1440998580 * L_9 = __this->get__beatIndices_15();
		// prevBeat = Mathf.Clamp(prevBeat - 1, 0, _beatIndices.Count - 1);
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m852068579(L_9, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		// prevBeat = Mathf.Clamp(prevBeat - 1, 0, _beatIndices.Count - 1);
		int32_t L_11 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, ((int32_t)((int32_t)L_8-(int32_t)1)), 0, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_11;
		// prevBeat = _beatIndices[prevBeat];
		List_1_t1440998580 * L_12 = __this->get__beatIndices_15();
		int32_t L_13 = V_1;
		// prevBeat = _beatIndices[prevBeat];
		NullCheck(L_12);
		int32_t L_14 = List_1_get_Item_m1921196075(L_12, L_13, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_1 = L_14;
		// return prevBeat;
		int32_t L_15 = V_1;
		V_0 = L_15;
		goto IL_0059;
	}

IL_0059:
	{
		// }
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.Single BeatTracker::BeatTimer(System.Single)
extern "C"  float BeatTracker_BeatTimer_m2155883739 (BeatTracker_t2801099156 * __this, float ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_BeatTimer_m2155883739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	{
		// int nextBeat = NextBeatIndex((int)Mathf.Ceil(index));
		float L_0 = ___index0;
		// int nextBeat = NextBeatIndex((int)Mathf.Ceil(index));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = ceilf(L_0);
		// int nextBeat = NextBeatIndex((int)Mathf.Ceil(index));
		int32_t L_2 = BeatTracker_NextBeatIndex_m2115259593(__this, (((int32_t)((int32_t)L_1))), /*hidden argument*/NULL);
		V_0 = L_2;
		// int prevBeat = PrevBeatIndex((int)Mathf.Ceil(index));
		float L_3 = ___index0;
		// int prevBeat = PrevBeatIndex((int)Mathf.Ceil(index));
		float L_4 = ceilf(L_3);
		// int prevBeat = PrevBeatIndex((int)Mathf.Ceil(index));
		int32_t L_5 = BeatTracker_PrevBeatIndex_m2127559407(__this, (((int32_t)((int32_t)L_4))), /*hidden argument*/NULL);
		V_1 = L_5;
		// int l = nextBeat - prevBeat;
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		V_2 = ((int32_t)((int32_t)L_6-(int32_t)L_7));
		// if (l == 0)
		int32_t L_8 = V_2;
		if (L_8)
		{
			goto IL_0032;
		}
	}
	{
		// return 0;
		V_3 = (0.0f);
		goto IL_0045;
	}

IL_0032:
	{
		// return 1 - ((nextBeat - index) / l);
		int32_t L_9 = V_0;
		float L_10 = ___index0;
		int32_t L_11 = V_2;
		V_3 = ((float)((float)(1.0f)-(float)((float)((float)((float)((float)(((float)((float)L_9)))-(float)L_10))/(float)(((float)((float)L_11)))))));
		goto IL_0045;
	}

IL_0045:
	{
		// }
		float L_12 = V_3;
		return L_12;
	}
}
// System.Int32 BeatTracker::IsBeat(System.Int32,System.Int32)
extern "C"  int32_t BeatTracker_IsBeat_m3805475791 (BeatTracker_t2801099156 * __this, int32_t ___index0, int32_t ___min1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatTracker_IsBeat_m3805475791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		// if (_beats.Count == 0)
		Dictionary_2_t1703509207 * L_0 = __this->get__beats_16();
		// if (_beats.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m2326583579(L_0, /*hidden argument*/Dictionary_2_get_Count_m2326583579_MethodInfo_var);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return 0;
		V_0 = 0;
		goto IL_00b7;
	}

IL_0018:
	{
		// int nextBeat = NextBeatIndex((int)index);
		int32_t L_2 = ___index0;
		// int nextBeat = NextBeatIndex((int)index);
		int32_t L_3 = BeatTracker_NextBeatIndex_m2115259593(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// int prevBeat = PrevBeatIndex((int)index);
		int32_t L_4 = ___index0;
		// int prevBeat = PrevBeatIndex((int)index);
		int32_t L_5 = BeatTracker_PrevBeatIndex_m2127559407(__this, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		// float length = _beats[prevBeat].length;
		Dictionary_2_t1703509207 * L_6 = __this->get__beats_16();
		int32_t L_7 = V_2;
		// float length = _beats[prevBeat].length;
		NullCheck(L_6);
		Beat_t2695683572 * L_8 = Dictionary_2_get_Item_m2258219861(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m2258219861_MethodInfo_var);
		NullCheck(L_8);
		float L_9 = L_8->get_length_0();
		V_3 = L_9;
		// int r = nextBeat - prevBeat;
		int32_t L_10 = V_1;
		int32_t L_11 = V_2;
		V_4 = ((int32_t)((int32_t)L_10-(int32_t)L_11));
		// int f = nextBeat - index;
		int32_t L_12 = V_1;
		int32_t L_13 = ___index0;
		V_5 = ((int32_t)((int32_t)L_12-(int32_t)L_13));
		// if (f == 0)
		int32_t L_14 = V_5;
		if (L_14)
		{
			goto IL_0052;
		}
	}
	{
		// return 1;
		V_0 = 1;
		goto IL_00b7;
	}

IL_0052:
	{
		// if (length / 2 < min)
		float L_15 = V_3;
		int32_t L_16 = ___min1;
		if ((!(((float)((float)((float)L_15/(float)(2.0f)))) < ((float)(((float)((float)L_16)))))))
		{
			goto IL_0067;
		}
	}
	{
		// return 0;
		V_0 = 0;
		goto IL_00b7;
	}

IL_0067:
	{
		// if (f == r / 2)
		int32_t L_17 = V_5;
		int32_t L_18 = V_4;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)((int32_t)L_18/(int32_t)2))))))
		{
			goto IL_0079;
		}
	}
	{
		// return 2;
		V_0 = 2;
		goto IL_00b7;
	}

IL_0079:
	{
		// if (length / 4 < min)
		float L_19 = V_3;
		int32_t L_20 = ___min1;
		if ((!(((float)((float)((float)L_19/(float)(4.0f)))) < ((float)(((float)((float)L_20)))))))
		{
			goto IL_008e;
		}
	}
	{
		// return 0;
		V_0 = 0;
		goto IL_00b7;
	}

IL_008e:
	{
		// if (f == r / 4 || f == (r / 2) + (r / 4))
		int32_t L_21 = V_5;
		int32_t L_22 = V_4;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)((int32_t)L_22/(int32_t)4)))))
		{
			goto IL_00a9;
		}
	}
	{
		int32_t L_23 = V_5;
		int32_t L_24 = V_4;
		int32_t L_25 = V_4;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_24/(int32_t)2))+(int32_t)((int32_t)((int32_t)L_25/(int32_t)4))))))))
		{
			goto IL_00b0;
		}
	}

IL_00a9:
	{
		// return 4;
		V_0 = 4;
		goto IL_00b7;
	}

IL_00b0:
	{
		// return 0;
		V_0 = 0;
		goto IL_00b7;
	}

IL_00b7:
	{
		// }
		int32_t L_26 = V_0;
		return L_26;
	}
}
// System.Void BeatTracker::<TrackBeat>m__0(System.Object)
extern "C"  void BeatTracker_U3CTrackBeatU3Em__0_m1598643413 (BeatTracker_t2801099156 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	{
		// ThreadPool.QueueUserWorkItem(o => FindBeat(), null);
		// ThreadPool.QueueUserWorkItem(o => FindBeat(), null);
		BeatTracker_FindBeat_m212206812(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraMotor::.ctor()
extern "C"  void CameraMotor__ctor_m3922228633 (CameraMotor_t550925884 * __this, const MethodInfo* method)
{
	{
		// private float transition = 0.0f;
		__this->set_transition_5((0.0f));
		// private float animationDuration = 2.0f;
		__this->set_animationDuration_6((2.0f));
		// private Vector3 animationOffset = new Vector3 (0, 5, 5);
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (5.0f), (5.0f), /*hidden argument*/NULL);
		__this->set_animationOffset_7(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraMotor::Start()
extern "C"  void CameraMotor_Start_m1077771969 (CameraMotor_t550925884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraMotor_Start_m1077771969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// lookAt = GameObject.FindGameObjectWithTag ("Player").transform;
		// lookAt = GameObject.FindGameObjectWithTag ("Player").transform;
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		// lookAt = GameObject.FindGameObjectWithTag ("Player").transform;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		__this->set_lookAt_2(L_1);
		// startOffset = transform.position - lookAt.position;
		// startOffset = transform.position - lookAt.position;
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// startOffset = transform.position - lookAt.position;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = __this->get_lookAt_2();
		// startOffset = transform.position - lookAt.position;
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		// startOffset = transform.position - lookAt.position;
		Vector3_t2243707580  L_6 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		__this->set_startOffset_3(L_6);
		// }
		return;
	}
}
// System.Void CameraMotor::Update()
extern "C"  void CameraMotor_Update_m300393638 (CameraMotor_t550925884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraMotor_Update_m300393638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// moveVector = lookAt.position + startOffset;
		Transform_t3275118058 * L_0 = __this->get_lookAt_2();
		// moveVector = lookAt.position + startOffset;
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = __this->get_startOffset_3();
		// moveVector = lookAt.position + startOffset;
		Vector3_t2243707580  L_3 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		__this->set_moveVector_4(L_3);
		// moveVector.x = 0;
		Vector3_t2243707580 * L_4 = __this->get_address_of_moveVector_4();
		L_4->set_x_1((0.0f));
		// moveVector.y = Mathf.Clamp (moveVector.y, 3, 5);
		Vector3_t2243707580 * L_5 = __this->get_address_of_moveVector_4();
		Vector3_t2243707580 * L_6 = __this->get_address_of_moveVector_4();
		float L_7 = L_6->get_y_2();
		// moveVector.y = Mathf.Clamp (moveVector.y, 3, 5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_7, (3.0f), (5.0f), /*hidden argument*/NULL);
		L_5->set_y_2(L_8);
		// if (transition > 1.0f) {
		float L_9 = __this->get_transition_5();
		if ((!(((float)L_9) > ((float)(1.0f)))))
		{
			goto IL_007a;
		}
	}
	{
		// transform.position = moveVector;
		// transform.position = moveVector;
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = __this->get_moveVector_4();
		// transform.position = moveVector;
		NullCheck(L_10);
		Transform_set_position_m2469242620(L_10, L_11, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_007a:
	{
		// transform.position = Vector3.Lerp (moveVector + animationOffset, moveVector, transition);
		// transform.position = Vector3.Lerp (moveVector + animationOffset, moveVector, transition);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = __this->get_moveVector_4();
		Vector3_t2243707580  L_14 = __this->get_animationOffset_7();
		// transform.position = Vector3.Lerp (moveVector + animationOffset, moveVector, transition);
		Vector3_t2243707580  L_15 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = __this->get_moveVector_4();
		float L_17 = __this->get_transition_5();
		// transform.position = Vector3.Lerp (moveVector + animationOffset, moveVector, transition);
		Vector3_t2243707580  L_18 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		// transform.position = Vector3.Lerp (moveVector + animationOffset, moveVector, transition);
		NullCheck(L_12);
		Transform_set_position_m2469242620(L_12, L_18, /*hidden argument*/NULL);
		// transition += Time.deltaTime * 1 / animationDuration;
		float L_19 = __this->get_transition_5();
		// transition += Time.deltaTime * 1 / animationDuration;
		float L_20 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_21 = __this->get_animationDuration_6();
		__this->set_transition_5(((float)((float)L_19+(float)((float)((float)((float)((float)L_20*(float)(1.0f)))/(float)L_21)))));
		// transform.LookAt (lookAt.position + Vector3.up);
		// transform.LookAt (lookAt.position + Vector3.up);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_23 = __this->get_lookAt_2();
		// transform.LookAt (lookAt.position + Vector3.up);
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_position_m1104419803(L_23, /*hidden argument*/NULL);
		// transform.LookAt (lookAt.position + Vector3.up);
		Vector3_t2243707580  L_25 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		// transform.LookAt (lookAt.position + Vector3.up);
		Vector3_t2243707580  L_26 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		// transform.LookAt (lookAt.position + Vector3.up);
		NullCheck(L_22);
		Transform_LookAt_m3314153180(L_22, L_26, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		// }
		return;
	}
}
// System.Void DataController::.ctor()
extern "C"  void DataController__ctor_m774798863 (DataController_t1918319064 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataController::Start()
extern "C"  void DataController_Start_m1356344579 (DataController_t1918319064 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataController_Start_m1356344579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// rhythmTool=GetComponent<RhythmTool>();
		// rhythmTool=GetComponent<RhythmTool>();
		RhythmTool_t215962618 * L_0 = Component_GetComponent_TisRhythmTool_t215962618_m966574361(__this, /*hidden argument*/Component_GetComponent_TisRhythmTool_t215962618_m966574361_MethodInfo_var);
		__this->set_rhythmTool_2(L_0);
		// low = rhythmTool.low;
		RhythmTool_t215962618 * L_1 = __this->get_rhythmTool_2();
		// low = rhythmTool.low;
		NullCheck(L_1);
		AnalysisData_t108342674 * L_2 = RhythmTool_get_low_m2159614661(L_1, /*hidden argument*/NULL);
		__this->set_low_4(L_2);
		// rhythmTool.SongLoaded += OnSongLoaded;
		RhythmTool_t215962618 * L_3 = __this->get_rhythmTool_2();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)DataController_OnSongLoaded_m440076366_MethodInfo_var);
		Action_t3226471752 * L_5 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_5, __this, L_4, /*hidden argument*/NULL);
		// rhythmTool.SongLoaded += OnSongLoaded;
		NullCheck(L_3);
		RhythmTool_add_SongLoaded_m1500519296(L_3, L_5, /*hidden argument*/NULL);
		// rhythmTool.NewSong(audioClip);
		RhythmTool_t215962618 * L_6 = __this->get_rhythmTool_2();
		AudioClip_t1932558630 * L_7 = __this->get_audioClip_3();
		// rhythmTool.NewSong(audioClip);
		NullCheck(L_6);
		RhythmTool_NewSong_m2509631375(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DataController::OnSongLoaded()
extern "C"  void DataController_OnSongLoaded_m440076366 (DataController_t1918319064 * __this, const MethodInfo* method)
{
	{
		// rhythmTool.Play ();
		RhythmTool_t215962618 * L_0 = __this->get_rhythmTool_2();
		// rhythmTool.Play ();
		NullCheck(L_0);
		RhythmTool_Play_m2237616365(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DataController::Update()
extern "C"  void DataController_Update_m2156759382 (DataController_t1918319064 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataController_Update_m2156759382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		// if (!rhythmTool.songLoaded)
		RhythmTool_t215962618 * L_0 = __this->get_rhythmTool_2();
		// if (!rhythmTool.songLoaded)
		NullCheck(L_0);
		bool L_1 = RhythmTool_get_songLoaded_m3458667938(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		// return;
		goto IL_01b5;
	}

IL_0016:
	{
		// int currentFrame = rhythmTool.currentFrame;
		RhythmTool_t215962618 * L_2 = __this->get_rhythmTool_2();
		// int currentFrame = rhythmTool.currentFrame;
		NullCheck(L_2);
		int32_t L_3 = RhythmTool_get_currentFrame_m867144816(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// for(int i = 0; i < 100; i++){
		V_1 = 0;
		goto IL_018f;
	}

IL_0029:
	{
		// int frameIndex = Mathf.Min(currentFrame + i, rhythmTool.totalFrames);
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		RhythmTool_t215962618 * L_6 = __this->get_rhythmTool_2();
		// int frameIndex = Mathf.Min(currentFrame + i, rhythmTool.totalFrames);
		NullCheck(L_6);
		int32_t L_7 = RhythmTool_get_totalFrames_m68629944(L_6, /*hidden argument*/NULL);
		// int frameIndex = Mathf.Min(currentFrame + i, rhythmTool.totalFrames);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_8 = Mathf_Min_m2906823867(NULL /*static, unused*/, ((int32_t)((int32_t)L_4+(int32_t)L_5)), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		// float x = i - rhythmTool.interpolation;
		int32_t L_9 = V_1;
		RhythmTool_t215962618 * L_10 = __this->get_rhythmTool_2();
		// float x = i - rhythmTool.interpolation;
		NullCheck(L_10);
		float L_11 = RhythmTool_get_interpolation_m2645903814(L_10, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((float)L_9)))-(float)L_11));
		// Vector3 start = new Vector3(x, low.magnitude[frameIndex], 0);
		float L_12 = V_3;
		AnalysisData_t108342674 * L_13 = __this->get_low_4();
		// Vector3 start = new Vector3(x, low.magnitude[frameIndex], 0);
		NullCheck(L_13);
		ReadOnlyCollection_1_t2262295624 * L_14 = AnalysisData_get_magnitude_m671389901(L_13, /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		// Vector3 start = new Vector3(x, low.magnitude[frameIndex], 0);
		NullCheck(L_14);
		float L_16 = ReadOnlyCollection_1_get_Item_m210738684(L_14, L_15, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		// Vector3 start = new Vector3(x, low.magnitude[frameIndex], 0);
		Vector3__ctor_m2638739322((&V_4), L_12, L_16, (0.0f), /*hidden argument*/NULL);
		// Vector3 end = new Vector3(x+1, low.magnitude[frameIndex+1], 0);
		float L_17 = V_3;
		AnalysisData_t108342674 * L_18 = __this->get_low_4();
		// Vector3 end = new Vector3(x+1, low.magnitude[frameIndex+1], 0);
		NullCheck(L_18);
		ReadOnlyCollection_1_t2262295624 * L_19 = AnalysisData_get_magnitude_m671389901(L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_2;
		// Vector3 end = new Vector3(x+1, low.magnitude[frameIndex+1], 0);
		NullCheck(L_19);
		float L_21 = ReadOnlyCollection_1_get_Item_m210738684(L_19, ((int32_t)((int32_t)L_20+(int32_t)1)), /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		// Vector3 end = new Vector3(x+1, low.magnitude[frameIndex+1], 0);
		Vector3__ctor_m2638739322((&V_5), ((float)((float)L_17+(float)(1.0f))), L_21, (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.black);
		Vector3_t2243707580  L_22 = V_4;
		Vector3_t2243707580  L_23 = V_5;
		// Debug.DrawLine(start, end, Color.black);
		Color_t2020392075  L_24 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.black);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		// float onset = low.GetOnset(frameIndex);
		AnalysisData_t108342674 * L_25 = __this->get_low_4();
		int32_t L_26 = V_2;
		// float onset = low.GetOnset(frameIndex);
		NullCheck(L_25);
		Onset_t732596509 * L_27 = AnalysisData_GetOnset_m643039293(L_25, L_26, /*hidden argument*/NULL);
		// float onset = low.GetOnset(frameIndex);
		float L_28 = Onset_op_Implicit_m4069753098(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		// if (onset>0)
		float L_29 = V_6;
		if ((!(((float)L_29) > ((float)(0.0f)))))
		{
			goto IL_00ef;
		}
	}
	{
		// start = new Vector3(x,0,0);
		float L_30 = V_3;
		// start = new Vector3(x,0,0);
		Vector3__ctor_m2638739322((&V_4), L_30, (0.0f), (0.0f), /*hidden argument*/NULL);
		// end = new Vector3(x,onset,0);
		float L_31 = V_3;
		float L_32 = V_6;
		// end = new Vector3(x,onset,0);
		Vector3__ctor_m2638739322((&V_5), L_31, L_32, (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start,end,Color.red);
		Vector3_t2243707580  L_33 = V_4;
		Vector3_t2243707580  L_34 = V_5;
		// Debug.DrawLine(start,end,Color.red);
		Color_t2020392075  L_35 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(start,end,Color.red);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_33, L_34, L_35, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		// if(rhythmTool.IsBeat(frameIndex)==1)
		RhythmTool_t215962618 * L_36 = __this->get_rhythmTool_2();
		int32_t L_37 = V_2;
		// if(rhythmTool.IsBeat(frameIndex)==1)
		NullCheck(L_36);
		int32_t L_38 = RhythmTool_IsBeat_m3576622446(L_36, L_37, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_38) == ((uint32_t)1))))
		{
			goto IL_0135;
		}
	}
	{
		// start = new Vector3(x,0,0);
		float L_39 = V_3;
		// start = new Vector3(x,0,0);
		Vector3__ctor_m2638739322((&V_4), L_39, (0.0f), (0.0f), /*hidden argument*/NULL);
		// end = new Vector3(x,10,0);
		float L_40 = V_3;
		// end = new Vector3(x,10,0);
		Vector3__ctor_m2638739322((&V_5), L_40, (10.0f), (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start,end,Color.white);
		Vector3_t2243707580  L_41 = V_4;
		Vector3_t2243707580  L_42 = V_5;
		// Debug.DrawLine(start,end,Color.white);
		Color_t2020392075  L_43 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(start,end,Color.white);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_41, L_42, L_43, /*hidden argument*/NULL);
	}

IL_0135:
	{
		// if(rhythmTool.IsChange(frameIndex))
		RhythmTool_t215962618 * L_44 = __this->get_rhythmTool_2();
		int32_t L_45 = V_2;
		// if(rhythmTool.IsChange(frameIndex))
		NullCheck(L_44);
		bool L_46 = RhythmTool_IsChange_m1817224686(L_44, L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_018a;
		}
	}
	{
		// float change = rhythmTool.NextChange(frameIndex);
		RhythmTool_t215962618 * L_47 = __this->get_rhythmTool_2();
		int32_t L_48 = V_2;
		// float change = rhythmTool.NextChange(frameIndex);
		NullCheck(L_47);
		float L_49 = RhythmTool_NextChange_m3596269439(L_47, L_48, /*hidden argument*/NULL);
		V_7 = L_49;
		// start = new Vector3(x, 0, 0);
		float L_50 = V_3;
		// start = new Vector3(x, 0, 0);
		Vector3__ctor_m2638739322((&V_4), L_50, (0.0f), (0.0f), /*hidden argument*/NULL);
		// end = new Vector3(x, Mathf.Abs(change), 0);
		float L_51 = V_3;
		float L_52 = V_7;
		// end = new Vector3(x, Mathf.Abs(change), 0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_53 = fabsf(L_52);
		// end = new Vector3(x, Mathf.Abs(change), 0);
		Vector3__ctor_m2638739322((&V_5), L_51, L_53, (0.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.yellow);
		Vector3_t2243707580  L_54 = V_4;
		Vector3_t2243707580  L_55 = V_5;
		// Debug.DrawLine(start, end, Color.yellow);
		Color_t2020392075  L_56 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(start, end, Color.yellow);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_54, L_55, L_56, /*hidden argument*/NULL);
	}

IL_018a:
	{
		// for(int i = 0; i < 100; i++){
		int32_t L_57 = V_1;
		V_1 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_018f:
	{
		// for(int i = 0; i < 100; i++){
		int32_t L_58 = V_1;
		if ((((int32_t)L_58) < ((int32_t)((int32_t)100))))
		{
			goto IL_0029;
		}
	}
	{
		// Debug.DrawLine(Vector3.zero,Vector3.up * 100,Color.red);
		Vector3_t2243707580  L_59 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(Vector3.zero,Vector3.up * 100,Color.red);
		Vector3_t2243707580  L_60 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(Vector3.zero,Vector3.up * 100,Color.red);
		Vector3_t2243707580  L_61 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_60, (100.0f), /*hidden argument*/NULL);
		// Debug.DrawLine(Vector3.zero,Vector3.up * 100,Color.red);
		Color_t2020392075  L_62 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(Vector3.zero,Vector3.up * 100,Color.red);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_59, L_61, L_62, /*hidden argument*/NULL);
	}

IL_01b5:
	{
		// }
		return;
	}
}
// System.Void EnemyController::.ctor()
extern "C"  void EnemyController__ctor_m1153179309 (EnemyController_t2146768720 * __this, const MethodInfo* method)
{
	{
		// float time = 0.25f;
		__this->set_time_4((0.25f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyController::Start()
extern "C"  void EnemyController_Start_m2470974037 (EnemyController_t2146768720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_Start_m2470974037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		// gameManagerObj = GameObject.FindWithTag("GameController");
		// gameManagerObj = GameObject.FindWithTag("GameController");
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral40213808, /*hidden argument*/NULL);
		__this->set_gameManagerObj_5(L_0);
		// m_startPos = transform.position;
		// m_startPos = transform.position;
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// m_startPos = transform.position;
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		__this->set_m_startPos_2(L_2);
		// m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
		// m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		// m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_y_2();
		GameObject_t1756533147 * L_9 = __this->get_gameManagerObj_5();
		// m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		// m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_z_3();
		// m_endPos = new Vector3(transform.position.x, transform.position.y, gameManagerObj.transform.position.z);
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, L_5, L_8, L_12, /*hidden argument*/NULL);
		__this->set_m_endPos_3(L_13);
		// }
		return;
	}
}
// System.Void EnemyController::FixedUpdate()
extern "C"  void EnemyController_FixedUpdate_m3241215968 (EnemyController_t2146768720 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// float t = time;  // * gameManagerObj.GetComponent<RhythmEventProvider>().interpolation;
		float L_0 = __this->get_time_4();
		V_0 = L_0;
		// transform.position = Vector3.MoveTowards(this.transform.position,m_endPos,t);
		// transform.position = Vector3.MoveTowards(this.transform.position,m_endPos,t);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// transform.position = Vector3.MoveTowards(this.transform.position,m_endPos,t);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// transform.position = Vector3.MoveTowards(this.transform.position,m_endPos,t);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = __this->get_m_endPos_3();
		float L_5 = V_0;
		// transform.position = Vector3.MoveTowards(this.transform.position,m_endPos,t);
		Vector3_t2243707580  L_6 = Vector3_MoveTowards_m1358638081(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		// transform.position = Vector3.MoveTowards(this.transform.position,m_endPos,t);
		NullCheck(L_1);
		Transform_set_position_m2469242620(L_1, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EventsController::.ctor()
extern "C"  void EventsController__ctor_m3244095524 (EventsController_t2543309541 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventsController::Start()
extern "C"  void EventsController_Start_m1558683108 (EventsController_t2543309541 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventsController_Start_m1558683108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// rhythmTool = GetComponent<RhythmTool>();
		// rhythmTool = GetComponent<RhythmTool>();
		RhythmTool_t215962618 * L_0 = Component_GetComponent_TisRhythmTool_t215962618_m966574361(__this, /*hidden argument*/Component_GetComponent_TisRhythmTool_t215962618_m966574361_MethodInfo_var);
		__this->set_rhythmTool_4(L_0);
		// eventProvider = GetComponent<RhythmEventProvider>();
		// eventProvider = GetComponent<RhythmEventProvider>();
		RhythmEventProvider_t215006757 * L_1 = Component_GetComponent_TisRhythmEventProvider_t215006757_m163918678(__this, /*hidden argument*/Component_GetComponent_TisRhythmEventProvider_t215006757_m163918678_MethodInfo_var);
		__this->set_eventProvider_5(L_1);
		// eventProvider.onSongLoaded.AddListener(OnSongLoaded);
		RhythmEventProvider_t215006757 * L_2 = __this->get_eventProvider_5();
		NullCheck(L_2);
		OnNewSong_t4169165798 * L_3 = L_2->get_onSongLoaded_15();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)EventsController_OnSongLoaded_m327293090_MethodInfo_var);
		UnityAction_2_t1195085273 * L_5 = (UnityAction_2_t1195085273 *)il2cpp_codegen_object_new(UnityAction_2_t1195085273_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m3874320903(L_5, __this, L_4, /*hidden argument*/UnityAction_2__ctor_m3874320903_MethodInfo_var);
		// eventProvider.onSongLoaded.AddListener(OnSongLoaded);
		NullCheck(L_3);
		RhythmEvent_2_AddListener_m3749635587(L_3, L_5, /*hidden argument*/RhythmEvent_2_AddListener_m3749635587_MethodInfo_var);
		// eventProvider.onBeat.AddListener(OnBeat);
		RhythmEventProvider_t215006757 * L_6 = __this->get_eventProvider_5();
		NullCheck(L_6);
		BeatEvent_t33541086 * L_7 = L_6->get_onBeat_9();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)EventsController_OnBeat_m1455163469_MethodInfo_var);
		UnityAction_1_t4062269323 * L_9 = (UnityAction_1_t4062269323 *)il2cpp_codegen_object_new(UnityAction_1_t4062269323_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3300750889(L_9, __this, L_8, /*hidden argument*/UnityAction_1__ctor_m3300750889_MethodInfo_var);
		// eventProvider.onBeat.AddListener(OnBeat);
		NullCheck(L_7);
		RhythmEvent_1_AddListener_m1356841763(L_7, L_9, /*hidden argument*/RhythmEvent_1_AddListener_m1356841763_MethodInfo_var);
		// eventProvider.onSubBeat.AddListener(OnSubBeat);
		RhythmEventProvider_t215006757 * L_10 = __this->get_eventProvider_5();
		NullCheck(L_10);
		SubBeatEvent_t2499915164 * L_11 = L_10->get_onSubBeat_10();
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)EventsController_OnSubBeat_m1296718868_MethodInfo_var);
		UnityAction_2_t771031906 * L_13 = (UnityAction_2_t771031906 *)il2cpp_codegen_object_new(UnityAction_2_t771031906_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m3846259155(L_13, __this, L_12, /*hidden argument*/UnityAction_2__ctor_m3846259155_MethodInfo_var);
		// eventProvider.onSubBeat.AddListener(OnSubBeat);
		NullCheck(L_11);
		RhythmEvent_2_AddListener_m1848967427(L_11, L_13, /*hidden argument*/RhythmEvent_2_AddListener_m1848967427_MethodInfo_var);
		// rhythmTool.NewSong(audioClip);
		RhythmTool_t215962618 * L_14 = __this->get_rhythmTool_4();
		AudioClip_t1932558630 * L_15 = __this->get_audioClip_6();
		// rhythmTool.NewSong(audioClip);
		NullCheck(L_14);
		RhythmTool_NewSong_m2509631375(L_14, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EventsController::OnSongLoaded(System.String,System.Int32)
extern "C"  void EventsController_OnSongLoaded_m327293090 (EventsController_t2543309541 * __this, String_t* ___name0, int32_t ___totalFrames1, const MethodInfo* method)
{
	{
		// rhythmTool.Play();
		RhythmTool_t215962618 * L_0 = __this->get_rhythmTool_4();
		// rhythmTool.Play();
		NullCheck(L_0);
		RhythmTool_Play_m2237616365(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EventsController::OnBeat(Beat)
extern "C"  void EventsController_OnBeat_m1455163469 (EventsController_t2543309541 * __this, Beat_t2695683572 * ___beat0, const MethodInfo* method)
{
	{
		// cube1Transform.localScale = Random.insideUnitSphere;
		Transform_t3275118058 * L_0 = __this->get_cube1Transform_2();
		// cube1Transform.localScale = Random.insideUnitSphere;
		Vector3_t2243707580  L_1 = Random_get_insideUnitSphere_m4012327022(NULL /*static, unused*/, /*hidden argument*/NULL);
		// cube1Transform.localScale = Random.insideUnitSphere;
		NullCheck(L_0);
		Transform_set_localScale_m2325460848(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EventsController::OnSubBeat(Beat,System.Int32)
extern "C"  void EventsController_OnSubBeat_m1296718868 (EventsController_t2543309541 * __this, Beat_t2695683572 * ___beat0, int32_t ___count1, const MethodInfo* method)
{
	{
		// if(count == 0 || count == 2)
		int32_t L_0 = ___count1;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___count1;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_001e;
		}
	}

IL_000e:
	{
		// cube2Transform.localScale = Random.insideUnitSphere;
		Transform_t3275118058 * L_2 = __this->get_cube2Transform_3();
		// cube2Transform.localScale = Random.insideUnitSphere;
		Vector3_t2243707580  L_3 = Random_get_insideUnitSphere_m4012327022(NULL /*static, unused*/, /*hidden argument*/NULL);
		// cube2Transform.localScale = Random.insideUnitSphere;
		NullCheck(L_2);
		Transform_set_localScale_m2325460848(L_2, L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.Void FailScreenButtonControl::.ctor()
extern "C"  void FailScreenButtonControl__ctor_m3713977468 (FailScreenButtonControl_t1753294869 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FailScreenButtonControl::OnRetry()
extern "C"  void FailScreenButtonControl_OnRetry_m917374273 (FailScreenButtonControl_t1753294869 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// string sceneName = SceneManager.GetActiveScene().name;
		Scene_t1684909666  L_0 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		// string sceneName = SceneManager.GetActiveScene().name;
		String_t* L_1 = Scene_get_name_m745914591((&V_1), /*hidden argument*/NULL);
		V_0 = L_1;
		// SceneManager.LoadScene(sceneName);
		String_t* L_2 = V_0;
		// SceneManager.LoadScene(sceneName);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FailScreenButtonControl::OnQuit()
extern "C"  void FailScreenButtonControl_OnQuit_m3118670708 (FailScreenButtonControl_t1753294869 * __this, const MethodInfo* method)
{
	{
		// Application.Quit();
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::.ctor()
extern "C"  void GameManager__ctor_m293624896 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Awake()
extern "C"  void GameManager_Awake_m99497495 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Awake_m99497495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerHealthManager>();
		// playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerHealthManager>();
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		// playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerHealthManager>();
		NullCheck(L_0);
		PlayerHealthManager_t3067865410 * L_1 = GameObject_GetComponent_TisPlayerHealthManager_t3067865410_m1535056983(L_0, /*hidden argument*/GameObject_GetComponent_TisPlayerHealthManager_t3067865410_m1535056983_MethodInfo_var);
		__this->set_playerHealthManager_4(L_1);
		// failScreenCanvas = GameObject.FindWithTag("FailScreen").GetComponent<Canvas>();
		// failScreenCanvas = GameObject.FindWithTag("FailScreen").GetComponent<Canvas>();
		GameObject_t1756533147 * L_2 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1362668110, /*hidden argument*/NULL);
		// failScreenCanvas = GameObject.FindWithTag("FailScreen").GetComponent<Canvas>();
		NullCheck(L_2);
		Canvas_t209405766 * L_3 = GameObject_GetComponent_TisCanvas_t209405766_m195193039(L_2, /*hidden argument*/GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var);
		__this->set_failScreenCanvas_5(L_3);
		// }
		return;
	}
}
// System.Void GameManager::Start()
extern "C"  void GameManager_Start_m2655388892 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Start_m2655388892_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_rhythmTool = GetComponent<RhythmTool>();
		// m_rhythmTool = GetComponent<RhythmTool>();
		RhythmTool_t215962618 * L_0 = Component_GetComponent_TisRhythmTool_t215962618_m966574361(__this, /*hidden argument*/Component_GetComponent_TisRhythmTool_t215962618_m966574361_MethodInfo_var);
		__this->set_m_rhythmTool_2(L_0);
		// m_rhythmTool.NewSong(m_audioClip);
		RhythmTool_t215962618 * L_1 = __this->get_m_rhythmTool_2();
		AudioClip_t1932558630 * L_2 = __this->get_m_audioClip_3();
		// m_rhythmTool.NewSong(m_audioClip);
		NullCheck(L_1);
		RhythmTool_NewSong_m2509631375(L_1, L_2, /*hidden argument*/NULL);
		// m_rhythmTool.SongLoaded += OnSongLoaded;
		RhythmTool_t215962618 * L_3 = __this->get_m_rhythmTool_2();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)GameManager_OnSongLoaded_m1532214553_MethodInfo_var);
		Action_t3226471752 * L_5 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_5, __this, L_4, /*hidden argument*/NULL);
		// m_rhythmTool.SongLoaded += OnSongLoaded;
		NullCheck(L_3);
		RhythmTool_add_SongLoaded_m1500519296(L_3, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::OnSongLoaded()
extern "C"  void GameManager_OnSongLoaded_m1532214553 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		// m_rhythmTool.Play();
		RhythmTool_t215962618 * L_0 = __this->get_m_rhythmTool_2();
		// m_rhythmTool.Play();
		NullCheck(L_0);
		RhythmTool_Play_m2237616365(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::Update()
extern "C"  void GameManager_Update_m969954595 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		// if (playerHealthManager.playerHealth <= 0)
		PlayerHealthManager_t3067865410 * L_0 = __this->get_playerHealthManager_4();
		NullCheck(L_0);
		float L_1 = L_0->get_playerHealth_2();
		if ((!(((float)L_1) <= ((float)(0.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		// m_rhythmTool.Stop();
		RhythmTool_t215962618 * L_2 = __this->get_m_rhythmTool_2();
		// m_rhythmTool.Stop();
		NullCheck(L_2);
		RhythmTool_Stop_m1172477109(L_2, /*hidden argument*/NULL);
		// failScreenCanvas.enabled = true;
		Canvas_t209405766 * L_3 = __this->get_failScreenCanvas_5();
		// failScreenCanvas.enabled = true;
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void HitZoneControl::.ctor()
extern "C"  void HitZoneControl__ctor_m1754069111 (HitZoneControl_t2657704264 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HitZoneControl::Awake()
extern "C"  void HitZoneControl_Awake_m2387973540 (HitZoneControl_t2657704264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitZoneControl_Awake_m2387973540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerHealthManager>();
		// playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerHealthManager>();
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		// playerHealthManager = GameObject.FindWithTag("Player").GetComponent<PlayerHealthManager>();
		NullCheck(L_0);
		PlayerHealthManager_t3067865410 * L_1 = GameObject_GetComponent_TisPlayerHealthManager_t3067865410_m1535056983(L_0, /*hidden argument*/GameObject_GetComponent_TisPlayerHealthManager_t3067865410_m1535056983_MethodInfo_var);
		__this->set_playerHealthManager_4(L_1);
		// }
		return;
	}
}
// System.Void HitZoneControl::OnTriggerStay(UnityEngine.Collider)
extern "C"  void HitZoneControl_OnTriggerStay_m2720416308 (HitZoneControl_t2657704264 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	{
		// if (other.gameObject.layer == this.gameObject.layer)
		Collider_t3497673348 * L_0 = ___other0;
		// if (other.gameObject.layer == this.gameObject.layer)
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		// if (other.gameObject.layer == this.gameObject.layer)
		NullCheck(L_1);
		int32_t L_2 = GameObject_get_layer_m725607808(L_1, /*hidden argument*/NULL);
		// if (other.gameObject.layer == this.gameObject.layer)
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		// if (other.gameObject.layer == this.gameObject.layer)
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m725607808(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0036;
		}
	}
	{
		// m_isInZone = true;
		__this->set_m_isInZone_2((bool)1);
		// m_targetEnemy = other.gameObject;
		Collider_t3497673348 * L_5 = ___other0;
		// m_targetEnemy = other.gameObject;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		__this->set_m_targetEnemy_3(L_6);
		goto IL_003f;
	}

IL_0036:
	{
		// m_isInZone = false;
		__this->set_m_isInZone_2((bool)0);
	}

IL_003f:
	{
		// }
		return;
	}
}
// System.Void HitZoneControl::OnTriggerExit(UnityEngine.Collider)
extern "C"  void HitZoneControl_OnTriggerExit_m1536364767 (HitZoneControl_t2657704264 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	{
		// if (other.gameObject.layer == this.gameObject.layer)
		Collider_t3497673348 * L_0 = ___other0;
		// if (other.gameObject.layer == this.gameObject.layer)
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		// if (other.gameObject.layer == this.gameObject.layer)
		NullCheck(L_1);
		int32_t L_2 = GameObject_get_layer_m725607808(L_1, /*hidden argument*/NULL);
		// if (other.gameObject.layer == this.gameObject.layer)
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		// if (other.gameObject.layer == this.gameObject.layer)
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m725607808(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0035;
		}
	}
	{
		// playerHealthManager.playerHealth -= 1;
		PlayerHealthManager_t3067865410 * L_5 = __this->get_playerHealthManager_4();
		PlayerHealthManager_t3067865410 * L_6 = L_5;
		NullCheck(L_6);
		float L_7 = L_6->get_playerHealth_2();
		NullCheck(L_6);
		L_6->set_playerHealth_2(((float)((float)L_7-(float)(1.0f))));
	}

IL_0035:
	{
		// }
		return;
	}
}
// System.Void HitZoneManager::.ctor()
extern "C"  void HitZoneManager__ctor_m2522452375 (HitZoneManager_t1308216902 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HitZoneManager::Start()
extern "C"  void HitZoneManager_Start_m654590803 (HitZoneManager_t1308216902 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitZoneManager_Start_m654590803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_greenHitZone = GameObject.FindWithTag("HitZone0");
		// m_greenHitZone = GameObject.FindWithTag("HitZone0");
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral2436044795, /*hidden argument*/NULL);
		__this->set_m_greenHitZone_6(L_0);
		// m_redHitZone = GameObject.FindWithTag("HitZone1");
		// m_redHitZone = GameObject.FindWithTag("HitZone1");
		GameObject_t1756533147 * L_1 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral4002128736, /*hidden argument*/NULL);
		__this->set_m_redHitZone_7(L_1);
		// m_yellowHitZone = GameObject.FindWithTag("HitZone2");
		// m_yellowHitZone = GameObject.FindWithTag("HitZone2");
		GameObject_t1756533147 * L_2 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1273245381, /*hidden argument*/NULL);
		__this->set_m_yellowHitZone_9(L_2);
		// m_blueHitZone = GameObject.FindWithTag("HitZone3");
		// m_blueHitZone = GameObject.FindWithTag("HitZone3");
		GameObject_t1756533147 * L_3 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral2839329322, /*hidden argument*/NULL);
		__this->set_m_blueHitZone_8(L_3);
		// }
		return;
	}
}
// System.Void HitZoneManager::FixedUpdate()
extern "C"  void HitZoneManager_FixedUpdate_m1197673902 (HitZoneManager_t1308216902 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitZoneManager_FixedUpdate_m1197673902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_green = m_greenHitZone.GetComponent<HitZoneControl>().m_isInZone;
		GameObject_t1756533147 * L_0 = __this->get_m_greenHitZone_6();
		// m_green = m_greenHitZone.GetComponent<HitZoneControl>().m_isInZone;
		NullCheck(L_0);
		HitZoneControl_t2657704264 * L_1 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_0, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_m_isInZone_2();
		__this->set_m_green_2(L_2);
		// if(m_greenHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		GameObject_t1756533147 * L_3 = __this->get_m_greenHitZone_6();
		// if(m_greenHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		NullCheck(L_3);
		HitZoneControl_t2657704264 * L_4 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_3, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_m_targetEnemy_3();
		// if(m_greenHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		// m_greenTarget = m_greenHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
		GameObject_t1756533147 * L_7 = __this->get_m_greenHitZone_6();
		// m_greenTarget = m_greenHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
		NullCheck(L_7);
		HitZoneControl_t2657704264 * L_8 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_7, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_m_targetEnemy_3();
		__this->set_m_greenTarget_10(L_9);
	}

IL_004a:
	{
		// m_red = m_redHitZone.GetComponent<HitZoneControl>().m_isInZone;
		GameObject_t1756533147 * L_10 = __this->get_m_redHitZone_7();
		// m_red = m_redHitZone.GetComponent<HitZoneControl>().m_isInZone;
		NullCheck(L_10);
		HitZoneControl_t2657704264 * L_11 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_10, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_11);
		bool L_12 = L_11->get_m_isInZone_2();
		__this->set_m_red_3(L_12);
		// if(m_redHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		GameObject_t1756533147 * L_13 = __this->get_m_redHitZone_7();
		// if(m_redHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		NullCheck(L_13);
		HitZoneControl_t2657704264 * L_14 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_13, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = L_14->get_m_targetEnemy_3();
		// if(m_redHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_15, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0093;
		}
	}
	{
		// m_redTarget = m_redHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
		GameObject_t1756533147 * L_17 = __this->get_m_redHitZone_7();
		// m_redTarget = m_redHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
		NullCheck(L_17);
		HitZoneControl_t2657704264 * L_18 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_17, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_18);
		GameObject_t1756533147 * L_19 = L_18->get_m_targetEnemy_3();
		__this->set_m_redTarget_11(L_19);
	}

IL_0093:
	{
		// m_yellow = m_yellowHitZone.GetComponent<HitZoneControl>().m_isInZone;
		GameObject_t1756533147 * L_20 = __this->get_m_yellowHitZone_9();
		// m_yellow = m_yellowHitZone.GetComponent<HitZoneControl>().m_isInZone;
		NullCheck(L_20);
		HitZoneControl_t2657704264 * L_21 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_20, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_21);
		bool L_22 = L_21->get_m_isInZone_2();
		__this->set_m_yellow_4(L_22);
		// if (m_yellowHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		GameObject_t1756533147 * L_23 = __this->get_m_yellowHitZone_9();
		// if (m_yellowHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		NullCheck(L_23);
		HitZoneControl_t2657704264 * L_24 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_23, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = L_24->get_m_targetEnemy_3();
		// if (m_yellowHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_25, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00dc;
		}
	}
	{
		// m_yellowTarget = m_yellowHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
		GameObject_t1756533147 * L_27 = __this->get_m_yellowHitZone_9();
		// m_yellowTarget = m_yellowHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
		NullCheck(L_27);
		HitZoneControl_t2657704264 * L_28 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_27, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = L_28->get_m_targetEnemy_3();
		__this->set_m_yellowTarget_12(L_29);
	}

IL_00dc:
	{
		// m_blue = m_blueHitZone.GetComponent<HitZoneControl>().m_isInZone;
		GameObject_t1756533147 * L_30 = __this->get_m_blueHitZone_8();
		// m_blue = m_blueHitZone.GetComponent<HitZoneControl>().m_isInZone;
		NullCheck(L_30);
		HitZoneControl_t2657704264 * L_31 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_30, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_31);
		bool L_32 = L_31->get_m_isInZone_2();
		__this->set_m_blue_5(L_32);
		// if(m_blueHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		GameObject_t1756533147 * L_33 = __this->get_m_blueHitZone_8();
		// if(m_blueHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		NullCheck(L_33);
		HitZoneControl_t2657704264 * L_34 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_33, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_34);
		GameObject_t1756533147 * L_35 = L_34->get_m_targetEnemy_3();
		// if(m_blueHitZone.GetComponent<HitZoneControl>().m_targetEnemy != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_35, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0125;
		}
	}
	{
		// m_blueTarget = m_blueHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
		GameObject_t1756533147 * L_37 = __this->get_m_blueHitZone_8();
		// m_blueTarget = m_blueHitZone.GetComponent<HitZoneControl>().m_targetEnemy;
		NullCheck(L_37);
		HitZoneControl_t2657704264 * L_38 = GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343(L_37, /*hidden argument*/GameObject_GetComponent_TisHitZoneControl_t2657704264_m178625343_MethodInfo_var);
		NullCheck(L_38);
		GameObject_t1756533147 * L_39 = L_38->get_m_targetEnemy_3();
		__this->set_m_blueTarget_13(L_39);
	}

IL_0125:
	{
		// }
		return;
	}
}
// System.Void Line::.ctor()
extern "C"  void Line__ctor_m3651664013 (Line_t2729441502 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Line::get_index()
extern "C"  int32_t Line_get_index_m1915203340 (Line_t2729441502 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// public int index { get; private set; }
		int32_t L_0 = __this->get_U3CindexU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void Line::set_index(System.Int32)
extern "C"  void Line_set_index_m719778531 (Line_t2729441502 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		// public int index { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CindexU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Line::Init(UnityEngine.Color,System.Single,System.Int32)
extern "C"  void Line_Init_m3596432585 (Line_t2729441502 * __this, Color_t2020392075  ___color0, float ___opacity1, int32_t ___index2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Line_Init_m3596432585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshRenderer_t1268241104 * V_0 = NULL;
	{
		// this.index = index;
		int32_t L_0 = ___index2;
		// this.index = index;
		Line_set_index_m719778531(__this, L_0, /*hidden argument*/NULL);
		// MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
		// MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
		MeshRenderer_t1268241104 * L_1 = Component_GetComponent_TisMeshRenderer_t1268241104_m3460404950(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t1268241104_m3460404950_MethodInfo_var);
		V_0 = L_1;
		// color = Color.Lerp(Color.black, color, opacity * .01f);
		Color_t2020392075  L_2 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_3 = ___color0;
		float L_4 = ___opacity1;
		// color = Color.Lerp(Color.black, color, opacity * .01f);
		Color_t2020392075  L_5 = Color_Lerp_m3323752807(NULL /*static, unused*/, L_2, L_3, ((float)((float)L_4*(float)(0.01f))), /*hidden argument*/NULL);
		___color0 = L_5;
		// meshRenderer.material.SetColor("_TintColor", color);
		MeshRenderer_t1268241104 * L_6 = V_0;
		// meshRenderer.material.SetColor("_TintColor", color);
		NullCheck(L_6);
		Material_t193706927 * L_7 = Renderer_get_material_m2553789785(L_6, /*hidden argument*/NULL);
		Color_t2020392075  L_8 = ___color0;
		// meshRenderer.material.SetColor("_TintColor", color);
		NullCheck(L_7);
		Material_SetColor_m650857509(L_7, _stringLiteral3855512561, L_8, /*hidden argument*/NULL);
		// transform.localScale = new Vector3(.5f, 10, .5f);
		// transform.localScale = new Vector3(.5f, 10, .5f);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// transform.localScale = new Vector3(.5f, 10, .5f);
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322(&L_10, (0.5f), (10.0f), (0.5f), /*hidden argument*/NULL);
		// transform.localScale = new Vector3(.5f, 10, .5f);
		NullCheck(L_9);
		Transform_set_localScale_m2325460848(L_9, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LomontFFT::.ctor()
extern "C"  void LomontFFT__ctor_m1683549620 (LomontFFT_t895069557 * __this, const MethodInfo* method)
{
	{
		// public LomontFFT()
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// A = 0;
		// A = 0;
		LomontFFT_set_A_m3461982773(__this, 0, /*hidden argument*/NULL);
		// B = 1;
		// B = 1;
		LomontFFT_set_B_m1642579824(__this, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LomontFFT::FFT(System.Single[],System.Boolean)
extern "C"  void LomontFFT_FFT_m2236739420 (LomontFFT_t895069557 * __this, SingleU5BU5D_t577127397* ___data0, bool ___forward1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LomontFFT_FFT_m2236739420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	int32_t G_B5_0 = 0;
	{
		// var n = data.Length;
		SingleU5BU5D_t577127397* L_0 = ___data0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		// if ((n & (n - 1)) != 0)
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		if (!((int32_t)((int32_t)L_1&(int32_t)((int32_t)((int32_t)L_2-(int32_t)1)))))
		{
			goto IL_002a;
		}
	}
	{
		// throw new ArgumentException(
		int32_t L_3 = V_0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral2183092574, L_5, _stringLiteral2444813273, /*hidden argument*/NULL);
		// throw new ArgumentException(
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002a:
	{
		// n /= 2;    // n is the number of samples
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8/(int32_t)2));
		// Reverse(data, n); // bit index data reversal
		SingleU5BU5D_t577127397* L_9 = ___data0;
		int32_t L_10 = V_0;
		// Reverse(data, n); // bit index data reversal
		LomontFFT_Reverse_m2434779414(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		// float sign = forward ? B : -B;
		bool L_11 = ___forward1;
		if (!L_11)
		{
			goto IL_0046;
		}
	}
	{
		// float sign = forward ? B : -B;
		int32_t L_12 = LomontFFT_get_B_m513422853(__this, /*hidden argument*/NULL);
		G_B5_0 = L_12;
		goto IL_004d;
	}

IL_0046:
	{
		// float sign = forward ? B : -B;
		int32_t L_13 = LomontFFT_get_B_m513422853(__this, /*hidden argument*/NULL);
		G_B5_0 = ((-L_13));
	}

IL_004d:
	{
		V_1 = (((float)((float)G_B5_0)));
		// var mmax = 1;
		V_2 = 1;
		// while (n > mmax)
		goto IL_0141;
	}

IL_0056:
	{
		// var istep = 2 * mmax;
		int32_t L_14 = V_2;
		V_3 = ((int32_t)((int32_t)2*(int32_t)L_14));
		// var theta = sign * (float)Math.PI / mmax;
		float L_15 = V_1;
		int32_t L_16 = V_2;
		V_4 = ((float)((float)((float)((float)L_15*(float)(3.14159274f)))/(float)(((float)((float)L_16)))));
		// float wr = 1, wi = 0;
		V_5 = (1.0f);
		// float wr = 1, wi = 0;
		V_6 = (0.0f);
		// var wpr = (float)Math.Cos(theta);
		float L_17 = V_4;
		// var wpr = (float)Math.Cos(theta);
		double L_18 = cos((((double)((double)L_17))));
		V_7 = (((float)((float)L_18)));
		// var wpi = (float)Math.Sin(theta);
		float L_19 = V_4;
		// var wpi = (float)Math.Sin(theta);
		double L_20 = sin((((double)((double)L_19))));
		V_8 = (((float)((float)L_20)));
		// for (var m = 0; m < istep; m += 2)
		V_9 = 0;
		goto IL_0136;
	}

IL_0093:
	{
		// for (var k = m; k < 2 * n; k += 2 * istep)
		int32_t L_21 = V_9;
		V_10 = L_21;
		goto IL_0107;
	}

IL_009d:
	{
		// var j = k + istep;
		int32_t L_22 = V_10;
		int32_t L_23 = V_3;
		V_11 = ((int32_t)((int32_t)L_22+(int32_t)L_23));
		// var tempr = wr * data[j] - wi * data[j + 1];
		float L_24 = V_5;
		SingleU5BU5D_t577127397* L_25 = ___data0;
		int32_t L_26 = V_11;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		float L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		float L_29 = V_6;
		SingleU5BU5D_t577127397* L_30 = ___data0;
		int32_t L_31 = V_11;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		float L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		V_12 = ((float)((float)((float)((float)L_24*(float)L_28))-(float)((float)((float)L_29*(float)L_33))));
		// var tempi = wi * data[j] + wr * data[j + 1];
		float L_34 = V_6;
		SingleU5BU5D_t577127397* L_35 = ___data0;
		int32_t L_36 = V_11;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		float L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		float L_39 = V_5;
		SingleU5BU5D_t577127397* L_40 = ___data0;
		int32_t L_41 = V_11;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		float L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		V_13 = ((float)((float)((float)((float)L_34*(float)L_38))+(float)((float)((float)L_39*(float)L_43))));
		// data[j] = data[k] - tempr;
		SingleU5BU5D_t577127397* L_44 = ___data0;
		int32_t L_45 = V_11;
		SingleU5BU5D_t577127397* L_46 = ___data0;
		int32_t L_47 = V_10;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		float L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		float L_50 = V_12;
		NullCheck(L_44);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(L_45), (float)((float)((float)L_49-(float)L_50)));
		// data[j + 1] = data[k + 1] - tempi;
		SingleU5BU5D_t577127397* L_51 = ___data0;
		int32_t L_52 = V_11;
		SingleU5BU5D_t577127397* L_53 = ___data0;
		int32_t L_54 = V_10;
		NullCheck(L_53);
		int32_t L_55 = ((int32_t)((int32_t)L_54+(int32_t)1));
		float L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		float L_57 = V_13;
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_52+(int32_t)1))), (float)((float)((float)L_56-(float)L_57)));
		// data[k] = data[k] + tempr;
		SingleU5BU5D_t577127397* L_58 = ___data0;
		int32_t L_59 = V_10;
		SingleU5BU5D_t577127397* L_60 = ___data0;
		int32_t L_61 = V_10;
		NullCheck(L_60);
		int32_t L_62 = L_61;
		float L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		float L_64 = V_12;
		NullCheck(L_58);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(L_59), (float)((float)((float)L_63+(float)L_64)));
		// data[k + 1] = data[k + 1] + tempi;
		SingleU5BU5D_t577127397* L_65 = ___data0;
		int32_t L_66 = V_10;
		SingleU5BU5D_t577127397* L_67 = ___data0;
		int32_t L_68 = V_10;
		NullCheck(L_67);
		int32_t L_69 = ((int32_t)((int32_t)L_68+(int32_t)1));
		float L_70 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_69));
		float L_71 = V_13;
		NullCheck(L_65);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_66+(int32_t)1))), (float)((float)((float)L_70+(float)L_71)));
		// for (var k = m; k < 2 * n; k += 2 * istep)
		int32_t L_72 = V_10;
		int32_t L_73 = V_3;
		V_10 = ((int32_t)((int32_t)L_72+(int32_t)((int32_t)((int32_t)2*(int32_t)L_73))));
	}

IL_0107:
	{
		// for (var k = m; k < 2 * n; k += 2 * istep)
		int32_t L_74 = V_10;
		int32_t L_75 = V_0;
		if ((((int32_t)L_74) < ((int32_t)((int32_t)((int32_t)2*(int32_t)L_75)))))
		{
			goto IL_009d;
		}
	}
	{
		// var t = wr; // trig recurrence
		float L_76 = V_5;
		V_14 = L_76;
		// wr = wr * wpr - wi * wpi;
		float L_77 = V_5;
		float L_78 = V_7;
		float L_79 = V_6;
		float L_80 = V_8;
		V_5 = ((float)((float)((float)((float)L_77*(float)L_78))-(float)((float)((float)L_79*(float)L_80))));
		// wi = wi * wpr + t * wpi;
		float L_81 = V_6;
		float L_82 = V_7;
		float L_83 = V_14;
		float L_84 = V_8;
		V_6 = ((float)((float)((float)((float)L_81*(float)L_82))+(float)((float)((float)L_83*(float)L_84))));
		// for (var m = 0; m < istep; m += 2)
		int32_t L_85 = V_9;
		V_9 = ((int32_t)((int32_t)L_85+(int32_t)2));
	}

IL_0136:
	{
		// for (var m = 0; m < istep; m += 2)
		int32_t L_86 = V_9;
		int32_t L_87 = V_3;
		if ((((int32_t)L_86) < ((int32_t)L_87)))
		{
			goto IL_0093;
		}
	}
	{
		// mmax = istep;
		int32_t L_88 = V_3;
		V_2 = L_88;
	}

IL_0141:
	{
		// while (n > mmax)
		int32_t L_89 = V_0;
		int32_t L_90 = V_2;
		if ((((int32_t)L_89) > ((int32_t)L_90)))
		{
			goto IL_0056;
		}
	}
	{
		// Scale(data,n, forward);
		SingleU5BU5D_t577127397* L_91 = ___data0;
		int32_t L_92 = V_0;
		bool L_93 = ___forward1;
		// Scale(data,n, forward);
		LomontFFT_Scale_m1113003897(__this, L_91, L_92, L_93, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LomontFFT::TableFFT(System.Single[],System.Boolean)
extern "C"  void LomontFFT_TableFFT_m1393901266 (LomontFFT_t895069557 * __this, SingleU5BU5D_t577127397* ___data0, bool ___forward1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LomontFFT_TableFFT_m1393901266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	int32_t G_B8_0 = 0;
	{
		// var n = data.Length;
		SingleU5BU5D_t577127397* L_0 = ___data0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		// if ((n & (n - 1)) != 0)
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		if (!((int32_t)((int32_t)L_1&(int32_t)((int32_t)((int32_t)L_2-(int32_t)1)))))
		{
			goto IL_002a;
		}
	}
	{
		// throw new ArgumentException(
		int32_t L_3 = V_0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral2183092574, L_5, _stringLiteral2444813273, /*hidden argument*/NULL);
		// throw new ArgumentException(
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002a:
	{
		// n /= 2;    // n is the number of samples
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8/(int32_t)2));
		// Reverse(data, n); // bit index data reversal
		SingleU5BU5D_t577127397* L_9 = ___data0;
		int32_t L_10 = V_0;
		// Reverse(data, n); // bit index data reversal
		LomontFFT_Reverse_m2434779414(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		// if ((cosTable == null) || (cosTable.Length != n))
		SingleU5BU5D_t577127397* L_11 = __this->get_cosTable_2();
		if (!L_11)
		{
			goto IL_004e;
		}
	}
	{
		SingleU5BU5D_t577127397* L_12 = __this->get_cosTable_2();
		NullCheck(L_12);
		int32_t L_13 = V_0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))) == ((int32_t)L_13)))
		{
			goto IL_0055;
		}
	}

IL_004e:
	{
		// Initialize(n);
		int32_t L_14 = V_0;
		// Initialize(n);
		LomontFFT_Initialize_m2055186573(__this, L_14, /*hidden argument*/NULL);
	}

IL_0055:
	{
		// float sign = forward ? B : -B;
		bool L_15 = ___forward1;
		if (!L_15)
		{
			goto IL_0066;
		}
	}
	{
		// float sign = forward ? B : -B;
		int32_t L_16 = LomontFFT_get_B_m513422853(__this, /*hidden argument*/NULL);
		G_B8_0 = L_16;
		goto IL_006d;
	}

IL_0066:
	{
		// float sign = forward ? B : -B;
		int32_t L_17 = LomontFFT_get_B_m513422853(__this, /*hidden argument*/NULL);
		G_B8_0 = ((-L_17));
	}

IL_006d:
	{
		V_1 = (((float)((float)G_B8_0)));
		// var mmax = 1;
		V_2 = 1;
		// var tptr = 0;
		V_3 = 0;
		// while (n > mmax)
		goto IL_0134;
	}

IL_0078:
	{
		// var istep = 2 * mmax;
		int32_t L_18 = V_2;
		V_4 = ((int32_t)((int32_t)2*(int32_t)L_18));
		// for (var m = 0; m < istep; m += 2)
		V_5 = 0;
		goto IL_0127;
	}

IL_0086:
	{
		// var wr = cosTable[tptr];
		SingleU5BU5D_t577127397* L_19 = __this->get_cosTable_2();
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		float L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_6 = L_22;
		// var wi = sign * sinTable[tptr++];
		float L_23 = V_1;
		SingleU5BU5D_t577127397* L_24 = __this->get_sinTable_3();
		int32_t L_25 = V_3;
		int32_t L_26 = L_25;
		V_3 = ((int32_t)((int32_t)L_26+(int32_t)1));
		NullCheck(L_24);
		int32_t L_27 = L_26;
		float L_28 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		V_7 = ((float)((float)L_23*(float)L_28));
		// for (var k = m; k < 2 * n; k += 2 * istep)
		int32_t L_29 = V_5;
		V_8 = L_29;
		goto IL_0116;
	}

IL_00aa:
	{
		// var j = k + istep;
		int32_t L_30 = V_8;
		int32_t L_31 = V_4;
		V_9 = ((int32_t)((int32_t)L_30+(int32_t)L_31));
		// var tempr = wr * data[j] - wi * data[j + 1];
		float L_32 = V_6;
		SingleU5BU5D_t577127397* L_33 = ___data0;
		int32_t L_34 = V_9;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		float L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		float L_37 = V_7;
		SingleU5BU5D_t577127397* L_38 = ___data0;
		int32_t L_39 = V_9;
		NullCheck(L_38);
		int32_t L_40 = ((int32_t)((int32_t)L_39+(int32_t)1));
		float L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		V_10 = ((float)((float)((float)((float)L_32*(float)L_36))-(float)((float)((float)L_37*(float)L_41))));
		// var tempi = wi * data[j] + wr * data[j + 1];
		float L_42 = V_7;
		SingleU5BU5D_t577127397* L_43 = ___data0;
		int32_t L_44 = V_9;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		float L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		float L_47 = V_6;
		SingleU5BU5D_t577127397* L_48 = ___data0;
		int32_t L_49 = V_9;
		NullCheck(L_48);
		int32_t L_50 = ((int32_t)((int32_t)L_49+(int32_t)1));
		float L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		V_11 = ((float)((float)((float)((float)L_42*(float)L_46))+(float)((float)((float)L_47*(float)L_51))));
		// data[j] = data[k] - tempr;
		SingleU5BU5D_t577127397* L_52 = ___data0;
		int32_t L_53 = V_9;
		SingleU5BU5D_t577127397* L_54 = ___data0;
		int32_t L_55 = V_8;
		NullCheck(L_54);
		int32_t L_56 = L_55;
		float L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		float L_58 = V_10;
		NullCheck(L_52);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(L_53), (float)((float)((float)L_57-(float)L_58)));
		// data[j + 1] = data[k + 1] - tempi;
		SingleU5BU5D_t577127397* L_59 = ___data0;
		int32_t L_60 = V_9;
		SingleU5BU5D_t577127397* L_61 = ___data0;
		int32_t L_62 = V_8;
		NullCheck(L_61);
		int32_t L_63 = ((int32_t)((int32_t)L_62+(int32_t)1));
		float L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		float L_65 = V_11;
		NullCheck(L_59);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_60+(int32_t)1))), (float)((float)((float)L_64-(float)L_65)));
		// data[k] = data[k] + tempr;
		SingleU5BU5D_t577127397* L_66 = ___data0;
		int32_t L_67 = V_8;
		SingleU5BU5D_t577127397* L_68 = ___data0;
		int32_t L_69 = V_8;
		NullCheck(L_68);
		int32_t L_70 = L_69;
		float L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		float L_72 = V_10;
		NullCheck(L_66);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(L_67), (float)((float)((float)L_71+(float)L_72)));
		// data[k + 1] = data[k + 1] + tempi;
		SingleU5BU5D_t577127397* L_73 = ___data0;
		int32_t L_74 = V_8;
		SingleU5BU5D_t577127397* L_75 = ___data0;
		int32_t L_76 = V_8;
		NullCheck(L_75);
		int32_t L_77 = ((int32_t)((int32_t)L_76+(int32_t)1));
		float L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		float L_79 = V_11;
		NullCheck(L_73);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_74+(int32_t)1))), (float)((float)((float)L_78+(float)L_79)));
		// for (var k = m; k < 2 * n; k += 2 * istep)
		int32_t L_80 = V_8;
		int32_t L_81 = V_4;
		V_8 = ((int32_t)((int32_t)L_80+(int32_t)((int32_t)((int32_t)2*(int32_t)L_81))));
	}

IL_0116:
	{
		// for (var k = m; k < 2 * n; k += 2 * istep)
		int32_t L_82 = V_8;
		int32_t L_83 = V_0;
		if ((((int32_t)L_82) < ((int32_t)((int32_t)((int32_t)2*(int32_t)L_83)))))
		{
			goto IL_00aa;
		}
	}
	{
		// for (var m = 0; m < istep; m += 2)
		int32_t L_84 = V_5;
		V_5 = ((int32_t)((int32_t)L_84+(int32_t)2));
	}

IL_0127:
	{
		// for (var m = 0; m < istep; m += 2)
		int32_t L_85 = V_5;
		int32_t L_86 = V_4;
		if ((((int32_t)L_85) < ((int32_t)L_86)))
		{
			goto IL_0086;
		}
	}
	{
		// mmax = istep;
		int32_t L_87 = V_4;
		V_2 = L_87;
	}

IL_0134:
	{
		// while (n > mmax)
		int32_t L_88 = V_0;
		int32_t L_89 = V_2;
		if ((((int32_t)L_88) > ((int32_t)L_89)))
		{
			goto IL_0078;
		}
	}
	{
		// Scale(data, n, forward);
		SingleU5BU5D_t577127397* L_90 = ___data0;
		int32_t L_91 = V_0;
		bool L_92 = ___forward1;
		// Scale(data, n, forward);
		LomontFFT_Scale_m1113003897(__this, L_90, L_91, L_92, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LomontFFT::RealFFT(System.Single[],System.Boolean)
extern "C"  void LomontFFT_RealFFT_m1443477460 (LomontFFT_t895069557 * __this, SingleU5BU5D_t577127397* ___data0, bool ___forward1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LomontFFT_RealFFT_m1443477460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	float V_20 = 0.0f;
	float V_21 = 0.0f;
	float V_22 = 0.0f;
	float V_23 = 0.0f;
	float V_24 = 0.0f;
	int32_t V_25 = 0;
	{
		// var n = data.Length; // # of real inputs, 1/2 the complex length
		SingleU5BU5D_t577127397* L_0 = ___data0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		// if ((n & (n - 1)) != 0)
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		if (!((int32_t)((int32_t)L_1&(int32_t)((int32_t)((int32_t)L_2-(int32_t)1)))))
		{
			goto IL_002a;
		}
	}
	{
		// throw new ArgumentException(
		int32_t L_3 = V_0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral2183092574, L_5, _stringLiteral2444813273, /*hidden argument*/NULL);
		// throw new ArgumentException(
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002a:
	{
		// var sign = -1.0f; // assume inverse FFT, this controls how algebra below works
		V_1 = (-1.0f);
		// if (forward)
		bool L_8 = ___forward1;
		if (!L_8)
		{
			goto IL_0094;
		}
	}
	{
		// TableFFT(data, true);
		SingleU5BU5D_t577127397* L_9 = ___data0;
		// TableFFT(data, true);
		LomontFFT_TableFFT_m1393901266(__this, L_9, (bool)1, /*hidden argument*/NULL);
		// sign = 1.0f;
		V_1 = (1.0f);
		// if (A != 1)
		// if (A != 1)
		int32_t L_10 = LomontFFT_get_A_m513422754(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)1)))
		{
			goto IL_0093;
		}
	}
	{
		// var scale = (float)Math.Pow(2.0f, (A - 1) / 2.0f);
		// var scale = (float)Math.Pow(2.0f, (A - 1) / 2.0f);
		int32_t L_11 = LomontFFT_get_A_m513422754(__this, /*hidden argument*/NULL);
		// var scale = (float)Math.Pow(2.0f, (A - 1) / 2.0f);
		double L_12 = pow((2.0), (((double)((double)((float)((float)(((float)((float)((int32_t)((int32_t)L_11-(int32_t)1)))))/(float)(2.0f)))))));
		V_2 = (((float)((float)L_12)));
		// for (var i = 0; i < data.Length; ++i)
		V_3 = 0;
		goto IL_0089;
	}

IL_0079:
	{
		// data[i] *= scale;
		SingleU5BU5D_t577127397* L_13 = ___data0;
		int32_t L_14 = V_3;
		NullCheck(L_13);
		float* L_15 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)));
		float L_16 = V_2;
		*((float*)(L_15)) = (float)((float)((float)(*((float*)L_15))*(float)L_16));
		// for (var i = 0; i < data.Length; ++i)
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0089:
	{
		// for (var i = 0; i < data.Length; ++i)
		int32_t L_18 = V_3;
		SingleU5BU5D_t577127397* L_19 = ___data0;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0079;
		}
	}
	{
	}

IL_0093:
	{
	}

IL_0094:
	{
		// var theta = B * sign * 2 * (float)Math.PI / n;
		// var theta = B * sign * 2 * (float)Math.PI / n;
		int32_t L_20 = LomontFFT_get_B_m513422853(__this, /*hidden argument*/NULL);
		float L_21 = V_1;
		int32_t L_22 = V_0;
		V_4 = ((float)((float)((float)((float)((float)((float)((float)((float)(((float)((float)L_20)))*(float)L_21))*(float)(2.0f)))*(float)(3.14159274f)))/(float)(((float)((float)L_22)))));
		// var wpr = (float)Math.Cos(theta);
		float L_23 = V_4;
		// var wpr = (float)Math.Cos(theta);
		double L_24 = cos((((double)((double)L_23))));
		V_5 = (((float)((float)L_24)));
		// var wpi = (float)Math.Sin(theta);
		float L_25 = V_4;
		// var wpi = (float)Math.Sin(theta);
		double L_26 = sin((((double)((double)L_25))));
		V_6 = (((float)((float)L_26)));
		// var wjr = wpr;
		float L_27 = V_5;
		V_7 = L_27;
		// var wji = wpi;
		float L_28 = V_6;
		V_8 = L_28;
		// for (var j = 1; j <= n/4; ++j)
		V_9 = 1;
		goto IL_01b8;
	}

IL_00d4:
	{
		// var k = n / 2 - j;
		int32_t L_29 = V_0;
		int32_t L_30 = V_9;
		V_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_29/(int32_t)2))-(int32_t)L_30));
		// var tkr = data[2 * k];    // real and imaginary parts of t_k  = t_(n/2 - j)
		SingleU5BU5D_t577127397* L_31 = ___data0;
		int32_t L_32 = V_10;
		NullCheck(L_31);
		int32_t L_33 = ((int32_t)((int32_t)2*(int32_t)L_32));
		float L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		V_11 = L_34;
		// var tki = data[2 * k + 1];
		SingleU5BU5D_t577127397* L_35 = ___data0;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_36))+(int32_t)1));
		float L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		V_12 = L_38;
		// var tjr = data[2 * j];    // real and imaginary parts of t_j
		SingleU5BU5D_t577127397* L_39 = ___data0;
		int32_t L_40 = V_9;
		NullCheck(L_39);
		int32_t L_41 = ((int32_t)((int32_t)2*(int32_t)L_40));
		float L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_13 = L_42;
		// var tji = data[2 * j + 1];
		SingleU5BU5D_t577127397* L_43 = ___data0;
		int32_t L_44 = V_9;
		NullCheck(L_43);
		int32_t L_45 = ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_44))+(int32_t)1));
		float L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		V_14 = L_46;
		// var a = (tjr - tkr) * wji;
		float L_47 = V_13;
		float L_48 = V_11;
		float L_49 = V_8;
		V_15 = ((float)((float)((float)((float)L_47-(float)L_48))*(float)L_49));
		// var b = (tji + tki) * wjr;
		float L_50 = V_14;
		float L_51 = V_12;
		float L_52 = V_7;
		V_16 = ((float)((float)((float)((float)L_50+(float)L_51))*(float)L_52));
		// var c = (tjr - tkr) * wjr;
		float L_53 = V_13;
		float L_54 = V_11;
		float L_55 = V_7;
		V_17 = ((float)((float)((float)((float)L_53-(float)L_54))*(float)L_55));
		// var d = (tji + tki) * wji;
		float L_56 = V_14;
		float L_57 = V_12;
		float L_58 = V_8;
		V_18 = ((float)((float)((float)((float)L_56+(float)L_57))*(float)L_58));
		// var e = (tjr + tkr);
		float L_59 = V_13;
		float L_60 = V_11;
		V_19 = ((float)((float)L_59+(float)L_60));
		// var f = (tji - tki);
		float L_61 = V_14;
		float L_62 = V_12;
		V_20 = ((float)((float)L_61-(float)L_62));
		// data[2 * j] = 0.5f * (e + sign * (a + b));
		SingleU5BU5D_t577127397* L_63 = ___data0;
		int32_t L_64 = V_9;
		float L_65 = V_19;
		float L_66 = V_1;
		float L_67 = V_15;
		float L_68 = V_16;
		NullCheck(L_63);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)2*(int32_t)L_64))), (float)((float)((float)(0.5f)*(float)((float)((float)L_65+(float)((float)((float)L_66*(float)((float)((float)L_67+(float)L_68)))))))));
		// data[2 * j + 1] = 0.5f * (f + sign * (d - c));
		SingleU5BU5D_t577127397* L_69 = ___data0;
		int32_t L_70 = V_9;
		float L_71 = V_20;
		float L_72 = V_1;
		float L_73 = V_18;
		float L_74 = V_17;
		NullCheck(L_69);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_70))+(int32_t)1))), (float)((float)((float)(0.5f)*(float)((float)((float)L_71+(float)((float)((float)L_72*(float)((float)((float)L_73-(float)L_74)))))))));
		// data[2 * k] = 0.5f * (e - sign * (b + a));
		SingleU5BU5D_t577127397* L_75 = ___data0;
		int32_t L_76 = V_10;
		float L_77 = V_19;
		float L_78 = V_1;
		float L_79 = V_16;
		float L_80 = V_15;
		NullCheck(L_75);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)2*(int32_t)L_76))), (float)((float)((float)(0.5f)*(float)((float)((float)L_77-(float)((float)((float)L_78*(float)((float)((float)L_79+(float)L_80)))))))));
		// data[2 * k + 1] = 0.5f * (sign * (d - c) - f);
		SingleU5BU5D_t577127397* L_81 = ___data0;
		int32_t L_82 = V_10;
		float L_83 = V_1;
		float L_84 = V_18;
		float L_85 = V_17;
		float L_86 = V_20;
		NullCheck(L_81);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_82))+(int32_t)1))), (float)((float)((float)(0.5f)*(float)((float)((float)((float)((float)L_83*(float)((float)((float)L_84-(float)L_85))))-(float)L_86)))));
		// var temp = wjr;
		float L_87 = V_7;
		V_21 = L_87;
		// wjr = wjr * wpr - wji * wpi;
		float L_88 = V_7;
		float L_89 = V_5;
		float L_90 = V_8;
		float L_91 = V_6;
		V_7 = ((float)((float)((float)((float)L_88*(float)L_89))-(float)((float)((float)L_90*(float)L_91))));
		// wji = temp * wpi + wji * wpr;
		float L_92 = V_21;
		float L_93 = V_6;
		float L_94 = V_8;
		float L_95 = V_5;
		V_8 = ((float)((float)((float)((float)L_92*(float)L_93))+(float)((float)((float)L_94*(float)L_95))));
		// for (var j = 1; j <= n/4; ++j)
		int32_t L_96 = V_9;
		V_9 = ((int32_t)((int32_t)L_96+(int32_t)1));
	}

IL_01b8:
	{
		// for (var j = 1; j <= n/4; ++j)
		int32_t L_97 = V_9;
		int32_t L_98 = V_0;
		if ((((int32_t)L_97) <= ((int32_t)((int32_t)((int32_t)L_98/(int32_t)4)))))
		{
			goto IL_00d4;
		}
	}
	{
		// if (forward)
		bool L_99 = ___forward1;
		if (!L_99)
		{
			goto IL_01eb;
		}
	}
	{
		// var temp = data[0];
		SingleU5BU5D_t577127397* L_100 = ___data0;
		NullCheck(L_100);
		int32_t L_101 = 0;
		float L_102 = (L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_101));
		V_22 = L_102;
		// data[0] += data[1];
		SingleU5BU5D_t577127397* L_103 = ___data0;
		NullCheck(L_103);
		float* L_104 = ((L_103)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		SingleU5BU5D_t577127397* L_105 = ___data0;
		NullCheck(L_105);
		int32_t L_106 = 1;
		float L_107 = (L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_106));
		*((float*)(L_104)) = (float)((float)((float)(*((float*)L_104))+(float)L_107));
		// data[1] = temp - data[1];
		SingleU5BU5D_t577127397* L_108 = ___data0;
		float L_109 = V_22;
		SingleU5BU5D_t577127397* L_110 = ___data0;
		NullCheck(L_110);
		int32_t L_111 = 1;
		float L_112 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
		NullCheck(L_108);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)((float)((float)L_109-(float)L_112)));
		goto IL_0268;
	}

IL_01eb:
	{
		// var temp = data[0]; // unpack the y0 and y_{N/2}, then invert FFT
		SingleU5BU5D_t577127397* L_113 = ___data0;
		NullCheck(L_113);
		int32_t L_114 = 0;
		float L_115 = (L_113)->GetAt(static_cast<il2cpp_array_size_t>(L_114));
		V_23 = L_115;
		// data[0] = 0.5f * (temp + data[1]);
		SingleU5BU5D_t577127397* L_116 = ___data0;
		float L_117 = V_23;
		SingleU5BU5D_t577127397* L_118 = ___data0;
		NullCheck(L_118);
		int32_t L_119 = 1;
		float L_120 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		NullCheck(L_116);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)((float)((float)(0.5f)*(float)((float)((float)L_117+(float)L_120)))));
		// data[1] = 0.5f * (temp - data[1]);
		SingleU5BU5D_t577127397* L_121 = ___data0;
		float L_122 = V_23;
		SingleU5BU5D_t577127397* L_123 = ___data0;
		NullCheck(L_123);
		int32_t L_124 = 1;
		float L_125 = (L_123)->GetAt(static_cast<il2cpp_array_size_t>(L_124));
		NullCheck(L_121);
		(L_121)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)((float)((float)(0.5f)*(float)((float)((float)L_122-(float)L_125)))));
		// TableFFT(data, false);
		SingleU5BU5D_t577127397* L_126 = ___data0;
		// TableFFT(data, false);
		LomontFFT_TableFFT_m1393901266(__this, L_126, (bool)0, /*hidden argument*/NULL);
		// var scale = (float)Math.Pow(2.0f, -(A + 1) / 2.0f)*2;
		// var scale = (float)Math.Pow(2.0f, -(A + 1) / 2.0f)*2;
		int32_t L_127 = LomontFFT_get_A_m513422754(__this, /*hidden argument*/NULL);
		// var scale = (float)Math.Pow(2.0f, -(A + 1) / 2.0f)*2;
		double L_128 = pow((2.0), (((double)((double)((float)((float)(((float)((float)((-((int32_t)((int32_t)L_127+(int32_t)1)))))))/(float)(2.0f)))))));
		V_24 = ((float)((float)(((float)((float)L_128)))*(float)(2.0f)));
		// for (var i = 0; i < data.Length; ++i)
		V_25 = 0;
		goto IL_025c;
	}

IL_0248:
	{
		// data[i] *= scale;
		SingleU5BU5D_t577127397* L_129 = ___data0;
		int32_t L_130 = V_25;
		NullCheck(L_129);
		float* L_131 = ((L_129)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_130)));
		float L_132 = V_24;
		*((float*)(L_131)) = (float)((float)((float)(*((float*)L_131))*(float)L_132));
		// for (var i = 0; i < data.Length; ++i)
		int32_t L_133 = V_25;
		V_25 = ((int32_t)((int32_t)L_133+(int32_t)1));
	}

IL_025c:
	{
		// for (var i = 0; i < data.Length; ++i)
		int32_t L_134 = V_25;
		SingleU5BU5D_t577127397* L_135 = ___data0;
		NullCheck(L_135);
		if ((((int32_t)L_134) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_135)->max_length)))))))
		{
			goto IL_0248;
		}
	}
	{
	}

IL_0268:
	{
		// }
		return;
	}
}
// System.Int32 LomontFFT::get_A()
extern "C"  int32_t LomontFFT_get_A_m513422754 (LomontFFT_t895069557 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// public int A { get; set; }
		int32_t L_0 = __this->get_U3CAU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void LomontFFT::set_A(System.Int32)
extern "C"  void LomontFFT_set_A_m3461982773 (LomontFFT_t895069557 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		// public int A { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CAU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 LomontFFT::get_B()
extern "C"  int32_t LomontFFT_get_B_m513422853 (LomontFFT_t895069557 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// public int B { get; set; }
		int32_t L_0 = __this->get_U3CBU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void LomontFFT::set_B(System.Int32)
extern "C"  void LomontFFT_set_B_m1642579824 (LomontFFT_t895069557 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		// public int B { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CBU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void LomontFFT::Scale(System.Single[],System.Int32,System.Boolean)
extern "C"  void LomontFFT_Scale_m1113003897 (LomontFFT_t895069557 * __this, SingleU5BU5D_t577127397* ___data0, int32_t ___n1, bool ___forward2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	{
		// if ((forward) && (A != 1))
		bool L_0 = ___forward2;
		if (!L_0)
		{
			goto IL_004e;
		}
	}
	{
		// if ((forward) && (A != 1))
		int32_t L_1 = LomontFFT_get_A_m513422754(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_004e;
		}
	}
	{
		// var scale = (float)Math.Pow(n, (A - 1) / 2.0f);
		int32_t L_2 = ___n1;
		// var scale = (float)Math.Pow(n, (A - 1) / 2.0f);
		int32_t L_3 = LomontFFT_get_A_m513422754(__this, /*hidden argument*/NULL);
		// var scale = (float)Math.Pow(n, (A - 1) / 2.0f);
		double L_4 = pow((((double)((double)L_2))), (((double)((double)((float)((float)(((float)((float)((int32_t)((int32_t)L_3-(int32_t)1)))))/(float)(2.0f)))))));
		V_0 = (((float)((float)L_4)));
		// for (var i = 0; i < data.Length; ++i)
		V_1 = 0;
		goto IL_0044;
	}

IL_0034:
	{
		// data[i] *= scale;
		SingleU5BU5D_t577127397* L_5 = ___data0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		float* L_7 = ((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)));
		float L_8 = V_0;
		*((float*)(L_7)) = (float)((float)((float)(*((float*)L_7))*(float)L_8));
		// for (var i = 0; i < data.Length; ++i)
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0044:
	{
		// for (var i = 0; i < data.Length; ++i)
		int32_t L_10 = V_1;
		SingleU5BU5D_t577127397* L_11 = ___data0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0034;
		}
	}
	{
	}

IL_004e:
	{
		// if ((!forward) && (A != -1))
		bool L_12 = ___forward2;
		if (L_12)
		{
			goto IL_009c;
		}
	}
	{
		// if ((!forward) && (A != -1))
		int32_t L_13 = LomontFFT_get_A_m513422754(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_13) == ((int32_t)(-1))))
		{
			goto IL_009c;
		}
	}
	{
		// var scale = (float)Math.Pow(n, -(A + 1) / 2.0f);
		int32_t L_14 = ___n1;
		// var scale = (float)Math.Pow(n, -(A + 1) / 2.0f);
		int32_t L_15 = LomontFFT_get_A_m513422754(__this, /*hidden argument*/NULL);
		// var scale = (float)Math.Pow(n, -(A + 1) / 2.0f);
		double L_16 = pow((((double)((double)L_14))), (((double)((double)((float)((float)(((float)((float)((-((int32_t)((int32_t)L_15+(int32_t)1)))))))/(float)(2.0f)))))));
		V_2 = (((float)((float)L_16)));
		// for (var i = 0; i < data.Length; ++i)
		V_3 = 0;
		goto IL_0092;
	}

IL_0082:
	{
		// data[i] *= scale;
		SingleU5BU5D_t577127397* L_17 = ___data0;
		int32_t L_18 = V_3;
		NullCheck(L_17);
		float* L_19 = ((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)));
		float L_20 = V_2;
		*((float*)(L_19)) = (float)((float)((float)(*((float*)L_19))*(float)L_20));
		// for (var i = 0; i < data.Length; ++i)
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0092:
	{
		// for (var i = 0; i < data.Length; ++i)
		int32_t L_22 = V_3;
		SingleU5BU5D_t577127397* L_23 = ___data0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_0082;
		}
	}
	{
	}

IL_009c:
	{
		// }
		return;
	}
}
// System.Void LomontFFT::Initialize(System.Int32)
extern "C"  void LomontFFT_Initialize_m2055186573 (LomontFFT_t895069557 * __this, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LomontFFT_Initialize_m2055186573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	{
		// cosTable = new float[size];
		int32_t L_0 = ___size0;
		__this->set_cosTable_2(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		// sinTable = new float[size];
		int32_t L_1 = ___size0;
		__this->set_sinTable_3(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		// var n = size;
		int32_t L_2 = ___size0;
		V_0 = L_2;
		// int mmax = 1, pos = 0;
		V_1 = 1;
		// int mmax = 1, pos = 0;
		V_2 = 0;
		// while (n > mmax)
		goto IL_00c1;
	}

IL_0024:
	{
		// var istep = 2 * mmax;
		int32_t L_3 = V_1;
		V_3 = ((int32_t)((int32_t)2*(int32_t)L_3));
		// var theta = (float)Math.PI / mmax;
		int32_t L_4 = V_1;
		V_4 = ((float)((float)(3.14159274f)/(float)(((float)((float)L_4)))));
		// float wr = 1, wi = 0;
		V_5 = (1.0f);
		// float wr = 1, wi = 0;
		V_6 = (0.0f);
		// var wpi = (float)Math.Sin(theta);
		float L_5 = V_4;
		// var wpi = (float)Math.Sin(theta);
		double L_6 = sin((((double)((double)L_5))));
		V_7 = (((float)((float)L_6)));
		// var wpr = (float)Math.Sin(theta / 2);
		float L_7 = V_4;
		// var wpr = (float)Math.Sin(theta / 2);
		double L_8 = sin((((double)((double)((float)((float)L_7/(float)(2.0f)))))));
		V_8 = (((float)((float)L_8)));
		// wpr = -2 * wpr * wpr;
		float L_9 = V_8;
		float L_10 = V_8;
		V_8 = ((float)((float)((float)((float)(-2.0f)*(float)L_9))*(float)L_10));
		// for (var m = 0; m < istep; m += 2)
		V_9 = 0;
		goto IL_00b6;
	}

IL_0072:
	{
		// cosTable[pos] = wr;
		SingleU5BU5D_t577127397* L_11 = __this->get_cosTable_2();
		int32_t L_12 = V_2;
		float L_13 = V_5;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (float)L_13);
		// sinTable[pos++] = wi;
		SingleU5BU5D_t577127397* L_14 = __this->get_sinTable_3();
		int32_t L_15 = V_2;
		int32_t L_16 = L_15;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
		float L_17 = V_6;
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (float)L_17);
		// var t = wr;
		float L_18 = V_5;
		V_10 = L_18;
		// wr = wr * wpr - wi * wpi + wr;
		float L_19 = V_5;
		float L_20 = V_8;
		float L_21 = V_6;
		float L_22 = V_7;
		float L_23 = V_5;
		V_5 = ((float)((float)((float)((float)((float)((float)L_19*(float)L_20))-(float)((float)((float)L_21*(float)L_22))))+(float)L_23));
		// wi = wi * wpr + t * wpi + wi;
		float L_24 = V_6;
		float L_25 = V_8;
		float L_26 = V_10;
		float L_27 = V_7;
		float L_28 = V_6;
		V_6 = ((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)L_28));
		// for (var m = 0; m < istep; m += 2)
		int32_t L_29 = V_9;
		V_9 = ((int32_t)((int32_t)L_29+(int32_t)2));
	}

IL_00b6:
	{
		// for (var m = 0; m < istep; m += 2)
		int32_t L_30 = V_9;
		int32_t L_31 = V_3;
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_0072;
		}
	}
	{
		// mmax = istep;
		int32_t L_32 = V_3;
		V_1 = L_32;
	}

IL_00c1:
	{
		// while (n > mmax)
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		if ((((int32_t)L_33) > ((int32_t)L_34)))
		{
			goto IL_0024;
		}
	}
	{
		// }
		return;
	}
}
// System.Void LomontFFT::Reverse(System.Single[],System.Int32)
extern "C"  void LomontFFT_Reverse_m2434779414 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___data0, int32_t ___n1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		// int j = 0, k = 0; // Knuth R1: initialize
		V_0 = 0;
		// int j = 0, k = 0; // Knuth R1: initialize
		V_1 = 0;
		// var top = n / 2;  // this is Knuth's 2^(n-1)
		int32_t L_0 = ___n1;
		V_2 = ((int32_t)((int32_t)L_0/(int32_t)2));
	}

IL_0009:
	{
		// var t = data[j + 2];
		SingleU5BU5D_t577127397* L_1 = ___data0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)2));
		float L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_3 = L_4;
		// data[j + 2] = data[k + n];
		SingleU5BU5D_t577127397* L_5 = ___data0;
		int32_t L_6 = V_0;
		SingleU5BU5D_t577127397* L_7 = ___data0;
		int32_t L_8 = V_1;
		int32_t L_9 = ___n1;
		NullCheck(L_7);
		int32_t L_10 = ((int32_t)((int32_t)L_8+(int32_t)L_9));
		float L_11 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_6+(int32_t)2))), (float)L_11);
		// data[k + n] = t;
		SingleU5BU5D_t577127397* L_12 = ___data0;
		int32_t L_13 = V_1;
		int32_t L_14 = ___n1;
		float L_15 = V_3;
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_13+(int32_t)L_14))), (float)L_15);
		// t = data[j + 3];
		SingleU5BU5D_t577127397* L_16 = ___data0;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)3));
		float L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_3 = L_19;
		// data[j + 3] = data[k + n + 1];
		SingleU5BU5D_t577127397* L_20 = ___data0;
		int32_t L_21 = V_0;
		SingleU5BU5D_t577127397* L_22 = ___data0;
		int32_t L_23 = V_1;
		int32_t L_24 = ___n1;
		NullCheck(L_22);
		int32_t L_25 = ((int32_t)((int32_t)((int32_t)((int32_t)L_23+(int32_t)L_24))+(int32_t)1));
		float L_26 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_21+(int32_t)3))), (float)L_26);
		// data[k + n + 1] = t;
		SingleU5BU5D_t577127397* L_27 = ___data0;
		int32_t L_28 = V_1;
		int32_t L_29 = ___n1;
		float L_30 = V_3;
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_28+(int32_t)L_29))+(int32_t)1))), (float)L_30);
		// if (j > k)
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_00a4;
		}
	}
	{
		// t = data[j];
		SingleU5BU5D_t577127397* L_33 = ___data0;
		int32_t L_34 = V_0;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		float L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		V_3 = L_36;
		// data[j] = data[k];
		SingleU5BU5D_t577127397* L_37 = ___data0;
		int32_t L_38 = V_0;
		SingleU5BU5D_t577127397* L_39 = ___data0;
		int32_t L_40 = V_1;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		float L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (float)L_42);
		// data[k] = t;
		SingleU5BU5D_t577127397* L_43 = ___data0;
		int32_t L_44 = V_1;
		float L_45 = V_3;
		NullCheck(L_43);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(L_44), (float)L_45);
		// t = data[j + 1];
		SingleU5BU5D_t577127397* L_46 = ___data0;
		int32_t L_47 = V_0;
		NullCheck(L_46);
		int32_t L_48 = ((int32_t)((int32_t)L_47+(int32_t)1));
		float L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		V_3 = L_49;
		// data[j + 1] = data[k + 1];
		SingleU5BU5D_t577127397* L_50 = ___data0;
		int32_t L_51 = V_0;
		SingleU5BU5D_t577127397* L_52 = ___data0;
		int32_t L_53 = V_1;
		NullCheck(L_52);
		int32_t L_54 = ((int32_t)((int32_t)L_53+(int32_t)1));
		float L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_51+(int32_t)1))), (float)L_55);
		// data[k + 1] = t;
		SingleU5BU5D_t577127397* L_56 = ___data0;
		int32_t L_57 = V_1;
		float L_58 = V_3;
		NullCheck(L_56);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_57+(int32_t)1))), (float)L_58);
		// t = data[j + n + 2];
		SingleU5BU5D_t577127397* L_59 = ___data0;
		int32_t L_60 = V_0;
		int32_t L_61 = ___n1;
		NullCheck(L_59);
		int32_t L_62 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60+(int32_t)L_61))+(int32_t)2));
		float L_63 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		V_3 = L_63;
		// data[j + n + 2] = data[k + n + 2];
		SingleU5BU5D_t577127397* L_64 = ___data0;
		int32_t L_65 = V_0;
		int32_t L_66 = ___n1;
		SingleU5BU5D_t577127397* L_67 = ___data0;
		int32_t L_68 = V_1;
		int32_t L_69 = ___n1;
		NullCheck(L_67);
		int32_t L_70 = ((int32_t)((int32_t)((int32_t)((int32_t)L_68+(int32_t)L_69))+(int32_t)2));
		float L_71 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck(L_64);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_65+(int32_t)L_66))+(int32_t)2))), (float)L_71);
		// data[k + n + 2] = t;
		SingleU5BU5D_t577127397* L_72 = ___data0;
		int32_t L_73 = V_1;
		int32_t L_74 = ___n1;
		float L_75 = V_3;
		NullCheck(L_72);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_73+(int32_t)L_74))+(int32_t)2))), (float)L_75);
		// t = data[j + n + 3];
		SingleU5BU5D_t577127397* L_76 = ___data0;
		int32_t L_77 = V_0;
		int32_t L_78 = ___n1;
		NullCheck(L_76);
		int32_t L_79 = ((int32_t)((int32_t)((int32_t)((int32_t)L_77+(int32_t)L_78))+(int32_t)3));
		float L_80 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		V_3 = L_80;
		// data[j + n + 3] = data[k + n + 3];
		SingleU5BU5D_t577127397* L_81 = ___data0;
		int32_t L_82 = V_0;
		int32_t L_83 = ___n1;
		SingleU5BU5D_t577127397* L_84 = ___data0;
		int32_t L_85 = V_1;
		int32_t L_86 = ___n1;
		NullCheck(L_84);
		int32_t L_87 = ((int32_t)((int32_t)((int32_t)((int32_t)L_85+(int32_t)L_86))+(int32_t)3));
		float L_88 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_87));
		NullCheck(L_81);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_82+(int32_t)L_83))+(int32_t)3))), (float)L_88);
		// data[k + n + 3] = t;
		SingleU5BU5D_t577127397* L_89 = ___data0;
		int32_t L_90 = V_1;
		int32_t L_91 = ___n1;
		float L_92 = V_3;
		NullCheck(L_89);
		(L_89)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_90+(int32_t)L_91))+(int32_t)3))), (float)L_92);
	}

IL_00a4:
	{
		// k += 4;
		int32_t L_93 = V_1;
		V_1 = ((int32_t)((int32_t)L_93+(int32_t)4));
		// if (k >= n)
		int32_t L_94 = V_1;
		int32_t L_95 = ___n1;
		if ((((int32_t)L_94) < ((int32_t)L_95)))
		{
			goto IL_00b4;
		}
	}
	{
		// break;
		goto IL_00dc;
	}

IL_00b4:
	{
		// var h = top;
		int32_t L_96 = V_2;
		V_4 = L_96;
		// while (j >= h)
		goto IL_00c9;
	}

IL_00bc:
	{
		// j -= h;
		int32_t L_97 = V_0;
		int32_t L_98 = V_4;
		V_0 = ((int32_t)((int32_t)L_97-(int32_t)L_98));
		// h /= 2;
		int32_t L_99 = V_4;
		V_4 = ((int32_t)((int32_t)L_99/(int32_t)2));
	}

IL_00c9:
	{
		// while (j >= h)
		int32_t L_100 = V_0;
		int32_t L_101 = V_4;
		if ((((int32_t)L_100) >= ((int32_t)L_101)))
		{
			goto IL_00bc;
		}
	}
	{
		// j += h;
		int32_t L_102 = V_0;
		int32_t L_103 = V_4;
		V_0 = ((int32_t)((int32_t)L_102+(int32_t)L_103));
		goto IL_0009;
	}

IL_00dc:
	{
		// }
		return;
	}
}
// System.Void Onset::.ctor(System.Int32,System.Single,System.Int32)
extern "C"  void Onset__ctor_m2770042849 (Onset_t732596509 * __this, int32_t ___index0, float ___strength1, int32_t ___rank2, const MethodInfo* method)
{
	{
		// public Onset(int index, float strength, int rank)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// this.index = index;
		int32_t L_0 = ___index0;
		__this->set_index_0(L_0);
		// this.rank = rank;
		int32_t L_1 = ___rank2;
		__this->set_rank_2(L_1);
		// this.strength = strength;
		float L_2 = ___strength1;
		__this->set_strength_1(L_2);
		// }
		return;
	}
}
// System.Boolean Onset::op_LessThan(Onset,Onset)
extern "C"  bool Onset_op_LessThan_m3548526260 (Il2CppObject * __this /* static, unused */, Onset_t732596509 * ___x0, Onset_t732596509 * ___y1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// if (x == null && y == null)
		Onset_t732596509 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Onset_t732596509 * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_0045;
	}

IL_0015:
	{
		// if (x == null)
		Onset_t732596509 * L_2 = ___x0;
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		// return true;
		V_0 = (bool)1;
		goto IL_0045;
	}

IL_0023:
	{
		// if (y == null)
		Onset_t732596509 * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_0045;
	}

IL_0031:
	{
		// return x.strength < y.strength;
		Onset_t732596509 * L_4 = ___x0;
		NullCheck(L_4);
		float L_5 = L_4->get_strength_1();
		Onset_t732596509 * L_6 = ___y1;
		NullCheck(L_6);
		float L_7 = L_6->get_strength_1();
		V_0 = (bool)((((float)L_5) < ((float)L_7))? 1 : 0);
		goto IL_0045;
	}

IL_0045:
	{
		// }
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean Onset::op_LessThan(Onset,System.Single)
extern "C"  bool Onset_op_LessThan_m1075825226 (Il2CppObject * __this /* static, unused */, Onset_t732596509 * ___x0, float ___y1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// if (x == null)
		Onset_t732596509 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// return true;
		V_0 = (bool)1;
		goto IL_001e;
	}

IL_000f:
	{
		// return x.strength < y;
		Onset_t732596509 * L_1 = ___x0;
		NullCheck(L_1);
		float L_2 = L_1->get_strength_1();
		float L_3 = ___y1;
		V_0 = (bool)((((float)L_2) < ((float)L_3))? 1 : 0);
		goto IL_001e;
	}

IL_001e:
	{
		// }
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean Onset::op_GreaterThan(Onset,Onset)
extern "C"  bool Onset_op_GreaterThan_m3073515561 (Il2CppObject * __this /* static, unused */, Onset_t732596509 * ___x0, Onset_t732596509 * ___y1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// if (x == null && y == null)
		Onset_t732596509 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Onset_t732596509 * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_0045;
	}

IL_0015:
	{
		// if (x == null)
		Onset_t732596509 * L_2 = ___x0;
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_0045;
	}

IL_0023:
	{
		// if (y == null)
		Onset_t732596509 * L_3 = ___y1;
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		// return true;
		V_0 = (bool)1;
		goto IL_0045;
	}

IL_0031:
	{
		// return x.strength > y.strength;
		Onset_t732596509 * L_4 = ___x0;
		NullCheck(L_4);
		float L_5 = L_4->get_strength_1();
		Onset_t732596509 * L_6 = ___y1;
		NullCheck(L_6);
		float L_7 = L_6->get_strength_1();
		V_0 = (bool)((((float)L_5) > ((float)L_7))? 1 : 0);
		goto IL_0045;
	}

IL_0045:
	{
		// }
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean Onset::op_GreaterThan(Onset,System.Single)
extern "C"  bool Onset_op_GreaterThan_m1430579383 (Il2CppObject * __this /* static, unused */, Onset_t732596509 * ___x0, float ___y1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// if (x == null)
		Onset_t732596509 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_001e;
	}

IL_000f:
	{
		// return x.strength > y;
		Onset_t732596509 * L_1 = ___x0;
		NullCheck(L_1);
		float L_2 = L_1->get_strength_1();
		float L_3 = ___y1;
		V_0 = (bool)((((float)L_2) > ((float)L_3))? 1 : 0);
		goto IL_001e;
	}

IL_001e:
	{
		// }
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Single Onset::op_Implicit(Onset)
extern "C"  float Onset_op_Implicit_m4069753098 (Il2CppObject * __this /* static, unused */, Onset_t732596509 * ___x0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// if (x == null)
		Onset_t732596509 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		// return 0;
		V_0 = (0.0f);
		goto IL_001e;
	}

IL_0012:
	{
		// return x.strength;
		Onset_t732596509 * L_1 = ___x0;
		NullCheck(L_1);
		float L_2 = L_1->get_strength_1();
		V_0 = L_2;
		goto IL_001e;
	}

IL_001e:
	{
		// }
		float L_3 = V_0;
		return L_3;
	}
}
// System.Void PlayerHealthManager::.ctor()
extern "C"  void PlayerHealthManager__ctor_m37655655 (PlayerHealthManager_t3067865410 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerHealthManager::Awake()
extern "C"  void PlayerHealthManager_Awake_m2582082686 (PlayerHealthManager_t3067865410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerHealthManager_Awake_m2582082686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// playerHealthBar = GameObject.FindWithTag("PlayerHealthBar").GetComponent<Slider>();
		// playerHealthBar = GameObject.FindWithTag("PlayerHealthBar").GetComponent<Slider>();
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral975222662, /*hidden argument*/NULL);
		// playerHealthBar = GameObject.FindWithTag("PlayerHealthBar").GetComponent<Slider>();
		NullCheck(L_0);
		Slider_t297367283 * L_1 = GameObject_GetComponent_TisSlider_t297367283_m33033319(L_0, /*hidden argument*/GameObject_GetComponent_TisSlider_t297367283_m33033319_MethodInfo_var);
		__this->set_playerHealthBar_4(L_1);
		// }
		return;
	}
}
// System.Void PlayerHealthManager::Start()
extern "C"  void PlayerHealthManager_Start_m1524183795 (PlayerHealthManager_t3067865410 * __this, const MethodInfo* method)
{
	{
		// playerHealthBar.minValue = 0;
		Slider_t297367283 * L_0 = __this->get_playerHealthBar_4();
		// playerHealthBar.minValue = 0;
		NullCheck(L_0);
		Slider_set_minValue_m1484509981(L_0, (0.0f), /*hidden argument*/NULL);
		// playerHealthBar.maxValue = playerMaxHealth;
		Slider_t297367283 * L_1 = __this->get_playerHealthBar_4();
		float L_2 = __this->get_playerMaxHealth_3();
		// playerHealthBar.maxValue = playerMaxHealth;
		NullCheck(L_1);
		Slider_set_maxValue_m2951480075(L_1, L_2, /*hidden argument*/NULL);
		// playerHealth = playerMaxHealth;
		float L_3 = __this->get_playerMaxHealth_3();
		__this->set_playerHealth_2(L_3);
		// }
		return;
	}
}
// System.Void PlayerHealthManager::Update()
extern "C"  void PlayerHealthManager_Update_m2577932396 (PlayerHealthManager_t3067865410 * __this, const MethodInfo* method)
{
	{
		// playerHealthBar.value = playerHealth;
		Slider_t297367283 * L_0 = __this->get_playerHealthBar_4();
		float L_1 = __this->get_playerHealth_2();
		// playerHealthBar.value = playerHealth;
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_0, L_1);
		// }
		return;
	}
}
// System.Void PlayerMotor::.ctor()
extern "C"  void PlayerMotor__ctor_m2810060059 (PlayerMotor_t2528789646 * __this, const MethodInfo* method)
{
	{
		// private float speed = 5.0f;
		__this->set_speed_4((5.0f));
		// private float verticalVelocity = 0.0f;
		__this->set_verticalVelocity_5((0.0f));
		// private float gravity = 12.0f;
		__this->set_gravity_6((12.0f));
		// private float animationDuration = 2.0f;
		__this->set_animationDuration_7((2.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerMotor::Start()
extern "C"  void PlayerMotor_Start_m2017763775 (PlayerMotor_t2528789646 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMotor_Start_m2017763775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// controller = GetComponent<CharacterController> ();
		// controller = GetComponent<CharacterController> ();
		CharacterController_t4094781467 * L_0 = Component_GetComponent_TisCharacterController_t4094781467_m1582798737(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_t4094781467_m1582798737_MethodInfo_var);
		__this->set_controller_2(L_0);
		// }
		return;
	}
}
// System.Void PlayerMotor::Update()
extern "C"  void PlayerMotor_Update_m80195384 (PlayerMotor_t2528789646 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMotor_Update_m80195384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Time.time <animationDuration)
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_animationDuration_7();
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_003d;
		}
	}
	{
		// controller.Move (Vector3.forward * speed * Time.deltaTime);
		CharacterController_t4094781467 * L_2 = __this->get_controller_2();
		// controller.Move (Vector3.forward * speed * Time.deltaTime);
		Vector3_t2243707580  L_3 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_speed_4();
		// controller.Move (Vector3.forward * speed * Time.deltaTime);
		Vector3_t2243707580  L_5 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		// controller.Move (Vector3.forward * speed * Time.deltaTime);
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		// controller.Move (Vector3.forward * speed * Time.deltaTime);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		// controller.Move (Vector3.forward * speed * Time.deltaTime);
		NullCheck(L_2);
		CharacterController_Move_m3456882757(L_2, L_7, /*hidden argument*/NULL);
		// return;
		goto IL_00f0;
	}

IL_003d:
	{
		// moveVector = Vector3.zero;
		// moveVector = Vector3.zero;
		Vector3_t2243707580  L_8 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_moveVector_3(L_8);
		// if (controller.isGrounded)
		CharacterController_t4094781467 * L_9 = __this->get_controller_2();
		// if (controller.isGrounded)
		NullCheck(L_9);
		bool L_10 = CharacterController_get_isGrounded_m2594228107(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006a;
		}
	}
	{
		// verticalVelocity = -0.5f;
		__this->set_verticalVelocity_5((-0.5f));
		goto IL_0085;
	}

IL_006a:
	{
		// verticalVelocity -= gravity * Time.deltaTime;
		float L_11 = __this->get_verticalVelocity_5();
		float L_12 = __this->get_gravity_6();
		// verticalVelocity -= gravity * Time.deltaTime;
		float L_13 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_verticalVelocity_5(((float)((float)L_11-(float)((float)((float)L_12*(float)L_13)))));
	}

IL_0085:
	{
		// moveVector.x = Input.GetAxisRaw("Horizontal") * speed;
		Vector3_t2243707580 * L_14 = __this->get_address_of_moveVector_3();
		// moveVector.x = Input.GetAxisRaw("Horizontal") * speed;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_15 = Input_GetAxisRaw_m4133353720(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		float L_16 = __this->get_speed_4();
		L_14->set_x_1(((float)((float)L_15*(float)L_16)));
		// moveVector.y = verticalVelocity;
		Vector3_t2243707580 * L_17 = __this->get_address_of_moveVector_3();
		float L_18 = __this->get_verticalVelocity_5();
		L_17->set_y_2(L_18);
		// moveVector.z = speed;
		Vector3_t2243707580 * L_19 = __this->get_address_of_moveVector_3();
		float L_20 = __this->get_speed_4();
		L_19->set_z_3(L_20);
		// controller.Move (moveVector * Time.deltaTime);
		CharacterController_t4094781467 * L_21 = __this->get_controller_2();
		Vector3_t2243707580  L_22 = __this->get_moveVector_3();
		// controller.Move (moveVector * Time.deltaTime);
		float L_23 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		// controller.Move (moveVector * Time.deltaTime);
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		// controller.Move (moveVector * Time.deltaTime);
		NullCheck(L_21);
		CharacterController_Move_m3456882757(L_21, L_24, /*hidden argument*/NULL);
		// GetComponent<Animation>().Play ("run");
		// GetComponent<Animation>().Play ("run");
		Animation_t2068071072 * L_25 = Component_GetComponent_TisAnimation_t2068071072_m2503703020(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m2503703020_MethodInfo_var);
		// GetComponent<Animation>().Play ("run");
		NullCheck(L_25);
		Animation_Play_m976361057(L_25, _stringLiteral2309168105, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		// }
		return;
	}
}
// System.Void PlayerUserControl::.ctor()
extern "C"  void PlayerUserControl__ctor_m4234692410 (PlayerUserControl_t2572554735 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerUserControl::Start()
extern "C"  void PlayerUserControl_Start_m501980854 (PlayerUserControl_t2572554735 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerUserControl_Start_m501980854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hitZoneManager = GameObject.FindWithTag("HitZoneParent").GetComponent<HitZoneManager>();
		// hitZoneManager = GameObject.FindWithTag("HitZoneParent").GetComponent<HitZoneManager>();
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1397813543, /*hidden argument*/NULL);
		// hitZoneManager = GameObject.FindWithTag("HitZoneParent").GetComponent<HitZoneManager>();
		NullCheck(L_0);
		HitZoneManager_t1308216902 * L_1 = GameObject_GetComponent_TisHitZoneManager_t1308216902_m2080816239(L_0, /*hidden argument*/GameObject_GetComponent_TisHitZoneManager_t1308216902_m2080816239_MethodInfo_var);
		__this->set_hitZoneManager_2(L_1);
		// animator = GetComponent<Animator>();
		// animator = GetComponent<Animator>();
		Animator_t69676727 * L_2 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set_animator_3(L_2);
		// }
		return;
	}
}
// System.Void PlayerUserControl::FixedUpdate()
extern "C"  void PlayerUserControl_FixedUpdate_m1847931561 (PlayerUserControl_t2572554735 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerUserControl_FixedUpdate_m1847931561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Input.GetButtonDown("Green"))
		// if(Input.GetButtonDown("Green"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		// if (hitZoneManager.m_green)
		HitZoneManager_t1308216902 * L_1 = __this->get_hitZoneManager_2();
		NullCheck(L_1);
		bool L_2 = L_1->get_m_green_2();
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		// GameManager.Destroy(hitZoneManager.m_greenTarget);
		HitZoneManager_t1308216902 * L_3 = __this->get_hitZoneManager_2();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = L_3->get_m_greenTarget_10();
		// GameManager.Destroy(hitZoneManager.m_greenTarget);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		// hitZoneManager.m_green = false;
		HitZoneManager_t1308216902 * L_5 = __this->get_hitZoneManager_2();
		NullCheck(L_5);
		L_5->set_m_green_2((bool)0);
	}

IL_003f:
	{
	}

IL_0040:
	{
		// if (Input.GetButtonDown("Red"))
		// if (Input.GetButtonDown("Red"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_007f;
		}
	}
	{
		// if (hitZoneManager.m_red)
		HitZoneManager_t1308216902 * L_7 = __this->get_hitZoneManager_2();
		NullCheck(L_7);
		bool L_8 = L_7->get_m_red_3();
		if (!L_8)
		{
			goto IL_007e;
		}
	}
	{
		// GameManager.Destroy(hitZoneManager.m_redTarget);
		HitZoneManager_t1308216902 * L_9 = __this->get_hitZoneManager_2();
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = L_9->get_m_redTarget_11();
		// GameManager.Destroy(hitZoneManager.m_redTarget);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		// hitZoneManager.m_red = false;
		HitZoneManager_t1308216902 * L_11 = __this->get_hitZoneManager_2();
		NullCheck(L_11);
		L_11->set_m_red_3((bool)0);
	}

IL_007e:
	{
	}

IL_007f:
	{
		// if (Input.GetButtonDown("Yellow"))
		// if (Input.GetButtonDown("Yellow"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00be;
		}
	}
	{
		// if (hitZoneManager.m_yellow)
		HitZoneManager_t1308216902 * L_13 = __this->get_hitZoneManager_2();
		NullCheck(L_13);
		bool L_14 = L_13->get_m_yellow_4();
		if (!L_14)
		{
			goto IL_00bd;
		}
	}
	{
		// GameManager.Destroy(hitZoneManager.m_yellowTarget);
		HitZoneManager_t1308216902 * L_15 = __this->get_hitZoneManager_2();
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = L_15->get_m_yellowTarget_12();
		// GameManager.Destroy(hitZoneManager.m_yellowTarget);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		// hitZoneManager.m_yellow = false;
		HitZoneManager_t1308216902 * L_17 = __this->get_hitZoneManager_2();
		NullCheck(L_17);
		L_17->set_m_yellow_4((bool)0);
	}

IL_00bd:
	{
	}

IL_00be:
	{
		// if (Input.GetButtonDown("Blue"))
		// if (Input.GetButtonDown("Blue"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_18 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00fd;
		}
	}
	{
		// if (hitZoneManager.m_blue)
		HitZoneManager_t1308216902 * L_19 = __this->get_hitZoneManager_2();
		NullCheck(L_19);
		bool L_20 = L_19->get_m_blue_5();
		if (!L_20)
		{
			goto IL_00fc;
		}
	}
	{
		// GameManager.Destroy(hitZoneManager.m_blueTarget);
		HitZoneManager_t1308216902 * L_21 = __this->get_hitZoneManager_2();
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = L_21->get_m_blueTarget_13();
		// GameManager.Destroy(hitZoneManager.m_blueTarget);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		// hitZoneManager.m_blue = false;
		HitZoneManager_t1308216902 * L_23 = __this->get_hitZoneManager_2();
		NullCheck(L_23);
		L_23->set_m_blue_5((bool)0);
	}

IL_00fc:
	{
	}

IL_00fd:
	{
		// }
		return;
	}
}
// System.Void RhythmEventProvider::.ctor()
extern "C"  void RhythmEventProvider__ctor_m2933971684 (RhythmEventProvider_t215006757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmEventProvider__ctor_m2933971684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public int currentFrame = 0;
		__this->set_currentFrame_6(0);
		// public float interpolation = 0;
		__this->set_interpolation_7((0.0f));
		// public int totalFrames = 0;
		__this->set_totalFrames_8(0);
		// public BeatEvent onBeat = new BeatEvent();
		BeatEvent_t33541086 * L_0 = (BeatEvent_t33541086 *)il2cpp_codegen_object_new(BeatEvent_t33541086_il2cpp_TypeInfo_var);
		BeatEvent__ctor_m1246752065(L_0, /*hidden argument*/NULL);
		__this->set_onBeat_9(L_0);
		// public SubBeatEvent onSubBeat = new SubBeatEvent();
		SubBeatEvent_t2499915164 * L_1 = (SubBeatEvent_t2499915164 *)il2cpp_codegen_object_new(SubBeatEvent_t2499915164_il2cpp_TypeInfo_var);
		SubBeatEvent__ctor_m1401496825(L_1, /*hidden argument*/NULL);
		__this->set_onSubBeat_10(L_1);
		// public OnsetEvent onOnset = new OnsetEvent();
		OnsetEvent_t1329939269 * L_2 = (OnsetEvent_t1329939269 *)il2cpp_codegen_object_new(OnsetEvent_t1329939269_il2cpp_TypeInfo_var);
		OnsetEvent__ctor_m1857777506(L_2, /*hidden argument*/NULL);
		__this->set_onOnset_11(L_2);
		// public ChangeEvent onChange = new ChangeEvent();
		ChangeEvent_t1135638418 * L_3 = (ChangeEvent_t1135638418 *)il2cpp_codegen_object_new(ChangeEvent_t1135638418_il2cpp_TypeInfo_var);
		ChangeEvent__ctor_m2572504441(L_3, /*hidden argument*/NULL);
		__this->set_onChange_12(L_3);
		// public TimingUpdateEvent timingUpdate = new TimingUpdateEvent();
		TimingUpdateEvent_t543213975 * L_4 = (TimingUpdateEvent_t543213975 *)il2cpp_codegen_object_new(TimingUpdateEvent_t543213975_il2cpp_TypeInfo_var);
		TimingUpdateEvent__ctor_m1890207098(L_4, /*hidden argument*/NULL);
		__this->set_timingUpdate_13(L_4);
		// public FrameChangedEvent onFrameChanged = new FrameChangedEvent();
		FrameChangedEvent_t800671821 * L_5 = (FrameChangedEvent_t800671821 *)il2cpp_codegen_object_new(FrameChangedEvent_t800671821_il2cpp_TypeInfo_var);
		FrameChangedEvent__ctor_m790505990(L_5, /*hidden argument*/NULL);
		__this->set_onFrameChanged_14(L_5);
		// public OnNewSong onSongLoaded = new OnNewSong();
		OnNewSong_t4169165798 * L_6 = (OnNewSong_t4169165798 *)il2cpp_codegen_object_new(OnNewSong_t4169165798_il2cpp_TypeInfo_var);
		OnNewSong__ctor_m1780728203(L_6, /*hidden argument*/NULL);
		__this->set_onSongLoaded_15(L_6);
		// public UnityEvent onSongEnded = new UnityEvent();
		UnityEvent_t408735097 * L_7 = (UnityEvent_t408735097 *)il2cpp_codegen_object_new(UnityEvent_t408735097_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m588741179(L_7, /*hidden argument*/NULL);
		__this->set_onSongEnded_16(L_7);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<RhythmEventProvider> RhythmEventProvider::get_eventProviders()
extern "C"  ReadOnlyCollection_1_t400792449 * RhythmEventProvider_get_eventProviders_m1235811388 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmEventProvider_get_eventProviders_m1235811388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReadOnlyCollection_1_t400792449 * V_0 = NULL;
	{
		// if (_eventProviders == null)
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_0 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get__eventProviders_2();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		// _eventProviders = eventProviderList.AsReadOnly();
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		List_1_t3879095185 * L_1 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_eventProviderList_17();
		// _eventProviders = eventProviderList.AsReadOnly();
		NullCheck(L_1);
		ReadOnlyCollection_1_t400792449 * L_2 = List_1_AsReadOnly_m3056134219(L_1, /*hidden argument*/List_1_AsReadOnly_m3056134219_MethodInfo_var);
		((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->set__eventProviders_2(L_2);
	}

IL_001a:
	{
		// return _eventProviders;
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_3 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get__eventProviders_2();
		V_0 = L_3;
		goto IL_0025;
	}

IL_0025:
	{
		// }
		ReadOnlyCollection_1_t400792449 * L_4 = V_0;
		return L_4;
	}
}
// System.Void RhythmEventProvider::add_EventProviderEnabled(System.Action`1<RhythmEventProvider>)
extern "C"  void RhythmEventProvider_add_EventProviderEnabled_m2395562879 (Il2CppObject * __this /* static, unused */, Action_1_t16806139 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmEventProvider_add_EventProviderEnabled_m2395562879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t16806139 * V_0 = NULL;
	Action_1_t16806139 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		Action_1_t16806139 * L_0 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_EventProviderEnabled_18();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t16806139 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		Action_1_t16806139 * L_2 = V_1;
		Action_1_t16806139 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_1_t16806139 * L_5 = V_0;
		Action_1_t16806139 * L_6 = InterlockedCompareExchangeImpl<Action_1_t16806139 *>((((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_address_of_EventProviderEnabled_18()), ((Action_1_t16806139 *)CastclassSealed(L_4, Action_1_t16806139_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_1_t16806139 * L_7 = V_0;
		Action_1_t16806139 * L_8 = V_1;
		if ((!(((Il2CppObject*)(Action_1_t16806139 *)L_7) == ((Il2CppObject*)(Action_1_t16806139 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void RhythmEventProvider::remove_EventProviderEnabled(System.Action`1<RhythmEventProvider>)
extern "C"  void RhythmEventProvider_remove_EventProviderEnabled_m3999705428 (Il2CppObject * __this /* static, unused */, Action_1_t16806139 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmEventProvider_remove_EventProviderEnabled_m3999705428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t16806139 * V_0 = NULL;
	Action_1_t16806139 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		Action_1_t16806139 * L_0 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_EventProviderEnabled_18();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t16806139 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		Action_1_t16806139 * L_2 = V_1;
		Action_1_t16806139 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_1_t16806139 * L_5 = V_0;
		Action_1_t16806139 * L_6 = InterlockedCompareExchangeImpl<Action_1_t16806139 *>((((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_address_of_EventProviderEnabled_18()), ((Action_1_t16806139 *)CastclassSealed(L_4, Action_1_t16806139_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_1_t16806139 * L_7 = V_0;
		Action_1_t16806139 * L_8 = V_1;
		if ((!(((Il2CppObject*)(Action_1_t16806139 *)L_7) == ((Il2CppObject*)(Action_1_t16806139 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void RhythmEventProvider::OnEnable()
extern "C"  void RhythmEventProvider_OnEnable_m13362604 (RhythmEventProvider_t215006757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmEventProvider_OnEnable_m13362604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!eventProviderList.Contains(this))
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		List_1_t3879095185 * L_0 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_eventProviderList_17();
		// if (!eventProviderList.Contains(this))
		NullCheck(L_0);
		bool L_1 = List_1_Contains_m2342885972(L_0, __this, /*hidden argument*/List_1_Contains_m2342885972_MethodInfo_var);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		// eventProviderList.Add(this);
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		List_1_t3879095185 * L_2 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_eventProviderList_17();
		// eventProviderList.Add(this);
		NullCheck(L_2);
		List_1_Add_m3390357842(L_2, __this, /*hidden argument*/List_1_Add_m3390357842_MethodInfo_var);
		// if (EventProviderEnabled != null)
		Action_1_t16806139 * L_3 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_EventProviderEnabled_18();
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		// EventProviderEnabled(this);
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		Action_1_t16806139 * L_4 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_EventProviderEnabled_18();
		// EventProviderEnabled(this);
		NullCheck(L_4);
		Action_1_Invoke_m4148756095(L_4, __this, /*hidden argument*/Action_1_Invoke_m4148756095_MethodInfo_var);
	}

IL_0032:
	{
	}

IL_0033:
	{
		// }
		return;
	}
}
// System.Void RhythmEventProvider::OnDisable()
extern "C"  void RhythmEventProvider_OnDisable_m3828130085 (RhythmEventProvider_t215006757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmEventProvider_OnDisable_m3828130085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (eventProviderList.Contains(this))
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		List_1_t3879095185 * L_0 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_eventProviderList_17();
		// if (eventProviderList.Contains(this))
		NullCheck(L_0);
		bool L_1 = List_1_Contains_m2342885972(L_0, __this, /*hidden argument*/List_1_Contains_m2342885972_MethodInfo_var);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		// eventProviderList.Remove(this);
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		List_1_t3879095185 * L_2 = ((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->get_eventProviderList_17();
		// eventProviderList.Remove(this);
		NullCheck(L_2);
		List_1_Remove_m4083267577(L_2, __this, /*hidden argument*/List_1_Remove_m4083267577_MethodInfo_var);
	}

IL_001d:
	{
		// }
		return;
	}
}
// System.Void RhythmEventProvider::.cctor()
extern "C"  void RhythmEventProvider__cctor_m1114474335 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmEventProvider__cctor_m1114474335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static List<RhythmEventProvider> eventProviderList = new List<RhythmEventProvider>();
		List_1_t3879095185 * L_0 = (List_1_t3879095185 *)il2cpp_codegen_object_new(List_1_t3879095185_il2cpp_TypeInfo_var);
		List_1__ctor_m3143476342(L_0, /*hidden argument*/List_1__ctor_m3143476342_MethodInfo_var);
		((RhythmEventProvider_t215006757_StaticFields*)RhythmEventProvider_t215006757_il2cpp_TypeInfo_var->static_fields)->set_eventProviderList_17(L_0);
		return;
	}
}
// System.Void RhythmEventProvider/BeatEvent::.ctor()
extern "C"  void BeatEvent__ctor_m1246752065 (BeatEvent_t33541086 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BeatEvent__ctor_m1246752065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RhythmEvent_1__ctor_m2502037646(__this, /*hidden argument*/RhythmEvent_1__ctor_m2502037646_MethodInfo_var);
		return;
	}
}
// System.Void RhythmEventProvider/ChangeEvent::.ctor()
extern "C"  void ChangeEvent__ctor_m2572504441 (ChangeEvent_t1135638418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChangeEvent__ctor_m2572504441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RhythmEvent_2__ctor_m460284861(__this, /*hidden argument*/RhythmEvent_2__ctor_m460284861_MethodInfo_var);
		return;
	}
}
// System.Void RhythmEventProvider/FrameChangedEvent::.ctor()
extern "C"  void FrameChangedEvent__ctor_m790505990 (FrameChangedEvent_t800671821 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FrameChangedEvent__ctor_m790505990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RhythmEvent_2__ctor_m557224401(__this, /*hidden argument*/RhythmEvent_2__ctor_m557224401_MethodInfo_var);
		return;
	}
}
// System.Void RhythmEventProvider/OnNewSong::.ctor()
extern "C"  void OnNewSong__ctor_m1780728203 (OnNewSong_t4169165798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnNewSong__ctor_m1780728203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RhythmEvent_2__ctor_m2299413610(__this, /*hidden argument*/RhythmEvent_2__ctor_m2299413610_MethodInfo_var);
		return;
	}
}
// System.Void RhythmEventProvider/OnsetEvent::.ctor()
extern "C"  void OnsetEvent__ctor_m1857777506 (OnsetEvent_t1329939269 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnsetEvent__ctor_m1857777506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RhythmEvent_2__ctor_m1345214749(__this, /*hidden argument*/RhythmEvent_2__ctor_m1345214749_MethodInfo_var);
		return;
	}
}
// System.Void RhythmEventProvider/SubBeatEvent::.ctor()
extern "C"  void SubBeatEvent__ctor_m1401496825 (SubBeatEvent_t2499915164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubBeatEvent__ctor_m1401496825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RhythmEvent_2__ctor_m2210944366(__this, /*hidden argument*/RhythmEvent_2__ctor_m2210944366_MethodInfo_var);
		return;
	}
}
// System.Void RhythmEventProvider/TimingUpdateEvent::.ctor()
extern "C"  void TimingUpdateEvent__ctor_m1890207098 (TimingUpdateEvent_t543213975 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimingUpdateEvent__ctor_m1890207098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RhythmEvent_4__ctor_m2850559519(__this, /*hidden argument*/RhythmEvent_4__ctor_m2850559519_MethodInfo_var);
		return;
	}
}
// System.Void RhythmTool::.ctor()
extern "C"  void RhythmTool__ctor_m684380269 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	{
		// private bool _preCalculate = false;
		__this->set__preCalculate_14((bool)0);
		// private bool _calculateTempo = true;
		__this->set__calculateTempo_15((bool)1);
		// private bool _storeAnalyses = false;
		__this->set__storeAnalyses_16((bool)0);
		// private int _lead = 300;
		__this->set__lead_17(((int32_t)300));
		// private Coroutine analyzeRoutine = null;
		__this->set_analyzeRoutine_20((Coroutine_t2299508840 *)NULL);
		// private Coroutine queueRoutine = null;
		__this->set_queueRoutine_21((Coroutine_t2299508840 *)NULL);
		// private int lastDataFrame = 0;
		__this->set_lastDataFrame_26(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RhythmTool::add_SongLoaded(System.Action)
extern "C"  void RhythmTool_add_SongLoaded_m1500519296 (RhythmTool_t215962618 * __this, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_add_SongLoaded_m1500519296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t3226471752 * V_0 = NULL;
	Action_t3226471752 * V_1 = NULL;
	{
		Action_t3226471752 * L_0 = __this->get_SongLoaded_2();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_t3226471752 * L_1 = V_0;
		V_1 = L_1;
		Action_t3226471752 ** L_2 = __this->get_address_of_SongLoaded_2();
		Action_t3226471752 * L_3 = V_1;
		Action_t3226471752 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_t3226471752 * L_6 = V_0;
		Action_t3226471752 * L_7 = InterlockedCompareExchangeImpl<Action_t3226471752 *>(L_2, ((Action_t3226471752 *)CastclassSealed(L_5, Action_t3226471752_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_t3226471752 * L_8 = V_0;
		Action_t3226471752 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_t3226471752 *)L_8) == ((Il2CppObject*)(Action_t3226471752 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void RhythmTool::remove_SongLoaded(System.Action)
extern "C"  void RhythmTool_remove_SongLoaded_m3258091535 (RhythmTool_t215962618 * __this, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_remove_SongLoaded_m3258091535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t3226471752 * V_0 = NULL;
	Action_t3226471752 * V_1 = NULL;
	{
		Action_t3226471752 * L_0 = __this->get_SongLoaded_2();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_t3226471752 * L_1 = V_0;
		V_1 = L_1;
		Action_t3226471752 ** L_2 = __this->get_address_of_SongLoaded_2();
		Action_t3226471752 * L_3 = V_1;
		Action_t3226471752 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_t3226471752 * L_6 = V_0;
		Action_t3226471752 * L_7 = InterlockedCompareExchangeImpl<Action_t3226471752 *>(L_2, ((Action_t3226471752 *)CastclassSealed(L_5, Action_t3226471752_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_t3226471752 * L_8 = V_0;
		Action_t3226471752 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_t3226471752 *)L_8) == ((Il2CppObject*)(Action_t3226471752 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void RhythmTool::add_SongEnded(System.Action)
extern "C"  void RhythmTool_add_SongEnded_m3686190681 (RhythmTool_t215962618 * __this, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_add_SongEnded_m3686190681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t3226471752 * V_0 = NULL;
	Action_t3226471752 * V_1 = NULL;
	{
		Action_t3226471752 * L_0 = __this->get_SongEnded_3();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_t3226471752 * L_1 = V_0;
		V_1 = L_1;
		Action_t3226471752 ** L_2 = __this->get_address_of_SongEnded_3();
		Action_t3226471752 * L_3 = V_1;
		Action_t3226471752 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_t3226471752 * L_6 = V_0;
		Action_t3226471752 * L_7 = InterlockedCompareExchangeImpl<Action_t3226471752 *>(L_2, ((Action_t3226471752 *)CastclassSealed(L_5, Action_t3226471752_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_t3226471752 * L_8 = V_0;
		Action_t3226471752 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_t3226471752 *)L_8) == ((Il2CppObject*)(Action_t3226471752 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void RhythmTool::remove_SongEnded(System.Action)
extern "C"  void RhythmTool_remove_SongEnded_m951710700 (RhythmTool_t215962618 * __this, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_remove_SongEnded_m951710700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t3226471752 * V_0 = NULL;
	Action_t3226471752 * V_1 = NULL;
	{
		Action_t3226471752 * L_0 = __this->get_SongEnded_3();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_t3226471752 * L_1 = V_0;
		V_1 = L_1;
		Action_t3226471752 ** L_2 = __this->get_address_of_SongEnded_3();
		Action_t3226471752 * L_3 = V_1;
		Action_t3226471752 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_t3226471752 * L_6 = V_0;
		Action_t3226471752 * L_7 = InterlockedCompareExchangeImpl<Action_t3226471752 *>(L_2, ((Action_t3226471752 *)CastclassSealed(L_5, Action_t3226471752_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_t3226471752 * L_8 = V_0;
		Action_t3226471752 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_t3226471752 *)L_8) == ((Il2CppObject*)(Action_t3226471752 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Int32 RhythmTool::get_fftWindowSize()
extern "C"  int32_t RhythmTool_get_fftWindowSize_m3317244415 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// public static int fftWindowSize { get { return 2048; } }
		V_0 = ((int32_t)2048);
		goto IL_000c;
	}

IL_000c:
	{
		// public static int fftWindowSize { get { return 2048; } }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Int32 RhythmTool::get_frameSpacing()
extern "C"  int32_t RhythmTool_get_frameSpacing_m936531512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// public static int frameSpacing { get { return 1470; } }
		V_0 = ((int32_t)1470);
		goto IL_000c;
	}

IL_000c:
	{
		// public static int frameSpacing { get { return 1470; } }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// ReadOnlyDictionary`2<System.Int32,Beat> RhythmTool::get_beats()
extern "C"  ReadOnlyDictionary_2_t1991166635 * RhythmTool_get_beats_m2345963615 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	ReadOnlyDictionary_2_t1991166635 * V_0 = NULL;
	{
		// public ReadOnlyDictionary<int, Beat> beats { get { return beatTracker.beats; } }
		BeatTracker_t2801099156 * L_0 = __this->get_beatTracker_18();
		// public ReadOnlyDictionary<int, Beat> beats { get { return beatTracker.beats; } }
		NullCheck(L_0);
		ReadOnlyDictionary_2_t1991166635 * L_1 = BeatTracker_get_beats_m3284239415(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// public ReadOnlyDictionary<int, Beat> beats { get { return beatTracker.beats; } }
		ReadOnlyDictionary_2_t1991166635 * L_2 = V_0;
		return L_2;
	}
}
// ReadOnlyDictionary`2<System.Int32,System.Single> RhythmTool::get_changes()
extern "C"  ReadOnlyDictionary_2_t1371992995 * RhythmTool_get_changes_m2588165142 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	ReadOnlyDictionary_2_t1371992995 * V_0 = NULL;
	{
		// public ReadOnlyDictionary<int, float> changes { get { return segmenter.changes; } }
		Segmenter_t2695296026 * L_0 = __this->get_segmenter_19();
		// public ReadOnlyDictionary<int, float> changes { get { return segmenter.changes; } }
		NullCheck(L_0);
		ReadOnlyDictionary_2_t1371992995 * L_1 = Segmenter_get_changes_m1846267788(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// public ReadOnlyDictionary<int, float> changes { get { return segmenter.changes; } }
		ReadOnlyDictionary_2_t1371992995 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean RhythmTool::get_preCalculate()
extern "C"  bool RhythmTool_get_preCalculate_m1317811739 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// get { return _preCalculate; }
		bool L_0 = __this->get__preCalculate_14();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return _preCalculate; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_preCalculate(System.Boolean)
extern "C"  void RhythmTool_set_preCalculate_m3586690966 (RhythmTool_t215962618 * __this, bool ___value0, const MethodInfo* method)
{
	{
		// set { if (analysisDone ^ !songLoaded) _preCalculate = value; }
		// set { if (analysisDone ^ !songLoaded) _preCalculate = value; }
		bool L_0 = RhythmTool_get_analysisDone_m3570634808(__this, /*hidden argument*/NULL);
		// set { if (analysisDone ^ !songLoaded) _preCalculate = value; }
		bool L_1 = RhythmTool_get_songLoaded_m3458667938(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_0^(int32_t)((((int32_t)L_1) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_001d;
		}
	}
	{
		// set { if (analysisDone ^ !songLoaded) _preCalculate = value; }
		bool L_2 = ___value0;
		__this->set__preCalculate_14(L_2);
	}

IL_001d:
	{
		// set { if (analysisDone ^ !songLoaded) _preCalculate = value; }
		return;
	}
}
// System.Boolean RhythmTool::get_calculateTempo()
extern "C"  bool RhythmTool_get_calculateTempo_m3725479207 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// get { return _calculateTempo; }
		bool L_0 = __this->get__calculateTempo_15();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return _calculateTempo; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_calculateTempo(System.Boolean)
extern "C"  void RhythmTool_set_calculateTempo_m2333951090 (RhythmTool_t215962618 * __this, bool ___value0, const MethodInfo* method)
{
	{
		// set { if (analysisDone ^ !songLoaded) _calculateTempo = value; }
		// set { if (analysisDone ^ !songLoaded) _calculateTempo = value; }
		bool L_0 = RhythmTool_get_analysisDone_m3570634808(__this, /*hidden argument*/NULL);
		// set { if (analysisDone ^ !songLoaded) _calculateTempo = value; }
		bool L_1 = RhythmTool_get_songLoaded_m3458667938(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_0^(int32_t)((((int32_t)L_1) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_001d;
		}
	}
	{
		// set { if (analysisDone ^ !songLoaded) _calculateTempo = value; }
		bool L_2 = ___value0;
		__this->set__calculateTempo_15(L_2);
	}

IL_001d:
	{
		// set { if (analysisDone ^ !songLoaded) _calculateTempo = value; }
		return;
	}
}
// System.Boolean RhythmTool::get_storeAnalyses()
extern "C"  bool RhythmTool_get_storeAnalyses_m604855781 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// get { return _storeAnalyses; }
		bool L_0 = __this->get__storeAnalyses_16();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return _storeAnalyses; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_storeAnalyses(System.Boolean)
extern "C"  void RhythmTool_set_storeAnalyses_m2870744536 (RhythmTool_t215962618 * __this, bool ___value0, const MethodInfo* method)
{
	{
		// set { if (analysisDone ^ !songLoaded) _storeAnalyses = value; }
		// set { if (analysisDone ^ !songLoaded) _storeAnalyses = value; }
		bool L_0 = RhythmTool_get_analysisDone_m3570634808(__this, /*hidden argument*/NULL);
		// set { if (analysisDone ^ !songLoaded) _storeAnalyses = value; }
		bool L_1 = RhythmTool_get_songLoaded_m3458667938(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_0^(int32_t)((((int32_t)L_1) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_001d;
		}
	}
	{
		// set { if (analysisDone ^ !songLoaded) _storeAnalyses = value; }
		bool L_2 = ___value0;
		__this->set__storeAnalyses_16(L_2);
	}

IL_001d:
	{
		// set { if (analysisDone ^ !songLoaded) _storeAnalyses = value; }
		return;
	}
}
// System.Int32 RhythmTool::get_lead()
extern "C"  int32_t RhythmTool_get_lead_m887496848 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// get { return _lead; }
		int32_t L_0 = __this->get__lead_17();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return _lead; }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_lead(System.Int32)
extern "C"  void RhythmTool_set_lead_m1732875513 (RhythmTool_t215962618 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_set_lead_m1732875513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// set { _lead = Mathf.Max(_lead, 300); }
		int32_t L_0 = __this->get__lead_17();
		// set { _lead = Mathf.Max(_lead, 300); }
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_Max_m1875893177(NULL /*static, unused*/, L_0, ((int32_t)300), /*hidden argument*/NULL);
		__this->set__lead_17(L_1);
		// set { _lead = Mathf.Max(_lead, 300); }
		return;
	}
}
// System.Single RhythmTool::get_currentSample()
extern "C"  float RhythmTool_get_currentSample_m2446084809 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// public float currentSample { get; private set; }
		float L_0 = __this->get_U3CcurrentSampleU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_currentSample(System.Single)
extern "C"  void RhythmTool_set_currentSample_m1718109522 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method)
{
	{
		// public float currentSample { get; private set; }
		float L_0 = ___value0;
		__this->set_U3CcurrentSampleU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 RhythmTool::get_lastFrame()
extern "C"  int32_t RhythmTool_get_lastFrame_m3065909813 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// public int lastFrame { get; private set; }
		int32_t L_0 = __this->get_U3ClastFrameU3Ek__BackingField_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_lastFrame(System.Int32)
extern "C"  void RhythmTool_set_lastFrame_m3646349138 (RhythmTool_t215962618 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		// public int lastFrame { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3ClastFrameU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Int32 RhythmTool::get_totalFrames()
extern "C"  int32_t RhythmTool_get_totalFrames_m68629944 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// public int totalFrames { get; private set; }
		int32_t L_0 = __this->get_U3CtotalFramesU3Ek__BackingField_6();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_totalFrames(System.Int32)
extern "C"  void RhythmTool_set_totalFrames_m509422205 (RhythmTool_t215962618 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		// public int totalFrames { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CtotalFramesU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Int32 RhythmTool::get_currentFrame()
extern "C"  int32_t RhythmTool_get_currentFrame_m867144816 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// public int currentFrame { get; private set; }
		int32_t L_0 = __this->get_U3CcurrentFrameU3Ek__BackingField_7();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_currentFrame(System.Int32)
extern "C"  void RhythmTool_set_currentFrame_m2370147509 (RhythmTool_t215962618 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		// public int currentFrame { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CcurrentFrameU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Single RhythmTool::get_interpolation()
extern "C"  float RhythmTool_get_interpolation_m2645903814 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// public float interpolation { get; private set; }
		float L_0 = __this->get_U3CinterpolationU3Ek__BackingField_8();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_interpolation(System.Single)
extern "C"  void RhythmTool_set_interpolation_m2276852035 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method)
{
	{
		// public float interpolation { get; private set; }
		float L_0 = ___value0;
		__this->set_U3CinterpolationU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Boolean RhythmTool::get_analysisDone()
extern "C"  bool RhythmTool_get_analysisDone_m3570634808 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// public bool analysisDone { get; private set; }
		bool L_0 = __this->get_U3CanalysisDoneU3Ek__BackingField_9();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_analysisDone(System.Boolean)
extern "C"  void RhythmTool_set_analysisDone_m701052397 (RhythmTool_t215962618 * __this, bool ___value0, const MethodInfo* method)
{
	{
		// public bool analysisDone { get; private set; }
		bool L_0 = ___value0;
		__this->set_U3CanalysisDoneU3Ek__BackingField_9(L_0);
		return;
	}
}
// AnalysisData RhythmTool::get_low()
extern "C"  AnalysisData_t108342674 * RhythmTool_get_low_m2159614661 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	AnalysisData_t108342674 * V_0 = NULL;
	{
		// public AnalysisData low { get { return _low.analysisData; } }
		Analysis_t439488098 * L_0 = __this->get__low_30();
		// public AnalysisData low { get { return _low.analysisData; } }
		NullCheck(L_0);
		AnalysisData_t108342674 * L_1 = Analysis_get_analysisData_m3591218099(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// public AnalysisData low { get { return _low.analysisData; } }
		AnalysisData_t108342674 * L_2 = V_0;
		return L_2;
	}
}
// AnalysisData RhythmTool::get_mid()
extern "C"  AnalysisData_t108342674 * RhythmTool_get_mid_m3477647933 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	AnalysisData_t108342674 * V_0 = NULL;
	{
		// public AnalysisData mid { get { return _mid.analysisData; } }
		Analysis_t439488098 * L_0 = __this->get__mid_31();
		// public AnalysisData mid { get { return _mid.analysisData; } }
		NullCheck(L_0);
		AnalysisData_t108342674 * L_1 = Analysis_get_analysisData_m3591218099(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// public AnalysisData mid { get { return _mid.analysisData; } }
		AnalysisData_t108342674 * L_2 = V_0;
		return L_2;
	}
}
// AnalysisData RhythmTool::get_high()
extern "C"  AnalysisData_t108342674 * RhythmTool_get_high_m3092150915 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	AnalysisData_t108342674 * V_0 = NULL;
	{
		// public AnalysisData high { get { return _high.analysisData; } }
		Analysis_t439488098 * L_0 = __this->get__high_32();
		// public AnalysisData high { get { return _high.analysisData; } }
		NullCheck(L_0);
		AnalysisData_t108342674 * L_1 = Analysis_get_analysisData_m3591218099(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// public AnalysisData high { get { return _high.analysisData; } }
		AnalysisData_t108342674 * L_2 = V_0;
		return L_2;
	}
}
// AnalysisData RhythmTool::get_all()
extern "C"  AnalysisData_t108342674 * RhythmTool_get_all_m1284823694 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	AnalysisData_t108342674 * V_0 = NULL;
	{
		// public AnalysisData all { get { return _all.analysisData; } }
		Analysis_t439488098 * L_0 = __this->get__all_33();
		// public AnalysisData all { get { return _all.analysisData; } }
		NullCheck(L_0);
		AnalysisData_t108342674 * L_1 = Analysis_get_analysisData_m3591218099(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// public AnalysisData all { get { return _all.analysisData; } }
		AnalysisData_t108342674 * L_2 = V_0;
		return L_2;
	}
}
// System.Single RhythmTool::get_bpm()
extern "C"  float RhythmTool_get_bpm_m154604281 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// public float bpm { get; private set; }
		float L_0 = __this->get_U3CbpmU3Ek__BackingField_10();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_bpm(System.Single)
extern "C"  void RhythmTool_set_bpm_m3815049600 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method)
{
	{
		// public float bpm { get; private set; }
		float L_0 = ___value0;
		__this->set_U3CbpmU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Single RhythmTool::get_beatLength()
extern "C"  float RhythmTool_get_beatLength_m3425246286 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// public float beatLength { get; private set; }
		float L_0 = __this->get_U3CbeatLengthU3Ek__BackingField_11();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_beatLength(System.Single)
extern "C"  void RhythmTool_set_beatLength_m1126195059 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method)
{
	{
		// public float beatLength { get; private set; }
		float L_0 = ___value0;
		__this->set_U3CbeatLengthU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Single RhythmTool::get_frameLength()
extern "C"  float RhythmTool_get_frameLength_m2593270439 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// public float frameLength { get; private set; }
		float L_0 = __this->get_U3CframeLengthU3Ek__BackingField_12();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_frameLength(System.Single)
extern "C"  void RhythmTool_set_frameLength_m595084212 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method)
{
	{
		// public float frameLength { get; private set; }
		float L_0 = ___value0;
		__this->set_U3CframeLengthU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Boolean RhythmTool::get_songLoaded()
extern "C"  bool RhythmTool_get_songLoaded_m3458667938 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// public bool songLoaded { get; private set; }
		bool L_0 = __this->get_U3CsongLoadedU3Ek__BackingField_13();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool::set_songLoaded(System.Boolean)
extern "C"  void RhythmTool_set_songLoaded_m1649989549 (RhythmTool_t215962618 * __this, bool ___value0, const MethodInfo* method)
{
	{
		// public bool songLoaded { get; private set; }
		bool L_0 = ___value0;
		__this->set_U3CsongLoadedU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Single RhythmTool::get_volume()
extern "C"  float RhythmTool_get_volume_m4235681660 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// get { return audioSource.volume; }
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		// get { return audioSource.volume; }
		NullCheck(L_0);
		float L_1 = AudioSource_get_volume_m66289169(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// get { return audioSource.volume; }
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void RhythmTool::set_volume(System.Single)
extern "C"  void RhythmTool_set_volume_m713961673 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method)
{
	{
		// set { audioSource.volume = value; }
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		float L_1 = ___value0;
		// set { audioSource.volume = value; }
		NullCheck(L_0);
		AudioSource_set_volume_m2777308722(L_0, L_1, /*hidden argument*/NULL);
		// set { audioSource.volume = value; }
		return;
	}
}
// System.Single RhythmTool::get_pitch()
extern "C"  float RhythmTool_get_pitch_m3814592294 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// get { return audioSource.pitch; }
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		// get { return audioSource.pitch; }
		NullCheck(L_0);
		float L_1 = AudioSource_get_pitch_m4220572439(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// get { return audioSource.pitch; }
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void RhythmTool::set_pitch(System.Single)
extern "C"  void RhythmTool_set_pitch_m23385823 (RhythmTool_t215962618 * __this, float ___value0, const MethodInfo* method)
{
	{
		// set { audioSource.pitch = value; }
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		float L_1 = ___value0;
		// set { audioSource.pitch = value; }
		NullCheck(L_0);
		AudioSource_set_pitch_m3064416458(L_0, L_1, /*hidden argument*/NULL);
		// set { audioSource.pitch = value; }
		return;
	}
}
// System.Boolean RhythmTool::get_isPlaying()
extern "C"  bool RhythmTool_get_isPlaying_m2081879838 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// get { return audioSource.isPlaying; }
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		// get { return audioSource.isPlaying; }
		NullCheck(L_0);
		bool L_1 = AudioSource_get_isPlaying_m3677592677(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// get { return audioSource.isPlaying; }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void RhythmTool::Init()
extern "C"  void RhythmTool_Init_m2742459395 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_Init_m2742459395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// analyses = new List<Analysis>();
		// analyses = new List<Analysis>();
		List_1_t4103576526 * L_0 = (List_1_t4103576526 *)il2cpp_codegen_object_new(List_1_t4103576526_il2cpp_TypeInfo_var);
		List_1__ctor_m9134919(L_0, /*hidden argument*/List_1__ctor_m9134919_MethodInfo_var);
		__this->set_analyses_29(L_0);
		// _low = new Analysis(0, 30, "low");
		// _low = new Analysis(0, 30, "low");
		Analysis_t439488098 * L_1 = (Analysis_t439488098 *)il2cpp_codegen_object_new(Analysis_t439488098_il2cpp_TypeInfo_var);
		Analysis__ctor_m1571091587(L_1, 0, ((int32_t)30), _stringLiteral1502598480, /*hidden argument*/NULL);
		__this->set__low_30(L_1);
		// analyses.Add(_low);
		List_1_t4103576526 * L_2 = __this->get_analyses_29();
		Analysis_t439488098 * L_3 = __this->get__low_30();
		// analyses.Add(_low);
		NullCheck(L_2);
		List_1_Add_m845626571(L_2, L_3, /*hidden argument*/List_1_Add_m845626571_MethodInfo_var);
		// _mid = new Analysis(30, 350, "mid");
		// _mid = new Analysis(30, 350, "mid");
		Analysis_t439488098 * L_4 = (Analysis_t439488098 *)il2cpp_codegen_object_new(Analysis_t439488098_il2cpp_TypeInfo_var);
		Analysis__ctor_m1571091587(L_4, ((int32_t)30), ((int32_t)350), _stringLiteral339799080, /*hidden argument*/NULL);
		__this->set__mid_31(L_4);
		// analyses.Add(_mid);
		List_1_t4103576526 * L_5 = __this->get_analyses_29();
		Analysis_t439488098 * L_6 = __this->get__mid_31();
		// analyses.Add(_mid);
		NullCheck(L_5);
		List_1_Add_m845626571(L_5, L_6, /*hidden argument*/List_1_Add_m845626571_MethodInfo_var);
		// _high = new Analysis(370, 900, "high");
		// _high = new Analysis(370, 900, "high");
		Analysis_t439488098 * L_7 = (Analysis_t439488098 *)il2cpp_codegen_object_new(Analysis_t439488098_il2cpp_TypeInfo_var);
		Analysis__ctor_m1571091587(L_7, ((int32_t)370), ((int32_t)900), _stringLiteral4217033790, /*hidden argument*/NULL);
		__this->set__high_32(L_7);
		// analyses.Add(_high);
		List_1_t4103576526 * L_8 = __this->get_analyses_29();
		Analysis_t439488098 * L_9 = __this->get__high_32();
		// analyses.Add(_high);
		NullCheck(L_8);
		List_1_Add_m845626571(L_8, L_9, /*hidden argument*/List_1_Add_m845626571_MethodInfo_var);
		// _all = new Analysis(0, 350, "all");
		// _all = new Analysis(0, 350, "all");
		Analysis_t439488098 * L_10 = (Analysis_t439488098 *)il2cpp_codegen_object_new(Analysis_t439488098_il2cpp_TypeInfo_var);
		Analysis__ctor_m1571091587(L_10, 0, ((int32_t)350), _stringLiteral4231481717, /*hidden argument*/NULL);
		__this->set__all_33(L_10);
		// analyses.Add(_all);
		List_1_t4103576526 * L_11 = __this->get_analyses_29();
		Analysis_t439488098 * L_12 = __this->get__all_33();
		// analyses.Add(_all);
		NullCheck(L_11);
		List_1_Add_m845626571(L_11, L_12, /*hidden argument*/List_1_Add_m845626571_MethodInfo_var);
		// beatTracker = new BeatTracker();
		// beatTracker = new BeatTracker();
		BeatTracker_t2801099156 * L_13 = (BeatTracker_t2801099156 *)il2cpp_codegen_object_new(BeatTracker_t2801099156_il2cpp_TypeInfo_var);
		BeatTracker__ctor_m1207605921(L_13, /*hidden argument*/NULL);
		__this->set_beatTracker_18(L_13);
		// segmenter = new Segmenter(all);
		// segmenter = new Segmenter(all);
		AnalysisData_t108342674 * L_14 = RhythmTool_get_all_m1284823694(__this, /*hidden argument*/NULL);
		// segmenter = new Segmenter(all);
		Segmenter_t2695296026 * L_15 = (Segmenter_t2695296026 *)il2cpp_codegen_object_new(Segmenter_t2695296026_il2cpp_TypeInfo_var);
		Segmenter__ctor_m3661281169(L_15, L_14, /*hidden argument*/NULL);
		__this->set_segmenter_19(L_15);
		// songLoaded = false;
		// songLoaded = false;
		RhythmTool_set_songLoaded_m1649989549(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void RhythmTool::Awake()
extern "C"  void RhythmTool_Awake_m49992394 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_Awake_m49992394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Init();
		// Init();
		RhythmTool_Init_m2742459395(__this, /*hidden argument*/NULL);
		// audioSource = GetComponent<AudioSource>();
		// audioSource = GetComponent<AudioSource>();
		AudioSource_t1135106623 * L_0 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var);
		__this->set_audioSource_34(L_0);
		// RhythmEventProvider.EventProviderEnabled += OnEventProviderEnabled;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)RhythmTool_OnEventProviderEnabled_m1814537051_MethodInfo_var);
		Action_1_t16806139 * L_2 = (Action_1_t16806139 *)il2cpp_codegen_object_new(Action_1_t16806139_il2cpp_TypeInfo_var);
		Action_1__ctor_m2757687936(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m2757687936_MethodInfo_var);
		// RhythmEventProvider.EventProviderEnabled += OnEventProviderEnabled;
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		RhythmEventProvider_add_EventProviderEnabled_m2395562879(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void RhythmTool::OnEventProviderEnabled(RhythmEventProvider)
extern "C"  void RhythmTool_OnEventProviderEnabled_m1814537051 (RhythmTool_t215962618 * __this, RhythmEventProvider_t215006757 * ___r0, const MethodInfo* method)
{
	{
		// if (songLoaded)
		// if (songLoaded)
		bool L_0 = RhythmTool_get_songLoaded_m3458667938(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		// InitializeEventProvider(r);
		RhythmEventProvider_t215006757 * L_1 = ___r0;
		// InitializeEventProvider(r);
		RhythmTool_InitializeEventProvider_m925349371(__this, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		// }
		return;
	}
}
// System.Void RhythmTool::InitializeEventProvider(RhythmEventProvider)
extern "C"  void RhythmTool_InitializeEventProvider_m925349371 (RhythmTool_t215962618 * __this, RhythmEventProvider_t215006757 * ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_InitializeEventProvider_m925349371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// r.offset = 0;
		RhythmEventProvider_t215006757 * L_0 = ___r0;
		NullCheck(L_0);
		L_0->set_offset_4(0);
		// r.onSongLoaded.Invoke(audioSource.clip.name, totalFrames);
		RhythmEventProvider_t215006757 * L_1 = ___r0;
		NullCheck(L_1);
		OnNewSong_t4169165798 * L_2 = L_1->get_onSongLoaded_15();
		AudioSource_t1135106623 * L_3 = __this->get_audioSource_34();
		// r.onSongLoaded.Invoke(audioSource.clip.name, totalFrames);
		NullCheck(L_3);
		AudioClip_t1932558630 * L_4 = AudioSource_get_clip_m2127996365(L_3, /*hidden argument*/NULL);
		// r.onSongLoaded.Invoke(audioSource.clip.name, totalFrames);
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m2079638459(L_4, /*hidden argument*/NULL);
		// r.onSongLoaded.Invoke(audioSource.clip.name, totalFrames);
		int32_t L_6 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		// r.onSongLoaded.Invoke(audioSource.clip.name, totalFrames);
		NullCheck(L_2);
		UnityEvent_2_Invoke_m2327814026(L_2, L_5, L_6, /*hidden argument*/UnityEvent_2_Invoke_m2327814026_MethodInfo_var);
		// r.totalFrames = totalFrames;
		RhythmEventProvider_t215006757 * L_7 = ___r0;
		// r.totalFrames = totalFrames;
		int32_t L_8 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_totalFrames_8(L_8);
		// }
		return;
	}
}
// System.Void RhythmTool::InitializeEventProviders()
extern "C"  void RhythmTool_InitializeEventProviders_m198279347 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_InitializeEventProviders_m198279347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RhythmEventProvider_t215006757 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// foreach (RhythmEventProvider r in RhythmEventProvider.eventProviders)
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_0 = RhythmEventProvider_get_eventProviders_m1235811388(NULL /*static, unused*/, /*hidden argument*/NULL);
		// foreach (RhythmEventProvider r in RhythmEventProvider.eventProviders)
		NullCheck(L_0);
		Il2CppObject* L_1 = ReadOnlyCollection_1_GetEnumerator_m2483346076(L_0, /*hidden argument*/ReadOnlyCollection_1_GetEnumerator_m2483346076_MethodInfo_var);
		V_1 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0022;
		}

IL_0012:
		{
			// foreach (RhythmEventProvider r in RhythmEventProvider.eventProviders)
			Il2CppObject* L_2 = V_1;
			// foreach (RhythmEventProvider r in RhythmEventProvider.eventProviders)
			NullCheck(L_2);
			RhythmEventProvider_t215006757 * L_3 = InterfaceFuncInvoker0< RhythmEventProvider_t215006757 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<RhythmEventProvider>::get_Current() */, IEnumerator_1_t1985497880_il2cpp_TypeInfo_var, L_2);
			V_0 = L_3;
			// InitializeEventProvider(r);
			RhythmEventProvider_t215006757 * L_4 = V_0;
			// InitializeEventProvider(r);
			RhythmTool_InitializeEventProvider_m925349371(__this, L_4, /*hidden argument*/NULL);
		}

IL_0022:
		{
			Il2CppObject* L_5 = V_1;
			// foreach (RhythmEventProvider r in RhythmEventProvider.eventProviders)
			NullCheck(L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_5);
			if (L_6)
			{
				goto IL_0012;
			}
		}

IL_002d:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0032);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (!L_7)
			{
				goto IL_003e;
			}
		}

IL_0038:
		{
			Il2CppObject* L_8 = V_1;
			// foreach (RhythmEventProvider r in RhythmEventProvider.eventProviders)
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_8);
		}

IL_003e:
		{
			IL2CPP_END_FINALLY(50)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003f:
	{
		// }
		return;
	}
}
// System.Void RhythmTool::NewSong(UnityEngine.AudioClip)
extern "C"  void RhythmTool_NewSong_m2509631375 (RhythmTool_t215962618 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method)
{
	{
		// if (queueRoutine != null)
		Coroutine_t2299508840 * L_0 = __this->get_queueRoutine_21();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		// StopCoroutine(queueRoutine);
		Coroutine_t2299508840 * L_1 = __this->get_queueRoutine_21();
		// StopCoroutine(queueRoutine);
		MonoBehaviour_StopCoroutine_m1668572632(__this, L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		// queueRoutine = StartCoroutine(QueueNewSong(audioClip));
		AudioClip_t1932558630 * L_2 = ___audioClip0;
		// queueRoutine = StartCoroutine(QueueNewSong(audioClip));
		Il2CppObject * L_3 = RhythmTool_QueueNewSong_m891591844(__this, L_2, /*hidden argument*/NULL);
		// queueRoutine = StartCoroutine(QueueNewSong(audioClip));
		Coroutine_t2299508840 * L_4 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		__this->set_queueRoutine_21(L_4);
		// }
		return;
	}
}
// System.Collections.IEnumerator RhythmTool::QueueNewSong(UnityEngine.AudioClip)
extern "C"  Il2CppObject * RhythmTool_QueueNewSong_m891591844 (RhythmTool_t215962618 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_QueueNewSong_m891591844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CQueueNewSongU3Ec__Iterator0_t1486555536 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CQueueNewSongU3Ec__Iterator0_t1486555536 * L_0 = (U3CQueueNewSongU3Ec__Iterator0_t1486555536 *)il2cpp_codegen_object_new(U3CQueueNewSongU3Ec__Iterator0_t1486555536_il2cpp_TypeInfo_var);
		U3CQueueNewSongU3Ec__Iterator0__ctor_m4124132409(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CQueueNewSongU3Ec__Iterator0_t1486555536 * L_1 = V_0;
		AudioClip_t1932558630 * L_2 = ___audioClip0;
		NullCheck(L_1);
		L_1->set_audioClip_0(L_2);
		U3CQueueNewSongU3Ec__Iterator0_t1486555536 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CQueueNewSongU3Ec__Iterator0_t1486555536 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Void RhythmTool::LoadNewSong(UnityEngine.AudioClip)
extern "C"  void RhythmTool_LoadNewSong_m1895013473 (RhythmTool_t215962618 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_LoadNewSong_m1895013473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Analysis_t439488098 * V_0 = NULL;
	Enumerator_t3638306200  V_1;
	memset(&V_1, 0, sizeof(V_1));
	SongData_t3132760915 * V_2 = NULL;
	AnalysisData_t108342674 * V_3 = NULL;
	Enumerator_t3307160776  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Analysis_t439488098 * V_5 = NULL;
	Enumerator_t3638306200  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// audioSource.Stop();
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		// audioSource.Stop();
		NullCheck(L_0);
		AudioSource_Stop_m3452679614(L_0, /*hidden argument*/NULL);
		// audioSource.clip = audioClip;
		AudioSource_t1135106623 * L_1 = __this->get_audioSource_34();
		AudioClip_t1932558630 * L_2 = ___audioClip0;
		// audioSource.clip = audioClip;
		NullCheck(L_1);
		AudioSource_set_clip_m738814682(L_1, L_2, /*hidden argument*/NULL);
		// channels = audioSource.clip.channels;
		AudioSource_t1135106623 * L_3 = __this->get_audioSource_34();
		// channels = audioSource.clip.channels;
		NullCheck(L_3);
		AudioClip_t1932558630 * L_4 = AudioSource_get_clip_m2127996365(L_3, /*hidden argument*/NULL);
		// channels = audioSource.clip.channels;
		NullCheck(L_4);
		int32_t L_5 = AudioClip_get_channels_m211770176(L_4, /*hidden argument*/NULL);
		__this->set_channels_25(L_5);
		// totalSamples = audioSource.clip.samples;
		AudioSource_t1135106623 * L_6 = __this->get_audioSource_34();
		// totalSamples = audioSource.clip.samples;
		NullCheck(L_6);
		AudioClip_t1932558630 * L_7 = AudioSource_get_clip_m2127996365(L_6, /*hidden argument*/NULL);
		// totalSamples = audioSource.clip.samples;
		NullCheck(L_7);
		int32_t L_8 = AudioClip_get_samples_m3690111759(L_7, /*hidden argument*/NULL);
		__this->set_totalSamples_27(L_8);
		// totalSamples -= totalSamples % frameSpacing;
		int32_t L_9 = __this->get_totalSamples_27();
		int32_t L_10 = __this->get_totalSamples_27();
		// totalSamples -= totalSamples % frameSpacing;
		int32_t L_11 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_totalSamples_27(((int32_t)((int32_t)L_9-(int32_t)((int32_t)((int32_t)L_10%(int32_t)L_11)))));
		// totalFrames = totalSamples / frameSpacing;
		int32_t L_12 = __this->get_totalSamples_27();
		// totalFrames = totalSamples / frameSpacing;
		int32_t L_13 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		// totalFrames = totalSamples / frameSpacing;
		RhythmTool_set_totalFrames_m509422205(__this, ((int32_t)((int32_t)L_12/(int32_t)L_13)), /*hidden argument*/NULL);
		// frameLength = 1 / ((float)audioSource.clip.frequency / (float)frameSpacing);
		AudioSource_t1135106623 * L_14 = __this->get_audioSource_34();
		// frameLength = 1 / ((float)audioSource.clip.frequency / (float)frameSpacing);
		NullCheck(L_14);
		AudioClip_t1932558630 * L_15 = AudioSource_get_clip_m2127996365(L_14, /*hidden argument*/NULL);
		// frameLength = 1 / ((float)audioSource.clip.frequency / (float)frameSpacing);
		NullCheck(L_15);
		int32_t L_16 = AudioClip_get_frequency_m237362468(L_15, /*hidden argument*/NULL);
		// frameLength = 1 / ((float)audioSource.clip.frequency / (float)frameSpacing);
		int32_t L_17 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		// frameLength = 1 / ((float)audioSource.clip.frequency / (float)frameSpacing);
		RhythmTool_set_frameLength_m595084212(__this, ((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_16)))/(float)(((float)((float)L_17))))))), /*hidden argument*/NULL);
		// foreach (Analysis s in analyses)
		List_1_t4103576526 * L_18 = __this->get_analyses_29();
		// foreach (Analysis s in analyses)
		NullCheck(L_18);
		Enumerator_t3638306200  L_19 = List_1_GetEnumerator_m3071804604(L_18, /*hidden argument*/List_1_GetEnumerator_m3071804604_MethodInfo_var);
		V_1 = L_19;
	}

IL_00a0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00bb;
		}

IL_00a5:
		{
			// foreach (Analysis s in analyses)
			// foreach (Analysis s in analyses)
			Analysis_t439488098 * L_20 = Enumerator_get_Current_m3316544882((&V_1), /*hidden argument*/Enumerator_get_Current_m3316544882_MethodInfo_var);
			V_0 = L_20;
			// s.Init(totalFrames);
			Analysis_t439488098 * L_21 = V_0;
			// s.Init(totalFrames);
			int32_t L_22 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
			// s.Init(totalFrames);
			NullCheck(L_21);
			Analysis_Init_m3996269154(L_21, L_22, /*hidden argument*/NULL);
		}

IL_00bb:
		{
			// foreach (Analysis s in analyses)
			bool L_23 = Enumerator_MoveNext_m391630496((&V_1), /*hidden argument*/Enumerator_MoveNext_m391630496_MethodInfo_var);
			if (L_23)
			{
				goto IL_00a5;
			}
		}

IL_00c7:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00cc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00cc;
	}

FINALLY_00cc:
	{ // begin finally (depth: 1)
		// foreach (Analysis s in analyses)
		Enumerator_Dispose_m1317449898((&V_1), /*hidden argument*/Enumerator_Dispose_m1317449898_MethodInfo_var);
		IL2CPP_END_FINALLY(204)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(204)
	{
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00da:
	{
		// beatTracker.Init(frameLength);
		BeatTracker_t2801099156 * L_24 = __this->get_beatTracker_18();
		// beatTracker.Init(frameLength);
		float L_25 = RhythmTool_get_frameLength_m2593270439(__this, /*hidden argument*/NULL);
		// beatTracker.Init(frameLength);
		NullCheck(L_24);
		BeatTracker_Init_m1298165546(L_24, L_25, /*hidden argument*/NULL);
		// segmenter.Init();
		Segmenter_t2695296026 * L_26 = __this->get_segmenter_19();
		// segmenter.Init();
		NullCheck(L_26);
		Segmenter_Init_m1948752761(L_26, /*hidden argument*/NULL);
		// samples = new float[fftWindowSize * channels];
		// samples = new float[fftWindowSize * channels];
		int32_t L_27 = RhythmTool_get_fftWindowSize_m3317244415(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_28 = __this->get_channels_25();
		__this->set_samples_22(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_27*(int32_t)L_28)))));
		// monoSamples = new float[fftWindowSize];
		// monoSamples = new float[fftWindowSize];
		int32_t L_29 = RhythmTool_get_fftWindowSize_m3317244415(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_monoSamples_23(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_29)));
		// spectrum = new float[monoSamples.Length/2];
		SingleU5BU5D_t577127397* L_30 = __this->get_monoSamples_23();
		NullCheck(L_30);
		__this->set_spectrum_24(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))/(int32_t)2)))));
		// currentFrame = 0;
		// currentFrame = 0;
		RhythmTool_set_currentFrame_m2370147509(__this, 0, /*hidden argument*/NULL);
		// lastFrame = 0;
		// lastFrame = 0;
		RhythmTool_set_lastFrame_m3646349138(__this, 0, /*hidden argument*/NULL);
		// lastDataFrame = 0;
		__this->set_lastDataFrame_26(0);
		// currentSample = 0;
		// currentSample = 0;
		RhythmTool_set_currentSample_m1718109522(__this, (0.0f), /*hidden argument*/NULL);
		// analysisDone = false;
		// analysisDone = false;
		RhythmTool_set_analysisDone_m701052397(__this, (bool)0, /*hidden argument*/NULL);
		// songLoaded = false;
		// songLoaded = false;
		RhythmTool_set_songLoaded_m1649989549(__this, (bool)0, /*hidden argument*/NULL);
		// if (!_preCalculate)
		bool L_31 = __this->get__preCalculate_14();
		if (L_31)
		{
			goto IL_0197;
		}
	}
	{
		// _storeAnalyses = false;
		__this->set__storeAnalyses_16((bool)0);
		// analyzeRoutine = StartCoroutine(AsyncAnalyze(_lead + 300));
		int32_t L_32 = __this->get__lead_17();
		// analyzeRoutine = StartCoroutine(AsyncAnalyze(_lead + 300));
		Il2CppObject * L_33 = RhythmTool_AsyncAnalyze_m1726189964(__this, ((int32_t)((int32_t)L_32+(int32_t)((int32_t)300))), /*hidden argument*/NULL);
		// analyzeRoutine = StartCoroutine(AsyncAnalyze(_lead + 300));
		Coroutine_t2299508840 * L_34 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_33, /*hidden argument*/NULL);
		__this->set_analyzeRoutine_20(L_34);
		goto IL_033b;
	}

IL_0197:
	{
		// _lead = 300;
		__this->set__lead_17(((int32_t)300));
		// if (_storeAnalyses)
		bool L_35 = __this->get__storeAnalyses_16();
		if (!L_35)
		{
			goto IL_0322;
		}
	}
	{
		// if (File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + audioSource.clip.name + ".rthm"))
		ObjectU5BU5D_t3614634134* L_36 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		// if (File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + audioSource.clip.name + ".rthm"))
		String_t* L_37 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_37);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_37);
		ObjectU5BU5D_t3614634134* L_38 = L_36;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		Il2CppChar L_39 = ((Path_t41728875_StaticFields*)Path_t41728875_il2cpp_TypeInfo_var->static_fields)->get_DirectorySeparatorChar_2();
		Il2CppChar L_40 = L_39;
		Il2CppObject * L_41 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_41);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_41);
		ObjectU5BU5D_t3614634134* L_42 = L_38;
		AudioSource_t1135106623 * L_43 = __this->get_audioSource_34();
		// if (File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + audioSource.clip.name + ".rthm"))
		NullCheck(L_43);
		AudioClip_t1932558630 * L_44 = AudioSource_get_clip_m2127996365(L_43, /*hidden argument*/NULL);
		// if (File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + audioSource.clip.name + ".rthm"))
		NullCheck(L_44);
		String_t* L_45 = Object_get_name_m2079638459(L_44, /*hidden argument*/NULL);
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, L_45);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_45);
		ObjectU5BU5D_t3614634134* L_46 = L_42;
		NullCheck(L_46);
		ArrayElementTypeCheck (L_46, _stringLiteral2395002637);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2395002637);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m3881798623(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		// if (File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + audioSource.clip.name + ".rthm"))
		bool L_48 = File_Exists_m1685968367(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0321;
		}
	}
	{
		// SongData songData = SongData.Deserialize(audioSource.clip.name);
		AudioSource_t1135106623 * L_49 = __this->get_audioSource_34();
		// SongData songData = SongData.Deserialize(audioSource.clip.name);
		NullCheck(L_49);
		AudioClip_t1932558630 * L_50 = AudioSource_get_clip_m2127996365(L_49, /*hidden argument*/NULL);
		// SongData songData = SongData.Deserialize(audioSource.clip.name);
		NullCheck(L_50);
		String_t* L_51 = Object_get_name_m2079638459(L_50, /*hidden argument*/NULL);
		// SongData songData = SongData.Deserialize(audioSource.clip.name);
		SongData_t3132760915 * L_52 = SongData_Deserialize_m3771651313(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		V_2 = L_52;
		// if (songData.length == totalFrames)
		SongData_t3132760915 * L_53 = V_2;
		NullCheck(L_53);
		int32_t L_54 = L_53->get_length_4();
		// if (songData.length == totalFrames)
		int32_t L_55 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_54) == ((uint32_t)L_55))))
		{
			goto IL_0320;
		}
	}
	{
		// lastFrame = totalFrames;
		// lastFrame = totalFrames;
		int32_t L_56 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		// lastFrame = totalFrames;
		RhythmTool_set_lastFrame_m3646349138(__this, L_56, /*hidden argument*/NULL);
		// analysisDone = true;
		// analysisDone = true;
		RhythmTool_set_analysisDone_m701052397(__this, (bool)1, /*hidden argument*/NULL);
		// foreach (AnalysisData data in songData.analyses)
		SongData_t3132760915 * L_57 = V_2;
		NullCheck(L_57);
		List_1_t3772431102 * L_58 = L_57->get_analyses_0();
		// foreach (AnalysisData data in songData.analyses)
		NullCheck(L_58);
		Enumerator_t3307160776  L_59 = List_1_GetEnumerator_m2880262412(L_58, /*hidden argument*/List_1_GetEnumerator_m2880262412_MethodInfo_var);
		V_4 = L_59;
	}

IL_023e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02ab;
		}

IL_0243:
		{
			// foreach (AnalysisData data in songData.analyses)
			// foreach (AnalysisData data in songData.analyses)
			AnalysisData_t108342674 * L_60 = Enumerator_get_Current_m3892498626((&V_4), /*hidden argument*/Enumerator_get_Current_m3892498626_MethodInfo_var);
			V_3 = L_60;
			// foreach (Analysis a in analyses)
			List_1_t4103576526 * L_61 = __this->get_analyses_29();
			// foreach (Analysis a in analyses)
			NullCheck(L_61);
			Enumerator_t3638306200  L_62 = List_1_GetEnumerator_m3071804604(L_61, /*hidden argument*/List_1_GetEnumerator_m3071804604_MethodInfo_var);
			V_6 = L_62;
		}

IL_025a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_028b;
			}

IL_025f:
			{
				// foreach (Analysis a in analyses)
				// foreach (Analysis a in analyses)
				Analysis_t439488098 * L_63 = Enumerator_get_Current_m3316544882((&V_6), /*hidden argument*/Enumerator_get_Current_m3316544882_MethodInfo_var);
				V_5 = L_63;
				// if (a.name == data.name)
				Analysis_t439488098 * L_64 = V_5;
				// if (a.name == data.name)
				NullCheck(L_64);
				String_t* L_65 = Analysis_get_name_m4149432526(L_64, /*hidden argument*/NULL);
				AnalysisData_t108342674 * L_66 = V_3;
				// if (a.name == data.name)
				NullCheck(L_66);
				String_t* L_67 = AnalysisData_get_name_m3184943038(L_66, /*hidden argument*/NULL);
				// if (a.name == data.name)
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				bool L_68 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_65, L_67, /*hidden argument*/NULL);
				if (!L_68)
				{
					goto IL_028a;
				}
			}

IL_0280:
			{
				// a.Init(data);
				Analysis_t439488098 * L_69 = V_5;
				AnalysisData_t108342674 * L_70 = V_3;
				// a.Init(data);
				NullCheck(L_69);
				Analysis_Init_m2824569801(L_69, L_70, /*hidden argument*/NULL);
			}

IL_028a:
			{
			}

IL_028b:
			{
				// foreach (Analysis a in analyses)
				bool L_71 = Enumerator_MoveNext_m391630496((&V_6), /*hidden argument*/Enumerator_MoveNext_m391630496_MethodInfo_var);
				if (L_71)
				{
					goto IL_025f;
				}
			}

IL_0297:
			{
				IL2CPP_LEAVE(0x2AA, FINALLY_029c);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_029c;
		}

FINALLY_029c:
		{ // begin finally (depth: 2)
			// foreach (Analysis a in analyses)
			Enumerator_Dispose_m1317449898((&V_6), /*hidden argument*/Enumerator_Dispose_m1317449898_MethodInfo_var);
			IL2CPP_END_FINALLY(668)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(668)
		{
			IL2CPP_JUMP_TBL(0x2AA, IL_02aa)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_02aa:
		{
		}

IL_02ab:
		{
			// foreach (AnalysisData data in songData.analyses)
			bool L_72 = Enumerator_MoveNext_m1403247792((&V_4), /*hidden argument*/Enumerator_MoveNext_m1403247792_MethodInfo_var);
			if (L_72)
			{
				goto IL_0243;
			}
		}

IL_02b7:
		{
			IL2CPP_LEAVE(0x2CA, FINALLY_02bc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_02bc;
	}

FINALLY_02bc:
	{ // begin finally (depth: 1)
		// foreach (AnalysisData data in songData.analyses)
		Enumerator_Dispose_m2097033946((&V_4), /*hidden argument*/Enumerator_Dispose_m2097033946_MethodInfo_var);
		IL2CPP_END_FINALLY(700)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(700)
	{
		IL2CPP_JUMP_TBL(0x2CA, IL_02ca)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_02ca:
	{
		// beatTracker.Init(songData);
		BeatTracker_t2801099156 * L_73 = __this->get_beatTracker_18();
		SongData_t3132760915 * L_74 = V_2;
		// beatTracker.Init(songData);
		NullCheck(L_73);
		BeatTracker_Init_m2105785558(L_73, L_74, /*hidden argument*/NULL);
		// segmenter.Init(songData);
		Segmenter_t2695296026 * L_75 = __this->get_segmenter_19();
		SongData_t3132760915 * L_76 = V_2;
		// segmenter.Init(songData);
		NullCheck(L_75);
		Segmenter_Init_m2037373012(L_75, L_76, /*hidden argument*/NULL);
		// songLoaded = true;
		// songLoaded = true;
		RhythmTool_set_songLoaded_m1649989549(__this, (bool)1, /*hidden argument*/NULL);
		// InitializeEventProviders();
		// InitializeEventProviders();
		RhythmTool_InitializeEventProviders_m198279347(__this, /*hidden argument*/NULL);
		// if (SongLoaded != null)
		Action_t3226471752 * L_77 = __this->get_SongLoaded_2();
		if (!L_77)
		{
			goto IL_030a;
		}
	}
	{
		// SongLoaded.Invoke();
		Action_t3226471752 * L_78 = __this->get_SongLoaded_2();
		// SongLoaded.Invoke();
		NullCheck(L_78);
		Action_Invoke_m3801112262(L_78, /*hidden argument*/NULL);
		goto IL_031b;
	}

IL_030a:
	{
		// gameObject.SendMessage("OnReadyToPlay", SendMessageOptions.DontRequireReceiver);
		// gameObject.SendMessage("OnReadyToPlay", SendMessageOptions.DontRequireReceiver);
		GameObject_t1756533147 * L_79 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		// gameObject.SendMessage("OnReadyToPlay", SendMessageOptions.DontRequireReceiver);
		NullCheck(L_79);
		GameObject_SendMessage_m3997572739(L_79, _stringLiteral2751490661, 1, /*hidden argument*/NULL);
	}

IL_031b:
	{
		// return;
		goto IL_033b;
	}

IL_0320:
	{
	}

IL_0321:
	{
	}

IL_0322:
	{
		// analyzeRoutine = StartCoroutine(AsyncAnalyze(totalFrames));
		// analyzeRoutine = StartCoroutine(AsyncAnalyze(totalFrames));
		int32_t L_80 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		// analyzeRoutine = StartCoroutine(AsyncAnalyze(totalFrames));
		Il2CppObject * L_81 = RhythmTool_AsyncAnalyze_m1726189964(__this, L_80, /*hidden argument*/NULL);
		// analyzeRoutine = StartCoroutine(AsyncAnalyze(totalFrames));
		Coroutine_t2299508840 * L_82 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_81, /*hidden argument*/NULL);
		__this->set_analyzeRoutine_20(L_82);
	}

IL_033b:
	{
		// }
		return;
	}
}
// System.Void RhythmTool::EndOfAnalysis()
extern "C"  void RhythmTool_EndOfAnalysis_m2036138409 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_EndOfAnalysis_m2036138409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3772431102 * V_0 = NULL;
	Analysis_t439488098 * V_1 = NULL;
	Enumerator_t3638306200  V_2;
	memset(&V_2, 0, sizeof(V_2));
	SongData_t3132760915 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// if (_calculateTempo)
		bool L_0 = __this->get__calculateTempo_15();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		// beatTracker.FillEnd();
		BeatTracker_t2801099156 * L_1 = __this->get_beatTracker_18();
		// beatTracker.FillEnd();
		NullCheck(L_1);
		BeatTracker_FillEnd_m1691323021(L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		// if (_preCalculate)
		bool L_2 = __this->get__preCalculate_14();
		if (!L_2)
		{
			goto IL_00ab;
		}
	}
	{
		// if (_storeAnalyses)
		bool L_3 = __this->get__storeAnalyses_16();
		if (!L_3)
		{
			goto IL_00aa;
		}
	}
	{
		// List<AnalysisData> datas = new List<AnalysisData>();
		List_1_t3772431102 * L_4 = (List_1_t3772431102 *)il2cpp_codegen_object_new(List_1_t3772431102_il2cpp_TypeInfo_var);
		List_1__ctor_m2896249751(L_4, /*hidden argument*/List_1__ctor_m2896249751_MethodInfo_var);
		V_0 = L_4;
		// foreach (Analysis a in analyses)
		List_1_t4103576526 * L_5 = __this->get_analyses_29();
		// foreach (Analysis a in analyses)
		NullCheck(L_5);
		Enumerator_t3638306200  L_6 = List_1_GetEnumerator_m3071804604(L_5, /*hidden argument*/List_1_GetEnumerator_m3071804604_MethodInfo_var);
		V_2 = L_6;
	}

IL_0042:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005b;
		}

IL_0047:
		{
			// foreach (Analysis a in analyses)
			// foreach (Analysis a in analyses)
			Analysis_t439488098 * L_7 = Enumerator_get_Current_m3316544882((&V_2), /*hidden argument*/Enumerator_get_Current_m3316544882_MethodInfo_var);
			V_1 = L_7;
			// datas.Add(a.analysisData);
			List_1_t3772431102 * L_8 = V_0;
			Analysis_t439488098 * L_9 = V_1;
			// datas.Add(a.analysisData);
			NullCheck(L_9);
			AnalysisData_t108342674 * L_10 = Analysis_get_analysisData_m3591218099(L_9, /*hidden argument*/NULL);
			// datas.Add(a.analysisData);
			NullCheck(L_8);
			List_1_Add_m3745327547(L_8, L_10, /*hidden argument*/List_1_Add_m3745327547_MethodInfo_var);
		}

IL_005b:
		{
			// foreach (Analysis a in analyses)
			bool L_11 = Enumerator_MoveNext_m391630496((&V_2), /*hidden argument*/Enumerator_MoveNext_m391630496_MethodInfo_var);
			if (L_11)
			{
				goto IL_0047;
			}
		}

IL_0067:
		{
			IL2CPP_LEAVE(0x7A, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		// foreach (Analysis a in analyses)
		Enumerator_Dispose_m1317449898((&V_2), /*hidden argument*/Enumerator_Dispose_m1317449898_MethodInfo_var);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x7A, IL_007a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007a:
	{
		// SongData songData = new SongData(datas, audioSource.clip.name, totalFrames, beatTracker, segmenter);
		List_1_t3772431102 * L_12 = V_0;
		AudioSource_t1135106623 * L_13 = __this->get_audioSource_34();
		// SongData songData = new SongData(datas, audioSource.clip.name, totalFrames, beatTracker, segmenter);
		NullCheck(L_13);
		AudioClip_t1932558630 * L_14 = AudioSource_get_clip_m2127996365(L_13, /*hidden argument*/NULL);
		// SongData songData = new SongData(datas, audioSource.clip.name, totalFrames, beatTracker, segmenter);
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		// SongData songData = new SongData(datas, audioSource.clip.name, totalFrames, beatTracker, segmenter);
		int32_t L_16 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		BeatTracker_t2801099156 * L_17 = __this->get_beatTracker_18();
		Segmenter_t2695296026 * L_18 = __this->get_segmenter_19();
		// SongData songData = new SongData(datas, audioSource.clip.name, totalFrames, beatTracker, segmenter);
		SongData_t3132760915 * L_19 = (SongData_t3132760915 *)il2cpp_codegen_object_new(SongData_t3132760915_il2cpp_TypeInfo_var);
		SongData__ctor_m676028367(L_19, L_12, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		// songData.Serialize();
		SongData_t3132760915 * L_20 = V_3;
		// songData.Serialize();
		NullCheck(L_20);
		SongData_Serialize_m1654775970(L_20, /*hidden argument*/NULL);
	}

IL_00aa:
	{
	}

IL_00ab:
	{
		// analysisDone = true;
		// analysisDone = true;
		RhythmTool_set_analysisDone_m701052397(__this, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void RhythmTool::EndOfSong()
extern "C"  void RhythmTool_EndOfSong_m2843491136 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_EndOfSong_m2843491136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// Stop();
		// Stop();
		RhythmTool_Stop_m1172477109(__this, /*hidden argument*/NULL);
		// if (SongEnded != null)
		Action_t3226471752 * L_0 = __this->get_SongEnded_3();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		// SongEnded.Invoke();
		Action_t3226471752 * L_1 = __this->get_SongEnded_3();
		// SongEnded.Invoke();
		NullCheck(L_1);
		Action_Invoke_m3801112262(L_1, /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_0022:
	{
		// gameObject.SendMessage("OnEndOfSong", SendMessageOptions.DontRequireReceiver);
		// gameObject.SendMessage("OnEndOfSong", SendMessageOptions.DontRequireReceiver);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		// gameObject.SendMessage("OnEndOfSong", SendMessageOptions.DontRequireReceiver);
		NullCheck(L_2);
		GameObject_SendMessage_m3997572739(L_2, _stringLiteral2957948280, 1, /*hidden argument*/NULL);
	}

IL_0033:
	{
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		V_0 = 0;
		goto IL_0053;
	}

IL_003a:
	{
		// RhythmEventProvider.eventProviders[i].onSongEnded.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_3 = RhythmEventProvider_get_eventProviders_m1235811388(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		// RhythmEventProvider.eventProviders[i].onSongEnded.Invoke();
		NullCheck(L_3);
		RhythmEventProvider_t215006757 * L_5 = ReadOnlyCollection_1_get_Item_m2127664468(L_3, L_4, /*hidden argument*/ReadOnlyCollection_1_get_Item_m2127664468_MethodInfo_var);
		NullCheck(L_5);
		UnityEvent_t408735097 * L_6 = L_5->get_onSongEnded_16();
		// RhythmEventProvider.eventProviders[i].onSongEnded.Invoke();
		NullCheck(L_6);
		UnityEvent_Invoke_m4163344491(L_6, /*hidden argument*/NULL);
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0053:
	{
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		int32_t L_8 = V_0;
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_9 = RhythmEventProvider_get_eventProviders_m1235811388(NULL /*static, unused*/, /*hidden argument*/NULL);
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		NullCheck(L_9);
		int32_t L_10 = ReadOnlyCollection_1_get_Count_m3788110603(L_9, /*hidden argument*/ReadOnlyCollection_1_get_Count_m3788110603_MethodInfo_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_003a;
		}
	}
	{
		// }
		return;
	}
}
// System.Void RhythmTool::Update()
extern "C"  void RhythmTool_Update_m3028187676 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_Update_m3028187676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Beat_t2695683572 * V_0 = NULL;
	{
		// if (!songLoaded)
		// if (!songLoaded)
		bool L_0 = RhythmTool_get_songLoaded_m3458667938(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// return;
		goto IL_0118;
	}

IL_0011:
	{
		// if (audioSource.clip == null)
		AudioSource_t1135106623 * L_1 = __this->get_audioSource_34();
		// if (audioSource.clip == null)
		NullCheck(L_1);
		AudioClip_t1932558630 * L_2 = AudioSource_get_clip_m2127996365(L_1, /*hidden argument*/NULL);
		// if (audioSource.clip == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		// return;
		goto IL_0118;
	}

IL_002c:
	{
		// if (isPlaying)
		// if (isPlaying)
		bool L_4 = RhythmTool_get_isPlaying_m2081879838(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0094;
		}
	}
	{
		// currentSample = Mathf.Clamp(currentSample + audioSource.clip.frequency * Time.unscaledDeltaTime * audioSource.pitch,
		// currentSample = Mathf.Clamp(currentSample + audioSource.clip.frequency * Time.unscaledDeltaTime * audioSource.pitch,
		float L_5 = RhythmTool_get_currentSample_m2446084809(__this, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_6 = __this->get_audioSource_34();
		// currentSample = Mathf.Clamp(currentSample + audioSource.clip.frequency * Time.unscaledDeltaTime * audioSource.pitch,
		NullCheck(L_6);
		AudioClip_t1932558630 * L_7 = AudioSource_get_clip_m2127996365(L_6, /*hidden argument*/NULL);
		// currentSample = Mathf.Clamp(currentSample + audioSource.clip.frequency * Time.unscaledDeltaTime * audioSource.pitch,
		NullCheck(L_7);
		int32_t L_8 = AudioClip_get_frequency_m237362468(L_7, /*hidden argument*/NULL);
		// currentSample = Mathf.Clamp(currentSample + audioSource.clip.frequency * Time.unscaledDeltaTime * audioSource.pitch,
		float L_9 = Time_get_unscaledDeltaTime_m4281640537(NULL /*static, unused*/, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_10 = __this->get_audioSource_34();
		// currentSample = Mathf.Clamp(currentSample + audioSource.clip.frequency * Time.unscaledDeltaTime * audioSource.pitch,
		NullCheck(L_10);
		float L_11 = AudioSource_get_pitch_m4220572439(L_10, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_12 = __this->get_audioSource_34();
		// audioSource.timeSamples - frameSpacing / 2,
		NullCheck(L_12);
		int32_t L_13 = AudioSource_get_timeSamples_m1719074425(L_12, /*hidden argument*/NULL);
		// audioSource.timeSamples - frameSpacing / 2,
		int32_t L_14 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_15 = __this->get_audioSource_34();
		// audioSource.timeSamples + frameSpacing / 2);
		NullCheck(L_15);
		int32_t L_16 = AudioSource_get_timeSamples_m1719074425(L_15, /*hidden argument*/NULL);
		// audioSource.timeSamples + frameSpacing / 2);
		int32_t L_17 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		// currentSample = Mathf.Clamp(currentSample + audioSource.clip.frequency * Time.unscaledDeltaTime * audioSource.pitch,
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_5+(float)((float)((float)((float)((float)(((float)((float)L_8)))*(float)L_9))*(float)L_11)))), (((float)((float)((int32_t)((int32_t)L_13-(int32_t)((int32_t)((int32_t)L_14/(int32_t)2))))))), (((float)((float)((int32_t)((int32_t)L_16+(int32_t)((int32_t)((int32_t)L_17/(int32_t)2))))))), /*hidden argument*/NULL);
		// currentSample = Mathf.Clamp(currentSample + audioSource.clip.frequency * Time.unscaledDeltaTime * audioSource.pitch,
		RhythmTool_set_currentSample_m1718109522(__this, L_18, /*hidden argument*/NULL);
	}

IL_0094:
	{
		// interpolation = currentSample / frameSpacing;
		// interpolation = currentSample / frameSpacing;
		float L_19 = RhythmTool_get_currentSample_m2446084809(__this, /*hidden argument*/NULL);
		// interpolation = currentSample / frameSpacing;
		int32_t L_20 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		// interpolation = currentSample / frameSpacing;
		RhythmTool_set_interpolation_m2276852035(__this, ((float)((float)L_19/(float)(((float)((float)L_20))))), /*hidden argument*/NULL);
		// currentFrame = (int)interpolation;
		// currentFrame = (int)interpolation;
		float L_21 = RhythmTool_get_interpolation_m2645903814(__this, /*hidden argument*/NULL);
		// currentFrame = (int)interpolation;
		RhythmTool_set_currentFrame_m2370147509(__this, (((int32_t)((int32_t)L_21))), /*hidden argument*/NULL);
		// interpolation -= currentFrame;
		// interpolation -= currentFrame;
		float L_22 = RhythmTool_get_interpolation_m2645903814(__this, /*hidden argument*/NULL);
		// interpolation -= currentFrame;
		int32_t L_23 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		// interpolation -= currentFrame;
		RhythmTool_set_interpolation_m2276852035(__this, ((float)((float)L_22-(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		// if (currentFrame >= totalFrames - 1)
		// if (currentFrame >= totalFrames - 1)
		int32_t L_24 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		// if (currentFrame >= totalFrames - 1)
		int32_t L_25 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_24) < ((int32_t)((int32_t)((int32_t)L_25-(int32_t)1)))))
		{
			goto IL_00e7;
		}
	}
	{
		// EndOfSong();
		// EndOfSong();
		RhythmTool_EndOfSong_m2843491136(__this, /*hidden argument*/NULL);
		// return;
		goto IL_0118;
	}

IL_00e7:
	{
		// Beat nextBeat = NextBeat(currentFrame);
		// Beat nextBeat = NextBeat(currentFrame);
		int32_t L_26 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		// Beat nextBeat = NextBeat(currentFrame);
		Beat_t2695683572 * L_27 = RhythmTool_NextBeat_m782067910(__this, L_26, /*hidden argument*/NULL);
		V_0 = L_27;
		// beatLength = nextBeat.length;
		Beat_t2695683572 * L_28 = V_0;
		NullCheck(L_28);
		float L_29 = L_28->get_length_0();
		// beatLength = nextBeat.length;
		RhythmTool_set_beatLength_m1126195059(__this, L_29, /*hidden argument*/NULL);
		// bpm = nextBeat.bpm;
		Beat_t2695683572 * L_30 = V_0;
		NullCheck(L_30);
		float L_31 = L_30->get_bpm_1();
		// bpm = nextBeat.bpm;
		RhythmTool_set_bpm_m3815049600(__this, L_31, /*hidden argument*/NULL);
		// Analyze();
		// Analyze();
		RhythmTool_Analyze_m1399810877(__this, /*hidden argument*/NULL);
		// PassData();
		// PassData();
		RhythmTool_PassData_m3430060328(__this, /*hidden argument*/NULL);
	}

IL_0118:
	{
		// }
		return;
	}
}
// System.Void RhythmTool::Analyze()
extern "C"  void RhythmTool_Analyze_m1399810877 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_Analyze_m1399810877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Analysis_t439488098 * V_1 = NULL;
	Enumerator_t3638306200  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// if (analysisDone)
		// if (analysisDone)
		bool L_0 = RhythmTool_get_analysisDone_m3570634808(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		// return;
		goto IL_0186;
	}

IL_0011:
	{
		// if (_preCalculate)
		bool L_1 = __this->get__preCalculate_14();
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		// _lead += 300;
		int32_t L_2 = __this->get__lead_17();
		__this->set__lead_17(((int32_t)((int32_t)L_2+(int32_t)((int32_t)300))));
	}

IL_002e:
	{
		// _lead = Mathf.Max(300, _lead);
		int32_t L_3 = __this->get__lead_17();
		// _lead = Mathf.Max(300, _lead);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Max_m1875893177(NULL /*static, unused*/, ((int32_t)300), L_3, /*hidden argument*/NULL);
		__this->set__lead_17(L_4);
		// for (int i = lastFrame + 1; i < currentFrame + _lead + 300; i++)
		// for (int i = lastFrame + 1; i < currentFrame + _lead + 300; i++)
		int32_t L_5 = RhythmTool_get_lastFrame_m3065909813(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
		goto IL_016d;
	}

IL_0052:
	{
		// if (i >= totalFrames)
		int32_t L_6 = V_0;
		// if (i >= totalFrames)
		int32_t L_7 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_006b;
		}
	}
	{
		// EndOfAnalysis();
		// EndOfAnalysis();
		RhythmTool_EndOfAnalysis_m2036138409(__this, /*hidden argument*/NULL);
		// break;
		goto IL_0186;
	}

IL_006b:
	{
		// audioSource.clip.GetData(samples, Mathf.Max((i * frameSpacing) - (samples.Length / 2), 0));
		AudioSource_t1135106623 * L_8 = __this->get_audioSource_34();
		// audioSource.clip.GetData(samples, Mathf.Max((i * frameSpacing) - (samples.Length / 2), 0));
		NullCheck(L_8);
		AudioClip_t1932558630 * L_9 = AudioSource_get_clip_m2127996365(L_8, /*hidden argument*/NULL);
		SingleU5BU5D_t577127397* L_10 = __this->get_samples_22();
		int32_t L_11 = V_0;
		// audioSource.clip.GetData(samples, Mathf.Max((i * frameSpacing) - (samples.Length / 2), 0));
		int32_t L_12 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		SingleU5BU5D_t577127397* L_13 = __this->get_samples_22();
		NullCheck(L_13);
		// audioSource.clip.GetData(samples, Mathf.Max((i * frameSpacing) - (samples.Length / 2), 0));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_14 = Mathf_Max_m1875893177(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)L_11*(int32_t)L_12))-(int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))/(int32_t)2)))), 0, /*hidden argument*/NULL);
		// audioSource.clip.GetData(samples, Mathf.Max((i * frameSpacing) - (samples.Length / 2), 0));
		NullCheck(L_9);
		AudioClip_GetData_m1645657273(L_9, L_10, L_14, /*hidden argument*/NULL);
		// Util.GetMono(samples, monoSamples, channels);
		SingleU5BU5D_t577127397* L_15 = __this->get_samples_22();
		SingleU5BU5D_t577127397* L_16 = __this->get_monoSamples_23();
		int32_t L_17 = __this->get_channels_25();
		// Util.GetMono(samples, monoSamples, channels);
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		Util_GetMono_m2664450603(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		// Util.GetSpectrum(monoSamples);
		SingleU5BU5D_t577127397* L_18 = __this->get_monoSamples_23();
		// Util.GetSpectrum(monoSamples);
		Util_GetSpectrum_m1376041373(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		// Util.GetSpectrumMagnitude(monoSamples, spectrum);
		SingleU5BU5D_t577127397* L_19 = __this->get_monoSamples_23();
		SingleU5BU5D_t577127397* L_20 = __this->get_spectrum_24();
		// Util.GetSpectrumMagnitude(monoSamples, spectrum);
		Util_GetSpectrumMagnitude_m3600392938(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		// foreach (Analysis s in analyses)
		List_1_t4103576526 * L_21 = __this->get_analyses_29();
		// foreach (Analysis s in analyses)
		NullCheck(L_21);
		Enumerator_t3638306200  L_22 = List_1_GetEnumerator_m3071804604(L_21, /*hidden argument*/List_1_GetEnumerator_m3071804604_MethodInfo_var);
		V_2 = L_22;
	}

IL_00da:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00f6;
		}

IL_00df:
		{
			// foreach (Analysis s in analyses)
			// foreach (Analysis s in analyses)
			Analysis_t439488098 * L_23 = Enumerator_get_Current_m3316544882((&V_2), /*hidden argument*/Enumerator_get_Current_m3316544882_MethodInfo_var);
			V_1 = L_23;
			// s.Analyze(spectrum, i);
			Analysis_t439488098 * L_24 = V_1;
			SingleU5BU5D_t577127397* L_25 = __this->get_spectrum_24();
			int32_t L_26 = V_0;
			// s.Analyze(spectrum, i);
			NullCheck(L_24);
			Analysis_Analyze_m2787083839(L_24, L_25, L_26, /*hidden argument*/NULL);
		}

IL_00f6:
		{
			// foreach (Analysis s in analyses)
			bool L_27 = Enumerator_MoveNext_m391630496((&V_2), /*hidden argument*/Enumerator_MoveNext_m391630496_MethodInfo_var);
			if (L_27)
			{
				goto IL_00df;
			}
		}

IL_0102:
		{
			IL2CPP_LEAVE(0x115, FINALLY_0107);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0107;
	}

FINALLY_0107:
	{ // begin finally (depth: 1)
		// foreach (Analysis s in analyses)
		Enumerator_Dispose_m1317449898((&V_2), /*hidden argument*/Enumerator_Dispose_m1317449898_MethodInfo_var);
		IL2CPP_END_FINALLY(263)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(263)
	{
		IL2CPP_JUMP_TBL(0x115, IL_0115)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0115:
	{
		// if (_calculateTempo)
		bool L_28 = __this->get__calculateTempo_15();
		if (!L_28)
		{
			goto IL_014f;
		}
	}
	{
		// if (i - 10 >= 0)
		int32_t L_29 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_29-(int32_t)((int32_t)10)))) < ((int32_t)0)))
		{
			goto IL_014e;
		}
	}
	{
		// float flux = all.flux[i - 10];
		// float flux = all.flux[i - 10];
		AnalysisData_t108342674 * L_30 = RhythmTool_get_all_m1284823694(__this, /*hidden argument*/NULL);
		// float flux = all.flux[i - 10];
		NullCheck(L_30);
		ReadOnlyCollection_1_t2262295624 * L_31 = AnalysisData_get_flux_m3559386780(L_30, /*hidden argument*/NULL);
		int32_t L_32 = V_0;
		// float flux = all.flux[i - 10];
		NullCheck(L_31);
		float L_33 = ReadOnlyCollection_1_get_Item_m210738684(L_31, ((int32_t)((int32_t)L_32-(int32_t)((int32_t)10))), /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_3 = L_33;
		// beatTracker.TrackBeat(flux);
		BeatTracker_t2801099156 * L_34 = __this->get_beatTracker_18();
		float L_35 = V_3;
		// beatTracker.TrackBeat(flux);
		NullCheck(L_34);
		BeatTracker_TrackBeat_m2149388341(L_34, L_35, /*hidden argument*/NULL);
	}

IL_014e:
	{
	}

IL_014f:
	{
		// segmenter.DetectChanges(i - 350);
		Segmenter_t2695296026 * L_36 = __this->get_segmenter_19();
		int32_t L_37 = V_0;
		// segmenter.DetectChanges(i - 350);
		NullCheck(L_36);
		Segmenter_DetectChanges_m3953497980(L_36, ((int32_t)((int32_t)L_37-(int32_t)((int32_t)350))), /*hidden argument*/NULL);
		// lastFrame = i;
		int32_t L_38 = V_0;
		// lastFrame = i;
		RhythmTool_set_lastFrame_m3646349138(__this, L_38, /*hidden argument*/NULL);
		// for (int i = lastFrame + 1; i < currentFrame + _lead + 300; i++)
		int32_t L_39 = V_0;
		V_0 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_016d:
	{
		// for (int i = lastFrame + 1; i < currentFrame + _lead + 300; i++)
		int32_t L_40 = V_0;
		// for (int i = lastFrame + 1; i < currentFrame + _lead + 300; i++)
		int32_t L_41 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		int32_t L_42 = __this->get__lead_17();
		if ((((int32_t)L_40) < ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_41+(int32_t)L_42))+(int32_t)((int32_t)300))))))
		{
			goto IL_0052;
		}
	}

IL_0186:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator RhythmTool::AsyncAnalyze(System.Int32)
extern "C"  Il2CppObject * RhythmTool_AsyncAnalyze_m1726189964 (RhythmTool_t215962618 * __this, int32_t ___frames0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_AsyncAnalyze_m1726189964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * L_0 = (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 *)il2cpp_codegen_object_new(U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971_il2cpp_TypeInfo_var);
		U3CAsyncAnalyzeU3Ec__Iterator1__ctor_m1537308272(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * L_1 = V_0;
		int32_t L_2 = ___frames0;
		NullCheck(L_1);
		L_1->set_frames_0(L_2);
		U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_3(__this);
		U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * L_4 = V_0;
		V_1 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_5 = V_1;
		return L_5;
	}
}
// System.Void RhythmTool::BackGroundAnalyze(System.Object)
extern "C"  void RhythmTool_BackGroundAnalyze_m3994437969 (RhythmTool_t215962618 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_BackGroundAnalyze_m3994437969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Analysis_t439488098 * V_4 = NULL;
	Enumerator_t3638306200  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// float[] s = (float[])o;
		Il2CppObject * L_0 = ___o0;
		V_0 = ((SingleU5BU5D_t577127397*)Castclass(L_0, SingleU5BU5D_t577127397_il2cpp_TypeInfo_var));
		// int count = s.Length / frameSpacing / channels;
		SingleU5BU5D_t577127397* L_1 = V_0;
		NullCheck(L_1);
		// int count = s.Length / frameSpacing / channels;
		int32_t L_2 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_channels_25();
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))/(int32_t)L_2))/(int32_t)L_3));
		// for (int i = 0; i < count; i++)
		V_2 = 0;
		goto IL_0151;
	}

IL_0020:
	{
		// for (int ii = 0; ii < samples.Length; ii++)
		V_3 = 0;
		goto IL_005b;
	}

IL_0028:
	{
		// samples[ii] = s[Mathf.Max((i * frameSpacing * channels) - (fftWindowSize * channels), 0) + ii];
		SingleU5BU5D_t577127397* L_4 = __this->get_samples_22();
		int32_t L_5 = V_3;
		SingleU5BU5D_t577127397* L_6 = V_0;
		int32_t L_7 = V_2;
		// samples[ii] = s[Mathf.Max((i * frameSpacing * channels) - (fftWindowSize * channels), 0) + ii];
		int32_t L_8 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_channels_25();
		// samples[ii] = s[Mathf.Max((i * frameSpacing * channels) - (fftWindowSize * channels), 0) + ii];
		int32_t L_10 = RhythmTool_get_fftWindowSize_m3317244415(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_channels_25();
		// samples[ii] = s[Mathf.Max((i * frameSpacing * channels) - (fftWindowSize * channels), 0) + ii];
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_12 = Mathf_Max_m1875893177(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7*(int32_t)L_8))*(int32_t)L_9))-(int32_t)((int32_t)((int32_t)L_10*(int32_t)L_11)))), 0, /*hidden argument*/NULL);
		int32_t L_13 = V_3;
		NullCheck(L_6);
		int32_t L_14 = ((int32_t)((int32_t)L_12+(int32_t)L_13));
		float L_15 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (float)L_15);
		// for (int ii = 0; ii < samples.Length; ii++)
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		// for (int ii = 0; ii < samples.Length; ii++)
		int32_t L_17 = V_3;
		SingleU5BU5D_t577127397* L_18 = __this->get_samples_22();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0028;
		}
	}
	{
		// Util.GetMono(samples, monoSamples, channels);
		SingleU5BU5D_t577127397* L_19 = __this->get_samples_22();
		SingleU5BU5D_t577127397* L_20 = __this->get_monoSamples_23();
		int32_t L_21 = __this->get_channels_25();
		// Util.GetMono(samples, monoSamples, channels);
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		Util_GetMono_m2664450603(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		// Util.GetSpectrum(monoSamples);
		SingleU5BU5D_t577127397* L_22 = __this->get_monoSamples_23();
		// Util.GetSpectrum(monoSamples);
		Util_GetSpectrum_m1376041373(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		// Util.GetSpectrumMagnitude(monoSamples, spectrum);
		SingleU5BU5D_t577127397* L_23 = __this->get_monoSamples_23();
		SingleU5BU5D_t577127397* L_24 = __this->get_spectrum_24();
		// Util.GetSpectrumMagnitude(monoSamples, spectrum);
		Util_GetSpectrumMagnitude_m3600392938(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		// foreach (Analysis a in analyses)
		List_1_t4103576526 * L_25 = __this->get_analyses_29();
		// foreach (Analysis a in analyses)
		NullCheck(L_25);
		Enumerator_t3638306200  L_26 = List_1_GetEnumerator_m3071804604(L_25, /*hidden argument*/List_1_GetEnumerator_m3071804604_MethodInfo_var);
		V_5 = L_26;
	}

IL_00aa:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c8;
		}

IL_00af:
		{
			// foreach (Analysis a in analyses)
			// foreach (Analysis a in analyses)
			Analysis_t439488098 * L_27 = Enumerator_get_Current_m3316544882((&V_5), /*hidden argument*/Enumerator_get_Current_m3316544882_MethodInfo_var);
			V_4 = L_27;
			// a.Analyze(spectrum, i);
			Analysis_t439488098 * L_28 = V_4;
			SingleU5BU5D_t577127397* L_29 = __this->get_spectrum_24();
			int32_t L_30 = V_2;
			// a.Analyze(spectrum, i);
			NullCheck(L_28);
			Analysis_Analyze_m2787083839(L_28, L_29, L_30, /*hidden argument*/NULL);
		}

IL_00c8:
		{
			// foreach (Analysis a in analyses)
			bool L_31 = Enumerator_MoveNext_m391630496((&V_5), /*hidden argument*/Enumerator_MoveNext_m391630496_MethodInfo_var);
			if (L_31)
			{
				goto IL_00af;
			}
		}

IL_00d4:
		{
			IL2CPP_LEAVE(0xE7, FINALLY_00d9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d9;
	}

FINALLY_00d9:
	{ // begin finally (depth: 1)
		// foreach (Analysis a in analyses)
		Enumerator_Dispose_m1317449898((&V_5), /*hidden argument*/Enumerator_Dispose_m1317449898_MethodInfo_var);
		IL2CPP_END_FINALLY(217)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(217)
	{
		IL2CPP_JUMP_TBL(0xE7, IL_00e7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e7:
	{
		// if (_calculateTempo)
		bool L_32 = __this->get__calculateTempo_15();
		if (!L_32)
		{
			goto IL_0123;
		}
	}
	{
		// if (i - 10 >= 0)
		int32_t L_33 = V_2;
		if ((((int32_t)((int32_t)((int32_t)L_33-(int32_t)((int32_t)10)))) < ((int32_t)0)))
		{
			goto IL_0122;
		}
	}
	{
		// float flux = all.flux[i - 10];
		// float flux = all.flux[i - 10];
		AnalysisData_t108342674 * L_34 = RhythmTool_get_all_m1284823694(__this, /*hidden argument*/NULL);
		// float flux = all.flux[i - 10];
		NullCheck(L_34);
		ReadOnlyCollection_1_t2262295624 * L_35 = AnalysisData_get_flux_m3559386780(L_34, /*hidden argument*/NULL);
		int32_t L_36 = V_2;
		// float flux = all.flux[i - 10];
		NullCheck(L_35);
		float L_37 = ReadOnlyCollection_1_get_Item_m210738684(L_35, ((int32_t)((int32_t)L_36-(int32_t)((int32_t)10))), /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_6 = L_37;
		// beatTracker.TrackBeat(flux);
		BeatTracker_t2801099156 * L_38 = __this->get_beatTracker_18();
		float L_39 = V_6;
		// beatTracker.TrackBeat(flux);
		NullCheck(L_38);
		BeatTracker_TrackBeat_m2149388341(L_38, L_39, /*hidden argument*/NULL);
	}

IL_0122:
	{
	}

IL_0123:
	{
		// segmenter.DetectChanges(i - 350);
		Segmenter_t2695296026 * L_40 = __this->get_segmenter_19();
		int32_t L_41 = V_2;
		// segmenter.DetectChanges(i - 350);
		NullCheck(L_40);
		Segmenter_DetectChanges_m3953497980(L_40, ((int32_t)((int32_t)L_41-(int32_t)((int32_t)350))), /*hidden argument*/NULL);
		// lastFrame = i;
		int32_t L_42 = V_2;
		// lastFrame = i;
		RhythmTool_set_lastFrame_m3646349138(__this, L_42, /*hidden argument*/NULL);
		// if (queueRoutine != null)
		Coroutine_t2299508840 * L_43 = __this->get_queueRoutine_21();
		if (!L_43)
		{
			goto IL_014c;
		}
	}
	{
		// break;
		goto IL_0158;
	}

IL_014c:
	{
		// for (int i = 0; i < count; i++)
		int32_t L_44 = V_2;
		V_2 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_0151:
	{
		// for (int i = 0; i < count; i++)
		int32_t L_45 = V_2;
		int32_t L_46 = V_1;
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_0020;
		}
	}

IL_0158:
	{
		// }
		return;
	}
}
// System.Void RhythmTool::PassData()
extern "C"  void RhythmTool_PassData_m3430060328 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_PassData_m3430060328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RhythmEventProvider_t215006757 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RhythmEventProvider_t215006757 * V_4 = NULL;
	{
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		V_0 = 0;
		goto IL_014f;
	}

IL_0008:
	{
		// RhythmEventProvider eventProvider = RhythmEventProvider.eventProviders[i];
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_0 = RhythmEventProvider_get_eventProviders_m1235811388(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		// RhythmEventProvider eventProvider = RhythmEventProvider.eventProviders[i];
		NullCheck(L_0);
		RhythmEventProvider_t215006757 * L_2 = ReadOnlyCollection_1_get_Item_m2127664468(L_0, L_1, /*hidden argument*/ReadOnlyCollection_1_get_Item_m2127664468_MethodInfo_var);
		V_1 = L_2;
		// eventProvider.targetOffset = Mathf.Clamp(eventProvider.targetOffset, 0, _lead - 25);
		RhythmEventProvider_t215006757 * L_3 = V_1;
		RhythmEventProvider_t215006757 * L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_targetOffset_3();
		int32_t L_6 = __this->get__lead_17();
		// eventProvider.targetOffset = Mathf.Clamp(eventProvider.targetOffset, 0, _lead - 25);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, L_5, 0, ((int32_t)((int32_t)L_6-(int32_t)((int32_t)25))), /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_targetOffset_3(L_7);
		// eventProvider.currentFrame = currentFrame;
		RhythmEventProvider_t215006757 * L_8 = V_1;
		// eventProvider.currentFrame = currentFrame;
		int32_t L_9 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_currentFrame_6(L_9);
		// eventProvider.interpolation = interpolation;
		RhythmEventProvider_t215006757 * L_10 = V_1;
		// eventProvider.interpolation = interpolation;
		float L_11 = RhythmTool_get_interpolation_m2645903814(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_interpolation_7(L_11);
		// if (eventProvider.offset != eventProvider.targetOffset)
		RhythmEventProvider_t215006757 * L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_offset_4();
		RhythmEventProvider_t215006757 * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_targetOffset_3();
		if ((((int32_t)L_13) == ((int32_t)L_15)))
		{
			goto IL_00cd;
		}
	}
	{
		// while (eventProvider.offset < eventProvider.targetOffset)
		goto IL_009f;
	}

IL_005f:
	{
		// PassSubBeat(eventProvider, currentFrame + eventProvider.offset + interpolation);
		RhythmEventProvider_t215006757 * L_16 = V_1;
		// PassSubBeat(eventProvider, currentFrame + eventProvider.offset + interpolation);
		int32_t L_17 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		RhythmEventProvider_t215006757 * L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->get_offset_4();
		// PassSubBeat(eventProvider, currentFrame + eventProvider.offset + interpolation);
		float L_20 = RhythmTool_get_interpolation_m2645903814(__this, /*hidden argument*/NULL);
		// PassSubBeat(eventProvider, currentFrame + eventProvider.offset + interpolation);
		RhythmTool_PassSubBeat_m3729313650(__this, L_16, ((float)((float)(((float)((float)((int32_t)((int32_t)L_17+(int32_t)L_19)))))+(float)L_20)), /*hidden argument*/NULL);
		// PassEvents(eventProvider, currentFrame + eventProvider.offset);
		RhythmEventProvider_t215006757 * L_21 = V_1;
		// PassEvents(eventProvider, currentFrame + eventProvider.offset);
		int32_t L_22 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		RhythmEventProvider_t215006757 * L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_offset_4();
		// PassEvents(eventProvider, currentFrame + eventProvider.offset);
		RhythmTool_PassEvents_m4272423917(__this, L_21, ((int32_t)((int32_t)L_22+(int32_t)L_24)), /*hidden argument*/NULL);
		// eventProvider.offset++;
		RhythmEventProvider_t215006757 * L_25 = V_1;
		RhythmEventProvider_t215006757 * L_26 = L_25;
		NullCheck(L_26);
		int32_t L_27 = L_26->get_offset_4();
		NullCheck(L_26);
		L_26->set_offset_4(((int32_t)((int32_t)L_27+(int32_t)1)));
	}

IL_009f:
	{
		// while (eventProvider.offset < eventProvider.targetOffset)
		RhythmEventProvider_t215006757 * L_28 = V_1;
		NullCheck(L_28);
		int32_t L_29 = L_28->get_offset_4();
		RhythmEventProvider_t215006757 * L_30 = V_1;
		NullCheck(L_30);
		int32_t L_31 = L_30->get_targetOffset_3();
		if ((((int32_t)L_29) < ((int32_t)L_31)))
		{
			goto IL_005f;
		}
	}
	{
		// eventProvider.offset = Mathf.Min(eventProvider.offset, eventProvider.targetOffset);
		RhythmEventProvider_t215006757 * L_32 = V_1;
		RhythmEventProvider_t215006757 * L_33 = V_1;
		NullCheck(L_33);
		int32_t L_34 = L_33->get_offset_4();
		RhythmEventProvider_t215006757 * L_35 = V_1;
		NullCheck(L_35);
		int32_t L_36 = L_35->get_targetOffset_3();
		// eventProvider.offset = Mathf.Min(eventProvider.offset, eventProvider.targetOffset);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_37 = Mathf_Min_m2906823867(NULL /*static, unused*/, L_34, L_36, /*hidden argument*/NULL);
		NullCheck(L_32);
		L_32->set_offset_4(L_37);
		goto IL_00e9;
	}

IL_00cd:
	{
		// PassSubBeat(eventProvider, currentFrame + eventProvider.offset + interpolation);
		RhythmEventProvider_t215006757 * L_38 = V_1;
		// PassSubBeat(eventProvider, currentFrame + eventProvider.offset + interpolation);
		int32_t L_39 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		RhythmEventProvider_t215006757 * L_40 = V_1;
		NullCheck(L_40);
		int32_t L_41 = L_40->get_offset_4();
		// PassSubBeat(eventProvider, currentFrame + eventProvider.offset + interpolation);
		float L_42 = RhythmTool_get_interpolation_m2645903814(__this, /*hidden argument*/NULL);
		// PassSubBeat(eventProvider, currentFrame + eventProvider.offset + interpolation);
		RhythmTool_PassSubBeat_m3729313650(__this, L_38, ((float)((float)(((float)((float)((int32_t)((int32_t)L_39+(int32_t)L_41)))))+(float)L_42)), /*hidden argument*/NULL);
	}

IL_00e9:
	{
		// if (eventProvider.timingUpdate.listenerCount != 0)
		RhythmEventProvider_t215006757 * L_43 = V_1;
		NullCheck(L_43);
		TimingUpdateEvent_t543213975 * L_44 = L_43->get_timingUpdate_13();
		// if (eventProvider.timingUpdate.listenerCount != 0)
		NullCheck(L_44);
		int32_t L_45 = RhythmEvent_4_get_listenerCount_m3042521709(L_44, /*hidden argument*/RhythmEvent_4_get_listenerCount_m3042521709_MethodInfo_var);
		if (!L_45)
		{
			goto IL_014a;
		}
	}
	{
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		RhythmEventProvider_t215006757 * L_46 = V_1;
		NullCheck(L_46);
		TimingUpdateEvent_t543213975 * L_47 = L_46->get_timingUpdate_13();
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		int32_t L_48 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		RhythmEventProvider_t215006757 * L_49 = V_1;
		NullCheck(L_49);
		int32_t L_50 = L_49->get_offset_4();
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		float L_51 = RhythmTool_get_interpolation_m2645903814(__this, /*hidden argument*/NULL);
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		int32_t L_52 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		RhythmEventProvider_t215006757 * L_53 = V_1;
		NullCheck(L_53);
		int32_t L_54 = L_53->get_offset_4();
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		Beat_t2695683572 * L_55 = RhythmTool_NextBeat_m782067910(__this, ((int32_t)((int32_t)L_52+(int32_t)L_54)), /*hidden argument*/NULL);
		NullCheck(L_55);
		float L_56 = L_55->get_length_0();
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		int32_t L_57 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		RhythmEventProvider_t215006757 * L_58 = V_1;
		NullCheck(L_58);
		int32_t L_59 = L_58->get_offset_4();
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		float L_60 = RhythmTool_get_interpolation_m2645903814(__this, /*hidden argument*/NULL);
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		float L_61 = RhythmTool_BeatTimer_m3542768051(__this, ((float)((float)(((float)((float)((int32_t)((int32_t)L_57+(int32_t)L_59)))))+(float)L_60)), /*hidden argument*/NULL);
		// eventProvider.timingUpdate.Invoke(currentFrame + eventProvider.offset, interpolation, NextBeat(currentFrame + eventProvider.offset).length, BeatTimer(currentFrame + eventProvider.offset + interpolation));
		NullCheck(L_47);
		UnityEvent_4_Invoke_m1874634762(L_47, ((int32_t)((int32_t)L_48+(int32_t)L_50)), L_51, L_56, L_61, /*hidden argument*/UnityEvent_4_Invoke_m1874634762_MethodInfo_var);
	}

IL_014a:
	{
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		int32_t L_62 = V_0;
		V_0 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_014f:
	{
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		int32_t L_63 = V_0;
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_64 = RhythmEventProvider_get_eventProviders_m1235811388(NULL /*static, unused*/, /*hidden argument*/NULL);
		// for (int i = 0; i < RhythmEventProvider.eventProviders.Count; i++)
		NullCheck(L_64);
		int32_t L_65 = ReadOnlyCollection_1_get_Count_m3788110603(L_64, /*hidden argument*/ReadOnlyCollection_1_get_Count_m3788110603_MethodInfo_var);
		if ((((int32_t)L_63) < ((int32_t)L_65)))
		{
			goto IL_0008;
		}
	}
	{
		// for (int i = lastDataFrame + 1; i < currentFrame + 1; i++)
		int32_t L_66 = __this->get_lastDataFrame_26();
		V_2 = ((int32_t)((int32_t)L_66+(int32_t)1));
		goto IL_01c3;
	}

IL_016d:
	{
		// for (int ii = 0; ii < RhythmEventProvider.eventProviders.Count; ii++)
		V_3 = 0;
		goto IL_01a7;
	}

IL_0175:
	{
		// RhythmEventProvider eventProvider = RhythmEventProvider.eventProviders[ii];
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_67 = RhythmEventProvider_get_eventProviders_m1235811388(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_68 = V_3;
		// RhythmEventProvider eventProvider = RhythmEventProvider.eventProviders[ii];
		NullCheck(L_67);
		RhythmEventProvider_t215006757 * L_69 = ReadOnlyCollection_1_get_Item_m2127664468(L_67, L_68, /*hidden argument*/ReadOnlyCollection_1_get_Item_m2127664468_MethodInfo_var);
		V_4 = L_69;
		// PassEvents(eventProvider, i + eventProvider.offset);
		RhythmEventProvider_t215006757 * L_70 = V_4;
		int32_t L_71 = V_2;
		RhythmEventProvider_t215006757 * L_72 = V_4;
		NullCheck(L_72);
		int32_t L_73 = L_72->get_offset_4();
		// PassEvents(eventProvider, i + eventProvider.offset);
		RhythmTool_PassEvents_m4272423917(__this, L_70, ((int32_t)((int32_t)L_71+(int32_t)L_73)), /*hidden argument*/NULL);
		// eventProvider.offset = eventProvider.targetOffset;
		RhythmEventProvider_t215006757 * L_74 = V_4;
		RhythmEventProvider_t215006757 * L_75 = V_4;
		NullCheck(L_75);
		int32_t L_76 = L_75->get_targetOffset_3();
		NullCheck(L_74);
		L_74->set_offset_4(L_76);
		// for (int ii = 0; ii < RhythmEventProvider.eventProviders.Count; ii++)
		int32_t L_77 = V_3;
		V_3 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_01a7:
	{
		// for (int ii = 0; ii < RhythmEventProvider.eventProviders.Count; ii++)
		int32_t L_78 = V_3;
		// for (int ii = 0; ii < RhythmEventProvider.eventProviders.Count; ii++)
		IL2CPP_RUNTIME_CLASS_INIT(RhythmEventProvider_t215006757_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1_t400792449 * L_79 = RhythmEventProvider_get_eventProviders_m1235811388(NULL /*static, unused*/, /*hidden argument*/NULL);
		// for (int ii = 0; ii < RhythmEventProvider.eventProviders.Count; ii++)
		NullCheck(L_79);
		int32_t L_80 = ReadOnlyCollection_1_get_Count_m3788110603(L_79, /*hidden argument*/ReadOnlyCollection_1_get_Count_m3788110603_MethodInfo_var);
		if ((((int32_t)L_78) < ((int32_t)L_80)))
		{
			goto IL_0175;
		}
	}
	{
		// lastDataFrame = i;
		int32_t L_81 = V_2;
		__this->set_lastDataFrame_26(L_81);
		// for (int i = lastDataFrame + 1; i < currentFrame + 1; i++)
		int32_t L_82 = V_2;
		V_2 = ((int32_t)((int32_t)L_82+(int32_t)1));
	}

IL_01c3:
	{
		// for (int i = lastDataFrame + 1; i < currentFrame + 1; i++)
		int32_t L_83 = V_2;
		// for (int i = lastDataFrame + 1; i < currentFrame + 1; i++)
		int32_t L_84 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_83) < ((int32_t)((int32_t)((int32_t)L_84+(int32_t)1)))))
		{
			goto IL_016d;
		}
	}
	{
		// }
		return;
	}
}
// System.Void RhythmTool::PassSubBeat(RhythmEventProvider,System.Single)
extern "C"  void RhythmTool_PassSubBeat_m3729313650 (RhythmTool_t215962618 * __this, RhythmEventProvider_t215006757 * ___r0, float ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_PassSubBeat_m3729313650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Beat_t2695683572 * V_1 = NULL;
	{
		// float beatTime = BeatTimer(index);
		float L_0 = ___index1;
		// float beatTime = BeatTimer(index);
		float L_1 = RhythmTool_BeatTimer_m3542768051(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// Beat prevBeat = PrevBeat(Mathf.CeilToInt(index));
		float L_2 = ___index1;
		// Beat prevBeat = PrevBeat(Mathf.CeilToInt(index));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_CeilToInt_m2672598779(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// Beat prevBeat = PrevBeat(Mathf.CeilToInt(index));
		Beat_t2695683572 * L_4 = RhythmTool_PrevBeat_m2126131030(__this, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// if (r.lastBeatTime > beatTime)
		RhythmEventProvider_t215006757 * L_5 = ___r0;
		NullCheck(L_5);
		float L_6 = L_5->get_lastBeatTime_5();
		float L_7 = V_0;
		if ((!(((float)L_6) > ((float)L_7))))
		{
			goto IL_002f;
		}
	}
	{
		// r.onSubBeat.Invoke(prevBeat, 0);
		RhythmEventProvider_t215006757 * L_8 = ___r0;
		NullCheck(L_8);
		SubBeatEvent_t2499915164 * L_9 = L_8->get_onSubBeat_10();
		Beat_t2695683572 * L_10 = V_1;
		// r.onSubBeat.Invoke(prevBeat, 0);
		NullCheck(L_9);
		UnityEvent_2_Invoke_m407873616(L_9, L_10, 0, /*hidden argument*/UnityEvent_2_Invoke_m407873616_MethodInfo_var);
	}

IL_002f:
	{
		// if (r.lastBeatTime < .5f && beatTime >= .5f)
		RhythmEventProvider_t215006757 * L_11 = ___r0;
		NullCheck(L_11);
		float L_12 = L_11->get_lastBeatTime_5();
		if ((!(((float)L_12) < ((float)(0.5f)))))
		{
			goto IL_0057;
		}
	}
	{
		float L_13 = V_0;
		if ((!(((float)L_13) >= ((float)(0.5f)))))
		{
			goto IL_0057;
		}
	}
	{
		// r.onSubBeat.Invoke(prevBeat, 2);
		RhythmEventProvider_t215006757 * L_14 = ___r0;
		NullCheck(L_14);
		SubBeatEvent_t2499915164 * L_15 = L_14->get_onSubBeat_10();
		Beat_t2695683572 * L_16 = V_1;
		// r.onSubBeat.Invoke(prevBeat, 2);
		NullCheck(L_15);
		UnityEvent_2_Invoke_m407873616(L_15, L_16, 2, /*hidden argument*/UnityEvent_2_Invoke_m407873616_MethodInfo_var);
	}

IL_0057:
	{
		// if (r.lastBeatTime < .25f && beatTime >= .25f)
		RhythmEventProvider_t215006757 * L_17 = ___r0;
		NullCheck(L_17);
		float L_18 = L_17->get_lastBeatTime_5();
		if ((!(((float)L_18) < ((float)(0.25f)))))
		{
			goto IL_007f;
		}
	}
	{
		float L_19 = V_0;
		if ((!(((float)L_19) >= ((float)(0.25f)))))
		{
			goto IL_007f;
		}
	}
	{
		// r.onSubBeat.Invoke(prevBeat, 1);
		RhythmEventProvider_t215006757 * L_20 = ___r0;
		NullCheck(L_20);
		SubBeatEvent_t2499915164 * L_21 = L_20->get_onSubBeat_10();
		Beat_t2695683572 * L_22 = V_1;
		// r.onSubBeat.Invoke(prevBeat, 1);
		NullCheck(L_21);
		UnityEvent_2_Invoke_m407873616(L_21, L_22, 1, /*hidden argument*/UnityEvent_2_Invoke_m407873616_MethodInfo_var);
	}

IL_007f:
	{
		// if (r.lastBeatTime < .75f && beatTime >= .75f)
		RhythmEventProvider_t215006757 * L_23 = ___r0;
		NullCheck(L_23);
		float L_24 = L_23->get_lastBeatTime_5();
		if ((!(((float)L_24) < ((float)(0.75f)))))
		{
			goto IL_00a7;
		}
	}
	{
		float L_25 = V_0;
		if ((!(((float)L_25) >= ((float)(0.75f)))))
		{
			goto IL_00a7;
		}
	}
	{
		// r.onSubBeat.Invoke(prevBeat, 3);
		RhythmEventProvider_t215006757 * L_26 = ___r0;
		NullCheck(L_26);
		SubBeatEvent_t2499915164 * L_27 = L_26->get_onSubBeat_10();
		Beat_t2695683572 * L_28 = V_1;
		// r.onSubBeat.Invoke(prevBeat, 3);
		NullCheck(L_27);
		UnityEvent_2_Invoke_m407873616(L_27, L_28, 3, /*hidden argument*/UnityEvent_2_Invoke_m407873616_MethodInfo_var);
	}

IL_00a7:
	{
		// r.lastBeatTime = beatTime;
		RhythmEventProvider_t215006757 * L_29 = ___r0;
		float L_30 = V_0;
		NullCheck(L_29);
		L_29->set_lastBeatTime_5(L_30);
		// }
		return;
	}
}
// System.Void RhythmTool::PassEvents(RhythmEventProvider,System.Int32)
extern "C"  void RhythmTool_PassEvents_m4272423917 (RhythmTool_t215962618 * __this, RhythmEventProvider_t215006757 * ___r0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_PassEvents_m4272423917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Onset_t732596509 * V_0 = NULL;
	Onset_t732596509 * V_1 = NULL;
	Onset_t732596509 * V_2 = NULL;
	Onset_t732596509 * V_3 = NULL;
	Beat_t2695683572 * V_4 = NULL;
	{
		// r.onFrameChanged.Invoke(index, lastFrame);
		RhythmEventProvider_t215006757 * L_0 = ___r0;
		NullCheck(L_0);
		FrameChangedEvent_t800671821 * L_1 = L_0->get_onFrameChanged_14();
		int32_t L_2 = ___index1;
		// r.onFrameChanged.Invoke(index, lastFrame);
		int32_t L_3 = RhythmTool_get_lastFrame_m3065909813(__this, /*hidden argument*/NULL);
		// r.onFrameChanged.Invoke(index, lastFrame);
		NullCheck(L_1);
		UnityEvent_2_Invoke_m514975631(L_1, L_2, L_3, /*hidden argument*/UnityEvent_2_Invoke_m514975631_MethodInfo_var);
		// if (r.onOnset.listenerCount != 0)
		RhythmEventProvider_t215006757 * L_4 = ___r0;
		NullCheck(L_4);
		OnsetEvent_t1329939269 * L_5 = L_4->get_onOnset_11();
		// if (r.onOnset.listenerCount != 0)
		NullCheck(L_5);
		int32_t L_6 = RhythmEvent_2_get_listenerCount_m405578019(L_5, /*hidden argument*/RhythmEvent_2_get_listenerCount_m405578019_MethodInfo_var);
		if (!L_6)
		{
			goto IL_00cd;
		}
	}
	{
		// Onset l = _low.GetOnset(index);
		Analysis_t439488098 * L_7 = __this->get__low_30();
		int32_t L_8 = ___index1;
		// Onset l = _low.GetOnset(index);
		NullCheck(L_7);
		Onset_t732596509 * L_9 = Analysis_GetOnset_m3617908717(L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		// Onset m = _mid.GetOnset(index);
		Analysis_t439488098 * L_10 = __this->get__mid_31();
		int32_t L_11 = ___index1;
		// Onset m = _mid.GetOnset(index);
		NullCheck(L_10);
		Onset_t732596509 * L_12 = Analysis_GetOnset_m3617908717(L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		// Onset h = _high.GetOnset(index);
		Analysis_t439488098 * L_13 = __this->get__high_32();
		int32_t L_14 = ___index1;
		// Onset h = _high.GetOnset(index);
		NullCheck(L_13);
		Onset_t732596509 * L_15 = Analysis_GetOnset_m3617908717(L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		// Onset a = _all.GetOnset(index);
		Analysis_t439488098 * L_16 = __this->get__all_33();
		int32_t L_17 = ___index1;
		// Onset a = _all.GetOnset(index);
		NullCheck(L_16);
		Onset_t732596509 * L_18 = Analysis_GetOnset_m3617908717(L_16, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		// if (l > 0)
		Onset_t732596509 * L_19 = V_0;
		// if (l > 0)
		bool L_20 = Onset_op_GreaterThan_m1430579383(NULL /*static, unused*/, L_19, (0.0f), /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0075;
		}
	}
	{
		// r.onOnset.Invoke(OnsetType.Low, l);
		RhythmEventProvider_t215006757 * L_21 = ___r0;
		NullCheck(L_21);
		OnsetEvent_t1329939269 * L_22 = L_21->get_onOnset_11();
		Onset_t732596509 * L_23 = V_0;
		// r.onOnset.Invoke(OnsetType.Low, l);
		NullCheck(L_22);
		UnityEvent_2_Invoke_m1980955963(L_22, 0, L_23, /*hidden argument*/UnityEvent_2_Invoke_m1980955963_MethodInfo_var);
	}

IL_0075:
	{
		// if (m > 0)
		Onset_t732596509 * L_24 = V_1;
		// if (m > 0)
		bool L_25 = Onset_op_GreaterThan_m1430579383(NULL /*static, unused*/, L_24, (0.0f), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0092;
		}
	}
	{
		// r.onOnset.Invoke(OnsetType.Mid, m);
		RhythmEventProvider_t215006757 * L_26 = ___r0;
		NullCheck(L_26);
		OnsetEvent_t1329939269 * L_27 = L_26->get_onOnset_11();
		Onset_t732596509 * L_28 = V_1;
		// r.onOnset.Invoke(OnsetType.Mid, m);
		NullCheck(L_27);
		UnityEvent_2_Invoke_m1980955963(L_27, 1, L_28, /*hidden argument*/UnityEvent_2_Invoke_m1980955963_MethodInfo_var);
	}

IL_0092:
	{
		// if (h > 0)
		Onset_t732596509 * L_29 = V_2;
		// if (h > 0)
		bool L_30 = Onset_op_GreaterThan_m1430579383(NULL /*static, unused*/, L_29, (0.0f), /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00af;
		}
	}
	{
		// r.onOnset.Invoke(OnsetType.High, h);
		RhythmEventProvider_t215006757 * L_31 = ___r0;
		NullCheck(L_31);
		OnsetEvent_t1329939269 * L_32 = L_31->get_onOnset_11();
		Onset_t732596509 * L_33 = V_2;
		// r.onOnset.Invoke(OnsetType.High, h);
		NullCheck(L_32);
		UnityEvent_2_Invoke_m1980955963(L_32, 2, L_33, /*hidden argument*/UnityEvent_2_Invoke_m1980955963_MethodInfo_var);
	}

IL_00af:
	{
		// if (a > 0)
		Onset_t732596509 * L_34 = V_3;
		// if (a > 0)
		bool L_35 = Onset_op_GreaterThan_m1430579383(NULL /*static, unused*/, L_34, (0.0f), /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00cc;
		}
	}
	{
		// r.onOnset.Invoke(OnsetType.All, a);
		RhythmEventProvider_t215006757 * L_36 = ___r0;
		NullCheck(L_36);
		OnsetEvent_t1329939269 * L_37 = L_36->get_onOnset_11();
		Onset_t732596509 * L_38 = V_3;
		// r.onOnset.Invoke(OnsetType.All, a);
		NullCheck(L_37);
		UnityEvent_2_Invoke_m1980955963(L_37, 3, L_38, /*hidden argument*/UnityEvent_2_Invoke_m1980955963_MethodInfo_var);
	}

IL_00cc:
	{
	}

IL_00cd:
	{
		// if (r.onBeat.listenerCount != 0)
		RhythmEventProvider_t215006757 * L_39 = ___r0;
		NullCheck(L_39);
		BeatEvent_t33541086 * L_40 = L_39->get_onBeat_9();
		// if (r.onBeat.listenerCount != 0)
		NullCheck(L_40);
		int32_t L_41 = RhythmEvent_1_get_listenerCount_m1237053972(L_40, /*hidden argument*/RhythmEvent_1_get_listenerCount_m1237053972_MethodInfo_var);
		if (!L_41)
		{
			goto IL_0104;
		}
	}
	{
		// if (IsBeat(index) == 1)
		int32_t L_42 = ___index1;
		// if (IsBeat(index) == 1)
		int32_t L_43 = RhythmTool_IsBeat_m3576622446(__this, L_42, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_43) == ((uint32_t)1))))
		{
			goto IL_0103;
		}
	}
	{
		// Beat beat = NextBeat(index);
		int32_t L_44 = ___index1;
		// Beat beat = NextBeat(index);
		Beat_t2695683572 * L_45 = RhythmTool_NextBeat_m782067910(__this, L_44, /*hidden argument*/NULL);
		V_4 = L_45;
		// r.onBeat.Invoke(beat);
		RhythmEventProvider_t215006757 * L_46 = ___r0;
		NullCheck(L_46);
		BeatEvent_t33541086 * L_47 = L_46->get_onBeat_9();
		Beat_t2695683572 * L_48 = V_4;
		// r.onBeat.Invoke(beat);
		NullCheck(L_47);
		UnityEvent_1_Invoke_m4176776406(L_47, L_48, /*hidden argument*/UnityEvent_1_Invoke_m4176776406_MethodInfo_var);
	}

IL_0103:
	{
	}

IL_0104:
	{
		// if (r.onChange.listenerCount != 0)
		RhythmEventProvider_t215006757 * L_49 = ___r0;
		NullCheck(L_49);
		ChangeEvent_t1135638418 * L_50 = L_49->get_onChange_12();
		// if (r.onChange.listenerCount != 0)
		NullCheck(L_50);
		int32_t L_51 = RhythmEvent_2_get_listenerCount_m3928554923(L_50, /*hidden argument*/RhythmEvent_2_get_listenerCount_m3928554923_MethodInfo_var);
		if (!L_51)
		{
			goto IL_0137;
		}
	}
	{
		// if (IsChange(index))
		int32_t L_52 = ___index1;
		// if (IsChange(index))
		bool L_53 = RhythmTool_IsChange_m1817224686(__this, L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0136;
		}
	}
	{
		// r.onChange.Invoke(index, NextChange(index));
		RhythmEventProvider_t215006757 * L_54 = ___r0;
		NullCheck(L_54);
		ChangeEvent_t1135638418 * L_55 = L_54->get_onChange_12();
		int32_t L_56 = ___index1;
		int32_t L_57 = ___index1;
		// r.onChange.Invoke(index, NextChange(index));
		float L_58 = RhythmTool_NextChange_m3596269439(__this, L_57, /*hidden argument*/NULL);
		// r.onChange.Invoke(index, NextChange(index));
		NullCheck(L_55);
		UnityEvent_2_Invoke_m3012962327(L_55, L_56, L_58, /*hidden argument*/UnityEvent_2_Invoke_m3012962327_MethodInfo_var);
	}

IL_0136:
	{
	}

IL_0137:
	{
		// }
		return;
	}
}
// System.Void RhythmTool::Play()
extern "C"  void RhythmTool_Play_m2237616365 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	{
		// if (songLoaded)
		// if (songLoaded)
		bool L_0 = RhythmTool_get_songLoaded_m3458667938(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		// currentSample = audioSource.timeSamples;
		AudioSource_t1135106623 * L_1 = __this->get_audioSource_34();
		// currentSample = audioSource.timeSamples;
		NullCheck(L_1);
		int32_t L_2 = AudioSource_get_timeSamples_m1719074425(L_1, /*hidden argument*/NULL);
		// currentSample = audioSource.timeSamples;
		RhythmTool_set_currentSample_m1718109522(__this, (((float)((float)L_2))), /*hidden argument*/NULL);
		// lastDataFrame = currentFrame;
		// lastDataFrame = currentFrame;
		int32_t L_3 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		__this->set_lastDataFrame_26(L_3);
		// audioSource.Play();
		AudioSource_t1135106623 * L_4 = __this->get_audioSource_34();
		// audioSource.Play();
		NullCheck(L_4);
		AudioSource_Play_m353744792(L_4, /*hidden argument*/NULL);
	}

IL_0037:
	{
		// }
		return;
	}
}
// System.Void RhythmTool::Stop()
extern "C"  void RhythmTool_Stop_m1172477109 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	{
		// if (songLoaded)
		// if (songLoaded)
		bool L_0 = RhythmTool_get_songLoaded_m3458667938(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		// currentSample = 0;
		// currentSample = 0;
		RhythmTool_set_currentSample_m1718109522(__this, (0.0f), /*hidden argument*/NULL);
		// audioSource.Stop();
		AudioSource_t1135106623 * L_1 = __this->get_audioSource_34();
		// audioSource.Stop();
		NullCheck(L_1);
		AudioSource_Stop_m3452679614(L_1, /*hidden argument*/NULL);
	}

IL_0024:
	{
		// }
		return;
	}
}
// System.Void RhythmTool::Pause()
extern "C"  void RhythmTool_Pause_m749406559 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	{
		// audioSource.Pause();
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		// audioSource.Pause();
		NullCheck(L_0);
		AudioSource_Pause_m71375470(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void RhythmTool::UnPause()
extern "C"  void RhythmTool_UnPause_m2354753466 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	{
		// audioSource.UnPause();
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		// audioSource.UnPause();
		NullCheck(L_0);
		AudioSource_UnPause_m1911402783(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Single RhythmTool::TimeSeconds(System.Int32)
extern "C"  float RhythmTool_TimeSeconds_m1495203528 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// return (index * frameLength);
		int32_t L_0 = ___index0;
		// return (index * frameLength);
		float L_1 = RhythmTool_get_frameLength_m2593270439(__this, /*hidden argument*/NULL);
		V_0 = ((float)((float)(((float)((float)L_0)))*(float)L_1));
		goto IL_0010;
	}

IL_0010:
	{
		// }
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single RhythmTool::TimeSeconds()
extern "C"  float RhythmTool_TimeSeconds_m3803014121 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// return TimeSeconds(currentFrame);
		// return TimeSeconds(currentFrame);
		int32_t L_0 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		// return TimeSeconds(currentFrame);
		float L_1 = RhythmTool_TimeSeconds_m1495203528(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void RhythmTool::GetSpectrum(System.Single[],System.Int32,UnityEngine.FFTWindow)
extern "C"  void RhythmTool_GetSpectrum_m1160998189 (RhythmTool_t215962618 * __this, SingleU5BU5D_t577127397* ___samples0, int32_t ___channel1, int32_t ___window2, const MethodInfo* method)
{
	{
		// audioSource.GetSpectrumData(samples, channel, window);
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_34();
		SingleU5BU5D_t577127397* L_1 = ___samples0;
		int32_t L_2 = ___channel1;
		int32_t L_3 = ___window2;
		// audioSource.GetSpectrumData(samples, channel, window);
		NullCheck(L_0);
		AudioSource_GetSpectrumData_m4116675176(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// AnalysisData RhythmTool::AddAnalysis(System.Int32,System.Int32,System.String)
extern "C"  AnalysisData_t108342674 * RhythmTool_AddAnalysis_m2159689153 (RhythmTool_t215962618 * __this, int32_t ___s0, int32_t ___e1, String_t* ___name2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_AddAnalysis_m2159689153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Analysis_t439488098 * V_0 = NULL;
	Enumerator_t3638306200  V_1;
	memset(&V_1, 0, sizeof(V_1));
	AnalysisData_t108342674 * V_2 = NULL;
	Analysis_t439488098 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// foreach (Analysis an in analyses)
		List_1_t4103576526 * L_0 = __this->get_analyses_29();
		// foreach (Analysis an in analyses)
		NullCheck(L_0);
		Enumerator_t3638306200  L_1 = List_1_GetEnumerator_m3071804604(L_0, /*hidden argument*/List_1_GetEnumerator_m3071804604_MethodInfo_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004b;
		}

IL_0013:
		{
			// foreach (Analysis an in analyses)
			// foreach (Analysis an in analyses)
			Analysis_t439488098 * L_2 = Enumerator_get_Current_m3316544882((&V_1), /*hidden argument*/Enumerator_get_Current_m3316544882_MethodInfo_var);
			V_0 = L_2;
			// if (an.name == name)
			Analysis_t439488098 * L_3 = V_0;
			// if (an.name == name)
			NullCheck(L_3);
			String_t* L_4 = Analysis_get_name_m4149432526(L_3, /*hidden argument*/NULL);
			String_t* L_5 = ___name2;
			// if (an.name == name)
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_004a;
			}
		}

IL_002d:
		{
			// Debug.LogWarning("Analysis with name " + name + " already exists");
			String_t* L_7 = ___name2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_8 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1851748119, L_7, _stringLiteral2075151806, /*hidden argument*/NULL);
			// Debug.LogWarning("Analysis with name " + name + " already exists");
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			// return null;
			V_2 = (AnalysisData_t108342674 *)NULL;
			IL2CPP_LEAVE(0x97, FINALLY_005c);
		}

IL_004a:
		{
		}

IL_004b:
		{
			// foreach (Analysis an in analyses)
			bool L_9 = Enumerator_MoveNext_m391630496((&V_1), /*hidden argument*/Enumerator_MoveNext_m391630496_MethodInfo_var);
			if (L_9)
			{
				goto IL_0013;
			}
		}

IL_0057:
		{
			IL2CPP_LEAVE(0x6A, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		// foreach (Analysis an in analyses)
		Enumerator_Dispose_m1317449898((&V_1), /*hidden argument*/Enumerator_Dispose_m1317449898_MethodInfo_var);
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006a:
	{
		// Analysis a = new Analysis(s, e, name);
		int32_t L_10 = ___s0;
		int32_t L_11 = ___e1;
		String_t* L_12 = ___name2;
		// Analysis a = new Analysis(s, e, name);
		Analysis_t439488098 * L_13 = (Analysis_t439488098 *)il2cpp_codegen_object_new(Analysis_t439488098_il2cpp_TypeInfo_var);
		Analysis__ctor_m1571091587(L_13, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		// a.Init(totalFrames);
		Analysis_t439488098 * L_14 = V_3;
		// a.Init(totalFrames);
		int32_t L_15 = RhythmTool_get_totalFrames_m68629944(__this, /*hidden argument*/NULL);
		// a.Init(totalFrames);
		NullCheck(L_14);
		Analysis_Init_m3996269154(L_14, L_15, /*hidden argument*/NULL);
		// analyses.Add(a);
		List_1_t4103576526 * L_16 = __this->get_analyses_29();
		Analysis_t439488098 * L_17 = V_3;
		// analyses.Add(a);
		NullCheck(L_16);
		List_1_Add_m845626571(L_16, L_17, /*hidden argument*/List_1_Add_m845626571_MethodInfo_var);
		// return a.analysisData;
		Analysis_t439488098 * L_18 = V_3;
		// return a.analysisData;
		NullCheck(L_18);
		AnalysisData_t108342674 * L_19 = Analysis_get_analysisData_m3591218099(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		goto IL_0097;
	}

IL_0097:
	{
		// }
		AnalysisData_t108342674 * L_20 = V_2;
		return L_20;
	}
}
// AnalysisData RhythmTool::GetAnalysis(System.String)
extern "C"  AnalysisData_t108342674 * RhythmTool_GetAnalysis_m368113886 (RhythmTool_t215962618 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_GetAnalysis_m368113886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Analysis_t439488098 * V_0 = NULL;
	Enumerator_t3638306200  V_1;
	memset(&V_1, 0, sizeof(V_1));
	AnalysisData_t108342674 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// foreach (Analysis a in analyses)
		List_1_t4103576526 * L_0 = __this->get_analyses_29();
		// foreach (Analysis a in analyses)
		NullCheck(L_0);
		Enumerator_t3638306200  L_1 = List_1_GetEnumerator_m3071804604(L_0, /*hidden argument*/List_1_GetEnumerator_m3071804604_MethodInfo_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003a;
		}

IL_0013:
		{
			// foreach (Analysis a in analyses)
			// foreach (Analysis a in analyses)
			Analysis_t439488098 * L_2 = Enumerator_get_Current_m3316544882((&V_1), /*hidden argument*/Enumerator_get_Current_m3316544882_MethodInfo_var);
			V_0 = L_2;
			// if (a.name == name)
			Analysis_t439488098 * L_3 = V_0;
			// if (a.name == name)
			NullCheck(L_3);
			String_t* L_4 = Analysis_get_name_m4149432526(L_3, /*hidden argument*/NULL);
			String_t* L_5 = ___name0;
			// if (a.name == name)
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0039;
			}
		}

IL_002d:
		{
			// return a.analysisData;
			Analysis_t439488098 * L_7 = V_0;
			// return a.analysisData;
			NullCheck(L_7);
			AnalysisData_t108342674 * L_8 = Analysis_get_analysisData_m3591218099(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x75, FINALLY_004b);
		}

IL_0039:
		{
		}

IL_003a:
		{
			// foreach (Analysis a in analyses)
			bool L_9 = Enumerator_MoveNext_m391630496((&V_1), /*hidden argument*/Enumerator_MoveNext_m391630496_MethodInfo_var);
			if (L_9)
			{
				goto IL_0013;
			}
		}

IL_0046:
		{
			IL2CPP_LEAVE(0x59, FINALLY_004b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		// foreach (Analysis a in analyses)
		Enumerator_Dispose_m1317449898((&V_1), /*hidden argument*/Enumerator_Dispose_m1317449898_MethodInfo_var);
		IL2CPP_END_FINALLY(75)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0059:
	{
		// Debug.LogWarning("Analysis with name " + name + " was not found");
		String_t* L_10 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1851748119, L_10, _stringLiteral3981451750, /*hidden argument*/NULL);
		// Debug.LogWarning("Analysis with name " + name + " was not found");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		// return null;
		V_2 = (AnalysisData_t108342674 *)NULL;
		goto IL_0075;
	}

IL_0075:
	{
		// }
		AnalysisData_t108342674 * L_12 = V_2;
		return L_12;
	}
}
// System.Void RhythmTool::DrawDebugLines()
extern "C"  void RhythmTool_DrawDebugLines_m2101946209 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RhythmTool_DrawDebugLines_m2101946209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (!Application.isEditor)
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		// return;
		goto IL_0074;
	}

IL_0010:
	{
		// if (!songLoaded)
		// if (!songLoaded)
		bool L_1 = RhythmTool_get_songLoaded_m3458667938(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		// return;
		goto IL_0074;
	}

IL_0020:
	{
		// for (int i = 0; i < analyses.Count; i++)
		V_0 = 0;
		goto IL_0045;
	}

IL_0027:
	{
		// analyses[i].DrawDebugLines(currentFrame, i);
		List_1_t4103576526 * L_2 = __this->get_analyses_29();
		int32_t L_3 = V_0;
		// analyses[i].DrawDebugLines(currentFrame, i);
		NullCheck(L_2);
		Analysis_t439488098 * L_4 = List_1_get_Item_m3137246970(L_2, L_3, /*hidden argument*/List_1_get_Item_m3137246970_MethodInfo_var);
		// analyses[i].DrawDebugLines(currentFrame, i);
		int32_t L_5 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		// analyses[i].DrawDebugLines(currentFrame, i);
		NullCheck(L_4);
		Analysis_DrawDebugLines_m2160884685(L_4, L_5, L_6, /*hidden argument*/NULL);
		// for (int i = 0; i < analyses.Count; i++)
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0045:
	{
		// for (int i = 0; i < analyses.Count; i++)
		int32_t L_8 = V_0;
		List_1_t4103576526 * L_9 = __this->get_analyses_29();
		// for (int i = 0; i < analyses.Count; i++)
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m2439640319(L_9, /*hidden argument*/List_1_get_Count_m2439640319_MethodInfo_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0027;
		}
	}
	{
		// if (_calculateTempo)
		bool L_11 = __this->get__calculateTempo_15();
		if (!L_11)
		{
			goto IL_0074;
		}
	}
	{
		// beatTracker.DrawDebugLines(currentFrame);
		BeatTracker_t2801099156 * L_12 = __this->get_beatTracker_18();
		// beatTracker.DrawDebugLines(currentFrame);
		int32_t L_13 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		// beatTracker.DrawDebugLines(currentFrame);
		NullCheck(L_12);
		BeatTracker_DrawDebugLines_m1071911214(L_12, L_13, /*hidden argument*/NULL);
	}

IL_0074:
	{
		// }
		return;
	}
}
// Beat RhythmTool::NextBeat(System.Int32)
extern "C"  Beat_t2695683572 * RhythmTool_NextBeat_m782067910 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	Beat_t2695683572 * V_0 = NULL;
	{
		// return beatTracker.NextBeat(index);
		BeatTracker_t2801099156 * L_0 = __this->get_beatTracker_18();
		int32_t L_1 = ___index0;
		// return beatTracker.NextBeat(index);
		NullCheck(L_0);
		Beat_t2695683572 * L_2 = BeatTracker_NextBeat_m3845220818(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		Beat_t2695683572 * L_3 = V_0;
		return L_3;
	}
}
// Beat RhythmTool::PrevBeat(System.Int32)
extern "C"  Beat_t2695683572 * RhythmTool_PrevBeat_m2126131030 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	Beat_t2695683572 * V_0 = NULL;
	{
		// return beatTracker.PrevBeat(index);
		BeatTracker_t2801099156 * L_0 = __this->get_beatTracker_18();
		int32_t L_1 = ___index0;
		// return beatTracker.PrevBeat(index);
		NullCheck(L_0);
		Beat_t2695683572 * L_2 = BeatTracker_PrevBeat_m277720998(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		Beat_t2695683572 * L_3 = V_0;
		return L_3;
	}
}
// System.Int32 RhythmTool::NextBeatIndex(System.Int32)
extern "C"  int32_t RhythmTool_NextBeatIndex_m2622800185 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// return beatTracker.NextBeatIndex(index);
		BeatTracker_t2801099156 * L_0 = __this->get_beatTracker_18();
		int32_t L_1 = ___index0;
		// return beatTracker.NextBeatIndex(index);
		NullCheck(L_0);
		int32_t L_2 = BeatTracker_NextBeatIndex_m2115259593(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 RhythmTool::NextBeatIndex()
extern "C"  int32_t RhythmTool_NextBeatIndex_m1500120148 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// return NextBeatIndex(currentFrame);
		// return NextBeatIndex(currentFrame);
		int32_t L_0 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		// return NextBeatIndex(currentFrame);
		int32_t L_1 = RhythmTool_NextBeatIndex_m2622800185(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 RhythmTool::PrevBeatIndex()
extern "C"  int32_t RhythmTool_PrevBeatIndex_m139823888 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// return PrevBeatIndex(currentFrame);
		// return PrevBeatIndex(currentFrame);
		int32_t L_0 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		// return PrevBeatIndex(currentFrame);
		int32_t L_1 = RhythmTool_PrevBeatIndex_m2822132327(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 RhythmTool::PrevBeatIndex(System.Int32)
extern "C"  int32_t RhythmTool_PrevBeatIndex_m2822132327 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// return beatTracker.PrevBeatIndex(index);
		BeatTracker_t2801099156 * L_0 = __this->get_beatTracker_18();
		int32_t L_1 = ___index0;
		// return beatTracker.PrevBeatIndex(index);
		NullCheck(L_0);
		int32_t L_2 = BeatTracker_PrevBeatIndex_m2127559407(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Single RhythmTool::BeatTimer(System.Single)
extern "C"  float RhythmTool_BeatTimer_m3542768051 (RhythmTool_t215962618 * __this, float ___index0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// return beatTracker.BeatTimer(index);
		BeatTracker_t2801099156 * L_0 = __this->get_beatTracker_18();
		float L_1 = ___index0;
		// return beatTracker.BeatTimer(index);
		NullCheck(L_0);
		float L_2 = BeatTracker_BeatTimer_m2155883739(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single RhythmTool::BeatTimer()
extern "C"  float RhythmTool_BeatTimer_m179114674 (RhythmTool_t215962618 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// return BeatTimer(currentFrame + interpolation);
		// return BeatTimer(currentFrame + interpolation);
		int32_t L_0 = RhythmTool_get_currentFrame_m867144816(__this, /*hidden argument*/NULL);
		// return BeatTimer(currentFrame + interpolation);
		float L_1 = RhythmTool_get_interpolation_m2645903814(__this, /*hidden argument*/NULL);
		// return BeatTimer(currentFrame + interpolation);
		float L_2 = RhythmTool_BeatTimer_m3542768051(__this, ((float)((float)(((float)((float)L_0)))+(float)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001b;
	}

IL_001b:
	{
		// }
		float L_3 = V_0;
		return L_3;
	}
}
// System.Int32 RhythmTool::IsBeat(System.Int32,System.Int32)
extern "C"  int32_t RhythmTool_IsBeat_m2985775255 (RhythmTool_t215962618 * __this, int32_t ___index0, int32_t ___min1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// return beatTracker.IsBeat(index, min);
		BeatTracker_t2801099156 * L_0 = __this->get_beatTracker_18();
		int32_t L_1 = ___index0;
		int32_t L_2 = ___min1;
		// return beatTracker.IsBeat(index, min);
		NullCheck(L_0);
		int32_t L_3 = BeatTracker_IsBeat_m3805475791(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		// }
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Int32 RhythmTool::IsBeat(System.Int32)
extern "C"  int32_t RhythmTool_IsBeat_m3576622446 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// return IsBeat(index, 0);
		int32_t L_0 = ___index0;
		// return IsBeat(index, 0);
		int32_t L_1 = RhythmTool_IsBeat_m2985775255(__this, L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// }
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean RhythmTool::IsChange(System.Int32)
extern "C"  bool RhythmTool_IsChange_m1817224686 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		// return segmenter.IsChange(index);
		Segmenter_t2695296026 * L_0 = __this->get_segmenter_19();
		int32_t L_1 = ___index0;
		// return segmenter.IsChange(index);
		NullCheck(L_0);
		bool L_2 = Segmenter_IsChange_m246302866(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Int32 RhythmTool::PrevChangeIndex(System.Int32)
extern "C"  int32_t RhythmTool_PrevChangeIndex_m2904057899 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// return segmenter.PrevChangeIndex(index);
		Segmenter_t2695296026 * L_0 = __this->get_segmenter_19();
		int32_t L_1 = ___index0;
		// return segmenter.PrevChangeIndex(index);
		NullCheck(L_0);
		int32_t L_2 = Segmenter_PrevChangeIndex_m843645(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 RhythmTool::NextChangeIndex(System.Int32)
extern "C"  int32_t RhythmTool_NextChangeIndex_m3878653193 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		// return segmenter.NextChangeIndex(index);
		Segmenter_t2695296026 * L_0 = __this->get_segmenter_19();
		int32_t L_1 = ___index0;
		// return segmenter.NextChangeIndex(index);
		NullCheck(L_0);
		int32_t L_2 = Segmenter_NextChangeIndex_m3584235607(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Single RhythmTool::PrevChange(System.Int32)
extern "C"  float RhythmTool_PrevChange_m2710372421 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// return segmenter.PrevChange(index);
		Segmenter_t2695296026 * L_0 = __this->get_segmenter_19();
		int32_t L_1 = ___index0;
		// return segmenter.PrevChange(index);
		NullCheck(L_0);
		float L_2 = Segmenter_PrevChange_m778242627(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single RhythmTool::NextChange(System.Int32)
extern "C"  float RhythmTool_NextChange_m3596269439 (RhythmTool_t215962618 * __this, int32_t ___index0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		// return segmenter.NextChange(index);
		Segmenter_t2695296026 * L_0 = __this->get_segmenter_19();
		int32_t L_1 = ___index0;
		// return segmenter.NextChange(index);
		NullCheck(L_0);
		float L_2 = Segmenter_NextChange_m3516349633(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		// }
		float L_3 = V_0;
		return L_3;
	}
}
// System.Void RhythmTool/<AsyncAnalyze>c__Iterator1::.ctor()
extern "C"  void U3CAsyncAnalyzeU3Ec__Iterator1__ctor_m1537308272 (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean RhythmTool/<AsyncAnalyze>c__Iterator1::MoveNext()
extern "C"  bool U3CAsyncAnalyzeU3Ec__Iterator1_MoveNext_m3847191016 (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAsyncAnalyzeU3Ec__Iterator1_MoveNext_m3847191016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00cd;
			}
		}
	}
	{
		goto IL_01ad;
	}

IL_0021:
	{
		// frames = Mathf.Clamp(frames, 0, totalFrames);
		int32_t L_2 = __this->get_frames_0();
		RhythmTool_t215962618 * L_3 = __this->get_U24this_3();
		// frames = Mathf.Clamp(frames, 0, totalFrames);
		NullCheck(L_3);
		int32_t L_4 = RhythmTool_get_totalFrames_m68629944(L_3, /*hidden argument*/NULL);
		// frames = Mathf.Clamp(frames, 0, totalFrames);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, L_2, 0, L_4, /*hidden argument*/NULL);
		__this->set_frames_0(L_5);
		// float[] s = new float[frames * frameSpacing * channels];
		int32_t L_6 = __this->get_frames_0();
		// float[] s = new float[frames * frameSpacing * channels];
		int32_t L_7 = RhythmTool_get_frameSpacing_m936531512(NULL /*static, unused*/, /*hidden argument*/NULL);
		RhythmTool_t215962618 * L_8 = __this->get_U24this_3();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_channels_25();
		__this->set_U3CsU3E__0_1(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))*(int32_t)L_9)))));
		// audioSource.clip.GetData(s, 0);
		RhythmTool_t215962618 * L_10 = __this->get_U24this_3();
		NullCheck(L_10);
		AudioSource_t1135106623 * L_11 = L_10->get_audioSource_34();
		// audioSource.clip.GetData(s, 0);
		NullCheck(L_11);
		AudioClip_t1932558630 * L_12 = AudioSource_get_clip_m2127996365(L_11, /*hidden argument*/NULL);
		SingleU5BU5D_t577127397* L_13 = __this->get_U3CsU3E__0_1();
		// audioSource.clip.GetData(s, 0);
		NullCheck(L_12);
		AudioClip_GetData_m1645657273(L_12, L_13, 0, /*hidden argument*/NULL);
		// Thread analyzeThread = new Thread(BackGroundAnalyze);
		RhythmTool_t215962618 * L_14 = __this->get_U24this_3();
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)RhythmTool_BackGroundAnalyze_m3994437969_MethodInfo_var);
		ParameterizedThreadStart_t2412552885 * L_16 = (ParameterizedThreadStart_t2412552885 *)il2cpp_codegen_object_new(ParameterizedThreadStart_t2412552885_il2cpp_TypeInfo_var);
		ParameterizedThreadStart__ctor_m1215446210(L_16, L_14, L_15, /*hidden argument*/NULL);
		// Thread analyzeThread = new Thread(BackGroundAnalyze);
		Thread_t241561612 * L_17 = (Thread_t241561612 *)il2cpp_codegen_object_new(Thread_t241561612_il2cpp_TypeInfo_var);
		Thread__ctor_m583758171(L_17, L_16, /*hidden argument*/NULL);
		__this->set_U3CanalyzeThreadU3E__0_2(L_17);
		// analyzeThread.Start(s);
		Thread_t241561612 * L_18 = __this->get_U3CanalyzeThreadU3E__0_2();
		SingleU5BU5D_t577127397* L_19 = __this->get_U3CsU3E__0_1();
		// analyzeThread.Start(s);
		NullCheck(L_18);
		Thread_Start_m2652746659(L_18, (Il2CppObject *)(Il2CppObject *)L_19, /*hidden argument*/NULL);
		// while (analyzeThread.IsAlive)
		goto IL_00ce;
	}

IL_00b1:
	{
		// yield return null;
		__this->set_U24current_4(NULL);
		bool L_20 = __this->get_U24disposing_5();
		if (L_20)
		{
			goto IL_00c8;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_00c8:
	{
		goto IL_01af;
	}

IL_00cd:
	{
	}

IL_00ce:
	{
		// while (analyzeThread.IsAlive)
		Thread_t241561612 * L_21 = __this->get_U3CanalyzeThreadU3E__0_2();
		// while (analyzeThread.IsAlive)
		NullCheck(L_21);
		bool L_22 = Thread_get_IsAlive_m4169372557(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00b1;
		}
	}
	{
		// if (queueRoutine != null)
		RhythmTool_t215962618 * L_23 = __this->get_U24this_3();
		NullCheck(L_23);
		Coroutine_t2299508840 * L_24 = L_23->get_queueRoutine_21();
		if (!L_24)
		{
			goto IL_0100;
		}
	}
	{
		// analyzeRoutine = null;
		RhythmTool_t215962618 * L_25 = __this->get_U24this_3();
		NullCheck(L_25);
		L_25->set_analyzeRoutine_20((Coroutine_t2299508840 *)NULL);
		// yield break;
		goto IL_01ad;
	}

IL_0100:
	{
		// if (calculateTempo)
		RhythmTool_t215962618 * L_26 = __this->get_U24this_3();
		// if (calculateTempo)
		NullCheck(L_26);
		bool L_27 = RhythmTool_get_calculateTempo_m3725479207(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0120;
		}
	}
	{
		// beatTracker.FillStart();
		RhythmTool_t215962618 * L_28 = __this->get_U24this_3();
		NullCheck(L_28);
		BeatTracker_t2801099156 * L_29 = L_28->get_beatTracker_18();
		// beatTracker.FillStart();
		NullCheck(L_29);
		BeatTracker_FillStart_m2215021468(L_29, /*hidden argument*/NULL);
	}

IL_0120:
	{
		// if (lastFrame > totalFrames - 2)
		RhythmTool_t215962618 * L_30 = __this->get_U24this_3();
		// if (lastFrame > totalFrames - 2)
		NullCheck(L_30);
		int32_t L_31 = RhythmTool_get_lastFrame_m3065909813(L_30, /*hidden argument*/NULL);
		RhythmTool_t215962618 * L_32 = __this->get_U24this_3();
		// if (lastFrame > totalFrames - 2)
		NullCheck(L_32);
		int32_t L_33 = RhythmTool_get_totalFrames_m68629944(L_32, /*hidden argument*/NULL);
		if ((((int32_t)L_31) <= ((int32_t)((int32_t)((int32_t)L_33-(int32_t)2)))))
		{
			goto IL_0148;
		}
	}
	{
		// EndOfAnalysis();
		RhythmTool_t215962618 * L_34 = __this->get_U24this_3();
		// EndOfAnalysis();
		NullCheck(L_34);
		RhythmTool_EndOfAnalysis_m2036138409(L_34, /*hidden argument*/NULL);
	}

IL_0148:
	{
		// songLoaded = true;
		RhythmTool_t215962618 * L_35 = __this->get_U24this_3();
		// songLoaded = true;
		NullCheck(L_35);
		RhythmTool_set_songLoaded_m1649989549(L_35, (bool)1, /*hidden argument*/NULL);
		// analyzeRoutine = null;
		RhythmTool_t215962618 * L_36 = __this->get_U24this_3();
		NullCheck(L_36);
		L_36->set_analyzeRoutine_20((Coroutine_t2299508840 *)NULL);
		// InitializeEventProviders();
		RhythmTool_t215962618 * L_37 = __this->get_U24this_3();
		// InitializeEventProviders();
		NullCheck(L_37);
		RhythmTool_InitializeEventProviders_m198279347(L_37, /*hidden argument*/NULL);
		// if (SongLoaded != null)
		RhythmTool_t215962618 * L_38 = __this->get_U24this_3();
		NullCheck(L_38);
		Action_t3226471752 * L_39 = L_38->get_SongLoaded_2();
		if (!L_39)
		{
			goto IL_0190;
		}
	}
	{
		// SongLoaded.Invoke();
		RhythmTool_t215962618 * L_40 = __this->get_U24this_3();
		NullCheck(L_40);
		Action_t3226471752 * L_41 = L_40->get_SongLoaded_2();
		// SongLoaded.Invoke();
		NullCheck(L_41);
		Action_Invoke_m3801112262(L_41, /*hidden argument*/NULL);
		goto IL_01a6;
	}

IL_0190:
	{
		// gameObject.SendMessage("OnReadyToPlay", SendMessageOptions.DontRequireReceiver);
		RhythmTool_t215962618 * L_42 = __this->get_U24this_3();
		// gameObject.SendMessage("OnReadyToPlay", SendMessageOptions.DontRequireReceiver);
		NullCheck(L_42);
		GameObject_t1756533147 * L_43 = Component_get_gameObject_m3105766835(L_42, /*hidden argument*/NULL);
		// gameObject.SendMessage("OnReadyToPlay", SendMessageOptions.DontRequireReceiver);
		NullCheck(L_43);
		GameObject_SendMessage_m3997572739(L_43, _stringLiteral2751490661, 1, /*hidden argument*/NULL);
	}

IL_01a6:
	{
		// }
		__this->set_U24PC_6((-1));
	}

IL_01ad:
	{
		return (bool)0;
	}

IL_01af:
	{
		return (bool)1;
	}
}
// System.Object RhythmTool/<AsyncAnalyze>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAsyncAnalyzeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1522436278 (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object RhythmTool/<AsyncAnalyze>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsyncAnalyzeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1976054558 (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool/<AsyncAnalyze>c__Iterator1::Dispose()
extern "C"  void U3CAsyncAnalyzeU3Ec__Iterator1_Dispose_m972395573 (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void RhythmTool/<AsyncAnalyze>c__Iterator1::Reset()
extern "C"  void U3CAsyncAnalyzeU3Ec__Iterator1_Reset_m4013296515 (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAsyncAnalyzeU3Ec__Iterator1_Reset_m4013296515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void RhythmTool/<QueueNewSong>c__Iterator0::.ctor()
extern "C"  void U3CQueueNewSongU3Ec__Iterator0__ctor_m4124132409 (U3CQueueNewSongU3Ec__Iterator0_t1486555536 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean RhythmTool/<QueueNewSong>c__Iterator0::MoveNext()
extern "C"  bool U3CQueueNewSongU3Ec__Iterator0_MoveNext_m2576506383 (U3CQueueNewSongU3Ec__Iterator0_t1486555536 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0047;
			}
		}
	}
	{
		goto IL_006b;
	}

IL_0021:
	{
		// yield return analyzeRoutine;
		RhythmTool_t215962618 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		Coroutine_t2299508840 * L_3 = L_2->get_analyzeRoutine_20();
		__this->set_U24current_2(L_3);
		bool L_4 = __this->get_U24disposing_3();
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0042:
	{
		goto IL_006d;
	}

IL_0047:
	{
		// queueRoutine = null;
		RhythmTool_t215962618 * L_5 = __this->get_U24this_1();
		NullCheck(L_5);
		L_5->set_queueRoutine_21((Coroutine_t2299508840 *)NULL);
		// LoadNewSong(audioClip);
		RhythmTool_t215962618 * L_6 = __this->get_U24this_1();
		AudioClip_t1932558630 * L_7 = __this->get_audioClip_0();
		// LoadNewSong(audioClip);
		NullCheck(L_6);
		RhythmTool_LoadNewSong_m1895013473(L_6, L_7, /*hidden argument*/NULL);
		// }
		__this->set_U24PC_4((-1));
	}

IL_006b:
	{
		return (bool)0;
	}

IL_006d:
	{
		return (bool)1;
	}
}
// System.Object RhythmTool/<QueueNewSong>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CQueueNewSongU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2407209227 (U3CQueueNewSongU3Ec__Iterator0_t1486555536 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object RhythmTool/<QueueNewSong>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CQueueNewSongU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m546537459 (U3CQueueNewSongU3Ec__Iterator0_t1486555536 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void RhythmTool/<QueueNewSong>c__Iterator0::Dispose()
extern "C"  void U3CQueueNewSongU3Ec__Iterator0_Dispose_m3176075578 (U3CQueueNewSongU3Ec__Iterator0_t1486555536 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void RhythmTool/<QueueNewSong>c__Iterator0::Reset()
extern "C"  void U3CQueueNewSongU3Ec__Iterator0_Reset_m4175747132 (U3CQueueNewSongU3Ec__Iterator0_t1486555536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CQueueNewSongU3Ec__Iterator0_Reset_m4175747132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Segmenter::.ctor(AnalysisData)
extern "C"  void Segmenter__ctor_m3661281169 (Segmenter_t2695296026 * __this, AnalysisData_t108342674 * ___analysis0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter__ctor_m3661281169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public Segmenter(AnalysisData analysis)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// this.analysis = analysis;
		AnalysisData_t108342674 * L_0 = ___analysis0;
		__this->set_analysis_0(L_0);
		// _changes = new Dictionary<int, float>();
		// _changes = new Dictionary<int, float>();
		Dictionary_2_t1084335567 * L_1 = (Dictionary_2_t1084335567 *)il2cpp_codegen_object_new(Dictionary_2_t1084335567_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3391555206(L_1, /*hidden argument*/Dictionary_2__ctor_m3391555206_MethodInfo_var);
		__this->set__changes_8(L_1);
		// _changeIndices = new List<int>();
		// _changeIndices = new List<int>();
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_2, /*hidden argument*/List_1__ctor_m1598946593_MethodInfo_var);
		__this->set__changeIndices_10(L_2);
		// changes = new ReadOnlyDictionary<int, float>(_changes);
		Dictionary_2_t1084335567 * L_3 = __this->get__changes_8();
		// changes = new ReadOnlyDictionary<int, float>(_changes);
		ReadOnlyDictionary_2_t1371992995 * L_4 = (ReadOnlyDictionary_2_t1371992995 *)il2cpp_codegen_object_new(ReadOnlyDictionary_2_t1371992995_il2cpp_TypeInfo_var);
		ReadOnlyDictionary_2__ctor_m855543715(L_4, L_3, /*hidden argument*/ReadOnlyDictionary_2__ctor_m855543715_MethodInfo_var);
		// changes = new ReadOnlyDictionary<int, float>(_changes);
		Segmenter_set_changes_m1458862885(__this, L_4, /*hidden argument*/NULL);
		// changeIndices = _changeIndices.AsReadOnly();
		List_1_t1440998580 * L_5 = __this->get__changeIndices_10();
		// changeIndices = _changeIndices.AsReadOnly();
		NullCheck(L_5);
		ReadOnlyCollection_1_t2257663140 * L_6 = List_1_AsReadOnly_m3503877813(L_5, /*hidden argument*/List_1_AsReadOnly_m3503877813_MethodInfo_var);
		// changeIndices = _changeIndices.AsReadOnly();
		Segmenter_set_changeIndices_m3118712765(__this, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// ReadOnlyDictionary`2<System.Int32,System.Single> Segmenter::get_changes()
extern "C"  ReadOnlyDictionary_2_t1371992995 * Segmenter_get_changes_m1846267788 (Segmenter_t2695296026 * __this, const MethodInfo* method)
{
	ReadOnlyDictionary_2_t1371992995 * V_0 = NULL;
	{
		// public ReadOnlyDictionary<int, float> changes { get; private set; }
		ReadOnlyDictionary_2_t1371992995 * L_0 = __this->get_U3CchangesU3Ek__BackingField_9();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyDictionary_2_t1371992995 * L_1 = V_0;
		return L_1;
	}
}
// System.Void Segmenter::set_changes(ReadOnlyDictionary`2<System.Int32,System.Single>)
extern "C"  void Segmenter_set_changes_m1458862885 (Segmenter_t2695296026 * __this, ReadOnlyDictionary_2_t1371992995 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyDictionary<int, float> changes { get; private set; }
		ReadOnlyDictionary_2_t1371992995 * L_0 = ___value0;
		__this->set_U3CchangesU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32> Segmenter::get_changeIndices()
extern "C"  ReadOnlyCollection_1_t2257663140 * Segmenter_get_changeIndices_m4266597926 (Segmenter_t2695296026 * __this, const MethodInfo* method)
{
	ReadOnlyCollection_1_t2257663140 * V_0 = NULL;
	{
		// public ReadOnlyCollection<int> changeIndices { get; private set; }
		ReadOnlyCollection_1_t2257663140 * L_0 = __this->get_U3CchangeIndicesU3Ek__BackingField_11();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ReadOnlyCollection_1_t2257663140 * L_1 = V_0;
		return L_1;
	}
}
// System.Void Segmenter::set_changeIndices(System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>)
extern "C"  void Segmenter_set_changeIndices_m3118712765 (Segmenter_t2695296026 * __this, ReadOnlyCollection_1_t2257663140 * ___value0, const MethodInfo* method)
{
	{
		// public ReadOnlyCollection<int> changeIndices { get; private set; }
		ReadOnlyCollection_1_t2257663140 * L_0 = ___value0;
		__this->set_U3CchangeIndicesU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void Segmenter::Init()
extern "C"  void Segmenter_Init_m1948752761 (Segmenter_t2695296026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter_Init_m1948752761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// lastDif = 0;
		__this->set_lastDif_1((0.0f));
		// increaseStart = 0;
		__this->set_increaseStart_2(0);
		// increaseEnd = 0;
		__this->set_increaseEnd_3(0);
		// increaseDetected = false;
		__this->set_increaseDetected_4((bool)0);
		// decreaseStart = 0;
		__this->set_decreaseStart_5(0);
		// decreaseEnd = 0;
		__this->set_decreaseEnd_6(0);
		// decreaseDetected = false;
		__this->set_decreaseDetected_7((bool)0);
		// _changes.Clear();
		Dictionary_2_t1084335567 * L_0 = __this->get__changes_8();
		// _changes.Clear();
		NullCheck(L_0);
		Dictionary_2_Clear_m1652694321(L_0, /*hidden argument*/Dictionary_2_Clear_m1652694321_MethodInfo_var);
		// _changeIndices.Clear();
		List_1_t1440998580 * L_1 = __this->get__changeIndices_10();
		// _changeIndices.Clear();
		NullCheck(L_1);
		List_1_Clear_m3644677550(L_1, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		// }
		return;
	}
}
// System.Void Segmenter::Init(SongData)
extern "C"  void Segmenter_Init_m2037373012 (Segmenter_t2695296026 * __this, SongData_t3132760915 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter_Init_m2037373012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3136648085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// _changeIndices.Clear();
		List_1_t1440998580 * L_0 = __this->get__changeIndices_10();
		// _changeIndices.Clear();
		NullCheck(L_0);
		List_1_Clear_m3644677550(L_0, /*hidden argument*/List_1_Clear_m3644677550_MethodInfo_var);
		// _changeIndices.AddRange(data.segmenter.changeIndices);
		List_1_t1440998580 * L_1 = __this->get__changeIndices_10();
		SongData_t3132760915 * L_2 = ___data0;
		NullCheck(L_2);
		Segmenter_t2695296026 * L_3 = L_2->get_segmenter_2();
		// _changeIndices.AddRange(data.segmenter.changeIndices);
		NullCheck(L_3);
		ReadOnlyCollection_1_t2257663140 * L_4 = Segmenter_get_changeIndices_m4266597926(L_3, /*hidden argument*/NULL);
		// _changeIndices.AddRange(data.segmenter.changeIndices);
		NullCheck(L_1);
		List_1_AddRange_m2567809379(L_1, L_4, /*hidden argument*/List_1_AddRange_m2567809379_MethodInfo_var);
		// _changes.Clear();
		Dictionary_2_t1084335567 * L_5 = __this->get__changes_8();
		// _changes.Clear();
		NullCheck(L_5);
		Dictionary_2_Clear_m1652694321(L_5, /*hidden argument*/Dictionary_2_Clear_m1652694321_MethodInfo_var);
		// foreach (KeyValuePair<int, float> change in data.segmenter.changes)
		SongData_t3132760915 * L_6 = ___data0;
		NullCheck(L_6);
		Segmenter_t2695296026 * L_7 = L_6->get_segmenter_2();
		// foreach (KeyValuePair<int, float> change in data.segmenter.changes)
		NullCheck(L_7);
		ReadOnlyDictionary_2_t1371992995 * L_8 = Segmenter_get_changes_m1846267788(L_7, /*hidden argument*/NULL);
		// foreach (KeyValuePair<int, float> change in data.segmenter.changes)
		NullCheck(L_8);
		Il2CppObject* L_9 = ReadOnlyDictionary_2_GetEnumerator_m4119707746(L_8, /*hidden argument*/ReadOnlyDictionary_2_GetEnumerator_m4119707746_MethodInfo_var);
		V_1 = L_9;
	}

IL_003f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0066;
		}

IL_0044:
		{
			// foreach (KeyValuePair<int, float> change in data.segmenter.changes)
			Il2CppObject* L_10 = V_1;
			// foreach (KeyValuePair<int, float> change in data.segmenter.changes)
			NullCheck(L_10);
			KeyValuePair_2_t3136648085  L_11 = InterfaceFuncInvoker0< KeyValuePair_2_t3136648085  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>>::get_Current() */, IEnumerator_1_t612171912_il2cpp_TypeInfo_var, L_10);
			V_0 = L_11;
			// _changes.Add(change.Key, change.Value);
			Dictionary_2_t1084335567 * L_12 = __this->get__changes_8();
			// _changes.Add(change.Key, change.Value);
			int32_t L_13 = KeyValuePair_2_get_Key_m2768754870((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m2768754870_MethodInfo_var);
			// _changes.Add(change.Key, change.Value);
			float L_14 = KeyValuePair_2_get_Value_m2168417739((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m2168417739_MethodInfo_var);
			// _changes.Add(change.Key, change.Value);
			NullCheck(L_12);
			Dictionary_2_Add_m4070564334(L_12, L_13, L_14, /*hidden argument*/Dictionary_2_Add_m4070564334_MethodInfo_var);
		}

IL_0066:
		{
			Il2CppObject* L_15 = V_1;
			// foreach (KeyValuePair<int, float> change in data.segmenter.changes)
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0044;
			}
		}

IL_0071:
		{
			IL2CPP_LEAVE(0x83, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_1;
			if (!L_17)
			{
				goto IL_0082;
			}
		}

IL_007c:
		{
			Il2CppObject* L_18 = V_1;
			// foreach (KeyValuePair<int, float> change in data.segmenter.changes)
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_18);
		}

IL_0082:
		{
			IL2CPP_END_FINALLY(118)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x83, IL_0083)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0083:
	{
		// }
		return;
	}
}
// System.Void Segmenter::DetectChanges(System.Int32)
extern "C"  void Segmenter_DetectChanges_m3953497980 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter_DetectChanges_m3953497980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	{
		// if (index < 0)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		// return;
		goto IL_034c;
	}

IL_000d:
	{
		// float a = analysis.magnitudeAvg[index + 1];
		AnalysisData_t108342674 * L_1 = __this->get_analysis_0();
		// float a = analysis.magnitudeAvg[index + 1];
		NullCheck(L_1);
		ReadOnlyCollection_1_t2262295624 * L_2 = AnalysisData_get_magnitudeAvg_m3285528701(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___index0;
		// float a = analysis.magnitudeAvg[index + 1];
		NullCheck(L_2);
		float L_4 = ReadOnlyCollection_1_get_Item_m210738684(L_2, ((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_0 = L_4;
		// float b = analysis.magnitudeAvg[index];
		AnalysisData_t108342674 * L_5 = __this->get_analysis_0();
		// float b = analysis.magnitudeAvg[index];
		NullCheck(L_5);
		ReadOnlyCollection_1_t2262295624 * L_6 = AnalysisData_get_magnitudeAvg_m3285528701(L_5, /*hidden argument*/NULL);
		int32_t L_7 = ___index0;
		// float b = analysis.magnitudeAvg[index];
		NullCheck(L_6);
		float L_8 = ReadOnlyCollection_1_get_Item_m210738684(L_6, L_7, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_1 = L_8;
		// float dif = a - b;
		float L_9 = V_0;
		float L_10 = V_1;
		V_2 = ((float)((float)L_9-(float)L_10));
		// if (dif >= .05f && lastDif < .05f && !increaseDetected)
		float L_11 = V_2;
		if ((!(((float)L_11) >= ((float)(0.05f)))))
		{
			goto IL_006d;
		}
	}
	{
		float L_12 = __this->get_lastDif_1();
		if ((!(((float)L_12) < ((float)(0.05f)))))
		{
			goto IL_006d;
		}
	}
	{
		bool L_13 = __this->get_increaseDetected_4();
		if (L_13)
		{
			goto IL_006d;
		}
	}
	{
		// increaseStart = index;
		int32_t L_14 = ___index0;
		__this->set_increaseStart_2(L_14);
		// increaseDetected = true;
		__this->set_increaseDetected_4((bool)1);
	}

IL_006d:
	{
		// if (dif <= -.08f && lastDif > -.08f && !decreaseDetected)
		float L_15 = V_2;
		if ((!(((float)L_15) <= ((float)(-0.08f)))))
		{
			goto IL_00a3;
		}
	}
	{
		float L_16 = __this->get_lastDif_1();
		if ((!(((float)L_16) > ((float)(-0.08f)))))
		{
			goto IL_00a3;
		}
	}
	{
		bool L_17 = __this->get_decreaseDetected_7();
		if (L_17)
		{
			goto IL_00a3;
		}
	}
	{
		// decreaseStart = index;
		int32_t L_18 = ___index0;
		__this->set_decreaseStart_5(L_18);
		// decreaseDetected = true;
		__this->set_decreaseDetected_7((bool)1);
	}

IL_00a3:
	{
		// if (dif < .04f && lastDif > .04f && increaseDetected)
		float L_19 = V_2;
		if ((!(((float)L_19) < ((float)(0.04f)))))
		{
			goto IL_01f2;
		}
	}
	{
		float L_20 = __this->get_lastDif_1();
		if ((!(((float)L_20) > ((float)(0.04f)))))
		{
			goto IL_01f2;
		}
	}
	{
		bool L_21 = __this->get_increaseDetected_4();
		if (!L_21)
		{
			goto IL_01f2;
		}
	}
	{
		// float rl = 22; //22
		V_3 = (22.0f);
		// if (dif > -.04f)
		float L_22 = V_2;
		if ((!(((float)L_22) > ((float)(-0.04f)))))
		{
			goto IL_00e1;
		}
	}
	{
		// rl = 12; //12
		V_3 = (12.0f);
	}

IL_00e1:
	{
		// increaseEnd = index;
		int32_t L_23 = ___index0;
		__this->set_increaseEnd_3(L_23);
		// increaseDetected = false;
		__this->set_increaseDetected_4((bool)0);
		// float steepest = 0;
		V_4 = (0.0f);
		// int si = increaseStart;
		int32_t L_24 = __this->get_increaseStart_2();
		V_5 = L_24;
		// for (int i = increaseStart; i < increaseEnd; i++)
		int32_t L_25 = __this->get_increaseStart_2();
		V_6 = L_25;
		goto IL_0157;
	}

IL_010b:
	{
		// float a2 = (analysis.magnitudeSmooth[i + 1]);
		AnalysisData_t108342674 * L_26 = __this->get_analysis_0();
		// float a2 = (analysis.magnitudeSmooth[i + 1]);
		NullCheck(L_26);
		ReadOnlyCollection_1_t2262295624 * L_27 = AnalysisData_get_magnitudeSmooth_m1926036311(L_26, /*hidden argument*/NULL);
		int32_t L_28 = V_6;
		// float a2 = (analysis.magnitudeSmooth[i + 1]);
		NullCheck(L_27);
		float L_29 = ReadOnlyCollection_1_get_Item_m210738684(L_27, ((int32_t)((int32_t)L_28+(int32_t)1)), /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_7 = L_29;
		// float b2 = (analysis.magnitudeSmooth[i]);
		AnalysisData_t108342674 * L_30 = __this->get_analysis_0();
		// float b2 = (analysis.magnitudeSmooth[i]);
		NullCheck(L_30);
		ReadOnlyCollection_1_t2262295624 * L_31 = AnalysisData_get_magnitudeSmooth_m1926036311(L_30, /*hidden argument*/NULL);
		int32_t L_32 = V_6;
		// float b2 = (analysis.magnitudeSmooth[i]);
		NullCheck(L_31);
		float L_33 = ReadOnlyCollection_1_get_Item_m210738684(L_31, L_32, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_8 = L_33;
		// float nextDif = a2 - b2;
		float L_34 = V_7;
		float L_35 = V_8;
		V_9 = ((float)((float)L_34-(float)L_35));
		// if (nextDif > steepest)
		float L_36 = V_9;
		float L_37 = V_4;
		if ((!(((float)L_36) > ((float)L_37))))
		{
			goto IL_0150;
		}
	}
	{
		// steepest = nextDif;
		float L_38 = V_9;
		V_4 = L_38;
		// si = i;
		int32_t L_39 = V_6;
		V_5 = L_39;
	}

IL_0150:
	{
		// for (int i = increaseStart; i < increaseEnd; i++)
		int32_t L_40 = V_6;
		V_6 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_0157:
	{
		// for (int i = increaseStart; i < increaseEnd; i++)
		int32_t L_41 = V_6;
		int32_t L_42 = __this->get_increaseEnd_3();
		if ((((int32_t)L_41) < ((int32_t)L_42)))
		{
			goto IL_010b;
		}
	}
	{
		// a = analysis.magnitudeAvg[increaseEnd];
		AnalysisData_t108342674 * L_43 = __this->get_analysis_0();
		// a = analysis.magnitudeAvg[increaseEnd];
		NullCheck(L_43);
		ReadOnlyCollection_1_t2262295624 * L_44 = AnalysisData_get_magnitudeAvg_m3285528701(L_43, /*hidden argument*/NULL);
		int32_t L_45 = __this->get_increaseEnd_3();
		// a = analysis.magnitudeAvg[increaseEnd];
		NullCheck(L_44);
		float L_46 = ReadOnlyCollection_1_get_Item_m210738684(L_44, L_45, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_0 = L_46;
		// b = analysis.magnitudeAvg[increaseStart];
		AnalysisData_t108342674 * L_47 = __this->get_analysis_0();
		// b = analysis.magnitudeAvg[increaseStart];
		NullCheck(L_47);
		ReadOnlyCollection_1_t2262295624 * L_48 = AnalysisData_get_magnitudeAvg_m3285528701(L_47, /*hidden argument*/NULL);
		int32_t L_49 = __this->get_increaseStart_2();
		// b = analysis.magnitudeAvg[increaseStart];
		NullCheck(L_48);
		float L_50 = ReadOnlyCollection_1_get_Item_m210738684(L_48, L_49, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_1 = L_50;
		// float length = Mathf.Sqrt(Mathf.Pow((a - b), 2) + Mathf.Pow((increaseEnd * .1f - increaseStart * .1f), 2));
		float L_51 = V_0;
		float L_52 = V_1;
		// float length = Mathf.Sqrt(Mathf.Pow((a - b), 2) + Mathf.Pow((increaseEnd * .1f - increaseStart * .1f), 2));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_53 = powf(((float)((float)L_51-(float)L_52)), (2.0f));
		int32_t L_54 = __this->get_increaseEnd_3();
		int32_t L_55 = __this->get_increaseStart_2();
		// float length = Mathf.Sqrt(Mathf.Pow((a - b), 2) + Mathf.Pow((increaseEnd * .1f - increaseStart * .1f), 2));
		float L_56 = powf(((float)((float)((float)((float)(((float)((float)L_54)))*(float)(0.1f)))-(float)((float)((float)(((float)((float)L_55)))*(float)(0.1f))))), (2.0f));
		// float length = Mathf.Sqrt(Mathf.Pow((a - b), 2) + Mathf.Pow((increaseEnd * .1f - increaseStart * .1f), 2));
		float L_57 = sqrtf(((float)((float)L_53+(float)L_56)));
		V_10 = L_57;
		// if (length > rl)
		float L_58 = V_10;
		float L_59 = V_3;
		if ((!(((float)L_58) > ((float)L_59))))
		{
			goto IL_01f1;
		}
	}
	{
		// _changes.Add(si, a);
		Dictionary_2_t1084335567 * L_60 = __this->get__changes_8();
		int32_t L_61 = V_5;
		float L_62 = V_0;
		// _changes.Add(si, a);
		NullCheck(L_60);
		Dictionary_2_Add_m4070564334(L_60, L_61, L_62, /*hidden argument*/Dictionary_2_Add_m4070564334_MethodInfo_var);
		// _changeIndices.Add(si);
		List_1_t1440998580 * L_63 = __this->get__changeIndices_10();
		int32_t L_64 = V_5;
		// _changeIndices.Add(si);
		NullCheck(L_63);
		List_1_Add_m688682013(L_63, L_64, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
	}

IL_01f1:
	{
	}

IL_01f2:
	{
		// if (dif > -.04f && lastDif < -.04f && decreaseDetected)
		float L_65 = V_2;
		if ((!(((float)L_65) > ((float)(-0.04f)))))
		{
			goto IL_0345;
		}
	}
	{
		float L_66 = __this->get_lastDif_1();
		if ((!(((float)L_66) < ((float)(-0.04f)))))
		{
			goto IL_0345;
		}
	}
	{
		bool L_67 = __this->get_decreaseDetected_7();
		if (!L_67)
		{
			goto IL_0345;
		}
	}
	{
		// float rl = 22;
		V_11 = (22.0f);
		// if (dif < .04f)
		float L_68 = V_2;
		if ((!(((float)L_68) < ((float)(0.04f)))))
		{
			goto IL_0232;
		}
	}
	{
		// rl = 15;
		V_11 = (15.0f);
	}

IL_0232:
	{
		// decreaseEnd = index;
		int32_t L_69 = ___index0;
		__this->set_decreaseEnd_6(L_69);
		// decreaseDetected = false;
		__this->set_decreaseDetected_7((bool)0);
		// float steepest = 0;
		V_12 = (0.0f);
		// int si = decreaseStart;
		int32_t L_70 = __this->get_decreaseStart_5();
		V_13 = L_70;
		// for (int i = decreaseStart; i < decreaseEnd; i++)
		int32_t L_71 = __this->get_decreaseStart_5();
		V_14 = L_71;
		goto IL_02a8;
	}

IL_025c:
	{
		// float a2 = (analysis.magnitudeSmooth[i + 1]);
		AnalysisData_t108342674 * L_72 = __this->get_analysis_0();
		// float a2 = (analysis.magnitudeSmooth[i + 1]);
		NullCheck(L_72);
		ReadOnlyCollection_1_t2262295624 * L_73 = AnalysisData_get_magnitudeSmooth_m1926036311(L_72, /*hidden argument*/NULL);
		int32_t L_74 = V_14;
		// float a2 = (analysis.magnitudeSmooth[i + 1]);
		NullCheck(L_73);
		float L_75 = ReadOnlyCollection_1_get_Item_m210738684(L_73, ((int32_t)((int32_t)L_74+(int32_t)1)), /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_15 = L_75;
		// float b2 = (analysis.magnitudeSmooth[i]);
		AnalysisData_t108342674 * L_76 = __this->get_analysis_0();
		// float b2 = (analysis.magnitudeSmooth[i]);
		NullCheck(L_76);
		ReadOnlyCollection_1_t2262295624 * L_77 = AnalysisData_get_magnitudeSmooth_m1926036311(L_76, /*hidden argument*/NULL);
		int32_t L_78 = V_14;
		// float b2 = (analysis.magnitudeSmooth[i]);
		NullCheck(L_77);
		float L_79 = ReadOnlyCollection_1_get_Item_m210738684(L_77, L_78, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_16 = L_79;
		// float nextDif = a2 - b2;
		float L_80 = V_15;
		float L_81 = V_16;
		V_17 = ((float)((float)L_80-(float)L_81));
		// if (nextDif < steepest)
		float L_82 = V_17;
		float L_83 = V_12;
		if ((!(((float)L_82) < ((float)L_83))))
		{
			goto IL_02a1;
		}
	}
	{
		// steepest = nextDif;
		float L_84 = V_17;
		V_12 = L_84;
		// si = i;
		int32_t L_85 = V_14;
		V_13 = L_85;
	}

IL_02a1:
	{
		// for (int i = decreaseStart; i < decreaseEnd; i++)
		int32_t L_86 = V_14;
		V_14 = ((int32_t)((int32_t)L_86+(int32_t)1));
	}

IL_02a8:
	{
		// for (int i = decreaseStart; i < decreaseEnd; i++)
		int32_t L_87 = V_14;
		int32_t L_88 = __this->get_decreaseEnd_6();
		if ((((int32_t)L_87) < ((int32_t)L_88)))
		{
			goto IL_025c;
		}
	}
	{
		// a = analysis.magnitudeAvg[decreaseEnd];
		AnalysisData_t108342674 * L_89 = __this->get_analysis_0();
		// a = analysis.magnitudeAvg[decreaseEnd];
		NullCheck(L_89);
		ReadOnlyCollection_1_t2262295624 * L_90 = AnalysisData_get_magnitudeAvg_m3285528701(L_89, /*hidden argument*/NULL);
		int32_t L_91 = __this->get_decreaseEnd_6();
		// a = analysis.magnitudeAvg[decreaseEnd];
		NullCheck(L_90);
		float L_92 = ReadOnlyCollection_1_get_Item_m210738684(L_90, L_91, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_0 = L_92;
		// b = analysis.magnitudeAvg[decreaseStart];
		AnalysisData_t108342674 * L_93 = __this->get_analysis_0();
		// b = analysis.magnitudeAvg[decreaseStart];
		NullCheck(L_93);
		ReadOnlyCollection_1_t2262295624 * L_94 = AnalysisData_get_magnitudeAvg_m3285528701(L_93, /*hidden argument*/NULL);
		int32_t L_95 = __this->get_decreaseStart_5();
		// b = analysis.magnitudeAvg[decreaseStart];
		NullCheck(L_94);
		float L_96 = ReadOnlyCollection_1_get_Item_m210738684(L_94, L_95, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_1 = L_96;
		// float length = Mathf.Sqrt(Mathf.Pow((a - b), 2) + Mathf.Pow((decreaseEnd * .1f - decreaseStart * .1f), 2));
		float L_97 = V_0;
		float L_98 = V_1;
		// float length = Mathf.Sqrt(Mathf.Pow((a - b), 2) + Mathf.Pow((decreaseEnd * .1f - decreaseStart * .1f), 2));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_99 = powf(((float)((float)L_97-(float)L_98)), (2.0f));
		int32_t L_100 = __this->get_decreaseEnd_6();
		int32_t L_101 = __this->get_decreaseStart_5();
		// float length = Mathf.Sqrt(Mathf.Pow((a - b), 2) + Mathf.Pow((decreaseEnd * .1f - decreaseStart * .1f), 2));
		float L_102 = powf(((float)((float)((float)((float)(((float)((float)L_100)))*(float)(0.1f)))-(float)((float)((float)(((float)((float)L_101)))*(float)(0.1f))))), (2.0f));
		// float length = Mathf.Sqrt(Mathf.Pow((a - b), 2) + Mathf.Pow((decreaseEnd * .1f - decreaseStart * .1f), 2));
		float L_103 = sqrtf(((float)((float)L_99+(float)L_102)));
		V_18 = L_103;
		// if (length > rl)
		float L_104 = V_18;
		float L_105 = V_11;
		if ((!(((float)L_104) > ((float)L_105))))
		{
			goto IL_0344;
		}
	}
	{
		// _changes.Add(si, -a);
		Dictionary_2_t1084335567 * L_106 = __this->get__changes_8();
		int32_t L_107 = V_13;
		float L_108 = V_0;
		// _changes.Add(si, -a);
		NullCheck(L_106);
		Dictionary_2_Add_m4070564334(L_106, L_107, ((-L_108)), /*hidden argument*/Dictionary_2_Add_m4070564334_MethodInfo_var);
		// _changeIndices.Add(si);
		List_1_t1440998580 * L_109 = __this->get__changeIndices_10();
		int32_t L_110 = V_13;
		// _changeIndices.Add(si);
		NullCheck(L_109);
		List_1_Add_m688682013(L_109, L_110, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
	}

IL_0344:
	{
	}

IL_0345:
	{
		// lastDif = dif;
		float L_111 = V_2;
		__this->set_lastDif_1(L_111);
	}

IL_034c:
	{
		// }
		return;
	}
}
// System.Boolean Segmenter::IsChange(System.Int32)
extern "C"  bool Segmenter_IsChange_m246302866 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter_IsChange_m246302866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		// if (_changes.Count == 0)
		Dictionary_2_t1084335567 * L_0 = __this->get__changes_8();
		// if (_changes.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m3437212136(L_0, /*hidden argument*/Dictionary_2_get_Count_m3437212136_MethodInfo_var);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return false;
		V_0 = (bool)0;
		goto IL_0035;
	}

IL_0018:
	{
		// int nextChange = NextChangeIndex((int)index);
		int32_t L_2 = ___index0;
		// int nextChange = NextChangeIndex((int)index);
		int32_t L_3 = Segmenter_NextChangeIndex_m3584235607(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// if (nextChange == index)
		int32_t L_4 = V_1;
		int32_t L_5 = ___index0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_002e;
		}
	}
	{
		// return true;
		V_0 = (bool)1;
		goto IL_0035;
	}

IL_002e:
	{
		// return false;
		V_0 = (bool)0;
		goto IL_0035;
	}

IL_0035:
	{
		// }
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Int32 Segmenter::PrevChangeIndex(System.Int32)
extern "C"  int32_t Segmenter_PrevChangeIndex_m843645 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter_PrevChangeIndex_m843645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// if (_changeIndices.Count == 0)
		List_1_t1440998580 * L_0 = __this->get__changeIndices_10();
		// if (_changeIndices.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m852068579(L_0, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return 0;
		V_0 = 0;
		goto IL_0059;
	}

IL_0018:
	{
		// int prevChange = _changeIndices.BinarySearch(index);
		List_1_t1440998580 * L_2 = __this->get__changeIndices_10();
		int32_t L_3 = ___index0;
		// int prevChange = _changeIndices.BinarySearch(index);
		NullCheck(L_2);
		int32_t L_4 = List_1_BinarySearch_m4172392722(L_2, L_3, /*hidden argument*/List_1_BinarySearch_m4172392722_MethodInfo_var);
		V_1 = L_4;
		// prevChange = Mathf.Max(prevChange, ~prevChange);
		int32_t L_5 = V_1;
		int32_t L_6 = V_1;
		// prevChange = Mathf.Max(prevChange, ~prevChange);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_Max_m1875893177(NULL /*static, unused*/, L_5, ((~L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		// prevChange = Mathf.Clamp(prevChange - 1, 0, _changeIndices.Count - 1);
		int32_t L_8 = V_1;
		List_1_t1440998580 * L_9 = __this->get__changeIndices_10();
		// prevChange = Mathf.Clamp(prevChange - 1, 0, _changeIndices.Count - 1);
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m852068579(L_9, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		// prevChange = Mathf.Clamp(prevChange - 1, 0, _changeIndices.Count - 1);
		int32_t L_11 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, ((int32_t)((int32_t)L_8-(int32_t)1)), 0, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_11;
		// prevChange = _changeIndices[prevChange];
		List_1_t1440998580 * L_12 = __this->get__changeIndices_10();
		int32_t L_13 = V_1;
		// prevChange = _changeIndices[prevChange];
		NullCheck(L_12);
		int32_t L_14 = List_1_get_Item_m1921196075(L_12, L_13, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_1 = L_14;
		// return prevChange;
		int32_t L_15 = V_1;
		V_0 = L_15;
		goto IL_0059;
	}

IL_0059:
	{
		// }
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.Int32 Segmenter::NextChangeIndex(System.Int32)
extern "C"  int32_t Segmenter_NextChangeIndex_m3584235607 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter_NextChangeIndex_m3584235607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// if (_changeIndices.Count == 0)
		List_1_t1440998580 * L_0 = __this->get__changeIndices_10();
		// if (_changeIndices.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m852068579(L_0, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return 0;
		V_0 = 0;
		goto IL_0057;
	}

IL_0018:
	{
		// int nextChange = _changeIndices.BinarySearch(index);
		List_1_t1440998580 * L_2 = __this->get__changeIndices_10();
		int32_t L_3 = ___index0;
		// int nextChange = _changeIndices.BinarySearch(index);
		NullCheck(L_2);
		int32_t L_4 = List_1_BinarySearch_m4172392722(L_2, L_3, /*hidden argument*/List_1_BinarySearch_m4172392722_MethodInfo_var);
		V_1 = L_4;
		// nextChange = Mathf.Max(nextChange, ~nextChange);
		int32_t L_5 = V_1;
		int32_t L_6 = V_1;
		// nextChange = Mathf.Max(nextChange, ~nextChange);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_Max_m1875893177(NULL /*static, unused*/, L_5, ((~L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		// nextChange = Mathf.Clamp(nextChange, 0, _changeIndices.Count - 1);
		int32_t L_8 = V_1;
		List_1_t1440998580 * L_9 = __this->get__changeIndices_10();
		// nextChange = Mathf.Clamp(nextChange, 0, _changeIndices.Count - 1);
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m852068579(L_9, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		// nextChange = Mathf.Clamp(nextChange, 0, _changeIndices.Count - 1);
		int32_t L_11 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, L_8, 0, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_11;
		// nextChange = _changeIndices[nextChange];
		List_1_t1440998580 * L_12 = __this->get__changeIndices_10();
		int32_t L_13 = V_1;
		// nextChange = _changeIndices[nextChange];
		NullCheck(L_12);
		int32_t L_14 = List_1_get_Item_m1921196075(L_12, L_13, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_1 = L_14;
		// return nextChange;
		int32_t L_15 = V_1;
		V_0 = L_15;
		goto IL_0057;
	}

IL_0057:
	{
		// }
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.Single Segmenter::PrevChange(System.Int32)
extern "C"  float Segmenter_PrevChange_m778242627 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter_PrevChange_m778242627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// if (_changes.Count == 0)
		Dictionary_2_t1084335567 * L_0 = __this->get__changes_8();
		// if (_changes.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m3437212136(L_0, /*hidden argument*/Dictionary_2_get_Count_m3437212136_MethodInfo_var);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		// return 0;
		V_0 = (0.0f);
		goto IL_0036;
	}

IL_001c:
	{
		// int prevChange = PrevChangeIndex(index);
		int32_t L_2 = ___index0;
		// int prevChange = PrevChangeIndex(index);
		int32_t L_3 = Segmenter_PrevChangeIndex_m843645(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// return _changes[prevChange];
		Dictionary_2_t1084335567 * L_4 = __this->get__changes_8();
		int32_t L_5 = V_1;
		// return _changes[prevChange];
		NullCheck(L_4);
		float L_6 = Dictionary_2_get_Item_m1874253002(L_4, L_5, /*hidden argument*/Dictionary_2_get_Item_m1874253002_MethodInfo_var);
		V_0 = L_6;
		goto IL_0036;
	}

IL_0036:
	{
		// }
		float L_7 = V_0;
		return L_7;
	}
}
// System.Single Segmenter::NextChange(System.Int32)
extern "C"  float Segmenter_NextChange_m3516349633 (Segmenter_t2695296026 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Segmenter_NextChange_m3516349633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// if (_changes.Count == 0)
		Dictionary_2_t1084335567 * L_0 = __this->get__changes_8();
		// if (_changes.Count == 0)
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m3437212136(L_0, /*hidden argument*/Dictionary_2_get_Count_m3437212136_MethodInfo_var);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		// return 0;
		V_0 = (0.0f);
		goto IL_0036;
	}

IL_001c:
	{
		// int nextChange = NextChangeIndex(index);
		int32_t L_2 = ___index0;
		// int nextChange = NextChangeIndex(index);
		int32_t L_3 = Segmenter_NextChangeIndex_m3584235607(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// return _changes[nextChange];
		Dictionary_2_t1084335567 * L_4 = __this->get__changes_8();
		int32_t L_5 = V_1;
		// return _changes[nextChange];
		NullCheck(L_4);
		float L_6 = Dictionary_2_get_Item_m1874253002(L_4, L_5, /*hidden argument*/Dictionary_2_get_Item_m1874253002_MethodInfo_var);
		V_0 = L_6;
		goto IL_0036;
	}

IL_0036:
	{
		// }
		float L_7 = V_0;
		return L_7;
	}
}
// System.Void SongData::.ctor(System.Collections.Generic.List`1<AnalysisData>,System.String,System.Int32,BeatTracker,Segmenter)
extern "C"  void SongData__ctor_m676028367 (SongData_t3132760915 * __this, List_1_t3772431102 * ___analyses0, String_t* ___name1, int32_t ___length2, BeatTracker_t2801099156 * ___beatTracker3, Segmenter_t2695296026 * ___segmenter4, const MethodInfo* method)
{
	{
		// public SongData(List<AnalysisData> analyses, string name, int length, BeatTracker beatTracker, Segmenter segmenter)
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		// this.analyses = analyses;
		List_1_t3772431102 * L_0 = ___analyses0;
		__this->set_analyses_0(L_0);
		// this.name = name;
		String_t* L_1 = ___name1;
		__this->set_name_3(L_1);
		// this.length = length;
		int32_t L_2 = ___length2;
		__this->set_length_4(L_2);
		// this.segmenter = segmenter;
		Segmenter_t2695296026 * L_3 = ___segmenter4;
		__this->set_segmenter_2(L_3);
		// this.beatTracker = beatTracker;
		BeatTracker_t2801099156 * L_4 = ___beatTracker3;
		__this->set_beatTracker_1(L_4);
		// }
		return;
	}
}
// System.Void SongData::Serialize()
extern "C"  void SongData_Serialize_m1654775970 (SongData_t3132760915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SongData_Serialize_m1654775970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Stream_t3255436806 * V_1 = NULL;
	{
		// IFormatter formatter = new BinaryFormatter();
		BinaryFormatter_t1866979105 * L_0 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// Stream stream = new FileStream(Application.persistentDataPath + Path.DirectorySeparatorChar + name + ".rthm", FileMode.Create, FileAccess.Write, FileShare.None);
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		// Stream stream = new FileStream(Application.persistentDataPath + Path.DirectorySeparatorChar + name + ".rthm", FileMode.Create, FileAccess.Write, FileShare.None);
		String_t* L_2 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		Il2CppChar L_4 = ((Path_t41728875_StaticFields*)Path_t41728875_il2cpp_TypeInfo_var->static_fields)->get_DirectorySeparatorChar_2();
		Il2CppChar L_5 = L_4;
		Il2CppObject * L_6 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		ObjectU5BU5D_t3614634134* L_7 = L_3;
		String_t* L_8 = __this->get_name_3();
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_8);
		ObjectU5BU5D_t3614634134* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral2395002637);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2395002637);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3881798623(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		// Stream stream = new FileStream(Application.persistentDataPath + Path.DirectorySeparatorChar + name + ".rthm", FileMode.Create, FileAccess.Write, FileShare.None);
		FileStream_t1695958676 * L_11 = (FileStream_t1695958676 *)il2cpp_codegen_object_new(FileStream_t1695958676_il2cpp_TypeInfo_var);
		FileStream__ctor_m3699774824(L_11, L_10, 2, 2, 0, /*hidden argument*/NULL);
		V_1 = L_11;
		// formatter.Serialize(stream, this);
		Il2CppObject * L_12 = V_0;
		Stream_t3255436806 * L_13 = V_1;
		// formatter.Serialize(stream, this);
		NullCheck(L_12);
		InterfaceActionInvoker2< Stream_t3255436806 *, Il2CppObject * >::Invoke(1 /* System.Void System.Runtime.Serialization.IFormatter::Serialize(System.IO.Stream,System.Object) */, IFormatter_t936711909_il2cpp_TypeInfo_var, L_12, L_13, __this);
		// stream.Close();
		Stream_t3255436806 * L_14 = V_1;
		// stream.Close();
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_14);
		// }
		return;
	}
}
// SongData SongData::Deserialize(System.String)
extern "C"  SongData_t3132760915 * SongData_Deserialize_m3771651313 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SongData_Deserialize_m3771651313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Stream_t3255436806 * V_1 = NULL;
	SongData_t3132760915 * V_2 = NULL;
	SongData_t3132760915 * V_3 = NULL;
	{
		// IFormatter formatter = new BinaryFormatter();
		BinaryFormatter_t1866979105 * L_0 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// Stream stream = new FileStream(Application.persistentDataPath + Path.DirectorySeparatorChar + name + ".rthm", FileMode.Open, FileAccess.Read, FileShare.Read);
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		// Stream stream = new FileStream(Application.persistentDataPath + Path.DirectorySeparatorChar + name + ".rthm", FileMode.Open, FileAccess.Read, FileShare.Read);
		String_t* L_2 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		Il2CppChar L_4 = ((Path_t41728875_StaticFields*)Path_t41728875_il2cpp_TypeInfo_var->static_fields)->get_DirectorySeparatorChar_2();
		Il2CppChar L_5 = L_4;
		Il2CppObject * L_6 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		ObjectU5BU5D_t3614634134* L_7 = L_3;
		String_t* L_8 = ___name0;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_8);
		ObjectU5BU5D_t3614634134* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral2395002637);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2395002637);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3881798623(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		// Stream stream = new FileStream(Application.persistentDataPath + Path.DirectorySeparatorChar + name + ".rthm", FileMode.Open, FileAccess.Read, FileShare.Read);
		FileStream_t1695958676 * L_11 = (FileStream_t1695958676 *)il2cpp_codegen_object_new(FileStream_t1695958676_il2cpp_TypeInfo_var);
		FileStream__ctor_m3699774824(L_11, L_10, 3, 1, 1, /*hidden argument*/NULL);
		V_1 = L_11;
		// SongData obj = (SongData)formatter.Deserialize(stream);
		Il2CppObject * L_12 = V_0;
		Stream_t3255436806 * L_13 = V_1;
		// SongData obj = (SongData)formatter.Deserialize(stream);
		NullCheck(L_12);
		Il2CppObject * L_14 = InterfaceFuncInvoker1< Il2CppObject *, Stream_t3255436806 * >::Invoke(0 /* System.Object System.Runtime.Serialization.IFormatter::Deserialize(System.IO.Stream) */, IFormatter_t936711909_il2cpp_TypeInfo_var, L_12, L_13);
		V_2 = ((SongData_t3132760915 *)CastclassClass(L_14, SongData_t3132760915_il2cpp_TypeInfo_var));
		// stream.Close();
		Stream_t3255436806 * L_15 = V_1;
		// stream.Close();
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_15);
		// return obj;
		SongData_t3132760915 * L_16 = V_2;
		V_3 = L_16;
		goto IL_0056;
	}

IL_0056:
	{
		// }
		SongData_t3132760915 * L_17 = V_3;
		return L_17;
	}
}
// System.Void SpawnManager::.ctor()
extern "C"  void SpawnManager__ctor_m729849119 (SpawnManager_t4269630218 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnManager::Start()
extern "C"  void SpawnManager_Start_m521168043 (SpawnManager_t4269630218 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnManager_Start_m521168043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_spawnObjects = new GameObject[4];
		__this->set_m_spawnObjects_2(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)4)));
		// m_spawnObjects[0] = GameObject.FindWithTag("Spawn0");
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_m_spawnObjects_2();
		// m_spawnObjects[0] = GameObject.FindWithTag("Spawn0");
		GameObject_t1756533147 * L_1 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral859132627, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (GameObject_t1756533147 *)L_1);
		// m_spawnObjects[1] = GameObject.FindWithTag("Spawn1");
		GameObjectU5BU5D_t3057952154* L_2 = __this->get_m_spawnObjects_2();
		// m_spawnObjects[1] = GameObject.FindWithTag("Spawn1");
		GameObject_t1756533147 * L_3 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral2425216568, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (GameObject_t1756533147 *)L_3);
		// m_spawnObjects[2] = GameObject.FindWithTag("Spawn2");
		GameObjectU5BU5D_t3057952154* L_4 = __this->get_m_spawnObjects_2();
		// m_spawnObjects[2] = GameObject.FindWithTag("Spawn2");
		GameObject_t1756533147 * L_5 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral2021932041, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (GameObject_t1756533147 *)L_5);
		// m_spawnObjects[3] = GameObject.FindWithTag("Spawn3");
		GameObjectU5BU5D_t3057952154* L_6 = __this->get_m_spawnObjects_2();
		// m_spawnObjects[3] = GameObject.FindWithTag("Spawn3");
		GameObject_t1756533147 * L_7 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral3588015982, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (GameObject_t1756533147 *)L_7);
		// }
		return;
	}
}
// System.Void SpawnManager::Update()
extern "C"  void SpawnManager_Update_m987479736 (SpawnManager_t4269630218 * __this, const MethodInfo* method)
{
	{
		// }
		return;
	}
}
// System.Void SpawnManager::OnBeat(Beat)
extern "C"  void SpawnManager_OnBeat_m2725267976 (SpawnManager_t4269630218 * __this, Beat_t2695683572 * ___beat0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnManager_OnBeat_m2725267976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// beatCount += 1;
		int32_t L_0 = __this->get_beatCount_5();
		__this->set_beatCount_5(((int32_t)((int32_t)L_0+(int32_t)1)));
		// if(high)
		bool L_1 = __this->get_high_6();
		if (!L_1)
		{
			goto IL_0170;
		}
	}
	{
		// if (mid == false && low == false)
		bool L_2 = __this->get_mid_7();
		if (L_2)
		{
			goto IL_006f;
		}
	}
	{
		bool L_3 = __this->get_low_8();
		if (L_3)
		{
			goto IL_006f;
		}
	}
	{
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_4 = __this->get_m_enemySpawn_3();
		NullCheck(L_4);
		int32_t L_5 = 0;
		GameObject_t1756533147 * L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		GameObjectU5BU5D_t3057952154* L_7 = __this->get_m_spawnObjects_2();
		NullCheck(L_7);
		int32_t L_8 = 0;
		GameObject_t1756533147 * L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_12 = __this->get_m_spawnObjects_2();
		NullCheck(L_12);
		int32_t L_13 = 0;
		GameObject_t1756533147 * L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_15);
		Quaternion_t4030073918  L_16 = Transform_get_rotation_m1033555130(L_15, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_17 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_6, L_11, L_16, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_17);
		goto IL_016a;
	}

IL_006f:
	{
		// else if (mid && !low)
		bool L_18 = __this->get_mid_7();
		if (!L_18)
		{
			goto IL_00fa;
		}
	}
	{
		bool L_19 = __this->get_low_8();
		if (L_19)
		{
			goto IL_00fa;
		}
	}
	{
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_20 = __this->get_m_enemySpawn_3();
		NullCheck(L_20);
		int32_t L_21 = 0;
		GameObject_t1756533147 * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		GameObjectU5BU5D_t3057952154* L_23 = __this->get_m_spawnObjects_2();
		NullCheck(L_23);
		int32_t L_24 = 0;
		GameObject_t1756533147 * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = GameObject_get_transform_m909382139(L_25, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_26);
		Vector3_t2243707580  L_27 = Transform_get_position_m1104419803(L_26, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_28 = __this->get_m_spawnObjects_2();
		NullCheck(L_28);
		int32_t L_29 = 0;
		GameObject_t1756533147 * L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = GameObject_get_transform_m909382139(L_30, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_31);
		Quaternion_t4030073918  L_32 = Transform_get_rotation_m1033555130(L_31, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_33 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_22, L_27, L_32, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_33);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_34 = __this->get_m_enemySpawn_3();
		NullCheck(L_34);
		int32_t L_35 = 1;
		GameObject_t1756533147 * L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		GameObjectU5BU5D_t3057952154* L_37 = __this->get_m_spawnObjects_2();
		NullCheck(L_37);
		int32_t L_38 = 1;
		GameObject_t1756533147 * L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_39);
		Transform_t3275118058 * L_40 = GameObject_get_transform_m909382139(L_39, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_40);
		Vector3_t2243707580  L_41 = Transform_get_position_m1104419803(L_40, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_42 = __this->get_m_spawnObjects_2();
		NullCheck(L_42);
		int32_t L_43 = 1;
		GameObject_t1756533147 * L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_44);
		Transform_t3275118058 * L_45 = GameObject_get_transform_m909382139(L_44, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_45);
		Quaternion_t4030073918  L_46 = Transform_get_rotation_m1033555130(L_45, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		GameObject_t1756533147 * L_47 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_36, L_41, L_46, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_47);
		goto IL_016a;
	}

IL_00fa:
	{
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_48 = __this->get_m_enemySpawn_3();
		NullCheck(L_48);
		int32_t L_49 = 0;
		GameObject_t1756533147 * L_50 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		GameObjectU5BU5D_t3057952154* L_51 = __this->get_m_spawnObjects_2();
		NullCheck(L_51);
		int32_t L_52 = 0;
		GameObject_t1756533147 * L_53 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_53);
		Transform_t3275118058 * L_54 = GameObject_get_transform_m909382139(L_53, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_54);
		Vector3_t2243707580  L_55 = Transform_get_position_m1104419803(L_54, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_56 = __this->get_m_spawnObjects_2();
		NullCheck(L_56);
		int32_t L_57 = 0;
		GameObject_t1756533147 * L_58 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_57));
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_58);
		Transform_t3275118058 * L_59 = GameObject_get_transform_m909382139(L_58, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		NullCheck(L_59);
		Quaternion_t4030073918  L_60 = Transform_get_rotation_m1033555130(L_59, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[0], m_spawnObjects[0].transform.position, m_spawnObjects[0].transform.rotation);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_61 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_50, L_55, L_60, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_61);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_62 = __this->get_m_enemySpawn_3();
		NullCheck(L_62);
		int32_t L_63 = 2;
		GameObject_t1756533147 * L_64 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		GameObjectU5BU5D_t3057952154* L_65 = __this->get_m_spawnObjects_2();
		NullCheck(L_65);
		int32_t L_66 = 2;
		GameObject_t1756533147 * L_67 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_67);
		Transform_t3275118058 * L_68 = GameObject_get_transform_m909382139(L_67, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_68);
		Vector3_t2243707580  L_69 = Transform_get_position_m1104419803(L_68, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_70 = __this->get_m_spawnObjects_2();
		NullCheck(L_70);
		int32_t L_71 = 2;
		GameObject_t1756533147 * L_72 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_72);
		Transform_t3275118058 * L_73 = GameObject_get_transform_m909382139(L_72, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_73);
		Quaternion_t4030073918  L_74 = Transform_get_rotation_m1033555130(L_73, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		GameObject_t1756533147 * L_75 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_64, L_69, L_74, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_75);
	}

IL_016a:
	{
		goto IL_02bf;
	}

IL_0170:
	{
		// if(mid)
		bool L_76 = __this->get_mid_7();
		if (!L_76)
		{
			goto IL_023c;
		}
	}
	{
		// if(!low)
		bool L_77 = __this->get_low_8();
		if (L_77)
		{
			goto IL_01c6;
		}
	}
	{
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_78 = __this->get_m_enemySpawn_3();
		NullCheck(L_78);
		int32_t L_79 = 1;
		GameObject_t1756533147 * L_80 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		GameObjectU5BU5D_t3057952154* L_81 = __this->get_m_spawnObjects_2();
		NullCheck(L_81);
		int32_t L_82 = 1;
		GameObject_t1756533147 * L_83 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_83);
		Transform_t3275118058 * L_84 = GameObject_get_transform_m909382139(L_83, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_84);
		Vector3_t2243707580  L_85 = Transform_get_position_m1104419803(L_84, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_86 = __this->get_m_spawnObjects_2();
		NullCheck(L_86);
		int32_t L_87 = 1;
		GameObject_t1756533147 * L_88 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_87));
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_88);
		Transform_t3275118058 * L_89 = GameObject_get_transform_m909382139(L_88, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_89);
		Quaternion_t4030073918  L_90 = Transform_get_rotation_m1033555130(L_89, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_91 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_80, L_85, L_90, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_91);
		goto IL_0236;
	}

IL_01c6:
	{
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_92 = __this->get_m_enemySpawn_3();
		NullCheck(L_92);
		int32_t L_93 = 1;
		GameObject_t1756533147 * L_94 = (L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		GameObjectU5BU5D_t3057952154* L_95 = __this->get_m_spawnObjects_2();
		NullCheck(L_95);
		int32_t L_96 = 1;
		GameObject_t1756533147 * L_97 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_97);
		Transform_t3275118058 * L_98 = GameObject_get_transform_m909382139(L_97, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_98);
		Vector3_t2243707580  L_99 = Transform_get_position_m1104419803(L_98, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_100 = __this->get_m_spawnObjects_2();
		NullCheck(L_100);
		int32_t L_101 = 1;
		GameObject_t1756533147 * L_102 = (L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_101));
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_102);
		Transform_t3275118058 * L_103 = GameObject_get_transform_m909382139(L_102, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		NullCheck(L_103);
		Quaternion_t4030073918  L_104 = Transform_get_rotation_m1033555130(L_103, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[1], m_spawnObjects[1].transform.position, m_spawnObjects[1].transform.rotation);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_105 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_94, L_99, L_104, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_105);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_106 = __this->get_m_enemySpawn_3();
		NullCheck(L_106);
		int32_t L_107 = 2;
		GameObject_t1756533147 * L_108 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_107));
		GameObjectU5BU5D_t3057952154* L_109 = __this->get_m_spawnObjects_2();
		NullCheck(L_109);
		int32_t L_110 = 2;
		GameObject_t1756533147 * L_111 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_111);
		Transform_t3275118058 * L_112 = GameObject_get_transform_m909382139(L_111, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_112);
		Vector3_t2243707580  L_113 = Transform_get_position_m1104419803(L_112, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_114 = __this->get_m_spawnObjects_2();
		NullCheck(L_114);
		int32_t L_115 = 2;
		GameObject_t1756533147 * L_116 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_115));
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_116);
		Transform_t3275118058 * L_117 = GameObject_get_transform_m909382139(L_116, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_117);
		Quaternion_t4030073918  L_118 = Transform_get_rotation_m1033555130(L_117, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		GameObject_t1756533147 * L_119 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_108, L_113, L_118, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_119);
	}

IL_0236:
	{
		goto IL_02be;
	}

IL_023c:
	{
		// else if(low)
		bool L_120 = __this->get_low_8();
		if (!L_120)
		{
			goto IL_0285;
		}
	}
	{
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_121 = __this->get_m_enemySpawn_3();
		NullCheck(L_121);
		int32_t L_122 = 2;
		GameObject_t1756533147 * L_123 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_122));
		GameObjectU5BU5D_t3057952154* L_124 = __this->get_m_spawnObjects_2();
		NullCheck(L_124);
		int32_t L_125 = 2;
		GameObject_t1756533147 * L_126 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_125));
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_126);
		Transform_t3275118058 * L_127 = GameObject_get_transform_m909382139(L_126, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_127);
		Vector3_t2243707580  L_128 = Transform_get_position_m1104419803(L_127, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_129 = __this->get_m_spawnObjects_2();
		NullCheck(L_129);
		int32_t L_130 = 2;
		GameObject_t1756533147 * L_131 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_130));
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_131);
		Transform_t3275118058 * L_132 = GameObject_get_transform_m909382139(L_131, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		NullCheck(L_132);
		Quaternion_t4030073918  L_133 = Transform_get_rotation_m1033555130(L_132, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[2], m_spawnObjects[2].transform.position, m_spawnObjects[2].transform.rotation);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_134 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_123, L_128, L_133, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_134);
		goto IL_02be;
	}

IL_0285:
	{
		// enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
		GameObjectU5BU5D_t3057952154* L_135 = __this->get_m_enemySpawn_3();
		NullCheck(L_135);
		int32_t L_136 = 3;
		GameObject_t1756533147 * L_137 = (L_135)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		GameObjectU5BU5D_t3057952154* L_138 = __this->get_m_spawnObjects_2();
		NullCheck(L_138);
		int32_t L_139 = 3;
		GameObject_t1756533147 * L_140 = (L_138)->GetAt(static_cast<il2cpp_array_size_t>(L_139));
		// enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
		NullCheck(L_140);
		Transform_t3275118058 * L_141 = GameObject_get_transform_m909382139(L_140, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
		NullCheck(L_141);
		Vector3_t2243707580  L_142 = Transform_get_position_m1104419803(L_141, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_143 = __this->get_m_spawnObjects_2();
		NullCheck(L_143);
		int32_t L_144 = 3;
		GameObject_t1756533147 * L_145 = (L_143)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		// enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
		NullCheck(L_145);
		Transform_t3275118058 * L_146 = GameObject_get_transform_m909382139(L_145, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
		NullCheck(L_146);
		Quaternion_t4030073918  L_147 = Transform_get_rotation_m1033555130(L_146, /*hidden argument*/NULL);
		// enemySpawn = Instantiate(m_enemySpawn[3], m_spawnObjects[3].transform.position, m_spawnObjects[3].transform.rotation);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_148 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_137, L_142, L_147, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		__this->set_enemySpawn_4(L_148);
	}

IL_02be:
	{
	}

IL_02bf:
	{
		// }
		return;
	}
}
// System.Void SpawnManager::OnOnSet(OnsetType,Onset)
extern "C"  void SpawnManager_OnOnSet_m454083277 (SpawnManager_t4269630218 * __this, int32_t ___type0, Onset_t732596509 * ___onSet1, const MethodInfo* method)
{
	{
		// if (type == OnsetType.High)
		int32_t L_0 = ___type0;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0016;
		}
	}
	{
		// high = true;
		__this->set_high_6((bool)1);
		goto IL_001f;
	}

IL_0016:
	{
		// high = false;
		__this->set_high_6((bool)0);
	}

IL_001f:
	{
		// if(type == OnsetType.Mid)
		int32_t L_1 = ___type0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0034;
		}
	}
	{
		// mid = true;
		__this->set_mid_7((bool)1);
		goto IL_003d;
	}

IL_0034:
	{
		// mid = false;
		__this->set_mid_7((bool)0);
	}

IL_003d:
	{
		// if(type == OnsetType.Low)
		int32_t L_2 = ___type0;
		if (L_2)
		{
			goto IL_0051;
		}
	}
	{
		// low = true;
		__this->set_low_8((bool)1);
		goto IL_005a;
	}

IL_0051:
	{
		// low = false;
		__this->set_low_8((bool)0);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void TileManager::.ctor()
extern "C"  void TileManager__ctor_m282279270 (TileManager_t3422405329 * __this, const MethodInfo* method)
{
	{
		// private float spawnZ = -10f;
		__this->set_spawnZ_4((-10.0f));
		// private float tileLength = 10.0f;
		__this->set_tileLength_5((10.0f));
		// private float safeZone = 15.0f;
		__this->set_safeZone_6((15.0f));
		// private int amnTilesOnScreen = 10;
		__this->set_amnTilesOnScreen_7(((int32_t)10));
		// private int lastPrefabIndex = 0;
		__this->set_lastPrefabIndex_8(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TileManager::Start()
extern "C"  void TileManager_Start_m2619986134 (TileManager_t3422405329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TileManager_Start_m2619986134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// activeTiles = new List<GameObject> ();
		// activeTiles = new List<GameObject> ();
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set_activeTiles_9(L_0);
		// playerTransform = GameObject.FindGameObjectWithTag ("Player").transform;
		// playerTransform = GameObject.FindGameObjectWithTag ("Player").transform;
		GameObject_t1756533147 * L_1 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		// playerTransform = GameObject.FindGameObjectWithTag ("Player").transform;
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		__this->set_playerTransform_3(L_2);
		// for (int i = 0; i < amnTilesOnScreen; i++)
		V_0 = 0;
		goto IL_0048;
	}

IL_0028:
	{
		// if (i < 3)
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)3)))
		{
			goto IL_003c;
		}
	}
	{
		// SpawnTile (0);
		// SpawnTile (0);
		TileManager_SpawnTile_m1752937928(__this, 0, /*hidden argument*/NULL);
		goto IL_0043;
	}

IL_003c:
	{
		// SpawnTile ();
		// SpawnTile ();
		TileManager_SpawnTile_m1752937928(__this, (-1), /*hidden argument*/NULL);
	}

IL_0043:
	{
		// for (int i = 0; i < amnTilesOnScreen; i++)
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0048:
	{
		// for (int i = 0; i < amnTilesOnScreen; i++)
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_amnTilesOnScreen_7();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0028;
		}
	}
	{
		// }
		return;
	}
}
// System.Void TileManager::Update()
extern "C"  void TileManager_Update_m2253796281 (TileManager_t3422405329 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// if (playerTransform.position.z - safeZone> (spawnZ - amnTilesOnScreen * tileLength))
		Transform_t3275118058 * L_0 = __this->get_playerTransform_3();
		// if (playerTransform.position.z - safeZone> (spawnZ - amnTilesOnScreen * tileLength))
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_3();
		float L_3 = __this->get_safeZone_6();
		float L_4 = __this->get_spawnZ_4();
		int32_t L_5 = __this->get_amnTilesOnScreen_7();
		float L_6 = __this->get_tileLength_5();
		if ((!(((float)((float)((float)L_2-(float)L_3))) > ((float)((float)((float)L_4-(float)((float)((float)(((float)((float)L_5)))*(float)L_6))))))))
		{
			goto IL_0044;
		}
	}
	{
		// SpawnTile ();
		// SpawnTile ();
		TileManager_SpawnTile_m1752937928(__this, (-1), /*hidden argument*/NULL);
		// DeleteTile ();
		// DeleteTile ();
		TileManager_DeleteTile_m3895137793(__this, /*hidden argument*/NULL);
	}

IL_0044:
	{
		// }
		return;
	}
}
// System.Void TileManager::SpawnTile(System.Int32)
extern "C"  void TileManager_SpawnTile_m1752937928 (TileManager_t3422405329 * __this, int32_t ___prefabIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TileManager_SpawnTile_m1752937928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		// if (prefabIndex == -1)
		int32_t L_0 = ___prefabIndex0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0020;
		}
	}
	{
		// go = Instantiate (tilePrefabs [RandomPrefabIndex ()]) as GameObject;
		GameObjectU5BU5D_t3057952154* L_1 = __this->get_tilePrefabs_2();
		// go = Instantiate (tilePrefabs [RandomPrefabIndex ()]) as GameObject;
		int32_t L_2 = TileManager_RandomPrefabIndex_m124292481(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		// go = Instantiate (tilePrefabs [RandomPrefabIndex ()]) as GameObject;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_5 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_0 = L_5;
		goto IL_002e;
	}

IL_0020:
	{
		// go = Instantiate (tilePrefabs [prefabIndex]) as GameObject;
		GameObjectU5BU5D_t3057952154* L_6 = __this->get_tilePrefabs_2();
		int32_t L_7 = ___prefabIndex0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		GameObject_t1756533147 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		// go = Instantiate (tilePrefabs [prefabIndex]) as GameObject;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_10 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_9, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_0 = L_10;
	}

IL_002e:
	{
		// go.transform.SetParent (transform);
		GameObject_t1756533147 * L_11 = V_0;
		// go.transform.SetParent (transform);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		// go.transform.SetParent (transform);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		// go.transform.SetParent (transform);
		NullCheck(L_12);
		Transform_SetParent_m4124909910(L_12, L_13, /*hidden argument*/NULL);
		// go.transform.position = Vector3.forward * spawnZ;
		GameObject_t1756533147 * L_14 = V_0;
		// go.transform.position = Vector3.forward * spawnZ;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		// go.transform.position = Vector3.forward * spawnZ;
		Vector3_t2243707580  L_16 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = __this->get_spawnZ_4();
		// go.transform.position = Vector3.forward * spawnZ;
		Vector3_t2243707580  L_18 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		// go.transform.position = Vector3.forward * spawnZ;
		NullCheck(L_15);
		Transform_set_position_m2469242620(L_15, L_18, /*hidden argument*/NULL);
		// spawnZ += tileLength;
		float L_19 = __this->get_spawnZ_4();
		float L_20 = __this->get_tileLength_5();
		__this->set_spawnZ_4(((float)((float)L_19+(float)L_20)));
		// activeTiles.Add (go);
		List_1_t1125654279 * L_21 = __this->get_activeTiles_9();
		GameObject_t1756533147 * L_22 = V_0;
		// activeTiles.Add (go);
		NullCheck(L_21);
		List_1_Add_m3441471442(L_21, L_22, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		// }
		return;
	}
}
// System.Void TileManager::DeleteTile()
extern "C"  void TileManager_DeleteTile_m3895137793 (TileManager_t3422405329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TileManager_DeleteTile_m3895137793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy (activeTiles [0]);
		List_1_t1125654279 * L_0 = __this->get_activeTiles_9();
		// Destroy (activeTiles [0]);
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = List_1_get_Item_m939767277(L_0, 0, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		// Destroy (activeTiles [0]);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// activeTiles.RemoveAt (0);
		List_1_t1125654279 * L_2 = __this->get_activeTiles_9();
		// activeTiles.RemoveAt (0);
		NullCheck(L_2);
		List_1_RemoveAt_m3404034275(L_2, 0, /*hidden argument*/List_1_RemoveAt_m3404034275_MethodInfo_var);
		// }
		return;
	}
}
// System.Int32 TileManager::RandomPrefabIndex()
extern "C"  int32_t TileManager_RandomPrefabIndex_m124292481 (TileManager_t3422405329 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// if (tilePrefabs.Length <= 1)
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_tilePrefabs_2();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) > ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		// return 0;
		V_0 = 0;
		goto IL_004d;
	}

IL_0016:
	{
		// int randomIndex = lastPrefabIndex;
		int32_t L_1 = __this->get_lastPrefabIndex_8();
		V_1 = L_1;
		// while (randomIndex == lastPrefabIndex) {
		goto IL_0033;
	}

IL_0022:
	{
		// randomIndex = Random.Range (0, tilePrefabs.Length);
		GameObjectU5BU5D_t3057952154* L_2 = __this->get_tilePrefabs_2();
		NullCheck(L_2);
		// randomIndex = Random.Range (0, tilePrefabs.Length);
		int32_t L_3 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_0033:
	{
		// while (randomIndex == lastPrefabIndex) {
		int32_t L_4 = V_1;
		int32_t L_5 = __this->get_lastPrefabIndex_8();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0022;
		}
	}
	{
		// lastPrefabIndex = randomIndex;
		int32_t L_6 = V_1;
		__this->set_lastPrefabIndex_8(L_6);
		// return randomIndex;
		int32_t L_7 = V_1;
		V_0 = L_7;
		goto IL_004d;
	}

IL_004d:
	{
		// }
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Void Tutorial_1_Controller::.ctor()
extern "C"  void Tutorial_1_Controller__ctor_m1600451750 (Tutorial_1_Controller_t3740746359 * __this, const MethodInfo* method)
{
	{
		// public GameObject enemyObj = null;
		__this->set_enemyObj_9((GameObject_t1756533147 *)NULL);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tutorial_1_Controller::Start()
extern "C"  void Tutorial_1_Controller_Start_m1243183122 (Tutorial_1_Controller_t3740746359 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tutorial_1_Controller_Start_m1243183122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// rhythmTool = GetComponent<RhythmTool>();
		// rhythmTool = GetComponent<RhythmTool>();
		RhythmTool_t215962618 * L_0 = Component_GetComponent_TisRhythmTool_t215962618_m966574361(__this, /*hidden argument*/Component_GetComponent_TisRhythmTool_t215962618_m966574361_MethodInfo_var);
		__this->set_rhythmTool_3(L_0);
		// low = rhythmTool.low;
		RhythmTool_t215962618 * L_1 = __this->get_rhythmTool_3();
		// low = rhythmTool.low;
		NullCheck(L_1);
		AnalysisData_t108342674 * L_2 = RhythmTool_get_low_m2159614661(L_1, /*hidden argument*/NULL);
		__this->set_low_5(L_2);
		// }
		return;
	}
}
// System.Void Tutorial_1_Controller::OnSongLoaded()
extern "C"  void Tutorial_1_Controller_OnSongLoaded_m3584566401 (Tutorial_1_Controller_t3740746359 * __this, const MethodInfo* method)
{
	{
		// }
		return;
	}
}
// System.Void Tutorial_1_Controller::Update()
extern "C"  void Tutorial_1_Controller_Update_m96272187 (Tutorial_1_Controller_t3740746359 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tutorial_1_Controller_Update_m96272187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		// if(!rhythmTool.songLoaded)
		RhythmTool_t215962618 * L_0 = __this->get_rhythmTool_3();
		// if(!rhythmTool.songLoaded)
		NullCheck(L_0);
		bool L_1 = RhythmTool_get_songLoaded_m3458667938(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		// return;
		goto IL_0186;
	}

IL_0017:
	{
		// for(int i = 0; i < 200; i++)
		V_0 = 0;
		goto IL_017b;
	}

IL_001e:
	{
		// int frameIndex = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames);
		RhythmTool_t215962618 * L_2 = __this->get_rhythmTool_3();
		// int frameIndex = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames);
		NullCheck(L_2);
		int32_t L_3 = RhythmTool_get_currentFrame_m867144816(L_2, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		RhythmTool_t215962618 * L_5 = __this->get_rhythmTool_3();
		// int frameIndex = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames);
		NullCheck(L_5);
		int32_t L_6 = RhythmTool_get_totalFrames_m68629944(L_5, /*hidden argument*/NULL);
		// int frameIndex = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_Min_m2906823867(NULL /*static, unused*/, ((int32_t)((int32_t)L_3+(int32_t)L_4)), L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		// startPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex], i);
		float L_8 = __this->get_debugLineOffset_2();
		AnalysisData_t108342674 * L_9 = __this->get_low_5();
		// startPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex], i);
		NullCheck(L_9);
		ReadOnlyCollection_1_t2262295624 * L_10 = AnalysisData_get_magnitude_m671389901(L_9, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		// startPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex], i);
		NullCheck(L_10);
		float L_12 = ReadOnlyCollection_1_get_Item_m210738684(L_10, L_11, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		int32_t L_13 = V_0;
		// startPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex], i);
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, L_8, L_12, (((float)((float)L_13))), /*hidden argument*/NULL);
		__this->set_startPosition_6(L_14);
		// endPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex + 1], i+1);
		float L_15 = __this->get_debugLineOffset_2();
		AnalysisData_t108342674 * L_16 = __this->get_low_5();
		// endPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex + 1], i+1);
		NullCheck(L_16);
		ReadOnlyCollection_1_t2262295624 * L_17 = AnalysisData_get_magnitude_m671389901(L_16, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		// endPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex + 1], i+1);
		NullCheck(L_17);
		float L_19 = ReadOnlyCollection_1_get_Item_m210738684(L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		int32_t L_20 = V_0;
		// endPosition = new Vector3(debugLineOffset, low.magnitude[frameIndex + 1], i+1);
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322(&L_21, L_15, L_19, (((float)((float)((int32_t)((int32_t)L_20+(int32_t)1))))), /*hidden argument*/NULL);
		__this->set_endPosition_7(L_21);
		// Debug.DrawLine(startPosition, endPosition, Color.black);
		Vector3_t2243707580  L_22 = __this->get_startPosition_6();
		Vector3_t2243707580  L_23 = __this->get_endPosition_7();
		// Debug.DrawLine(startPosition, endPosition, Color.black);
		Color_t2020392075  L_24 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(startPosition, endPosition, Color.black);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		// float onset = low.GetOnset(frameIndex);
		AnalysisData_t108342674 * L_25 = __this->get_low_5();
		int32_t L_26 = V_1;
		// float onset = low.GetOnset(frameIndex);
		NullCheck(L_25);
		Onset_t732596509 * L_27 = AnalysisData_GetOnset_m643039293(L_25, L_26, /*hidden argument*/NULL);
		// float onset = low.GetOnset(frameIndex);
		float L_28 = Onset_op_Implicit_m4069753098(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		// if (rhythmTool.IsBeat(frameIndex) == 1)
		RhythmTool_t215962618 * L_29 = __this->get_rhythmTool_3();
		int32_t L_30 = V_1;
		// if (rhythmTool.IsBeat(frameIndex) == 1)
		NullCheck(L_29);
		int32_t L_31 = RhythmTool_IsBeat_m3576622446(L_29, L_30, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_31) == ((uint32_t)1))))
		{
			goto IL_010d;
		}
	}
	{
		// startPosition = new Vector3(debugLineOffset, 0, i);
		float L_32 = __this->get_debugLineOffset_2();
		int32_t L_33 = V_0;
		// startPosition = new Vector3(debugLineOffset, 0, i);
		Vector3_t2243707580  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector3__ctor_m2638739322(&L_34, L_32, (0.0f), (((float)((float)L_33))), /*hidden argument*/NULL);
		__this->set_startPosition_6(L_34);
		// endPosition = new Vector3(debugLineOffset, 10, i+1);
		float L_35 = __this->get_debugLineOffset_2();
		int32_t L_36 = V_0;
		// endPosition = new Vector3(debugLineOffset, 10, i+1);
		Vector3_t2243707580  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector3__ctor_m2638739322(&L_37, L_35, (10.0f), (((float)((float)((int32_t)((int32_t)L_36+(int32_t)1))))), /*hidden argument*/NULL);
		__this->set_endPosition_7(L_37);
		// Debug.DrawLine(startPosition, endPosition, Color.white);
		Vector3_t2243707580  L_38 = __this->get_startPosition_6();
		Vector3_t2243707580  L_39 = __this->get_endPosition_7();
		// Debug.DrawLine(startPosition, endPosition, Color.white);
		Color_t2020392075  L_40 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(startPosition, endPosition, Color.white);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
	}

IL_010d:
	{
		// if (rhythmTool.IsChange(frameIndex))
		RhythmTool_t215962618 * L_41 = __this->get_rhythmTool_3();
		int32_t L_42 = V_1;
		// if (rhythmTool.IsChange(frameIndex))
		NullCheck(L_41);
		bool L_43 = RhythmTool_IsChange_m1817224686(L_41, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0176;
		}
	}
	{
		// float change = rhythmTool.NextChange(frameIndex);
		RhythmTool_t215962618 * L_44 = __this->get_rhythmTool_3();
		int32_t L_45 = V_1;
		// float change = rhythmTool.NextChange(frameIndex);
		NullCheck(L_44);
		float L_46 = RhythmTool_NextChange_m3596269439(L_44, L_45, /*hidden argument*/NULL);
		V_3 = L_46;
		// startPosition = new Vector3(debugLineOffset, 0, i);
		float L_47 = __this->get_debugLineOffset_2();
		int32_t L_48 = V_0;
		// startPosition = new Vector3(debugLineOffset, 0, i);
		Vector3_t2243707580  L_49;
		memset(&L_49, 0, sizeof(L_49));
		Vector3__ctor_m2638739322(&L_49, L_47, (0.0f), (((float)((float)L_48))), /*hidden argument*/NULL);
		__this->set_startPosition_6(L_49);
		// endPosition = new Vector3(debugLineOffset, Mathf.Abs(change),i+1);
		float L_50 = __this->get_debugLineOffset_2();
		float L_51 = V_3;
		// endPosition = new Vector3(debugLineOffset, Mathf.Abs(change),i+1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_52 = fabsf(L_51);
		int32_t L_53 = V_0;
		// endPosition = new Vector3(debugLineOffset, Mathf.Abs(change),i+1);
		Vector3_t2243707580  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Vector3__ctor_m2638739322(&L_54, L_50, L_52, (((float)((float)((int32_t)((int32_t)L_53+(int32_t)1))))), /*hidden argument*/NULL);
		__this->set_endPosition_7(L_54);
		// Debug.DrawLine(startPosition, endPosition, Color.yellow);
		Vector3_t2243707580  L_55 = __this->get_startPosition_6();
		Vector3_t2243707580  L_56 = __this->get_endPosition_7();
		// Debug.DrawLine(startPosition, endPosition, Color.yellow);
		Color_t2020392075  L_57 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Debug.DrawLine(startPosition, endPosition, Color.yellow);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m3455422326(NULL /*static, unused*/, L_55, L_56, L_57, /*hidden argument*/NULL);
	}

IL_0176:
	{
		// for(int i = 0; i < 200; i++)
		int32_t L_58 = V_0;
		V_0 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_017b:
	{
		// for(int i = 0; i < 200; i++)
		int32_t L_59 = V_0;
		if ((((int32_t)L_59) < ((int32_t)((int32_t)200))))
		{
			goto IL_001e;
		}
	}

IL_0186:
	{
		// }
		return;
	}
}
// System.Void Util::GetSpectrum(System.Single[])
extern "C"  void Util_GetSpectrum_m1376041373 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___samples0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_GetSpectrum_m1376041373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// fft.RealFFT(samples, true);
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		LomontFFT_t895069557 * L_0 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_fft_0();
		SingleU5BU5D_t577127397* L_1 = ___samples0;
		// fft.RealFFT(samples, true);
		NullCheck(L_0);
		LomontFFT_RealFFT_m1443477460(L_0, L_1, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Single[] Util::GetSpectrumMagnitude(System.Single[])
extern "C"  SingleU5BU5D_t577127397* Util_GetSpectrumMagnitude_m1549234393 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___spectrum0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_GetSpectrumMagnitude_m1549234393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		// if (magnitude.Length != spectrum.Length / 2)
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		SingleU5BU5D_t577127397* L_0 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_magnitude_1();
		NullCheck(L_0);
		SingleU5BU5D_t577127397* L_1 = ___spectrum0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))/(int32_t)2)))))
		{
			goto IL_0021;
		}
	}
	{
		// magnitude = new float[spectrum.Length / 2];
		SingleU5BU5D_t577127397* L_2 = ___spectrum0;
		NullCheck(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->set_magnitude_1(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))/(int32_t)2)))));
	}

IL_0021:
	{
		// GetSpectrumMagnitude(spectrum, magnitude);
		SingleU5BU5D_t577127397* L_3 = ___spectrum0;
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		SingleU5BU5D_t577127397* L_4 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_magnitude_1();
		// GetSpectrumMagnitude(spectrum, magnitude);
		Util_GetSpectrumMagnitude_m3600392938(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		// return magnitude;
		SingleU5BU5D_t577127397* L_5 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_magnitude_1();
		V_0 = L_5;
		goto IL_0037;
	}

IL_0037:
	{
		// }
		SingleU5BU5D_t577127397* L_6 = V_0;
		return L_6;
	}
}
// System.Void Util::GetSpectrumMagnitude(System.Single[],System.Single[])
extern "C"  void Util_GetSpectrumMagnitude_m3600392938 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___spectrum0, SingleU5BU5D_t577127397* ___spectrumMagnitude1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_GetSpectrumMagnitude_m3600392938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		// if (spectrumMagnitude.Length != spectrum.Length / 2)
		SingleU5BU5D_t577127397* L_0 = ___spectrumMagnitude1;
		NullCheck(L_0);
		SingleU5BU5D_t577127397* L_1 = ___spectrum0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))/(int32_t)2)))))
		{
			goto IL_0019;
		}
	}
	{
		// throw new Exception("SpectrumMagnitude length has to be half of spectrum length.");
		// throw new Exception("SpectrumMagnitude length has to be half of spectrum length.");
		Exception_t1927440687 * L_2 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_2, _stringLiteral408410324, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		// for (int i = 0; i < spectrumMagnitude.Length-2; i++)
		V_0 = 0;
		goto IL_0045;
	}

IL_0020:
	{
		// int ii = (i * 2) + 2;
		int32_t L_3 = V_0;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_3*(int32_t)2))+(int32_t)2));
		// float re = spectrum[ii];
		SingleU5BU5D_t577127397* L_4 = ___spectrum0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		float L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		// float im = spectrum[ii + 1];
		SingleU5BU5D_t577127397* L_8 = ___spectrum0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)L_9+(int32_t)1));
		float L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		// spectrumMagnitude[i] = Mathf.Sqrt((re * re) + (im * im));
		SingleU5BU5D_t577127397* L_12 = ___spectrumMagnitude1;
		int32_t L_13 = V_0;
		float L_14 = V_2;
		float L_15 = V_2;
		float L_16 = V_3;
		float L_17 = V_3;
		// spectrumMagnitude[i] = Mathf.Sqrt((re * re) + (im * im));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_18 = sqrtf(((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17)))));
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (float)L_18);
		// for (int i = 0; i < spectrumMagnitude.Length-2; i++)
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0045:
	{
		// for (int i = 0; i < spectrumMagnitude.Length-2; i++)
		int32_t L_20 = V_0;
		SingleU5BU5D_t577127397* L_21 = ___spectrumMagnitude1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length))))-(int32_t)2)))))
		{
			goto IL_0020;
		}
	}
	{
		// spectrumMagnitude[spectrumMagnitude.Length - 2] = spectrum[0];
		SingleU5BU5D_t577127397* L_22 = ___spectrumMagnitude1;
		SingleU5BU5D_t577127397* L_23 = ___spectrumMagnitude1;
		NullCheck(L_23);
		SingleU5BU5D_t577127397* L_24 = ___spectrum0;
		NullCheck(L_24);
		int32_t L_25 = 0;
		float L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))))-(int32_t)2))), (float)L_26);
		// spectrumMagnitude[spectrumMagnitude.Length - 1] = spectrum[1];
		SingleU5BU5D_t577127397* L_27 = ___spectrumMagnitude1;
		SingleU5BU5D_t577127397* L_28 = ___spectrumMagnitude1;
		NullCheck(L_28);
		SingleU5BU5D_t577127397* L_29 = ___spectrum0;
		NullCheck(L_29);
		int32_t L_30 = 1;
		float L_31 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length))))-(int32_t)1))), (float)L_31);
		// }
		return;
	}
}
// System.Single[] Util::GetMono(System.Single[],System.Int32)
extern "C"  SingleU5BU5D_t577127397* Util_GetMono_m2743771466 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___samples0, int32_t ___channels1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_GetMono_m2743771466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		// if (mono.Length != samples.Length / channels)
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		SingleU5BU5D_t577127397* L_0 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_mono_3();
		NullCheck(L_0);
		SingleU5BU5D_t577127397* L_1 = ___samples0;
		NullCheck(L_1);
		int32_t L_2 = ___channels1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))/(int32_t)L_2)))))
		{
			goto IL_0021;
		}
	}
	{
		// mono = new float[samples.Length / channels];
		SingleU5BU5D_t577127397* L_3 = ___samples0;
		NullCheck(L_3);
		int32_t L_4 = ___channels1;
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->set_mono_3(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))/(int32_t)L_4)))));
	}

IL_0021:
	{
		// GetMono(samples, mono, channels);
		SingleU5BU5D_t577127397* L_5 = ___samples0;
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		SingleU5BU5D_t577127397* L_6 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_mono_3();
		int32_t L_7 = ___channels1;
		// GetMono(samples, mono, channels);
		Util_GetMono_m2664450603(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		// return mono;
		SingleU5BU5D_t577127397* L_8 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_mono_3();
		V_0 = L_8;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		SingleU5BU5D_t577127397* L_9 = V_0;
		return L_9;
	}
}
// System.Void Util::GetMono(System.Single[],System.Single[],System.Int32)
extern "C"  void Util_GetMono_m2664450603 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___samples0, SingleU5BU5D_t577127397* ___mono1, int32_t ___channels2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_GetMono_m2664450603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		// if(channels == 0)
		int32_t L_0 = ___channels2;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		// channels = samples.Length / mono.Length;
		SingleU5BU5D_t577127397* L_1 = ___samples0;
		NullCheck(L_1);
		SingleU5BU5D_t577127397* L_2 = ___mono1;
		NullCheck(L_2);
		___channels2 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))/(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
	}

IL_0010:
	{
		// if (samples.Length % mono.Length != 0)
		SingleU5BU5D_t577127397* L_3 = ___samples0;
		NullCheck(L_3);
		SingleU5BU5D_t577127397* L_4 = ___mono1;
		NullCheck(L_4);
		if (!((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		// throw new ArgumentException("Sample length is not a multiple of mono length.");
		// throw new ArgumentException("Sample length is not a multiple of mono length.");
		ArgumentException_t3259014390 * L_5 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_5, _stringLiteral1531776908, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0027:
	{
		// if(mono.Length * channels != samples.Length)
		SingleU5BU5D_t577127397* L_6 = ___mono1;
		NullCheck(L_6);
		int32_t L_7 = ___channels2;
		SingleU5BU5D_t577127397* L_8 = ___samples0;
		NullCheck(L_8);
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))*(int32_t)L_7))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		// throw new ArgumentException("Mono length does not match samples length for " + channels + " channels");
		int32_t L_9 = ___channels2;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral2777144342, L_11, _stringLiteral2157815886, /*hidden argument*/NULL);
		// throw new ArgumentException("Mono length does not match samples length for " + channels + " channels");
		ArgumentException_t3259014390 * L_13 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_13, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_004f:
	{
		// for (int i = 0; i < mono.Length; i++)
		V_0 = 0;
		goto IL_008d;
	}

IL_0056:
	{
		// float mean = 0;
		V_1 = (0.0f);
		// for (int ii = 0; ii < channels; ii++)
		V_2 = 0;
		goto IL_0072;
	}

IL_0064:
	{
		// mean += samples[i * channels + ii];
		float L_14 = V_1;
		SingleU5BU5D_t577127397* L_15 = ___samples0;
		int32_t L_16 = V_0;
		int32_t L_17 = ___channels2;
		int32_t L_18 = V_2;
		NullCheck(L_15);
		int32_t L_19 = ((int32_t)((int32_t)((int32_t)((int32_t)L_16*(int32_t)L_17))+(int32_t)L_18));
		float L_20 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_1 = ((float)((float)L_14+(float)L_20));
		// for (int ii = 0; ii < channels; ii++)
		int32_t L_21 = V_2;
		V_2 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0072:
	{
		// for (int ii = 0; ii < channels; ii++)
		int32_t L_22 = V_2;
		int32_t L_23 = ___channels2;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0064;
		}
	}
	{
		// mean /= channels;
		float L_24 = V_1;
		int32_t L_25 = ___channels2;
		V_1 = ((float)((float)L_24/(float)(((float)((float)L_25)))));
		// mono[i] = mean * 1.4f;
		SingleU5BU5D_t577127397* L_26 = ___mono1;
		int32_t L_27 = V_0;
		float L_28 = V_1;
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (float)((float)((float)L_28*(float)(1.4f))));
		// for (int i = 0; i < mono.Length; i++)
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_008d:
	{
		// for (int i = 0; i < mono.Length; i++)
		int32_t L_30 = V_0;
		SingleU5BU5D_t577127397* L_31 = ___mono1;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_0056;
		}
	}
	{
		// }
		return;
	}
}
// System.Single Util::Sum(System.Single[],System.Int32,System.Int32)
extern "C"  float Util_Sum_m3399145285 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___input0, int32_t ___start1, int32_t ___end2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		// float output = 0;
		V_0 = (0.0f);
		// for (int i = start; i < end; i++)
		int32_t L_0 = ___start1;
		V_1 = L_0;
		goto IL_001a;
	}

IL_000e:
	{
		// output += input[i];
		float L_1 = V_0;
		SingleU5BU5D_t577127397* L_2 = ___input0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		float L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = ((float)((float)L_1+(float)L_5));
		// for (int i = start; i < end; i++)
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001a:
	{
		// for (int i = start; i < end; i++)
		int32_t L_7 = V_1;
		int32_t L_8 = ___end2;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_000e;
		}
	}
	{
		// return output;
		float L_9 = V_0;
		V_2 = L_9;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		float L_10 = V_2;
		return L_10;
	}
}
// System.Void Util::Smooth(System.Single[],System.Int32)
extern "C"  void Util_Smooth_m3641735451 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___signal0, int32_t ___windowSize1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		// for (int i = 0; i < signal.Length; i++)
		V_0 = 0;
		goto IL_004d;
	}

IL_0008:
	{
		// float average = 0;
		V_1 = (0.0f);
		// for (int ii = i - (windowSize / 2); ii < i + (windowSize / 2); ii++)
		int32_t L_0 = V_0;
		int32_t L_1 = ___windowSize1;
		V_2 = ((int32_t)((int32_t)L_0-(int32_t)((int32_t)((int32_t)L_1/(int32_t)2))));
		goto IL_0036;
	}

IL_001a:
	{
		// if (ii > 0 && ii < signal.Length)
		int32_t L_2 = V_2;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_3 = V_2;
		SingleU5BU5D_t577127397* L_4 = ___signal0;
		NullCheck(L_4);
		if ((((int32_t)L_3) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		// average += signal[ii];
		float L_5 = V_1;
		SingleU5BU5D_t577127397* L_6 = ___signal0;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		float L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = ((float)((float)L_5+(float)L_9));
	}

IL_0031:
	{
		// for (int ii = i - (windowSize / 2); ii < i + (windowSize / 2); ii++)
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0036:
	{
		// for (int ii = i - (windowSize / 2); ii < i + (windowSize / 2); ii++)
		int32_t L_11 = V_2;
		int32_t L_12 = V_0;
		int32_t L_13 = ___windowSize1;
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)L_12+(int32_t)((int32_t)((int32_t)L_13/(int32_t)2)))))))
		{
			goto IL_001a;
		}
	}
	{
		// signal[i] = average / windowSize;
		SingleU5BU5D_t577127397* L_14 = ___signal0;
		int32_t L_15 = V_0;
		float L_16 = V_1;
		int32_t L_17 = ___windowSize1;
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (float)((float)((float)L_16/(float)(((float)((float)L_17))))));
		// for (int i = 0; i < signal.Length; i++)
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_004d:
	{
		// for (int i = 0; i < signal.Length; i++)
		int32_t L_19 = V_0;
		SingleU5BU5D_t577127397* L_20 = ___signal0;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0008;
		}
	}
	{
		// }
		return;
	}
}
// System.Single[] Util::UpsampleSingnal(System.Single[],System.Int32)
extern "C"  SingleU5BU5D_t577127397* Util_UpsampleSingnal_m120385462 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___signal0, int32_t ___multiplier1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_UpsampleSingnal_m120385462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		// if (upsampledSignal.Length != signal.Length * multiplier)
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		SingleU5BU5D_t577127397* L_0 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_upsampledSignal_2();
		NullCheck(L_0);
		SingleU5BU5D_t577127397* L_1 = ___signal0;
		NullCheck(L_1);
		int32_t L_2 = ___multiplier1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))*(int32_t)L_2)))))
		{
			goto IL_0021;
		}
	}
	{
		// upsampledSignal = new float[signal.Length * multiplier];
		SingleU5BU5D_t577127397* L_3 = ___signal0;
		NullCheck(L_3);
		int32_t L_4 = ___multiplier1;
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->set_upsampledSignal_2(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))*(int32_t)L_4)))));
	}

IL_0021:
	{
		// UpsampleSingnal(signal, upsampledSignal, multiplier);
		SingleU5BU5D_t577127397* L_5 = ___signal0;
		IL2CPP_RUNTIME_CLASS_INIT(Util_t4006552276_il2cpp_TypeInfo_var);
		SingleU5BU5D_t577127397* L_6 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_upsampledSignal_2();
		int32_t L_7 = ___multiplier1;
		// UpsampleSingnal(signal, upsampledSignal, multiplier);
		Util_UpsampleSingnal_m3064131617(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		// return upsampledSignal;
		SingleU5BU5D_t577127397* L_8 = ((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->get_upsampledSignal_2();
		V_0 = L_8;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		SingleU5BU5D_t577127397* L_9 = V_0;
		return L_9;
	}
}
// System.Void Util::UpsampleSingnal(System.Single[],System.Single[],System.Int32)
extern "C"  void Util_UpsampleSingnal_m3064131617 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___signal0, SingleU5BU5D_t577127397* ___upsampledSignal1, int32_t ___multiplier2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_UpsampleSingnal_m3064131617_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		// if (upsampledSignal.Length != signal.Length * multiplier)
		SingleU5BU5D_t577127397* L_0 = ___upsampledSignal1;
		NullCheck(L_0);
		SingleU5BU5D_t577127397* L_1 = ___signal0;
		NullCheck(L_1);
		int32_t L_2 = ___multiplier2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))*(int32_t)L_2)))))
		{
			goto IL_0019;
		}
	}
	{
		// throw new ArgumentException("UpsampledSignal does not match signal length and multiplier");
		// throw new ArgumentException("UpsampledSignal does not match signal length and multiplier");
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, _stringLiteral2872387472, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0019:
	{
		// for (int i = 0; i < signal.Length - 1; i++)
		V_0 = 0;
		goto IL_0055;
	}

IL_0020:
	{
		// for (int ii = 0; ii < multiplier; ii++)
		V_1 = 0;
		goto IL_0049;
	}

IL_0028:
	{
		// float f = (float)ii / (float)multiplier;
		int32_t L_4 = V_1;
		int32_t L_5 = ___multiplier2;
		V_2 = ((float)((float)(((float)((float)L_4)))/(float)(((float)((float)L_5)))));
		// upsampledSignal[(i * multiplier) + ii] = Mathf.Lerp(signal[i], signal[i + 1], f);
		SingleU5BU5D_t577127397* L_6 = ___upsampledSignal1;
		int32_t L_7 = V_0;
		int32_t L_8 = ___multiplier2;
		int32_t L_9 = V_1;
		SingleU5BU5D_t577127397* L_10 = ___signal0;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		float L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		SingleU5BU5D_t577127397* L_14 = ___signal0;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)((int32_t)L_15+(int32_t)1));
		float L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		float L_18 = V_2;
		// upsampledSignal[(i * multiplier) + ii] = Mathf.Lerp(signal[i], signal[i + 1], f);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_19 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_13, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_7*(int32_t)L_8))+(int32_t)L_9))), (float)L_19);
		// for (int ii = 0; ii < multiplier; ii++)
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0049:
	{
		// for (int ii = 0; ii < multiplier; ii++)
		int32_t L_21 = V_1;
		int32_t L_22 = ___multiplier2;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0028;
		}
	}
	{
		// for (int i = 0; i < signal.Length - 1; i++)
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0055:
	{
		// for (int i = 0; i < signal.Length - 1; i++)
		int32_t L_24 = V_0;
		SingleU5BU5D_t577127397* L_25 = ___signal0;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length))))-(int32_t)1)))))
		{
			goto IL_0020;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Util::.cctor()
extern "C"  void Util__cctor_m993639886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util__cctor_m993639886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static LomontFFT fft = new LomontFFT();
		LomontFFT_t895069557 * L_0 = (LomontFFT_t895069557 *)il2cpp_codegen_object_new(LomontFFT_t895069557_il2cpp_TypeInfo_var);
		LomontFFT__ctor_m1683549620(L_0, /*hidden argument*/NULL);
		((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->set_fft_0(L_0);
		// private static float[] magnitude = new float[0];
		((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->set_magnitude_1(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)0)));
		// private static float[] upsampledSignal = new float[0];
		((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->set_upsampledSignal_2(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)0)));
		// private static float[] mono = new float[0];
		((Util_t4006552276_StaticFields*)Util_t4006552276_il2cpp_TypeInfo_var->static_fields)->set_mono_3(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}
}
// System.Void VisualizerController::.ctor()
extern "C"  void VisualizerController__ctor_m2973258255 (VisualizerController_t119580760 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VisualizerController::Start()
extern "C"  void VisualizerController_Start_m125187779 (VisualizerController_t119580760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_Start_m125187779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// currentSong = -1;
		__this->set_currentSong_8((-1));
		// Application.runInBackground = true;
		// Application.runInBackground = true;
		Application_set_runInBackground_m3543179741(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		// lines = new List<Line>();
		// lines = new List<Line>();
		List_1_t2098562634 * L_0 = (List_1_t2098562634 *)il2cpp_codegen_object_new(List_1_t2098562634_il2cpp_TypeInfo_var);
		List_1__ctor_m2026045323(L_0, /*hidden argument*/List_1__ctor_m2026045323_MethodInfo_var);
		__this->set_lines_7(L_0);
		// eventProvider.onBeat.AddListener(OnBeat);
		RhythmEventProvider_t215006757 * L_1 = __this->get_eventProvider_3();
		NullCheck(L_1);
		BeatEvent_t33541086 * L_2 = L_1->get_onBeat_9();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)VisualizerController_OnBeat_m464411654_MethodInfo_var);
		UnityAction_1_t4062269323 * L_4 = (UnityAction_1_t4062269323 *)il2cpp_codegen_object_new(UnityAction_1_t4062269323_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3300750889(L_4, __this, L_3, /*hidden argument*/UnityAction_1__ctor_m3300750889_MethodInfo_var);
		// eventProvider.onBeat.AddListener(OnBeat);
		NullCheck(L_2);
		RhythmEvent_1_AddListener_m1356841763(L_2, L_4, /*hidden argument*/RhythmEvent_1_AddListener_m1356841763_MethodInfo_var);
		// eventProvider.onChange.AddListener(OnChange);
		RhythmEventProvider_t215006757 * L_5 = __this->get_eventProvider_3();
		NullCheck(L_5);
		ChangeEvent_t1135638418 * L_6 = L_5->get_onChange_12();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)VisualizerController_OnChange_m2842847708_MethodInfo_var);
		UnityAction_2_t2587731426 * L_8 = (UnityAction_2_t2587731426 *)il2cpp_codegen_object_new(UnityAction_2_t2587731426_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m4231767244(L_8, __this, L_7, /*hidden argument*/UnityAction_2__ctor_m4231767244_MethodInfo_var);
		// eventProvider.onChange.AddListener(OnChange);
		NullCheck(L_6);
		RhythmEvent_2_AddListener_m2211829322(L_6, L_8, /*hidden argument*/RhythmEvent_2_AddListener_m2211829322_MethodInfo_var);
		// eventProvider.onSongLoaded.AddListener(OnSongLoaded);
		RhythmEventProvider_t215006757 * L_9 = __this->get_eventProvider_3();
		NullCheck(L_9);
		OnNewSong_t4169165798 * L_10 = L_9->get_onSongLoaded_15();
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)VisualizerController_OnSongLoaded_m1911486583_MethodInfo_var);
		UnityAction_2_t1195085273 * L_12 = (UnityAction_2_t1195085273 *)il2cpp_codegen_object_new(UnityAction_2_t1195085273_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m3874320903(L_12, __this, L_11, /*hidden argument*/UnityAction_2__ctor_m3874320903_MethodInfo_var);
		// eventProvider.onSongLoaded.AddListener(OnSongLoaded);
		NullCheck(L_10);
		RhythmEvent_2_AddListener_m3749635587(L_10, L_12, /*hidden argument*/RhythmEvent_2_AddListener_m3749635587_MethodInfo_var);
		// eventProvider.onSongEnded.AddListener(OnSongEnded);
		RhythmEventProvider_t215006757 * L_13 = __this->get_eventProvider_3();
		NullCheck(L_13);
		UnityEvent_t408735097 * L_14 = L_13->get_onSongEnded_16();
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)VisualizerController_OnSongEnded_m337983393_MethodInfo_var);
		UnityAction_t4025899511 * L_16 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_16, __this, L_15, /*hidden argument*/NULL);
		// eventProvider.onSongEnded.AddListener(OnSongEnded);
		NullCheck(L_14);
		UnityEvent_AddListener_m1596810379(L_14, L_16, /*hidden argument*/NULL);
		// magnitudeSmooth = rhythmTool.low.magnitudeSmooth;
		RhythmTool_t215962618 * L_17 = __this->get_rhythmTool_2();
		// magnitudeSmooth = rhythmTool.low.magnitudeSmooth;
		NullCheck(L_17);
		AnalysisData_t108342674 * L_18 = RhythmTool_get_low_m2159614661(L_17, /*hidden argument*/NULL);
		// magnitudeSmooth = rhythmTool.low.magnitudeSmooth;
		NullCheck(L_18);
		ReadOnlyCollection_1_t2262295624 * L_19 = AnalysisData_get_magnitudeSmooth_m1926036311(L_18, /*hidden argument*/NULL);
		__this->set_magnitudeSmooth_9(L_19);
		// if (audioClips.Count <= 0)
		List_1_t1301679762 * L_20 = __this->get_audioClips_6();
		// if (audioClips.Count <= 0)
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m3361499332(L_20, /*hidden argument*/List_1_get_Count_m3361499332_MethodInfo_var);
		if ((((int32_t)L_21) > ((int32_t)0)))
		{
			goto IL_00bf;
		}
	}
	{
		// Debug.LogWarning ("no songs configured");
		// Debug.LogWarning ("no songs configured");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2419708395, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00bf:
	{
		// NextSong();
		// NextSong();
		VisualizerController_NextSong_m1666774829(__this, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		// }
		return;
	}
}
// System.Void VisualizerController::OnSongLoaded(System.String,System.Int32)
extern "C"  void VisualizerController_OnSongLoaded_m1911486583 (VisualizerController_t119580760 * __this, String_t* ___name0, int32_t ___totalFrames1, const MethodInfo* method)
{
	{
		// rhythmTool.Play ();
		RhythmTool_t215962618 * L_0 = __this->get_rhythmTool_2();
		// rhythmTool.Play ();
		NullCheck(L_0);
		RhythmTool_Play_m2237616365(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VisualizerController::OnSongEnded()
extern "C"  void VisualizerController_OnSongEnded_m337983393 (VisualizerController_t119580760 * __this, const MethodInfo* method)
{
	{
		// NextSong();
		// NextSong();
		VisualizerController_NextSong_m1666774829(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VisualizerController::NextSong()
extern "C"  void VisualizerController_NextSong_m1666774829 (VisualizerController_t119580760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_NextSong_m1666774829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ClearLines ();
		// ClearLines ();
		VisualizerController_ClearLines_m3213124107(__this, /*hidden argument*/NULL);
		// currentSong++;
		int32_t L_0 = __this->get_currentSong_8();
		__this->set_currentSong_8(((int32_t)((int32_t)L_0+(int32_t)1)));
		// if (currentSong >= audioClips.Count)
		int32_t L_1 = __this->get_currentSong_8();
		List_1_t1301679762 * L_2 = __this->get_audioClips_6();
		// if (currentSong >= audioClips.Count)
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m3361499332(L_2, /*hidden argument*/List_1_get_Count_m3361499332_MethodInfo_var);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0032;
		}
	}
	{
		// currentSong = 0;
		__this->set_currentSong_8(0);
	}

IL_0032:
	{
		// rhythmTool.NewSong (audioClips [currentSong]);
		RhythmTool_t215962618 * L_4 = __this->get_rhythmTool_2();
		List_1_t1301679762 * L_5 = __this->get_audioClips_6();
		int32_t L_6 = __this->get_currentSong_8();
		// rhythmTool.NewSong (audioClips [currentSong]);
		NullCheck(L_5);
		AudioClip_t1932558630 * L_7 = List_1_get_Item_m3879122397(L_5, L_6, /*hidden argument*/List_1_get_Item_m3879122397_MethodInfo_var);
		// rhythmTool.NewSong (audioClips [currentSong]);
		NullCheck(L_4);
		RhythmTool_NewSong_m2509631375(L_4, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VisualizerController::Update()
extern "C"  void VisualizerController_Update_m2253024630 (VisualizerController_t119580760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_Update_m2253024630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if (Input.GetKeyDown (KeyCode.Space)) {
		// if (Input.GetKeyDown (KeyCode.Space)) {
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		// NextSong ();
		// NextSong ();
		VisualizerController_NextSong_m1666774829(__this, /*hidden argument*/NULL);
		// return;
		goto IL_0077;
	}

IL_0019:
	{
		// if (Input.GetKey (KeyCode.Escape)) {
		// if (Input.GetKey (KeyCode.Escape)) {
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		// UnityEngine.Application.Quit ();
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_002c:
	{
		// if (!rhythmTool.songLoaded) {
		RhythmTool_t215962618 * L_2 = __this->get_rhythmTool_2();
		// if (!rhythmTool.songLoaded) {
		NullCheck(L_2);
		bool L_3 = RhythmTool_get_songLoaded_m3458667938(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		// return;
		goto IL_0077;
	}

IL_0042:
	{
		// UpdateLines();
		// UpdateLines();
		VisualizerController_UpdateLines_m940043679(__this, /*hidden argument*/NULL);
		// bpmText.text = rhythmTool.bpm.ToString();
		Text_t356221433 * L_4 = __this->get_bpmText_4();
		RhythmTool_t215962618 * L_5 = __this->get_rhythmTool_2();
		// bpmText.text = rhythmTool.bpm.ToString();
		NullCheck(L_5);
		float L_6 = RhythmTool_get_bpm_m154604281(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// bpmText.text = rhythmTool.bpm.ToString();
		String_t* L_7 = Single_ToString_m1813392066((&V_0), /*hidden argument*/NULL);
		// bpmText.text = rhythmTool.bpm.ToString();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_7);
		// rhythmTool.DrawDebugLines ();
		RhythmTool_t215962618 * L_8 = __this->get_rhythmTool_2();
		// rhythmTool.DrawDebugLines ();
		NullCheck(L_8);
		RhythmTool_DrawDebugLines_m2101946209(L_8, /*hidden argument*/NULL);
	}

IL_0077:
	{
		// }
		return;
	}
}
// System.Void VisualizerController::UpdateLines()
extern "C"  void VisualizerController_UpdateLines_m940043679 (VisualizerController_t119580760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_UpdateLines_m940043679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2098562634 * V_0 = NULL;
	Line_t2729441502 * V_1 = NULL;
	Enumerator_t1633292308  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Line_t2729441502 * V_3 = NULL;
	Enumerator_t1633292308  V_4;
	memset(&V_4, 0, sizeof(V_4));
	SingleU5BU5D_t577127397* V_5 = NULL;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Line_t2729441502 * V_9 = NULL;
	Enumerator_t1633292308  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// List<Line> toRemove = new List<Line>();
		List_1_t2098562634 * L_0 = (List_1_t2098562634 *)il2cpp_codegen_object_new(List_1_t2098562634_il2cpp_TypeInfo_var);
		List_1__ctor_m2026045323(L_0, /*hidden argument*/List_1__ctor_m2026045323_MethodInfo_var);
		V_0 = L_0;
		// foreach(Line l in lines)
		List_1_t2098562634 * L_1 = __this->get_lines_7();
		// foreach(Line l in lines)
		NullCheck(L_1);
		Enumerator_t1633292308  L_2 = List_1_GetEnumerator_m3424352800(L_1, /*hidden argument*/List_1_GetEnumerator_m3424352800_MethodInfo_var);
		V_2 = L_2;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0073;
		}

IL_0019:
		{
			// foreach(Line l in lines)
			// foreach(Line l in lines)
			Line_t2729441502 * L_3 = Enumerator_get_Current_m2930845510((&V_2), /*hidden argument*/Enumerator_get_Current_m2930845510_MethodInfo_var);
			V_1 = L_3;
			// if (l.index < rhythmTool.currentFrame || l.index > rhythmTool.currentFrame + eventProvider.offset)
			Line_t2729441502 * L_4 = V_1;
			// if (l.index < rhythmTool.currentFrame || l.index > rhythmTool.currentFrame + eventProvider.offset)
			NullCheck(L_4);
			int32_t L_5 = Line_get_index_m1915203340(L_4, /*hidden argument*/NULL);
			RhythmTool_t215962618 * L_6 = __this->get_rhythmTool_2();
			// if (l.index < rhythmTool.currentFrame || l.index > rhythmTool.currentFrame + eventProvider.offset)
			NullCheck(L_6);
			int32_t L_7 = RhythmTool_get_currentFrame_m867144816(L_6, /*hidden argument*/NULL);
			if ((((int32_t)L_5) < ((int32_t)L_7)))
			{
				goto IL_005a;
			}
		}

IL_0038:
		{
			Line_t2729441502 * L_8 = V_1;
			// if (l.index < rhythmTool.currentFrame || l.index > rhythmTool.currentFrame + eventProvider.offset)
			NullCheck(L_8);
			int32_t L_9 = Line_get_index_m1915203340(L_8, /*hidden argument*/NULL);
			RhythmTool_t215962618 * L_10 = __this->get_rhythmTool_2();
			// if (l.index < rhythmTool.currentFrame || l.index > rhythmTool.currentFrame + eventProvider.offset)
			NullCheck(L_10);
			int32_t L_11 = RhythmTool_get_currentFrame_m867144816(L_10, /*hidden argument*/NULL);
			RhythmEventProvider_t215006757 * L_12 = __this->get_eventProvider_3();
			NullCheck(L_12);
			int32_t L_13 = L_12->get_offset_4();
			if ((((int32_t)L_9) <= ((int32_t)((int32_t)((int32_t)L_11+(int32_t)L_13)))))
			{
				goto IL_0072;
			}
		}

IL_005a:
		{
			// Destroy(l.gameObject);
			Line_t2729441502 * L_14 = V_1;
			// Destroy(l.gameObject);
			NullCheck(L_14);
			GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(L_14, /*hidden argument*/NULL);
			// Destroy(l.gameObject);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_Destroy_m4145850038(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			// toRemove.Add(l);
			List_1_t2098562634 * L_16 = V_0;
			Line_t2729441502 * L_17 = V_1;
			// toRemove.Add(l);
			NullCheck(L_16);
			List_1_Add_m1195348343(L_16, L_17, /*hidden argument*/List_1_Add_m1195348343_MethodInfo_var);
			// continue;
			goto IL_0073;
		}

IL_0072:
		{
		}

IL_0073:
		{
			// foreach(Line l in lines)
			bool L_18 = Enumerator_MoveNext_m1329402980((&V_2), /*hidden argument*/Enumerator_MoveNext_m1329402980_MethodInfo_var);
			if (L_18)
			{
				goto IL_0019;
			}
		}

IL_007f:
		{
			IL2CPP_LEAVE(0x92, FINALLY_0084);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0084;
	}

FINALLY_0084:
	{ // begin finally (depth: 1)
		// foreach(Line l in lines)
		Enumerator_Dispose_m3502228714((&V_2), /*hidden argument*/Enumerator_Dispose_m3502228714_MethodInfo_var);
		IL2CPP_END_FINALLY(132)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(132)
	{
		IL2CPP_JUMP_TBL(0x92, IL_0092)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0092:
	{
		// foreach (Line l in toRemove)
		List_1_t2098562634 * L_19 = V_0;
		// foreach (Line l in toRemove)
		NullCheck(L_19);
		Enumerator_t1633292308  L_20 = List_1_GetEnumerator_m3424352800(L_19, /*hidden argument*/List_1_GetEnumerator_m3424352800_MethodInfo_var);
		V_4 = L_20;
	}

IL_009b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b5;
		}

IL_00a0:
		{
			// foreach (Line l in toRemove)
			// foreach (Line l in toRemove)
			Line_t2729441502 * L_21 = Enumerator_get_Current_m2930845510((&V_4), /*hidden argument*/Enumerator_get_Current_m2930845510_MethodInfo_var);
			V_3 = L_21;
			// lines.Remove(l);
			List_1_t2098562634 * L_22 = __this->get_lines_7();
			Line_t2729441502 * L_23 = V_3;
			// lines.Remove(l);
			NullCheck(L_22);
			List_1_Remove_m1874657968(L_22, L_23, /*hidden argument*/List_1_Remove_m1874657968_MethodInfo_var);
		}

IL_00b5:
		{
			// foreach (Line l in toRemove)
			bool L_24 = Enumerator_MoveNext_m1329402980((&V_4), /*hidden argument*/Enumerator_MoveNext_m1329402980_MethodInfo_var);
			if (L_24)
			{
				goto IL_00a0;
			}
		}

IL_00c1:
		{
			IL2CPP_LEAVE(0xD4, FINALLY_00c6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c6;
	}

FINALLY_00c6:
	{ // begin finally (depth: 1)
		// foreach (Line l in toRemove)
		Enumerator_Dispose_m3502228714((&V_4), /*hidden argument*/Enumerator_Dispose_m3502228714_MethodInfo_var);
		IL2CPP_END_FINALLY(198)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(198)
	{
		IL2CPP_JUMP_TBL(0xD4, IL_00d4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00d4:
	{
		// float[] cumulativeMagnitudeSmooth = new float[eventProvider.offset + 1];
		RhythmEventProvider_t215006757 * L_25 = __this->get_eventProvider_3();
		NullCheck(L_25);
		int32_t L_26 = L_25->get_offset_4();
		V_5 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_26+(int32_t)1))));
		// float total = 0;
		V_6 = (0.0f);
		// for (int i = 0; i < cumulativeMagnitudeSmooth.Length; i++)
		V_7 = 0;
		goto IL_013a;
	}

IL_00f7:
	{
		// int index = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames-1);
		RhythmTool_t215962618 * L_27 = __this->get_rhythmTool_2();
		// int index = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames-1);
		NullCheck(L_27);
		int32_t L_28 = RhythmTool_get_currentFrame_m867144816(L_27, /*hidden argument*/NULL);
		int32_t L_29 = V_7;
		RhythmTool_t215962618 * L_30 = __this->get_rhythmTool_2();
		// int index = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames-1);
		NullCheck(L_30);
		int32_t L_31 = RhythmTool_get_totalFrames_m68629944(L_30, /*hidden argument*/NULL);
		// int index = Mathf.Min(rhythmTool.currentFrame + i, rhythmTool.totalFrames-1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_32 = Mathf_Min_m2906823867(NULL /*static, unused*/, ((int32_t)((int32_t)L_28+(int32_t)L_29)), ((int32_t)((int32_t)L_31-(int32_t)1)), /*hidden argument*/NULL);
		V_8 = L_32;
		// total += magnitudeSmooth[index];
		float L_33 = V_6;
		ReadOnlyCollection_1_t2262295624 * L_34 = __this->get_magnitudeSmooth_9();
		int32_t L_35 = V_8;
		// total += magnitudeSmooth[index];
		NullCheck(L_34);
		float L_36 = ReadOnlyCollection_1_get_Item_m210738684(L_34, L_35, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
		V_6 = ((float)((float)L_33+(float)L_36));
		// cumulativeMagnitudeSmooth[i] = total;
		SingleU5BU5D_t577127397* L_37 = V_5;
		int32_t L_38 = V_7;
		float L_39 = V_6;
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (float)L_39);
		// for (int i = 0; i < cumulativeMagnitudeSmooth.Length; i++)
		int32_t L_40 = V_7;
		V_7 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_013a:
	{
		// for (int i = 0; i < cumulativeMagnitudeSmooth.Length; i++)
		int32_t L_41 = V_7;
		SingleU5BU5D_t577127397* L_42 = V_5;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length)))))))
		{
			goto IL_00f7;
		}
	}
	{
		// foreach (Line l in lines)
		List_1_t2098562634 * L_43 = __this->get_lines_7();
		// foreach (Line l in lines)
		NullCheck(L_43);
		Enumerator_t1633292308  L_44 = List_1_GetEnumerator_m3424352800(L_43, /*hidden argument*/List_1_GetEnumerator_m3424352800_MethodInfo_var);
		V_10 = L_44;
	}

IL_0153:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01d8;
		}

IL_0158:
		{
			// foreach (Line l in lines)
			// foreach (Line l in lines)
			Line_t2729441502 * L_45 = Enumerator_get_Current_m2930845510((&V_10), /*hidden argument*/Enumerator_get_Current_m2930845510_MethodInfo_var);
			V_9 = L_45;
			// Vector3 pos = l.transform.position;
			Line_t2729441502 * L_46 = V_9;
			// Vector3 pos = l.transform.position;
			NullCheck(L_46);
			Transform_t3275118058 * L_47 = Component_get_transform_m2697483695(L_46, /*hidden argument*/NULL);
			// Vector3 pos = l.transform.position;
			NullCheck(L_47);
			Vector3_t2243707580  L_48 = Transform_get_position_m1104419803(L_47, /*hidden argument*/NULL);
			V_11 = L_48;
			// pos.x = cumulativeMagnitudeSmooth[l.index - rhythmTool.currentFrame] * .2f;
			SingleU5BU5D_t577127397* L_49 = V_5;
			Line_t2729441502 * L_50 = V_9;
			// pos.x = cumulativeMagnitudeSmooth[l.index - rhythmTool.currentFrame] * .2f;
			NullCheck(L_50);
			int32_t L_51 = Line_get_index_m1915203340(L_50, /*hidden argument*/NULL);
			RhythmTool_t215962618 * L_52 = __this->get_rhythmTool_2();
			// pos.x = cumulativeMagnitudeSmooth[l.index - rhythmTool.currentFrame] * .2f;
			NullCheck(L_52);
			int32_t L_53 = RhythmTool_get_currentFrame_m867144816(L_52, /*hidden argument*/NULL);
			NullCheck(L_49);
			int32_t L_54 = ((int32_t)((int32_t)L_51-(int32_t)L_53));
			float L_55 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
			(&V_11)->set_x_1(((float)((float)L_55*(float)(0.2f))));
			// pos.x -= magnitudeSmooth[rhythmTool.currentFrame] * .2f * rhythmTool.interpolation;
			Vector3_t2243707580 * L_56 = (&V_11);
			float L_57 = L_56->get_x_1();
			ReadOnlyCollection_1_t2262295624 * L_58 = __this->get_magnitudeSmooth_9();
			RhythmTool_t215962618 * L_59 = __this->get_rhythmTool_2();
			// pos.x -= magnitudeSmooth[rhythmTool.currentFrame] * .2f * rhythmTool.interpolation;
			NullCheck(L_59);
			int32_t L_60 = RhythmTool_get_currentFrame_m867144816(L_59, /*hidden argument*/NULL);
			// pos.x -= magnitudeSmooth[rhythmTool.currentFrame] * .2f * rhythmTool.interpolation;
			NullCheck(L_58);
			float L_61 = ReadOnlyCollection_1_get_Item_m210738684(L_58, L_60, /*hidden argument*/ReadOnlyCollection_1_get_Item_m210738684_MethodInfo_var);
			RhythmTool_t215962618 * L_62 = __this->get_rhythmTool_2();
			// pos.x -= magnitudeSmooth[rhythmTool.currentFrame] * .2f * rhythmTool.interpolation;
			NullCheck(L_62);
			float L_63 = RhythmTool_get_interpolation_m2645903814(L_62, /*hidden argument*/NULL);
			L_56->set_x_1(((float)((float)L_57-(float)((float)((float)((float)((float)L_61*(float)(0.2f)))*(float)L_63)))));
			// l.transform.position = pos;
			Line_t2729441502 * L_64 = V_9;
			// l.transform.position = pos;
			NullCheck(L_64);
			Transform_t3275118058 * L_65 = Component_get_transform_m2697483695(L_64, /*hidden argument*/NULL);
			Vector3_t2243707580  L_66 = V_11;
			// l.transform.position = pos;
			NullCheck(L_65);
			Transform_set_position_m2469242620(L_65, L_66, /*hidden argument*/NULL);
		}

IL_01d8:
		{
			// foreach (Line l in lines)
			bool L_67 = Enumerator_MoveNext_m1329402980((&V_10), /*hidden argument*/Enumerator_MoveNext_m1329402980_MethodInfo_var);
			if (L_67)
			{
				goto IL_0158;
			}
		}

IL_01e4:
		{
			IL2CPP_LEAVE(0x1F7, FINALLY_01e9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01e9;
	}

FINALLY_01e9:
	{ // begin finally (depth: 1)
		// foreach (Line l in lines)
		Enumerator_Dispose_m3502228714((&V_10), /*hidden argument*/Enumerator_Dispose_m3502228714_MethodInfo_var);
		IL2CPP_END_FINALLY(489)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(489)
	{
		IL2CPP_JUMP_TBL(0x1F7, IL_01f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01f7:
	{
		// }
		return;
	}
}
// System.Void VisualizerController::OnBeat(Beat)
extern "C"  void VisualizerController_OnBeat_m464411654 (VisualizerController_t119580760 * __this, Beat_t2695683572 * ___beat0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_OnBeat_m464411654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// lines.Add(CreateLine(beat.index, Color.white, 20, -40));
		List_1_t2098562634 * L_0 = __this->get_lines_7();
		Beat_t2695683572 * L_1 = ___beat0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_index_2();
		// lines.Add(CreateLine(beat.index, Color.white, 20, -40));
		Color_t2020392075  L_3 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		// lines.Add(CreateLine(beat.index, Color.white, 20, -40));
		Line_t2729441502 * L_4 = VisualizerController_CreateLine_m3677149367(__this, L_2, L_3, (20.0f), (-40.0f), /*hidden argument*/NULL);
		// lines.Add(CreateLine(beat.index, Color.white, 20, -40));
		NullCheck(L_0);
		List_1_Add_m1195348343(L_0, L_4, /*hidden argument*/List_1_Add_m1195348343_MethodInfo_var);
		// }
		return;
	}
}
// System.Void VisualizerController::OnChange(System.Int32,System.Single)
extern "C"  void VisualizerController_OnChange_m2842847708 (VisualizerController_t119580760 * __this, int32_t ___index0, float ___change1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_OnChange_m2842847708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (change > 0)
		float L_0 = ___change1;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		// lines.Add(CreateLine(index, Color.yellow, 20, -60));
		List_1_t2098562634 * L_1 = __this->get_lines_7();
		int32_t L_2 = ___index0;
		// lines.Add(CreateLine(index, Color.yellow, 20, -60));
		Color_t2020392075  L_3 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		// lines.Add(CreateLine(index, Color.yellow, 20, -60));
		Line_t2729441502 * L_4 = VisualizerController_CreateLine_m3677149367(__this, L_2, L_3, (20.0f), (-60.0f), /*hidden argument*/NULL);
		// lines.Add(CreateLine(index, Color.yellow, 20, -60));
		NullCheck(L_1);
		List_1_Add_m1195348343(L_1, L_4, /*hidden argument*/List_1_Add_m1195348343_MethodInfo_var);
	}

IL_002d:
	{
		// }
		return;
	}
}
// System.Void VisualizerController::OnOnset(OnsetType,Onset)
extern "C"  void VisualizerController_OnOnset_m689097857 (VisualizerController_t119580760 * __this, int32_t ___type0, Onset_t732596509 * ___onset1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_OnOnset_m689097857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (onset.rank < 4 && onset < 5)
		Onset_t732596509 * L_0 = ___onset1;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_rank_2();
		if ((((int32_t)L_1) >= ((int32_t)4)))
		{
			goto IL_0022;
		}
	}
	{
		Onset_t732596509 * L_2 = ___onset1;
		// if (onset.rank < 4 && onset < 5)
		bool L_3 = Onset_op_LessThan_m1075825226(NULL /*static, unused*/, L_2, (5.0f), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		// return;
		goto IL_00ed;
	}

IL_0022:
	{
		// {
		int32_t L_4 = ___type0;
		switch (L_4)
		{
			case 0:
			{
				goto IL_003d;
			}
			case 1:
			{
				goto IL_0069;
			}
			case 2:
			{
				goto IL_0095;
			}
			case 3:
			{
				goto IL_00c1;
			}
		}
	}
	{
		goto IL_00ed;
	}

IL_003d:
	{
		// lines.Add(CreateLine(onset.index, Color.blue, onset, -20));
		List_1_t2098562634 * L_5 = __this->get_lines_7();
		Onset_t732596509 * L_6 = ___onset1;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_index_0();
		// lines.Add(CreateLine(onset.index, Color.blue, onset, -20));
		Color_t2020392075  L_8 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		Onset_t732596509 * L_9 = ___onset1;
		// lines.Add(CreateLine(onset.index, Color.blue, onset, -20));
		float L_10 = Onset_op_Implicit_m4069753098(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		// lines.Add(CreateLine(onset.index, Color.blue, onset, -20));
		Line_t2729441502 * L_11 = VisualizerController_CreateLine_m3677149367(__this, L_7, L_8, L_10, (-20.0f), /*hidden argument*/NULL);
		// lines.Add(CreateLine(onset.index, Color.blue, onset, -20));
		NullCheck(L_5);
		List_1_Add_m1195348343(L_5, L_11, /*hidden argument*/List_1_Add_m1195348343_MethodInfo_var);
		// break;
		goto IL_00ed;
	}

IL_0069:
	{
		// lines.Add(CreateLine(onset.index, Color.green, onset, 0));
		List_1_t2098562634 * L_12 = __this->get_lines_7();
		Onset_t732596509 * L_13 = ___onset1;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_index_0();
		// lines.Add(CreateLine(onset.index, Color.green, onset, 0));
		Color_t2020392075  L_15 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		Onset_t732596509 * L_16 = ___onset1;
		// lines.Add(CreateLine(onset.index, Color.green, onset, 0));
		float L_17 = Onset_op_Implicit_m4069753098(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		// lines.Add(CreateLine(onset.index, Color.green, onset, 0));
		Line_t2729441502 * L_18 = VisualizerController_CreateLine_m3677149367(__this, L_14, L_15, L_17, (0.0f), /*hidden argument*/NULL);
		// lines.Add(CreateLine(onset.index, Color.green, onset, 0));
		NullCheck(L_12);
		List_1_Add_m1195348343(L_12, L_18, /*hidden argument*/List_1_Add_m1195348343_MethodInfo_var);
		// break;
		goto IL_00ed;
	}

IL_0095:
	{
		// lines.Add(CreateLine(onset.index, Color.yellow, onset, 20));
		List_1_t2098562634 * L_19 = __this->get_lines_7();
		Onset_t732596509 * L_20 = ___onset1;
		NullCheck(L_20);
		int32_t L_21 = L_20->get_index_0();
		// lines.Add(CreateLine(onset.index, Color.yellow, onset, 20));
		Color_t2020392075  L_22 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		Onset_t732596509 * L_23 = ___onset1;
		// lines.Add(CreateLine(onset.index, Color.yellow, onset, 20));
		float L_24 = Onset_op_Implicit_m4069753098(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		// lines.Add(CreateLine(onset.index, Color.yellow, onset, 20));
		Line_t2729441502 * L_25 = VisualizerController_CreateLine_m3677149367(__this, L_21, L_22, L_24, (20.0f), /*hidden argument*/NULL);
		// lines.Add(CreateLine(onset.index, Color.yellow, onset, 20));
		NullCheck(L_19);
		List_1_Add_m1195348343(L_19, L_25, /*hidden argument*/List_1_Add_m1195348343_MethodInfo_var);
		// break;
		goto IL_00ed;
	}

IL_00c1:
	{
		// lines.Add(CreateLine(onset.index, Color.magenta, onset, 40));
		List_1_t2098562634 * L_26 = __this->get_lines_7();
		Onset_t732596509 * L_27 = ___onset1;
		NullCheck(L_27);
		int32_t L_28 = L_27->get_index_0();
		// lines.Add(CreateLine(onset.index, Color.magenta, onset, 40));
		Color_t2020392075  L_29 = Color_get_magenta_m3193089961(NULL /*static, unused*/, /*hidden argument*/NULL);
		Onset_t732596509 * L_30 = ___onset1;
		// lines.Add(CreateLine(onset.index, Color.magenta, onset, 40));
		float L_31 = Onset_op_Implicit_m4069753098(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		// lines.Add(CreateLine(onset.index, Color.magenta, onset, 40));
		Line_t2729441502 * L_32 = VisualizerController_CreateLine_m3677149367(__this, L_28, L_29, L_31, (40.0f), /*hidden argument*/NULL);
		// lines.Add(CreateLine(onset.index, Color.magenta, onset, 40));
		NullCheck(L_26);
		List_1_Add_m1195348343(L_26, L_32, /*hidden argument*/List_1_Add_m1195348343_MethodInfo_var);
		// break;
		goto IL_00ed;
	}

IL_00ed:
	{
		// }
		return;
	}
}
// Line VisualizerController::CreateLine(System.Int32,UnityEngine.Color,System.Single,System.Single)
extern "C"  Line_t2729441502 * VisualizerController_CreateLine_m3677149367 (VisualizerController_t119580760 * __this, int32_t ___index0, Color_t2020392075  ___color1, float ___opacity2, float ___yPosition3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_CreateLine_m3677149367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Line_t2729441502 * V_1 = NULL;
	Line_t2729441502 * V_2 = NULL;
	{
		// GameObject lineObject = Instantiate(linePrefab) as GameObject;
		GameObject_t1756533147 * L_0 = __this->get_linePrefab_5();
		// GameObject lineObject = Instantiate(linePrefab) as GameObject;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_1 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_0 = L_1;
		// lineObject.transform.position = new Vector3(0, yPosition, 0);
		GameObject_t1756533147 * L_2 = V_0;
		// lineObject.transform.position = new Vector3(0, yPosition, 0);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		float L_4 = ___yPosition3;
		// lineObject.transform.position = new Vector3(0, yPosition, 0);
		Vector3_t2243707580  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2638739322(&L_5, (0.0f), L_4, (0.0f), /*hidden argument*/NULL);
		// lineObject.transform.position = new Vector3(0, yPosition, 0);
		NullCheck(L_3);
		Transform_set_position_m2469242620(L_3, L_5, /*hidden argument*/NULL);
		// Line line = lineObject.GetComponent<Line>();
		GameObject_t1756533147 * L_6 = V_0;
		// Line line = lineObject.GetComponent<Line>();
		NullCheck(L_6);
		Line_t2729441502 * L_7 = GameObject_GetComponent_TisLine_t2729441502_m1643494813(L_6, /*hidden argument*/GameObject_GetComponent_TisLine_t2729441502_m1643494813_MethodInfo_var);
		V_1 = L_7;
		// line.Init(color, opacity, index);
		Line_t2729441502 * L_8 = V_1;
		Color_t2020392075  L_9 = ___color1;
		float L_10 = ___opacity2;
		int32_t L_11 = ___index0;
		// line.Init(color, opacity, index);
		NullCheck(L_8);
		Line_Init_m3596432585(L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		// return line;
		Line_t2729441502 * L_12 = V_1;
		V_2 = L_12;
		goto IL_0040;
	}

IL_0040:
	{
		// }
		Line_t2729441502 * L_13 = V_2;
		return L_13;
	}
}
// System.Void VisualizerController::ClearLines()
extern "C"  void VisualizerController_ClearLines_m3213124107 (VisualizerController_t119580760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VisualizerController_ClearLines_m3213124107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Line_t2729441502 * V_0 = NULL;
	Enumerator_t1633292308  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// foreach (Line l in lines)
		List_1_t2098562634 * L_0 = __this->get_lines_7();
		// foreach (Line l in lines)
		NullCheck(L_0);
		Enumerator_t1633292308  L_1 = List_1_GetEnumerator_m3424352800(L_0, /*hidden argument*/List_1_GetEnumerator_m3424352800_MethodInfo_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0013:
		{
			// foreach (Line l in lines)
			// foreach (Line l in lines)
			Line_t2729441502 * L_2 = Enumerator_get_Current_m2930845510((&V_1), /*hidden argument*/Enumerator_get_Current_m2930845510_MethodInfo_var);
			V_0 = L_2;
			// Destroy(l.gameObject);
			Line_t2729441502 * L_3 = V_0;
			// Destroy(l.gameObject);
			NullCheck(L_3);
			GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
			// Destroy(l.gameObject);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0026:
		{
			// foreach (Line l in lines)
			bool L_5 = Enumerator_MoveNext_m1329402980((&V_1), /*hidden argument*/Enumerator_MoveNext_m1329402980_MethodInfo_var);
			if (L_5)
			{
				goto IL_0013;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x45, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		// foreach (Line l in lines)
		Enumerator_Dispose_m3502228714((&V_1), /*hidden argument*/Enumerator_Dispose_m3502228714_MethodInfo_var);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0045:
	{
		// lines.Clear();
		List_1_t2098562634 * L_6 = __this->get_lines_7();
		// lines.Clear();
		NullCheck(L_6);
		List_1_Clear_m2227676928(L_6, /*hidden argument*/List_1_Clear_m2227676928_MethodInfo_var);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
