﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<AnalysisData>
struct List_1_t3772431102;
// BeatTracker
struct BeatTracker_t2801099156;
// Segmenter
struct Segmenter_t2695296026;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SongData
struct  SongData_t3132760915  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<AnalysisData> SongData::analyses
	List_1_t3772431102 * ___analyses_0;
	// BeatTracker SongData::beatTracker
	BeatTracker_t2801099156 * ___beatTracker_1;
	// Segmenter SongData::segmenter
	Segmenter_t2695296026 * ___segmenter_2;
	// System.String SongData::name
	String_t* ___name_3;
	// System.Int32 SongData::length
	int32_t ___length_4;

public:
	inline static int32_t get_offset_of_analyses_0() { return static_cast<int32_t>(offsetof(SongData_t3132760915, ___analyses_0)); }
	inline List_1_t3772431102 * get_analyses_0() const { return ___analyses_0; }
	inline List_1_t3772431102 ** get_address_of_analyses_0() { return &___analyses_0; }
	inline void set_analyses_0(List_1_t3772431102 * value)
	{
		___analyses_0 = value;
		Il2CppCodeGenWriteBarrier(&___analyses_0, value);
	}

	inline static int32_t get_offset_of_beatTracker_1() { return static_cast<int32_t>(offsetof(SongData_t3132760915, ___beatTracker_1)); }
	inline BeatTracker_t2801099156 * get_beatTracker_1() const { return ___beatTracker_1; }
	inline BeatTracker_t2801099156 ** get_address_of_beatTracker_1() { return &___beatTracker_1; }
	inline void set_beatTracker_1(BeatTracker_t2801099156 * value)
	{
		___beatTracker_1 = value;
		Il2CppCodeGenWriteBarrier(&___beatTracker_1, value);
	}

	inline static int32_t get_offset_of_segmenter_2() { return static_cast<int32_t>(offsetof(SongData_t3132760915, ___segmenter_2)); }
	inline Segmenter_t2695296026 * get_segmenter_2() const { return ___segmenter_2; }
	inline Segmenter_t2695296026 ** get_address_of_segmenter_2() { return &___segmenter_2; }
	inline void set_segmenter_2(Segmenter_t2695296026 * value)
	{
		___segmenter_2 = value;
		Il2CppCodeGenWriteBarrier(&___segmenter_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(SongData_t3132760915, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(SongData_t3132760915, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
