﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Onset
struct  Onset_t732596509  : public Il2CppObject
{
public:
	// System.Int32 Onset::index
	int32_t ___index_0;
	// System.Single Onset::strength
	float ___strength_1;
	// System.Int32 Onset::rank
	int32_t ___rank_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Onset_t732596509, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_strength_1() { return static_cast<int32_t>(offsetof(Onset_t732596509, ___strength_1)); }
	inline float get_strength_1() const { return ___strength_1; }
	inline float* get_address_of_strength_1() { return &___strength_1; }
	inline void set_strength_1(float value)
	{
		___strength_1 = value;
	}

	inline static int32_t get_offset_of_rank_2() { return static_cast<int32_t>(offsetof(Onset_t732596509, ___rank_2)); }
	inline int32_t get_rank_2() const { return ___rank_2; }
	inline int32_t* get_address_of_rank_2() { return &___rank_2; }
	inline void set_rank_2(int32_t value)
	{
		___rank_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
