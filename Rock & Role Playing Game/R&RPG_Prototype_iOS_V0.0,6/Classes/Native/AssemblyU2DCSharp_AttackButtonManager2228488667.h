﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// HitZoneManager
struct HitZoneManager_t1308216902;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackButtonManager
struct  AttackButtonManager_t2228488667  : public MonoBehaviour_t1158329972
{
public:
	// HitZoneManager AttackButtonManager::hitZoneManager
	HitZoneManager_t1308216902 * ___hitZoneManager_2;
	// UnityEngine.UI.Button AttackButtonManager::button
	Button_t2872111280 * ___button_3;

public:
	inline static int32_t get_offset_of_hitZoneManager_2() { return static_cast<int32_t>(offsetof(AttackButtonManager_t2228488667, ___hitZoneManager_2)); }
	inline HitZoneManager_t1308216902 * get_hitZoneManager_2() const { return ___hitZoneManager_2; }
	inline HitZoneManager_t1308216902 ** get_address_of_hitZoneManager_2() { return &___hitZoneManager_2; }
	inline void set_hitZoneManager_2(HitZoneManager_t1308216902 * value)
	{
		___hitZoneManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___hitZoneManager_2, value);
	}

	inline static int32_t get_offset_of_button_3() { return static_cast<int32_t>(offsetof(AttackButtonManager_t2228488667, ___button_3)); }
	inline Button_t2872111280 * get_button_3() const { return ___button_3; }
	inline Button_t2872111280 ** get_address_of_button_3() { return &___button_3; }
	inline void set_button_3(Button_t2872111280 * value)
	{
		___button_3 = value;
		Il2CppCodeGenWriteBarrier(&___button_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
