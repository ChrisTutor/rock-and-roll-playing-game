﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Line
struct  Line_t2729441502  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Line::<index>k__BackingField
	int32_t ___U3CindexU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CindexU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Line_t2729441502, ___U3CindexU3Ek__BackingField_2)); }
	inline int32_t get_U3CindexU3Ek__BackingField_2() const { return ___U3CindexU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CindexU3Ek__BackingField_2() { return &___U3CindexU3Ek__BackingField_2; }
	inline void set_U3CindexU3Ek__BackingField_2(int32_t value)
	{
		___U3CindexU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
