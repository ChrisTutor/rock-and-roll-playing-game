﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beat
struct  Beat_t2695683572  : public Il2CppObject
{
public:
	// System.Single Beat::length
	float ___length_0;
	// System.Single Beat::bpm
	float ___bpm_1;
	// System.Int32 Beat::index
	int32_t ___index_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(Beat_t2695683572, ___length_0)); }
	inline float get_length_0() const { return ___length_0; }
	inline float* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(float value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_bpm_1() { return static_cast<int32_t>(offsetof(Beat_t2695683572, ___bpm_1)); }
	inline float get_bpm_1() const { return ___bpm_1; }
	inline float* get_address_of_bpm_1() { return &___bpm_1; }
	inline void set_bpm_1(float value)
	{
		___bpm_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Beat_t2695683572, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
