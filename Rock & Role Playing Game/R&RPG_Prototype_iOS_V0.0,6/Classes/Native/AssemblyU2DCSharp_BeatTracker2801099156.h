﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.Dictionary`2<System.Int32,Beat>
struct Dictionary_2_t1703509207;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t2257663140;
// ReadOnlyDictionary`2<System.Int32,Beat>
struct ReadOnlyDictionary_2_t1991166635;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BeatTracker
struct  BeatTracker_t2801099156  : public Il2CppObject
{
public:
	// System.Single[] BeatTracker::signalBuffer
	SingleU5BU5D_t577127397* ___signalBuffer_0;
	// System.Single[] BeatTracker::currentSignal
	SingleU5BU5D_t577127397* ___currentSignal_1;
	// System.Single[] BeatTracker::upsampledSignal
	SingleU5BU5D_t577127397* ___upsampledSignal_2;
	// System.Single[] BeatTracker::repetitionScore
	SingleU5BU5D_t577127397* ___repetitionScore_3;
	// System.Single[] BeatTracker::gapScore
	SingleU5BU5D_t577127397* ___gapScore_4;
	// System.Single[] BeatTracker::offsetScore
	SingleU5BU5D_t577127397* ___offsetScore_5;
	// System.Collections.Generic.List`1<System.Int32> BeatTracker::peaks
	List_1_t1440998580 * ___peaks_6;
	// System.Single[] BeatTracker::beatHistogram
	SingleU5BU5D_t577127397* ___beatHistogram_7;
	// System.Int32 BeatTracker::bestRepetition
	int32_t ___bestRepetition_8;
	// System.Int32 BeatTracker::currentBeatLength
	int32_t ___currentBeatLength_9;
	// System.Single BeatTracker::syncOld
	float ___syncOld_10;
	// System.Single BeatTracker::sync
	float ___sync_11;
	// System.Single BeatTracker::frameLength
	float ___frameLength_12;
	// System.Int32 BeatTracker::beatTrackInterval
	int32_t ___beatTrackInterval_13;
	// System.Int32 BeatTracker::index
	int32_t ___index_14;
	// System.Collections.Generic.List`1<System.Int32> BeatTracker::_beatIndices
	List_1_t1440998580 * ____beatIndices_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,Beat> BeatTracker::_beats
	Dictionary_2_t1703509207 * ____beats_16;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32> BeatTracker::<beatIndices>k__BackingField
	ReadOnlyCollection_1_t2257663140 * ___U3CbeatIndicesU3Ek__BackingField_17;
	// ReadOnlyDictionary`2<System.Int32,Beat> BeatTracker::<beats>k__BackingField
	ReadOnlyDictionary_2_t1991166635 * ___U3CbeatsU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_signalBuffer_0() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___signalBuffer_0)); }
	inline SingleU5BU5D_t577127397* get_signalBuffer_0() const { return ___signalBuffer_0; }
	inline SingleU5BU5D_t577127397** get_address_of_signalBuffer_0() { return &___signalBuffer_0; }
	inline void set_signalBuffer_0(SingleU5BU5D_t577127397* value)
	{
		___signalBuffer_0 = value;
		Il2CppCodeGenWriteBarrier(&___signalBuffer_0, value);
	}

	inline static int32_t get_offset_of_currentSignal_1() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___currentSignal_1)); }
	inline SingleU5BU5D_t577127397* get_currentSignal_1() const { return ___currentSignal_1; }
	inline SingleU5BU5D_t577127397** get_address_of_currentSignal_1() { return &___currentSignal_1; }
	inline void set_currentSignal_1(SingleU5BU5D_t577127397* value)
	{
		___currentSignal_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentSignal_1, value);
	}

	inline static int32_t get_offset_of_upsampledSignal_2() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___upsampledSignal_2)); }
	inline SingleU5BU5D_t577127397* get_upsampledSignal_2() const { return ___upsampledSignal_2; }
	inline SingleU5BU5D_t577127397** get_address_of_upsampledSignal_2() { return &___upsampledSignal_2; }
	inline void set_upsampledSignal_2(SingleU5BU5D_t577127397* value)
	{
		___upsampledSignal_2 = value;
		Il2CppCodeGenWriteBarrier(&___upsampledSignal_2, value);
	}

	inline static int32_t get_offset_of_repetitionScore_3() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___repetitionScore_3)); }
	inline SingleU5BU5D_t577127397* get_repetitionScore_3() const { return ___repetitionScore_3; }
	inline SingleU5BU5D_t577127397** get_address_of_repetitionScore_3() { return &___repetitionScore_3; }
	inline void set_repetitionScore_3(SingleU5BU5D_t577127397* value)
	{
		___repetitionScore_3 = value;
		Il2CppCodeGenWriteBarrier(&___repetitionScore_3, value);
	}

	inline static int32_t get_offset_of_gapScore_4() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___gapScore_4)); }
	inline SingleU5BU5D_t577127397* get_gapScore_4() const { return ___gapScore_4; }
	inline SingleU5BU5D_t577127397** get_address_of_gapScore_4() { return &___gapScore_4; }
	inline void set_gapScore_4(SingleU5BU5D_t577127397* value)
	{
		___gapScore_4 = value;
		Il2CppCodeGenWriteBarrier(&___gapScore_4, value);
	}

	inline static int32_t get_offset_of_offsetScore_5() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___offsetScore_5)); }
	inline SingleU5BU5D_t577127397* get_offsetScore_5() const { return ___offsetScore_5; }
	inline SingleU5BU5D_t577127397** get_address_of_offsetScore_5() { return &___offsetScore_5; }
	inline void set_offsetScore_5(SingleU5BU5D_t577127397* value)
	{
		___offsetScore_5 = value;
		Il2CppCodeGenWriteBarrier(&___offsetScore_5, value);
	}

	inline static int32_t get_offset_of_peaks_6() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___peaks_6)); }
	inline List_1_t1440998580 * get_peaks_6() const { return ___peaks_6; }
	inline List_1_t1440998580 ** get_address_of_peaks_6() { return &___peaks_6; }
	inline void set_peaks_6(List_1_t1440998580 * value)
	{
		___peaks_6 = value;
		Il2CppCodeGenWriteBarrier(&___peaks_6, value);
	}

	inline static int32_t get_offset_of_beatHistogram_7() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___beatHistogram_7)); }
	inline SingleU5BU5D_t577127397* get_beatHistogram_7() const { return ___beatHistogram_7; }
	inline SingleU5BU5D_t577127397** get_address_of_beatHistogram_7() { return &___beatHistogram_7; }
	inline void set_beatHistogram_7(SingleU5BU5D_t577127397* value)
	{
		___beatHistogram_7 = value;
		Il2CppCodeGenWriteBarrier(&___beatHistogram_7, value);
	}

	inline static int32_t get_offset_of_bestRepetition_8() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___bestRepetition_8)); }
	inline int32_t get_bestRepetition_8() const { return ___bestRepetition_8; }
	inline int32_t* get_address_of_bestRepetition_8() { return &___bestRepetition_8; }
	inline void set_bestRepetition_8(int32_t value)
	{
		___bestRepetition_8 = value;
	}

	inline static int32_t get_offset_of_currentBeatLength_9() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___currentBeatLength_9)); }
	inline int32_t get_currentBeatLength_9() const { return ___currentBeatLength_9; }
	inline int32_t* get_address_of_currentBeatLength_9() { return &___currentBeatLength_9; }
	inline void set_currentBeatLength_9(int32_t value)
	{
		___currentBeatLength_9 = value;
	}

	inline static int32_t get_offset_of_syncOld_10() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___syncOld_10)); }
	inline float get_syncOld_10() const { return ___syncOld_10; }
	inline float* get_address_of_syncOld_10() { return &___syncOld_10; }
	inline void set_syncOld_10(float value)
	{
		___syncOld_10 = value;
	}

	inline static int32_t get_offset_of_sync_11() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___sync_11)); }
	inline float get_sync_11() const { return ___sync_11; }
	inline float* get_address_of_sync_11() { return &___sync_11; }
	inline void set_sync_11(float value)
	{
		___sync_11 = value;
	}

	inline static int32_t get_offset_of_frameLength_12() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___frameLength_12)); }
	inline float get_frameLength_12() const { return ___frameLength_12; }
	inline float* get_address_of_frameLength_12() { return &___frameLength_12; }
	inline void set_frameLength_12(float value)
	{
		___frameLength_12 = value;
	}

	inline static int32_t get_offset_of_beatTrackInterval_13() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___beatTrackInterval_13)); }
	inline int32_t get_beatTrackInterval_13() const { return ___beatTrackInterval_13; }
	inline int32_t* get_address_of_beatTrackInterval_13() { return &___beatTrackInterval_13; }
	inline void set_beatTrackInterval_13(int32_t value)
	{
		___beatTrackInterval_13 = value;
	}

	inline static int32_t get_offset_of_index_14() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___index_14)); }
	inline int32_t get_index_14() const { return ___index_14; }
	inline int32_t* get_address_of_index_14() { return &___index_14; }
	inline void set_index_14(int32_t value)
	{
		___index_14 = value;
	}

	inline static int32_t get_offset_of__beatIndices_15() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ____beatIndices_15)); }
	inline List_1_t1440998580 * get__beatIndices_15() const { return ____beatIndices_15; }
	inline List_1_t1440998580 ** get_address_of__beatIndices_15() { return &____beatIndices_15; }
	inline void set__beatIndices_15(List_1_t1440998580 * value)
	{
		____beatIndices_15 = value;
		Il2CppCodeGenWriteBarrier(&____beatIndices_15, value);
	}

	inline static int32_t get_offset_of__beats_16() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ____beats_16)); }
	inline Dictionary_2_t1703509207 * get__beats_16() const { return ____beats_16; }
	inline Dictionary_2_t1703509207 ** get_address_of__beats_16() { return &____beats_16; }
	inline void set__beats_16(Dictionary_2_t1703509207 * value)
	{
		____beats_16 = value;
		Il2CppCodeGenWriteBarrier(&____beats_16, value);
	}

	inline static int32_t get_offset_of_U3CbeatIndicesU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___U3CbeatIndicesU3Ek__BackingField_17)); }
	inline ReadOnlyCollection_1_t2257663140 * get_U3CbeatIndicesU3Ek__BackingField_17() const { return ___U3CbeatIndicesU3Ek__BackingField_17; }
	inline ReadOnlyCollection_1_t2257663140 ** get_address_of_U3CbeatIndicesU3Ek__BackingField_17() { return &___U3CbeatIndicesU3Ek__BackingField_17; }
	inline void set_U3CbeatIndicesU3Ek__BackingField_17(ReadOnlyCollection_1_t2257663140 * value)
	{
		___U3CbeatIndicesU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbeatIndicesU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3CbeatsU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(BeatTracker_t2801099156, ___U3CbeatsU3Ek__BackingField_18)); }
	inline ReadOnlyDictionary_2_t1991166635 * get_U3CbeatsU3Ek__BackingField_18() const { return ___U3CbeatsU3Ek__BackingField_18; }
	inline ReadOnlyDictionary_2_t1991166635 ** get_address_of_U3CbeatsU3Ek__BackingField_18() { return &___U3CbeatsU3Ek__BackingField_18; }
	inline void set_U3CbeatsU3Ek__BackingField_18(ReadOnlyDictionary_2_t1991166635 * value)
	{
		___U3CbeatsU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbeatsU3Ek__BackingField_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
