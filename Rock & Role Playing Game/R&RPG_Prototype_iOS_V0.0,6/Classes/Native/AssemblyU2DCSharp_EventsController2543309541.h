﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// RhythmTool
struct RhythmTool_t215962618;
// RhythmEventProvider
struct RhythmEventProvider_t215006757;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventsController
struct  EventsController_t2543309541  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform EventsController::cube1Transform
	Transform_t3275118058 * ___cube1Transform_2;
	// UnityEngine.Transform EventsController::cube2Transform
	Transform_t3275118058 * ___cube2Transform_3;
	// RhythmTool EventsController::rhythmTool
	RhythmTool_t215962618 * ___rhythmTool_4;
	// RhythmEventProvider EventsController::eventProvider
	RhythmEventProvider_t215006757 * ___eventProvider_5;
	// UnityEngine.AudioClip EventsController::audioClip
	AudioClip_t1932558630 * ___audioClip_6;

public:
	inline static int32_t get_offset_of_cube1Transform_2() { return static_cast<int32_t>(offsetof(EventsController_t2543309541, ___cube1Transform_2)); }
	inline Transform_t3275118058 * get_cube1Transform_2() const { return ___cube1Transform_2; }
	inline Transform_t3275118058 ** get_address_of_cube1Transform_2() { return &___cube1Transform_2; }
	inline void set_cube1Transform_2(Transform_t3275118058 * value)
	{
		___cube1Transform_2 = value;
		Il2CppCodeGenWriteBarrier(&___cube1Transform_2, value);
	}

	inline static int32_t get_offset_of_cube2Transform_3() { return static_cast<int32_t>(offsetof(EventsController_t2543309541, ___cube2Transform_3)); }
	inline Transform_t3275118058 * get_cube2Transform_3() const { return ___cube2Transform_3; }
	inline Transform_t3275118058 ** get_address_of_cube2Transform_3() { return &___cube2Transform_3; }
	inline void set_cube2Transform_3(Transform_t3275118058 * value)
	{
		___cube2Transform_3 = value;
		Il2CppCodeGenWriteBarrier(&___cube2Transform_3, value);
	}

	inline static int32_t get_offset_of_rhythmTool_4() { return static_cast<int32_t>(offsetof(EventsController_t2543309541, ___rhythmTool_4)); }
	inline RhythmTool_t215962618 * get_rhythmTool_4() const { return ___rhythmTool_4; }
	inline RhythmTool_t215962618 ** get_address_of_rhythmTool_4() { return &___rhythmTool_4; }
	inline void set_rhythmTool_4(RhythmTool_t215962618 * value)
	{
		___rhythmTool_4 = value;
		Il2CppCodeGenWriteBarrier(&___rhythmTool_4, value);
	}

	inline static int32_t get_offset_of_eventProvider_5() { return static_cast<int32_t>(offsetof(EventsController_t2543309541, ___eventProvider_5)); }
	inline RhythmEventProvider_t215006757 * get_eventProvider_5() const { return ___eventProvider_5; }
	inline RhythmEventProvider_t215006757 ** get_address_of_eventProvider_5() { return &___eventProvider_5; }
	inline void set_eventProvider_5(RhythmEventProvider_t215006757 * value)
	{
		___eventProvider_5 = value;
		Il2CppCodeGenWriteBarrier(&___eventProvider_5, value);
	}

	inline static int32_t get_offset_of_audioClip_6() { return static_cast<int32_t>(offsetof(EventsController_t2543309541, ___audioClip_6)); }
	inline AudioClip_t1932558630 * get_audioClip_6() const { return ___audioClip_6; }
	inline AudioClip_t1932558630 ** get_address_of_audioClip_6() { return &___audioClip_6; }
	inline void set_audioClip_6(AudioClip_t1932558630 * value)
	{
		___audioClip_6 = value;
		Il2CppCodeGenWriteBarrier(&___audioClip_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
