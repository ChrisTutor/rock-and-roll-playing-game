﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3842535002.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1749519406.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1746754562.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C900829694.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2691167515.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2157404822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3369627127.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2144252492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3675451859.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3634411257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2607665220.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3273007553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3398611001.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1039424009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cr69389957.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4103805620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1952940174.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3887193949.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C113868641.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3347016329.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_animation_keys385740455.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (AxisTouchButton_t3842535002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[6] = 
{
	AxisTouchButton_t3842535002::get_offset_of_axisName_2(),
	AxisTouchButton_t3842535002::get_offset_of_axisValue_3(),
	AxisTouchButton_t3842535002::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t3842535002::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t3842535002::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t3842535002::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (ButtonHandler_t1749519406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[1] = 
{
	ButtonHandler_t1749519406::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (CrossPlatformInputManager_t1746754562), -1, sizeof(CrossPlatformInputManager_t1746754562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1803[3] = 
{
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ActiveInputMethod_t900829694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1804[3] = 
{
	ActiveInputMethod_t900829694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (VirtualAxis_t2691167515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[3] = 
{
	VirtualAxis_t2691167515::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t2691167515::get_offset_of_m_Value_1(),
	VirtualAxis_t2691167515::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (VirtualButton_t2157404822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[5] = 
{
	VirtualButton_t2157404822::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2157404822::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2157404822::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2157404822::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2157404822::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (InputAxisScrollbar_t3369627127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	InputAxisScrollbar_t3369627127::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (Joystick_t2144252492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[9] = 
{
	Joystick_t2144252492::get_offset_of_MovementRange_2(),
	Joystick_t2144252492::get_offset_of_axesToUse_3(),
	Joystick_t2144252492::get_offset_of_horizontalAxisName_4(),
	Joystick_t2144252492::get_offset_of_verticalAxisName_5(),
	Joystick_t2144252492::get_offset_of_m_StartPos_6(),
	Joystick_t2144252492::get_offset_of_m_UseX_7(),
	Joystick_t2144252492::get_offset_of_m_UseY_8(),
	Joystick_t2144252492::get_offset_of_m_HorizontalVirtualAxis_9(),
	Joystick_t2144252492::get_offset_of_m_VerticalVirtualAxis_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (AxisOption_t3675451859)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[4] = 
{
	AxisOption_t3675451859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (MobileControlRig_t3634411257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (MobileInput_t2607665220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (StandaloneInput_t3273007553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (TiltInput_t3398611001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[5] = 
{
	TiltInput_t3398611001::get_offset_of_mapping_2(),
	TiltInput_t3398611001::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t3398611001::get_offset_of_fullTiltAngle_4(),
	TiltInput_t3398611001::get_offset_of_centreAngleOffset_5(),
	TiltInput_t3398611001::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (AxisOptions_t1039424009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1814[3] = 
{
	AxisOptions_t1039424009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (AxisMapping_t69389957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[2] = 
{
	AxisMapping_t69389957::get_offset_of_type_0(),
	AxisMapping_t69389957::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (MappingType_t4103805620)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[5] = 
{
	MappingType_t4103805620::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (TouchPad_t1952940174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[18] = 
{
	TouchPad_t1952940174::get_offset_of_axesToUse_2(),
	TouchPad_t1952940174::get_offset_of_controlStyle_3(),
	TouchPad_t1952940174::get_offset_of_horizontalAxisName_4(),
	TouchPad_t1952940174::get_offset_of_verticalAxisName_5(),
	TouchPad_t1952940174::get_offset_of_Xsensitivity_6(),
	TouchPad_t1952940174::get_offset_of_Ysensitivity_7(),
	TouchPad_t1952940174::get_offset_of_m_StartPos_8(),
	TouchPad_t1952940174::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t1952940174::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t1952940174::get_offset_of_m_UseX_11(),
	TouchPad_t1952940174::get_offset_of_m_UseY_12(),
	TouchPad_t1952940174::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t1952940174::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t1952940174::get_offset_of_m_Dragging_15(),
	TouchPad_t1952940174::get_offset_of_m_Id_16(),
	TouchPad_t1952940174::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t1952940174::get_offset_of_m_Center_18(),
	TouchPad_t1952940174::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (AxisOption_t3887193949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	AxisOption_t3887193949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (ControlStyle_t113868641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1819[4] = 
{
	ControlStyle_t113868641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (VirtualInput_t3347016329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[4] = 
{
	VirtualInput_t3347016329::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t3347016329::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t3347016329::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t3347016329::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (animation_keys_t385740455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[1] = 
{
	animation_keys_t385740455::get_offset_of_teclado_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
