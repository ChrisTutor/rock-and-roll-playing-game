﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// AnalysisData
struct AnalysisData_t108342674;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t1084335567;
// ReadOnlyDictionary`2<System.Int32,System.Single>
struct ReadOnlyDictionary_2_t1371992995;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t2257663140;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Segmenter
struct  Segmenter_t2695296026  : public Il2CppObject
{
public:
	// AnalysisData Segmenter::analysis
	AnalysisData_t108342674 * ___analysis_0;
	// System.Single Segmenter::lastDif
	float ___lastDif_1;
	// System.Int32 Segmenter::increaseStart
	int32_t ___increaseStart_2;
	// System.Int32 Segmenter::increaseEnd
	int32_t ___increaseEnd_3;
	// System.Boolean Segmenter::increaseDetected
	bool ___increaseDetected_4;
	// System.Int32 Segmenter::decreaseStart
	int32_t ___decreaseStart_5;
	// System.Int32 Segmenter::decreaseEnd
	int32_t ___decreaseEnd_6;
	// System.Boolean Segmenter::decreaseDetected
	bool ___decreaseDetected_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> Segmenter::_changes
	Dictionary_2_t1084335567 * ____changes_8;
	// ReadOnlyDictionary`2<System.Int32,System.Single> Segmenter::<changes>k__BackingField
	ReadOnlyDictionary_2_t1371992995 * ___U3CchangesU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<System.Int32> Segmenter::_changeIndices
	List_1_t1440998580 * ____changeIndices_10;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32> Segmenter::<changeIndices>k__BackingField
	ReadOnlyCollection_1_t2257663140 * ___U3CchangeIndicesU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_analysis_0() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___analysis_0)); }
	inline AnalysisData_t108342674 * get_analysis_0() const { return ___analysis_0; }
	inline AnalysisData_t108342674 ** get_address_of_analysis_0() { return &___analysis_0; }
	inline void set_analysis_0(AnalysisData_t108342674 * value)
	{
		___analysis_0 = value;
		Il2CppCodeGenWriteBarrier(&___analysis_0, value);
	}

	inline static int32_t get_offset_of_lastDif_1() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___lastDif_1)); }
	inline float get_lastDif_1() const { return ___lastDif_1; }
	inline float* get_address_of_lastDif_1() { return &___lastDif_1; }
	inline void set_lastDif_1(float value)
	{
		___lastDif_1 = value;
	}

	inline static int32_t get_offset_of_increaseStart_2() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___increaseStart_2)); }
	inline int32_t get_increaseStart_2() const { return ___increaseStart_2; }
	inline int32_t* get_address_of_increaseStart_2() { return &___increaseStart_2; }
	inline void set_increaseStart_2(int32_t value)
	{
		___increaseStart_2 = value;
	}

	inline static int32_t get_offset_of_increaseEnd_3() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___increaseEnd_3)); }
	inline int32_t get_increaseEnd_3() const { return ___increaseEnd_3; }
	inline int32_t* get_address_of_increaseEnd_3() { return &___increaseEnd_3; }
	inline void set_increaseEnd_3(int32_t value)
	{
		___increaseEnd_3 = value;
	}

	inline static int32_t get_offset_of_increaseDetected_4() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___increaseDetected_4)); }
	inline bool get_increaseDetected_4() const { return ___increaseDetected_4; }
	inline bool* get_address_of_increaseDetected_4() { return &___increaseDetected_4; }
	inline void set_increaseDetected_4(bool value)
	{
		___increaseDetected_4 = value;
	}

	inline static int32_t get_offset_of_decreaseStart_5() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___decreaseStart_5)); }
	inline int32_t get_decreaseStart_5() const { return ___decreaseStart_5; }
	inline int32_t* get_address_of_decreaseStart_5() { return &___decreaseStart_5; }
	inline void set_decreaseStart_5(int32_t value)
	{
		___decreaseStart_5 = value;
	}

	inline static int32_t get_offset_of_decreaseEnd_6() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___decreaseEnd_6)); }
	inline int32_t get_decreaseEnd_6() const { return ___decreaseEnd_6; }
	inline int32_t* get_address_of_decreaseEnd_6() { return &___decreaseEnd_6; }
	inline void set_decreaseEnd_6(int32_t value)
	{
		___decreaseEnd_6 = value;
	}

	inline static int32_t get_offset_of_decreaseDetected_7() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___decreaseDetected_7)); }
	inline bool get_decreaseDetected_7() const { return ___decreaseDetected_7; }
	inline bool* get_address_of_decreaseDetected_7() { return &___decreaseDetected_7; }
	inline void set_decreaseDetected_7(bool value)
	{
		___decreaseDetected_7 = value;
	}

	inline static int32_t get_offset_of__changes_8() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ____changes_8)); }
	inline Dictionary_2_t1084335567 * get__changes_8() const { return ____changes_8; }
	inline Dictionary_2_t1084335567 ** get_address_of__changes_8() { return &____changes_8; }
	inline void set__changes_8(Dictionary_2_t1084335567 * value)
	{
		____changes_8 = value;
		Il2CppCodeGenWriteBarrier(&____changes_8, value);
	}

	inline static int32_t get_offset_of_U3CchangesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___U3CchangesU3Ek__BackingField_9)); }
	inline ReadOnlyDictionary_2_t1371992995 * get_U3CchangesU3Ek__BackingField_9() const { return ___U3CchangesU3Ek__BackingField_9; }
	inline ReadOnlyDictionary_2_t1371992995 ** get_address_of_U3CchangesU3Ek__BackingField_9() { return &___U3CchangesU3Ek__BackingField_9; }
	inline void set_U3CchangesU3Ek__BackingField_9(ReadOnlyDictionary_2_t1371992995 * value)
	{
		___U3CchangesU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchangesU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of__changeIndices_10() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ____changeIndices_10)); }
	inline List_1_t1440998580 * get__changeIndices_10() const { return ____changeIndices_10; }
	inline List_1_t1440998580 ** get_address_of__changeIndices_10() { return &____changeIndices_10; }
	inline void set__changeIndices_10(List_1_t1440998580 * value)
	{
		____changeIndices_10 = value;
		Il2CppCodeGenWriteBarrier(&____changeIndices_10, value);
	}

	inline static int32_t get_offset_of_U3CchangeIndicesU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Segmenter_t2695296026, ___U3CchangeIndicesU3Ek__BackingField_11)); }
	inline ReadOnlyCollection_1_t2257663140 * get_U3CchangeIndicesU3Ek__BackingField_11() const { return ___U3CchangeIndicesU3Ek__BackingField_11; }
	inline ReadOnlyCollection_1_t2257663140 ** get_address_of_U3CchangeIndicesU3Ek__BackingField_11() { return &___U3CchangeIndicesU3Ek__BackingField_11; }
	inline void set_U3CchangeIndicesU3Ek__BackingField_11(ReadOnlyCollection_1_t2257663140 * value)
	{
		___U3CchangeIndicesU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchangeIndicesU3Ek__BackingField_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
