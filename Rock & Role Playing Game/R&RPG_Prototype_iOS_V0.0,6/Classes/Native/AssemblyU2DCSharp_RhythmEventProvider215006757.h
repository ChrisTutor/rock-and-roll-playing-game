﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<RhythmEventProvider>
struct ReadOnlyCollection_1_t400792449;
// RhythmEventProvider/BeatEvent
struct BeatEvent_t33541086;
// RhythmEventProvider/SubBeatEvent
struct SubBeatEvent_t2499915164;
// RhythmEventProvider/OnsetEvent
struct OnsetEvent_t1329939269;
// RhythmEventProvider/ChangeEvent
struct ChangeEvent_t1135638418;
// RhythmEventProvider/TimingUpdateEvent
struct TimingUpdateEvent_t543213975;
// RhythmEventProvider/FrameChangedEvent
struct FrameChangedEvent_t800671821;
// RhythmEventProvider/OnNewSong
struct OnNewSong_t4169165798;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.Collections.Generic.List`1<RhythmEventProvider>
struct List_1_t3879095185;
// System.Action`1<RhythmEventProvider>
struct Action_1_t16806139;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RhythmEventProvider
struct  RhythmEventProvider_t215006757  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 RhythmEventProvider::targetOffset
	int32_t ___targetOffset_3;
	// System.Int32 RhythmEventProvider::offset
	int32_t ___offset_4;
	// System.Single RhythmEventProvider::lastBeatTime
	float ___lastBeatTime_5;
	// System.Int32 RhythmEventProvider::currentFrame
	int32_t ___currentFrame_6;
	// System.Single RhythmEventProvider::interpolation
	float ___interpolation_7;
	// System.Int32 RhythmEventProvider::totalFrames
	int32_t ___totalFrames_8;
	// RhythmEventProvider/BeatEvent RhythmEventProvider::onBeat
	BeatEvent_t33541086 * ___onBeat_9;
	// RhythmEventProvider/SubBeatEvent RhythmEventProvider::onSubBeat
	SubBeatEvent_t2499915164 * ___onSubBeat_10;
	// RhythmEventProvider/OnsetEvent RhythmEventProvider::onOnset
	OnsetEvent_t1329939269 * ___onOnset_11;
	// RhythmEventProvider/ChangeEvent RhythmEventProvider::onChange
	ChangeEvent_t1135638418 * ___onChange_12;
	// RhythmEventProvider/TimingUpdateEvent RhythmEventProvider::timingUpdate
	TimingUpdateEvent_t543213975 * ___timingUpdate_13;
	// RhythmEventProvider/FrameChangedEvent RhythmEventProvider::onFrameChanged
	FrameChangedEvent_t800671821 * ___onFrameChanged_14;
	// RhythmEventProvider/OnNewSong RhythmEventProvider::onSongLoaded
	OnNewSong_t4169165798 * ___onSongLoaded_15;
	// UnityEngine.Events.UnityEvent RhythmEventProvider::onSongEnded
	UnityEvent_t408735097 * ___onSongEnded_16;

public:
	inline static int32_t get_offset_of_targetOffset_3() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___targetOffset_3)); }
	inline int32_t get_targetOffset_3() const { return ___targetOffset_3; }
	inline int32_t* get_address_of_targetOffset_3() { return &___targetOffset_3; }
	inline void set_targetOffset_3(int32_t value)
	{
		___targetOffset_3 = value;
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___offset_4)); }
	inline int32_t get_offset_4() const { return ___offset_4; }
	inline int32_t* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(int32_t value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_lastBeatTime_5() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___lastBeatTime_5)); }
	inline float get_lastBeatTime_5() const { return ___lastBeatTime_5; }
	inline float* get_address_of_lastBeatTime_5() { return &___lastBeatTime_5; }
	inline void set_lastBeatTime_5(float value)
	{
		___lastBeatTime_5 = value;
	}

	inline static int32_t get_offset_of_currentFrame_6() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___currentFrame_6)); }
	inline int32_t get_currentFrame_6() const { return ___currentFrame_6; }
	inline int32_t* get_address_of_currentFrame_6() { return &___currentFrame_6; }
	inline void set_currentFrame_6(int32_t value)
	{
		___currentFrame_6 = value;
	}

	inline static int32_t get_offset_of_interpolation_7() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___interpolation_7)); }
	inline float get_interpolation_7() const { return ___interpolation_7; }
	inline float* get_address_of_interpolation_7() { return &___interpolation_7; }
	inline void set_interpolation_7(float value)
	{
		___interpolation_7 = value;
	}

	inline static int32_t get_offset_of_totalFrames_8() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___totalFrames_8)); }
	inline int32_t get_totalFrames_8() const { return ___totalFrames_8; }
	inline int32_t* get_address_of_totalFrames_8() { return &___totalFrames_8; }
	inline void set_totalFrames_8(int32_t value)
	{
		___totalFrames_8 = value;
	}

	inline static int32_t get_offset_of_onBeat_9() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___onBeat_9)); }
	inline BeatEvent_t33541086 * get_onBeat_9() const { return ___onBeat_9; }
	inline BeatEvent_t33541086 ** get_address_of_onBeat_9() { return &___onBeat_9; }
	inline void set_onBeat_9(BeatEvent_t33541086 * value)
	{
		___onBeat_9 = value;
		Il2CppCodeGenWriteBarrier(&___onBeat_9, value);
	}

	inline static int32_t get_offset_of_onSubBeat_10() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___onSubBeat_10)); }
	inline SubBeatEvent_t2499915164 * get_onSubBeat_10() const { return ___onSubBeat_10; }
	inline SubBeatEvent_t2499915164 ** get_address_of_onSubBeat_10() { return &___onSubBeat_10; }
	inline void set_onSubBeat_10(SubBeatEvent_t2499915164 * value)
	{
		___onSubBeat_10 = value;
		Il2CppCodeGenWriteBarrier(&___onSubBeat_10, value);
	}

	inline static int32_t get_offset_of_onOnset_11() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___onOnset_11)); }
	inline OnsetEvent_t1329939269 * get_onOnset_11() const { return ___onOnset_11; }
	inline OnsetEvent_t1329939269 ** get_address_of_onOnset_11() { return &___onOnset_11; }
	inline void set_onOnset_11(OnsetEvent_t1329939269 * value)
	{
		___onOnset_11 = value;
		Il2CppCodeGenWriteBarrier(&___onOnset_11, value);
	}

	inline static int32_t get_offset_of_onChange_12() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___onChange_12)); }
	inline ChangeEvent_t1135638418 * get_onChange_12() const { return ___onChange_12; }
	inline ChangeEvent_t1135638418 ** get_address_of_onChange_12() { return &___onChange_12; }
	inline void set_onChange_12(ChangeEvent_t1135638418 * value)
	{
		___onChange_12 = value;
		Il2CppCodeGenWriteBarrier(&___onChange_12, value);
	}

	inline static int32_t get_offset_of_timingUpdate_13() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___timingUpdate_13)); }
	inline TimingUpdateEvent_t543213975 * get_timingUpdate_13() const { return ___timingUpdate_13; }
	inline TimingUpdateEvent_t543213975 ** get_address_of_timingUpdate_13() { return &___timingUpdate_13; }
	inline void set_timingUpdate_13(TimingUpdateEvent_t543213975 * value)
	{
		___timingUpdate_13 = value;
		Il2CppCodeGenWriteBarrier(&___timingUpdate_13, value);
	}

	inline static int32_t get_offset_of_onFrameChanged_14() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___onFrameChanged_14)); }
	inline FrameChangedEvent_t800671821 * get_onFrameChanged_14() const { return ___onFrameChanged_14; }
	inline FrameChangedEvent_t800671821 ** get_address_of_onFrameChanged_14() { return &___onFrameChanged_14; }
	inline void set_onFrameChanged_14(FrameChangedEvent_t800671821 * value)
	{
		___onFrameChanged_14 = value;
		Il2CppCodeGenWriteBarrier(&___onFrameChanged_14, value);
	}

	inline static int32_t get_offset_of_onSongLoaded_15() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___onSongLoaded_15)); }
	inline OnNewSong_t4169165798 * get_onSongLoaded_15() const { return ___onSongLoaded_15; }
	inline OnNewSong_t4169165798 ** get_address_of_onSongLoaded_15() { return &___onSongLoaded_15; }
	inline void set_onSongLoaded_15(OnNewSong_t4169165798 * value)
	{
		___onSongLoaded_15 = value;
		Il2CppCodeGenWriteBarrier(&___onSongLoaded_15, value);
	}

	inline static int32_t get_offset_of_onSongEnded_16() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757, ___onSongEnded_16)); }
	inline UnityEvent_t408735097 * get_onSongEnded_16() const { return ___onSongEnded_16; }
	inline UnityEvent_t408735097 ** get_address_of_onSongEnded_16() { return &___onSongEnded_16; }
	inline void set_onSongEnded_16(UnityEvent_t408735097 * value)
	{
		___onSongEnded_16 = value;
		Il2CppCodeGenWriteBarrier(&___onSongEnded_16, value);
	}
};

struct RhythmEventProvider_t215006757_StaticFields
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<RhythmEventProvider> RhythmEventProvider::_eventProviders
	ReadOnlyCollection_1_t400792449 * ____eventProviders_2;
	// System.Collections.Generic.List`1<RhythmEventProvider> RhythmEventProvider::eventProviderList
	List_1_t3879095185 * ___eventProviderList_17;
	// System.Action`1<RhythmEventProvider> RhythmEventProvider::EventProviderEnabled
	Action_1_t16806139 * ___EventProviderEnabled_18;

public:
	inline static int32_t get_offset_of__eventProviders_2() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757_StaticFields, ____eventProviders_2)); }
	inline ReadOnlyCollection_1_t400792449 * get__eventProviders_2() const { return ____eventProviders_2; }
	inline ReadOnlyCollection_1_t400792449 ** get_address_of__eventProviders_2() { return &____eventProviders_2; }
	inline void set__eventProviders_2(ReadOnlyCollection_1_t400792449 * value)
	{
		____eventProviders_2 = value;
		Il2CppCodeGenWriteBarrier(&____eventProviders_2, value);
	}

	inline static int32_t get_offset_of_eventProviderList_17() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757_StaticFields, ___eventProviderList_17)); }
	inline List_1_t3879095185 * get_eventProviderList_17() const { return ___eventProviderList_17; }
	inline List_1_t3879095185 ** get_address_of_eventProviderList_17() { return &___eventProviderList_17; }
	inline void set_eventProviderList_17(List_1_t3879095185 * value)
	{
		___eventProviderList_17 = value;
		Il2CppCodeGenWriteBarrier(&___eventProviderList_17, value);
	}

	inline static int32_t get_offset_of_EventProviderEnabled_18() { return static_cast<int32_t>(offsetof(RhythmEventProvider_t215006757_StaticFields, ___EventProviderEnabled_18)); }
	inline Action_1_t16806139 * get_EventProviderEnabled_18() const { return ___EventProviderEnabled_18; }
	inline Action_1_t16806139 ** get_address_of_EventProviderEnabled_18() { return &___EventProviderEnabled_18; }
	inline void set_EventProviderEnabled_18(Action_1_t16806139 * value)
	{
		___EventProviderEnabled_18 = value;
		Il2CppCodeGenWriteBarrier(&___EventProviderEnabled_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
