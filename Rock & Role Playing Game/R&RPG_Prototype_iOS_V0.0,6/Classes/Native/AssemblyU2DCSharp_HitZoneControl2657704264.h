﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayerHealthManager
struct PlayerHealthManager_t3067865410;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HitZoneControl
struct  HitZoneControl_t2657704264  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean HitZoneControl::m_isInZone
	bool ___m_isInZone_2;
	// UnityEngine.GameObject HitZoneControl::m_targetEnemy
	GameObject_t1756533147 * ___m_targetEnemy_3;
	// PlayerHealthManager HitZoneControl::playerHealthManager
	PlayerHealthManager_t3067865410 * ___playerHealthManager_4;

public:
	inline static int32_t get_offset_of_m_isInZone_2() { return static_cast<int32_t>(offsetof(HitZoneControl_t2657704264, ___m_isInZone_2)); }
	inline bool get_m_isInZone_2() const { return ___m_isInZone_2; }
	inline bool* get_address_of_m_isInZone_2() { return &___m_isInZone_2; }
	inline void set_m_isInZone_2(bool value)
	{
		___m_isInZone_2 = value;
	}

	inline static int32_t get_offset_of_m_targetEnemy_3() { return static_cast<int32_t>(offsetof(HitZoneControl_t2657704264, ___m_targetEnemy_3)); }
	inline GameObject_t1756533147 * get_m_targetEnemy_3() const { return ___m_targetEnemy_3; }
	inline GameObject_t1756533147 ** get_address_of_m_targetEnemy_3() { return &___m_targetEnemy_3; }
	inline void set_m_targetEnemy_3(GameObject_t1756533147 * value)
	{
		___m_targetEnemy_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_targetEnemy_3, value);
	}

	inline static int32_t get_offset_of_playerHealthManager_4() { return static_cast<int32_t>(offsetof(HitZoneControl_t2657704264, ___playerHealthManager_4)); }
	inline PlayerHealthManager_t3067865410 * get_playerHealthManager_4() const { return ___playerHealthManager_4; }
	inline PlayerHealthManager_t3067865410 ** get_address_of_playerHealthManager_4() { return &___playerHealthManager_4; }
	inline void set_playerHealthManager_4(PlayerHealthManager_t3067865410 * value)
	{
		___playerHealthManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerHealthManager_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
