﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// RhythmTool
struct RhythmTool_t215962618;
// RhythmEventProvider
struct RhythmEventProvider_t215006757;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t1301679762;
// System.Collections.Generic.List`1<Line>
struct List_1_t2098562634;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>
struct ReadOnlyCollection_1_t2262295624;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VisualizerController
struct  VisualizerController_t119580760  : public MonoBehaviour_t1158329972
{
public:
	// RhythmTool VisualizerController::rhythmTool
	RhythmTool_t215962618 * ___rhythmTool_2;
	// RhythmEventProvider VisualizerController::eventProvider
	RhythmEventProvider_t215006757 * ___eventProvider_3;
	// UnityEngine.UI.Text VisualizerController::bpmText
	Text_t356221433 * ___bpmText_4;
	// UnityEngine.GameObject VisualizerController::linePrefab
	GameObject_t1756533147 * ___linePrefab_5;
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> VisualizerController::audioClips
	List_1_t1301679762 * ___audioClips_6;
	// System.Collections.Generic.List`1<Line> VisualizerController::lines
	List_1_t2098562634 * ___lines_7;
	// System.Int32 VisualizerController::currentSong
	int32_t ___currentSong_8;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> VisualizerController::magnitudeSmooth
	ReadOnlyCollection_1_t2262295624 * ___magnitudeSmooth_9;

public:
	inline static int32_t get_offset_of_rhythmTool_2() { return static_cast<int32_t>(offsetof(VisualizerController_t119580760, ___rhythmTool_2)); }
	inline RhythmTool_t215962618 * get_rhythmTool_2() const { return ___rhythmTool_2; }
	inline RhythmTool_t215962618 ** get_address_of_rhythmTool_2() { return &___rhythmTool_2; }
	inline void set_rhythmTool_2(RhythmTool_t215962618 * value)
	{
		___rhythmTool_2 = value;
		Il2CppCodeGenWriteBarrier(&___rhythmTool_2, value);
	}

	inline static int32_t get_offset_of_eventProvider_3() { return static_cast<int32_t>(offsetof(VisualizerController_t119580760, ___eventProvider_3)); }
	inline RhythmEventProvider_t215006757 * get_eventProvider_3() const { return ___eventProvider_3; }
	inline RhythmEventProvider_t215006757 ** get_address_of_eventProvider_3() { return &___eventProvider_3; }
	inline void set_eventProvider_3(RhythmEventProvider_t215006757 * value)
	{
		___eventProvider_3 = value;
		Il2CppCodeGenWriteBarrier(&___eventProvider_3, value);
	}

	inline static int32_t get_offset_of_bpmText_4() { return static_cast<int32_t>(offsetof(VisualizerController_t119580760, ___bpmText_4)); }
	inline Text_t356221433 * get_bpmText_4() const { return ___bpmText_4; }
	inline Text_t356221433 ** get_address_of_bpmText_4() { return &___bpmText_4; }
	inline void set_bpmText_4(Text_t356221433 * value)
	{
		___bpmText_4 = value;
		Il2CppCodeGenWriteBarrier(&___bpmText_4, value);
	}

	inline static int32_t get_offset_of_linePrefab_5() { return static_cast<int32_t>(offsetof(VisualizerController_t119580760, ___linePrefab_5)); }
	inline GameObject_t1756533147 * get_linePrefab_5() const { return ___linePrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_linePrefab_5() { return &___linePrefab_5; }
	inline void set_linePrefab_5(GameObject_t1756533147 * value)
	{
		___linePrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___linePrefab_5, value);
	}

	inline static int32_t get_offset_of_audioClips_6() { return static_cast<int32_t>(offsetof(VisualizerController_t119580760, ___audioClips_6)); }
	inline List_1_t1301679762 * get_audioClips_6() const { return ___audioClips_6; }
	inline List_1_t1301679762 ** get_address_of_audioClips_6() { return &___audioClips_6; }
	inline void set_audioClips_6(List_1_t1301679762 * value)
	{
		___audioClips_6 = value;
		Il2CppCodeGenWriteBarrier(&___audioClips_6, value);
	}

	inline static int32_t get_offset_of_lines_7() { return static_cast<int32_t>(offsetof(VisualizerController_t119580760, ___lines_7)); }
	inline List_1_t2098562634 * get_lines_7() const { return ___lines_7; }
	inline List_1_t2098562634 ** get_address_of_lines_7() { return &___lines_7; }
	inline void set_lines_7(List_1_t2098562634 * value)
	{
		___lines_7 = value;
		Il2CppCodeGenWriteBarrier(&___lines_7, value);
	}

	inline static int32_t get_offset_of_currentSong_8() { return static_cast<int32_t>(offsetof(VisualizerController_t119580760, ___currentSong_8)); }
	inline int32_t get_currentSong_8() const { return ___currentSong_8; }
	inline int32_t* get_address_of_currentSong_8() { return &___currentSong_8; }
	inline void set_currentSong_8(int32_t value)
	{
		___currentSong_8 = value;
	}

	inline static int32_t get_offset_of_magnitudeSmooth_9() { return static_cast<int32_t>(offsetof(VisualizerController_t119580760, ___magnitudeSmooth_9)); }
	inline ReadOnlyCollection_1_t2262295624 * get_magnitudeSmooth_9() const { return ___magnitudeSmooth_9; }
	inline ReadOnlyCollection_1_t2262295624 ** get_address_of_magnitudeSmooth_9() { return &___magnitudeSmooth_9; }
	inline void set_magnitudeSmooth_9(ReadOnlyCollection_1_t2262295624 * value)
	{
		___magnitudeSmooth_9 = value;
		Il2CppCodeGenWriteBarrier(&___magnitudeSmooth_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
