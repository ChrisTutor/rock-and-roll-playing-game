﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// RhythmTool
struct RhythmTool_t215962618;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// AnalysisData
struct AnalysisData_t108342674;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tutorial_1_Controller
struct  Tutorial_1_Controller_t3740746359  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Tutorial_1_Controller::debugLineOffset
	float ___debugLineOffset_2;
	// RhythmTool Tutorial_1_Controller::rhythmTool
	RhythmTool_t215962618 * ___rhythmTool_3;
	// UnityEngine.AudioClip Tutorial_1_Controller::audioClip
	AudioClip_t1932558630 * ___audioClip_4;
	// AnalysisData Tutorial_1_Controller::low
	AnalysisData_t108342674 * ___low_5;
	// UnityEngine.Vector3 Tutorial_1_Controller::startPosition
	Vector3_t2243707580  ___startPosition_6;
	// UnityEngine.Vector3 Tutorial_1_Controller::endPosition
	Vector3_t2243707580  ___endPosition_7;
	// UnityEngine.GameObject Tutorial_1_Controller::sphere
	GameObject_t1756533147 * ___sphere_8;
	// UnityEngine.GameObject Tutorial_1_Controller::enemyObj
	GameObject_t1756533147 * ___enemyObj_9;
	// UnityEngine.GameObject[] Tutorial_1_Controller::enemyArray
	GameObjectU5BU5D_t3057952154* ___enemyArray_10;

public:
	inline static int32_t get_offset_of_debugLineOffset_2() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___debugLineOffset_2)); }
	inline float get_debugLineOffset_2() const { return ___debugLineOffset_2; }
	inline float* get_address_of_debugLineOffset_2() { return &___debugLineOffset_2; }
	inline void set_debugLineOffset_2(float value)
	{
		___debugLineOffset_2 = value;
	}

	inline static int32_t get_offset_of_rhythmTool_3() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___rhythmTool_3)); }
	inline RhythmTool_t215962618 * get_rhythmTool_3() const { return ___rhythmTool_3; }
	inline RhythmTool_t215962618 ** get_address_of_rhythmTool_3() { return &___rhythmTool_3; }
	inline void set_rhythmTool_3(RhythmTool_t215962618 * value)
	{
		___rhythmTool_3 = value;
		Il2CppCodeGenWriteBarrier(&___rhythmTool_3, value);
	}

	inline static int32_t get_offset_of_audioClip_4() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___audioClip_4)); }
	inline AudioClip_t1932558630 * get_audioClip_4() const { return ___audioClip_4; }
	inline AudioClip_t1932558630 ** get_address_of_audioClip_4() { return &___audioClip_4; }
	inline void set_audioClip_4(AudioClip_t1932558630 * value)
	{
		___audioClip_4 = value;
		Il2CppCodeGenWriteBarrier(&___audioClip_4, value);
	}

	inline static int32_t get_offset_of_low_5() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___low_5)); }
	inline AnalysisData_t108342674 * get_low_5() const { return ___low_5; }
	inline AnalysisData_t108342674 ** get_address_of_low_5() { return &___low_5; }
	inline void set_low_5(AnalysisData_t108342674 * value)
	{
		___low_5 = value;
		Il2CppCodeGenWriteBarrier(&___low_5, value);
	}

	inline static int32_t get_offset_of_startPosition_6() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___startPosition_6)); }
	inline Vector3_t2243707580  get_startPosition_6() const { return ___startPosition_6; }
	inline Vector3_t2243707580 * get_address_of_startPosition_6() { return &___startPosition_6; }
	inline void set_startPosition_6(Vector3_t2243707580  value)
	{
		___startPosition_6 = value;
	}

	inline static int32_t get_offset_of_endPosition_7() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___endPosition_7)); }
	inline Vector3_t2243707580  get_endPosition_7() const { return ___endPosition_7; }
	inline Vector3_t2243707580 * get_address_of_endPosition_7() { return &___endPosition_7; }
	inline void set_endPosition_7(Vector3_t2243707580  value)
	{
		___endPosition_7 = value;
	}

	inline static int32_t get_offset_of_sphere_8() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___sphere_8)); }
	inline GameObject_t1756533147 * get_sphere_8() const { return ___sphere_8; }
	inline GameObject_t1756533147 ** get_address_of_sphere_8() { return &___sphere_8; }
	inline void set_sphere_8(GameObject_t1756533147 * value)
	{
		___sphere_8 = value;
		Il2CppCodeGenWriteBarrier(&___sphere_8, value);
	}

	inline static int32_t get_offset_of_enemyObj_9() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___enemyObj_9)); }
	inline GameObject_t1756533147 * get_enemyObj_9() const { return ___enemyObj_9; }
	inline GameObject_t1756533147 ** get_address_of_enemyObj_9() { return &___enemyObj_9; }
	inline void set_enemyObj_9(GameObject_t1756533147 * value)
	{
		___enemyObj_9 = value;
		Il2CppCodeGenWriteBarrier(&___enemyObj_9, value);
	}

	inline static int32_t get_offset_of_enemyArray_10() { return static_cast<int32_t>(offsetof(Tutorial_1_Controller_t3740746359, ___enemyArray_10)); }
	inline GameObjectU5BU5D_t3057952154* get_enemyArray_10() const { return ___enemyArray_10; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_enemyArray_10() { return &___enemyArray_10; }
	inline void set_enemyArray_10(GameObjectU5BU5D_t3057952154* value)
	{
		___enemyArray_10 = value;
		Il2CppCodeGenWriteBarrier(&___enemyArray_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
