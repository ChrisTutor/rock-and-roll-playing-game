﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen1810603002.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RhythmEventProvider/RhythmEvent`4<System.Int32,System.Single,System.Single,System.Single>
struct  RhythmEvent_4_t3330968721  : public UnityEvent_4_t1810603002
{
public:
	// System.Int32 RhythmEventProvider/RhythmEvent`4::_listenerCount
	int32_t ____listenerCount_5;

public:
	inline static int32_t get_offset_of__listenerCount_5() { return static_cast<int32_t>(offsetof(RhythmEvent_4_t3330968721, ____listenerCount_5)); }
	inline int32_t get__listenerCount_5() const { return ____listenerCount_5; }
	inline int32_t* get_address_of__listenerCount_5() { return &____listenerCount_5; }
	inline void set__listenerCount_5(int32_t value)
	{
		____listenerCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
