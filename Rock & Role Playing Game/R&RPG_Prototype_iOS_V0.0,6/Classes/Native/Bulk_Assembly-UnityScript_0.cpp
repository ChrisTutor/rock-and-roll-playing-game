﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_animation_keys385740455.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"

// animation_keys
struct animation_keys_t385740455;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.String
struct String_t;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Animation
struct Animation_t2068071072;
// System.Object
struct Il2CppObject;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m2503703020_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029325;
extern Il2CppCodeGenString* _stringLiteral2309168105;
extern Il2CppCodeGenString* _stringLiteral372029328;
extern Il2CppCodeGenString* _stringLiteral4151059802;
extern Il2CppCodeGenString* _stringLiteral372029327;
extern Il2CppCodeGenString* _stringLiteral2778557961;
extern Il2CppCodeGenString* _stringLiteral372029322;
extern Il2CppCodeGenString* _stringLiteral842947042;
extern Il2CppCodeGenString* _stringLiteral1266176052;
extern const uint32_t animation_keys_Update_m2743143518_MetadataUsageId;



// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKey(System.String)
extern "C"  bool Input_GetKey_m1856484952 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t2068071072_m2503703020(__this, method) ((  Animation_t2068071072 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C"  bool Animation_Play_m976361057 (Animation_t2068071072 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void animation_keys::.ctor()
extern "C"  void animation_keys__ctor_m444109155 (animation_keys_t385740455 * __this, const MethodInfo* method)
{
	{
		// 
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void animation_keys::Update()
extern "C"  void animation_keys_Update_m2743143518 (animation_keys_t385740455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (animation_keys_Update_m2743143518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetKey("1"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1856484952(NULL /*static, unused*/, _stringLiteral372029325, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		// GetComponent.<Animation>().Play ("run");}
		Animation_t2068071072 * L_1 = Component_GetComponent_TisAnimation_t2068071072_m2503703020(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m2503703020_MethodInfo_var);
		NullCheck(L_1);
		Animation_Play_m976361057(L_1, _stringLiteral2309168105, /*hidden argument*/NULL);
		goto IL_00a5;
	}

IL_0025:
	{
		// else if(Input.GetKey("2"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m1856484952(NULL /*static, unused*/, _stringLiteral372029328, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		// GetComponent.<Animation>().Play ("attack");}
		Animation_t2068071072 * L_3 = Component_GetComponent_TisAnimation_t2068071072_m2503703020(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m2503703020_MethodInfo_var);
		NullCheck(L_3);
		Animation_Play_m976361057(L_3, _stringLiteral4151059802, /*hidden argument*/NULL);
		goto IL_00a5;
	}

IL_004a:
	{
		// else if(Input.GetKey("3"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKey_m1856484952(NULL /*static, unused*/, _stringLiteral372029327, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006f;
		}
	}
	{
		// GetComponent.<Animation>().Play ("walk");}
		Animation_t2068071072 * L_5 = Component_GetComponent_TisAnimation_t2068071072_m2503703020(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m2503703020_MethodInfo_var);
		NullCheck(L_5);
		Animation_Play_m976361057(L_5, _stringLiteral2778557961, /*hidden argument*/NULL);
		goto IL_00a5;
	}

IL_006f:
	{
		// else if(Input.GetKey("4"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetKey_m1856484952(NULL /*static, unused*/, _stringLiteral372029322, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0094;
		}
	}
	{
		// GetComponent.<Animation>().Play ("jump");}
		Animation_t2068071072 * L_7 = Component_GetComponent_TisAnimation_t2068071072_m2503703020(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m2503703020_MethodInfo_var);
		NullCheck(L_7);
		Animation_Play_m976361057(L_7, _stringLiteral842947042, /*hidden argument*/NULL);
		goto IL_00a5;
	}

IL_0094:
	{
		// GetComponent.<Animation>().Play ("idle");}
		Animation_t2068071072 * L_8 = Component_GetComponent_TisAnimation_t2068071072_m2503703020(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m2503703020_MethodInfo_var);
		NullCheck(L_8);
		Animation_Play_m976361057(L_8, _stringLiteral1266176052, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Void animation_keys::Main()
extern "C"  void animation_keys_Main_m1736024698 (animation_keys_t385740455 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
