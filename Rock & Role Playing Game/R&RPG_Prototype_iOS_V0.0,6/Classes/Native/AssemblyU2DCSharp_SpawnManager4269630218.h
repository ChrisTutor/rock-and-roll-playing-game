﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpawnManager
struct  SpawnManager_t4269630218  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] SpawnManager::m_spawnObjects
	GameObjectU5BU5D_t3057952154* ___m_spawnObjects_2;
	// UnityEngine.GameObject[] SpawnManager::m_enemySpawn
	GameObjectU5BU5D_t3057952154* ___m_enemySpawn_3;
	// UnityEngine.GameObject SpawnManager::enemySpawn
	GameObject_t1756533147 * ___enemySpawn_4;
	// System.Int32 SpawnManager::beatCount
	int32_t ___beatCount_5;
	// System.Boolean SpawnManager::high
	bool ___high_6;
	// System.Boolean SpawnManager::mid
	bool ___mid_7;
	// System.Boolean SpawnManager::low
	bool ___low_8;

public:
	inline static int32_t get_offset_of_m_spawnObjects_2() { return static_cast<int32_t>(offsetof(SpawnManager_t4269630218, ___m_spawnObjects_2)); }
	inline GameObjectU5BU5D_t3057952154* get_m_spawnObjects_2() const { return ___m_spawnObjects_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_spawnObjects_2() { return &___m_spawnObjects_2; }
	inline void set_m_spawnObjects_2(GameObjectU5BU5D_t3057952154* value)
	{
		___m_spawnObjects_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_spawnObjects_2, value);
	}

	inline static int32_t get_offset_of_m_enemySpawn_3() { return static_cast<int32_t>(offsetof(SpawnManager_t4269630218, ___m_enemySpawn_3)); }
	inline GameObjectU5BU5D_t3057952154* get_m_enemySpawn_3() const { return ___m_enemySpawn_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_enemySpawn_3() { return &___m_enemySpawn_3; }
	inline void set_m_enemySpawn_3(GameObjectU5BU5D_t3057952154* value)
	{
		___m_enemySpawn_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_enemySpawn_3, value);
	}

	inline static int32_t get_offset_of_enemySpawn_4() { return static_cast<int32_t>(offsetof(SpawnManager_t4269630218, ___enemySpawn_4)); }
	inline GameObject_t1756533147 * get_enemySpawn_4() const { return ___enemySpawn_4; }
	inline GameObject_t1756533147 ** get_address_of_enemySpawn_4() { return &___enemySpawn_4; }
	inline void set_enemySpawn_4(GameObject_t1756533147 * value)
	{
		___enemySpawn_4 = value;
		Il2CppCodeGenWriteBarrier(&___enemySpawn_4, value);
	}

	inline static int32_t get_offset_of_beatCount_5() { return static_cast<int32_t>(offsetof(SpawnManager_t4269630218, ___beatCount_5)); }
	inline int32_t get_beatCount_5() const { return ___beatCount_5; }
	inline int32_t* get_address_of_beatCount_5() { return &___beatCount_5; }
	inline void set_beatCount_5(int32_t value)
	{
		___beatCount_5 = value;
	}

	inline static int32_t get_offset_of_high_6() { return static_cast<int32_t>(offsetof(SpawnManager_t4269630218, ___high_6)); }
	inline bool get_high_6() const { return ___high_6; }
	inline bool* get_address_of_high_6() { return &___high_6; }
	inline void set_high_6(bool value)
	{
		___high_6 = value;
	}

	inline static int32_t get_offset_of_mid_7() { return static_cast<int32_t>(offsetof(SpawnManager_t4269630218, ___mid_7)); }
	inline bool get_mid_7() const { return ___mid_7; }
	inline bool* get_address_of_mid_7() { return &___mid_7; }
	inline void set_mid_7(bool value)
	{
		___mid_7 = value;
	}

	inline static int32_t get_offset_of_low_8() { return static_cast<int32_t>(offsetof(SpawnManager_t4269630218, ___low_8)); }
	inline bool get_low_8() const { return ___low_8; }
	inline bool* get_address_of_low_8() { return &___low_8; }
	inline void set_low_8(bool value)
	{
		___low_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
