﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// RhythmTool
struct RhythmTool_t215962618;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// PlayerHealthManager
struct PlayerHealthManager_t3067865410;
// UnityEngine.Canvas
struct Canvas_t209405766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// RhythmTool GameManager::m_rhythmTool
	RhythmTool_t215962618 * ___m_rhythmTool_2;
	// UnityEngine.AudioClip GameManager::m_audioClip
	AudioClip_t1932558630 * ___m_audioClip_3;
	// PlayerHealthManager GameManager::playerHealthManager
	PlayerHealthManager_t3067865410 * ___playerHealthManager_4;
	// UnityEngine.Canvas GameManager::failScreenCanvas
	Canvas_t209405766 * ___failScreenCanvas_5;

public:
	inline static int32_t get_offset_of_m_rhythmTool_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___m_rhythmTool_2)); }
	inline RhythmTool_t215962618 * get_m_rhythmTool_2() const { return ___m_rhythmTool_2; }
	inline RhythmTool_t215962618 ** get_address_of_m_rhythmTool_2() { return &___m_rhythmTool_2; }
	inline void set_m_rhythmTool_2(RhythmTool_t215962618 * value)
	{
		___m_rhythmTool_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_rhythmTool_2, value);
	}

	inline static int32_t get_offset_of_m_audioClip_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___m_audioClip_3)); }
	inline AudioClip_t1932558630 * get_m_audioClip_3() const { return ___m_audioClip_3; }
	inline AudioClip_t1932558630 ** get_address_of_m_audioClip_3() { return &___m_audioClip_3; }
	inline void set_m_audioClip_3(AudioClip_t1932558630 * value)
	{
		___m_audioClip_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_audioClip_3, value);
	}

	inline static int32_t get_offset_of_playerHealthManager_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___playerHealthManager_4)); }
	inline PlayerHealthManager_t3067865410 * get_playerHealthManager_4() const { return ___playerHealthManager_4; }
	inline PlayerHealthManager_t3067865410 ** get_address_of_playerHealthManager_4() { return &___playerHealthManager_4; }
	inline void set_playerHealthManager_4(PlayerHealthManager_t3067865410 * value)
	{
		___playerHealthManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerHealthManager_4, value);
	}

	inline static int32_t get_offset_of_failScreenCanvas_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___failScreenCanvas_5)); }
	inline Canvas_t209405766 * get_failScreenCanvas_5() const { return ___failScreenCanvas_5; }
	inline Canvas_t209405766 ** get_address_of_failScreenCanvas_5() { return &___failScreenCanvas_5; }
	inline void set_failScreenCanvas_5(Canvas_t209405766 * value)
	{
		___failScreenCanvas_5 = value;
		Il2CppCodeGenWriteBarrier(&___failScreenCanvas_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
