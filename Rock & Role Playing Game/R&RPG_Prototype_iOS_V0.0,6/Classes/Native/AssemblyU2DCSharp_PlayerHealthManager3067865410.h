﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Slider
struct Slider_t297367283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerHealthManager
struct  PlayerHealthManager_t3067865410  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PlayerHealthManager::playerHealth
	float ___playerHealth_2;
	// System.Single PlayerHealthManager::playerMaxHealth
	float ___playerMaxHealth_3;
	// UnityEngine.UI.Slider PlayerHealthManager::playerHealthBar
	Slider_t297367283 * ___playerHealthBar_4;

public:
	inline static int32_t get_offset_of_playerHealth_2() { return static_cast<int32_t>(offsetof(PlayerHealthManager_t3067865410, ___playerHealth_2)); }
	inline float get_playerHealth_2() const { return ___playerHealth_2; }
	inline float* get_address_of_playerHealth_2() { return &___playerHealth_2; }
	inline void set_playerHealth_2(float value)
	{
		___playerHealth_2 = value;
	}

	inline static int32_t get_offset_of_playerMaxHealth_3() { return static_cast<int32_t>(offsetof(PlayerHealthManager_t3067865410, ___playerMaxHealth_3)); }
	inline float get_playerMaxHealth_3() const { return ___playerMaxHealth_3; }
	inline float* get_address_of_playerMaxHealth_3() { return &___playerMaxHealth_3; }
	inline void set_playerMaxHealth_3(float value)
	{
		___playerMaxHealth_3 = value;
	}

	inline static int32_t get_offset_of_playerHealthBar_4() { return static_cast<int32_t>(offsetof(PlayerHealthManager_t3067865410, ___playerHealthBar_4)); }
	inline Slider_t297367283 * get_playerHealthBar_4() const { return ___playerHealthBar_4; }
	inline Slider_t297367283 ** get_address_of_playerHealthBar_4() { return &___playerHealthBar_4; }
	inline void set_playerHealthBar_4(Slider_t297367283 * value)
	{
		___playerHealthBar_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerHealthBar_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
