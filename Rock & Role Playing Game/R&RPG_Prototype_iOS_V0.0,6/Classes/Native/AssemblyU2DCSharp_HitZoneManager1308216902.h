﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HitZoneManager
struct  HitZoneManager_t1308216902  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean HitZoneManager::m_green
	bool ___m_green_2;
	// System.Boolean HitZoneManager::m_red
	bool ___m_red_3;
	// System.Boolean HitZoneManager::m_yellow
	bool ___m_yellow_4;
	// System.Boolean HitZoneManager::m_blue
	bool ___m_blue_5;
	// UnityEngine.GameObject HitZoneManager::m_greenHitZone
	GameObject_t1756533147 * ___m_greenHitZone_6;
	// UnityEngine.GameObject HitZoneManager::m_redHitZone
	GameObject_t1756533147 * ___m_redHitZone_7;
	// UnityEngine.GameObject HitZoneManager::m_blueHitZone
	GameObject_t1756533147 * ___m_blueHitZone_8;
	// UnityEngine.GameObject HitZoneManager::m_yellowHitZone
	GameObject_t1756533147 * ___m_yellowHitZone_9;
	// UnityEngine.GameObject HitZoneManager::m_greenTarget
	GameObject_t1756533147 * ___m_greenTarget_10;
	// UnityEngine.GameObject HitZoneManager::m_redTarget
	GameObject_t1756533147 * ___m_redTarget_11;
	// UnityEngine.GameObject HitZoneManager::m_yellowTarget
	GameObject_t1756533147 * ___m_yellowTarget_12;
	// UnityEngine.GameObject HitZoneManager::m_blueTarget
	GameObject_t1756533147 * ___m_blueTarget_13;

public:
	inline static int32_t get_offset_of_m_green_2() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_green_2)); }
	inline bool get_m_green_2() const { return ___m_green_2; }
	inline bool* get_address_of_m_green_2() { return &___m_green_2; }
	inline void set_m_green_2(bool value)
	{
		___m_green_2 = value;
	}

	inline static int32_t get_offset_of_m_red_3() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_red_3)); }
	inline bool get_m_red_3() const { return ___m_red_3; }
	inline bool* get_address_of_m_red_3() { return &___m_red_3; }
	inline void set_m_red_3(bool value)
	{
		___m_red_3 = value;
	}

	inline static int32_t get_offset_of_m_yellow_4() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_yellow_4)); }
	inline bool get_m_yellow_4() const { return ___m_yellow_4; }
	inline bool* get_address_of_m_yellow_4() { return &___m_yellow_4; }
	inline void set_m_yellow_4(bool value)
	{
		___m_yellow_4 = value;
	}

	inline static int32_t get_offset_of_m_blue_5() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_blue_5)); }
	inline bool get_m_blue_5() const { return ___m_blue_5; }
	inline bool* get_address_of_m_blue_5() { return &___m_blue_5; }
	inline void set_m_blue_5(bool value)
	{
		___m_blue_5 = value;
	}

	inline static int32_t get_offset_of_m_greenHitZone_6() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_greenHitZone_6)); }
	inline GameObject_t1756533147 * get_m_greenHitZone_6() const { return ___m_greenHitZone_6; }
	inline GameObject_t1756533147 ** get_address_of_m_greenHitZone_6() { return &___m_greenHitZone_6; }
	inline void set_m_greenHitZone_6(GameObject_t1756533147 * value)
	{
		___m_greenHitZone_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_greenHitZone_6, value);
	}

	inline static int32_t get_offset_of_m_redHitZone_7() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_redHitZone_7)); }
	inline GameObject_t1756533147 * get_m_redHitZone_7() const { return ___m_redHitZone_7; }
	inline GameObject_t1756533147 ** get_address_of_m_redHitZone_7() { return &___m_redHitZone_7; }
	inline void set_m_redHitZone_7(GameObject_t1756533147 * value)
	{
		___m_redHitZone_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_redHitZone_7, value);
	}

	inline static int32_t get_offset_of_m_blueHitZone_8() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_blueHitZone_8)); }
	inline GameObject_t1756533147 * get_m_blueHitZone_8() const { return ___m_blueHitZone_8; }
	inline GameObject_t1756533147 ** get_address_of_m_blueHitZone_8() { return &___m_blueHitZone_8; }
	inline void set_m_blueHitZone_8(GameObject_t1756533147 * value)
	{
		___m_blueHitZone_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_blueHitZone_8, value);
	}

	inline static int32_t get_offset_of_m_yellowHitZone_9() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_yellowHitZone_9)); }
	inline GameObject_t1756533147 * get_m_yellowHitZone_9() const { return ___m_yellowHitZone_9; }
	inline GameObject_t1756533147 ** get_address_of_m_yellowHitZone_9() { return &___m_yellowHitZone_9; }
	inline void set_m_yellowHitZone_9(GameObject_t1756533147 * value)
	{
		___m_yellowHitZone_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_yellowHitZone_9, value);
	}

	inline static int32_t get_offset_of_m_greenTarget_10() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_greenTarget_10)); }
	inline GameObject_t1756533147 * get_m_greenTarget_10() const { return ___m_greenTarget_10; }
	inline GameObject_t1756533147 ** get_address_of_m_greenTarget_10() { return &___m_greenTarget_10; }
	inline void set_m_greenTarget_10(GameObject_t1756533147 * value)
	{
		___m_greenTarget_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_greenTarget_10, value);
	}

	inline static int32_t get_offset_of_m_redTarget_11() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_redTarget_11)); }
	inline GameObject_t1756533147 * get_m_redTarget_11() const { return ___m_redTarget_11; }
	inline GameObject_t1756533147 ** get_address_of_m_redTarget_11() { return &___m_redTarget_11; }
	inline void set_m_redTarget_11(GameObject_t1756533147 * value)
	{
		___m_redTarget_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_redTarget_11, value);
	}

	inline static int32_t get_offset_of_m_yellowTarget_12() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_yellowTarget_12)); }
	inline GameObject_t1756533147 * get_m_yellowTarget_12() const { return ___m_yellowTarget_12; }
	inline GameObject_t1756533147 ** get_address_of_m_yellowTarget_12() { return &___m_yellowTarget_12; }
	inline void set_m_yellowTarget_12(GameObject_t1756533147 * value)
	{
		___m_yellowTarget_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_yellowTarget_12, value);
	}

	inline static int32_t get_offset_of_m_blueTarget_13() { return static_cast<int32_t>(offsetof(HitZoneManager_t1308216902, ___m_blueTarget_13)); }
	inline GameObject_t1756533147 * get_m_blueTarget_13() const { return ___m_blueTarget_13; }
	inline GameObject_t1756533147 ** get_address_of_m_blueTarget_13() { return &___m_blueTarget_13; }
	inline void set_m_blueTarget_13(GameObject_t1756533147 * value)
	{
		___m_blueTarget_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_blueTarget_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
