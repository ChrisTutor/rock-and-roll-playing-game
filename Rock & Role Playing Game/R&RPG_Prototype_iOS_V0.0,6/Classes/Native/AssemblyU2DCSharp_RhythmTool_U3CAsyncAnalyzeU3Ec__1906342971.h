﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Threading.Thread
struct Thread_t241561612;
// RhythmTool
struct RhythmTool_t215962618;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RhythmTool/<AsyncAnalyze>c__Iterator1
struct  U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971  : public Il2CppObject
{
public:
	// System.Int32 RhythmTool/<AsyncAnalyze>c__Iterator1::frames
	int32_t ___frames_0;
	// System.Single[] RhythmTool/<AsyncAnalyze>c__Iterator1::<s>__0
	SingleU5BU5D_t577127397* ___U3CsU3E__0_1;
	// System.Threading.Thread RhythmTool/<AsyncAnalyze>c__Iterator1::<analyzeThread>__0
	Thread_t241561612 * ___U3CanalyzeThreadU3E__0_2;
	// RhythmTool RhythmTool/<AsyncAnalyze>c__Iterator1::$this
	RhythmTool_t215962618 * ___U24this_3;
	// System.Object RhythmTool/<AsyncAnalyze>c__Iterator1::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean RhythmTool/<AsyncAnalyze>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 RhythmTool/<AsyncAnalyze>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971, ___frames_0)); }
	inline int32_t get_frames_0() const { return ___frames_0; }
	inline int32_t* get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(int32_t value)
	{
		___frames_0 = value;
	}

	inline static int32_t get_offset_of_U3CsU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971, ___U3CsU3E__0_1)); }
	inline SingleU5BU5D_t577127397* get_U3CsU3E__0_1() const { return ___U3CsU3E__0_1; }
	inline SingleU5BU5D_t577127397** get_address_of_U3CsU3E__0_1() { return &___U3CsU3E__0_1; }
	inline void set_U3CsU3E__0_1(SingleU5BU5D_t577127397* value)
	{
		___U3CsU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CanalyzeThreadU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971, ___U3CanalyzeThreadU3E__0_2)); }
	inline Thread_t241561612 * get_U3CanalyzeThreadU3E__0_2() const { return ___U3CanalyzeThreadU3E__0_2; }
	inline Thread_t241561612 ** get_address_of_U3CanalyzeThreadU3E__0_2() { return &___U3CanalyzeThreadU3E__0_2; }
	inline void set_U3CanalyzeThreadU3E__0_2(Thread_t241561612 * value)
	{
		___U3CanalyzeThreadU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanalyzeThreadU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971, ___U24this_3)); }
	inline RhythmTool_t215962618 * get_U24this_3() const { return ___U24this_3; }
	inline RhythmTool_t215962618 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(RhythmTool_t215962618 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
