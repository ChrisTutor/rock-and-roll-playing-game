﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_BasicController2369989902.h"
#include "AssemblyU2DCSharp_DataController1918319064.h"
#include "AssemblyU2DCSharp_EventsController2543309541.h"
#include "AssemblyU2DCSharp_Line2729441502.h"
#include "AssemblyU2DCSharp_VisualizerController119580760.h"
#include "AssemblyU2DCSharp_Analysis439488098.h"
#include "AssemblyU2DCSharp_AnalysisData108342674.h"
#include "AssemblyU2DCSharp_Beat2695683572.h"
#include "AssemblyU2DCSharp_BeatTracker2801099156.h"
#include "AssemblyU2DCSharp_LomontFFT895069557.h"
#include "AssemblyU2DCSharp_Onset732596509.h"
#include "AssemblyU2DCSharp_OnsetType3680206797.h"
#include "AssemblyU2DCSharp_RhythmEventProvider215006757.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_BeatEvent33541086.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_SubBeatEvent2499915164.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_TimingUpdateE543213975.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_FrameChangedE800671821.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_OnsetEvent1329939269.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_ChangeEvent1135638418.h"
#include "AssemblyU2DCSharp_RhythmEventProvider_OnNewSong4169165798.h"
#include "AssemblyU2DCSharp_RhythmTool215962618.h"
#include "AssemblyU2DCSharp_RhythmTool_U3CQueueNewSongU3Ec__1486555536.h"
#include "AssemblyU2DCSharp_RhythmTool_U3CAsyncAnalyzeU3Ec__1906342971.h"
#include "AssemblyU2DCSharp_Segmenter2695296026.h"
#include "AssemblyU2DCSharp_SongData3132760915.h"
#include "AssemblyU2DCSharp_Util4006552276.h"
#include "AssemblyU2DCSharp_HitZoneControl2657704264.h"
#include "AssemblyU2DCSharp_HitZoneManager1308216902.h"
#include "AssemblyU2DCSharp_CameraMotor550925884.h"
#include "AssemblyU2DCSharp_PlayerMotor2528789646.h"
#include "AssemblyU2DCSharp_TileManager3422405329.h"
#include "AssemblyU2DCSharp_AttackButtonManager2228488667.h"
#include "AssemblyU2DCSharp_EnemyController2146768720.h"
#include "AssemblyU2DCSharp_FailScreenButtonControl1753294869.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_PlayerHealthManager3067865410.h"
#include "AssemblyU2DCSharp_PlayerUserControl2572554735.h"
#include "AssemblyU2DCSharp_SpawnManager4269630218.h"
#include "AssemblyU2DCSharp_Tutorial_1_Controller3740746359.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1701[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1702[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1708[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1710[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1711[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1712[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1714[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1716[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1717[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1718[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1729[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1730[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1736[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1741[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1749[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1753[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (BasicController_t2369989902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[2] = 
{
	BasicController_t2369989902::get_offset_of_rhythmTool_2(),
	BasicController_t2369989902::get_offset_of_audioClip_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (DataController_t1918319064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[3] = 
{
	DataController_t1918319064::get_offset_of_rhythmTool_2(),
	DataController_t1918319064::get_offset_of_audioClip_3(),
	DataController_t1918319064::get_offset_of_low_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (EventsController_t2543309541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[5] = 
{
	EventsController_t2543309541::get_offset_of_cube1Transform_2(),
	EventsController_t2543309541::get_offset_of_cube2Transform_3(),
	EventsController_t2543309541::get_offset_of_rhythmTool_4(),
	EventsController_t2543309541::get_offset_of_eventProvider_5(),
	EventsController_t2543309541::get_offset_of_audioClip_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (Line_t2729441502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[1] = 
{
	Line_t2729441502::get_offset_of_U3CindexU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (VisualizerController_t119580760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[8] = 
{
	VisualizerController_t119580760::get_offset_of_rhythmTool_2(),
	VisualizerController_t119580760::get_offset_of_eventProvider_3(),
	VisualizerController_t119580760::get_offset_of_bpmText_4(),
	VisualizerController_t119580760::get_offset_of_linePrefab_5(),
	VisualizerController_t119580760::get_offset_of_audioClips_6(),
	VisualizerController_t119580760::get_offset_of_lines_7(),
	VisualizerController_t119580760::get_offset_of_currentSong_8(),
	VisualizerController_t119580760::get_offset_of_magnitudeSmooth_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (Analysis_t439488098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[15] = 
{
	Analysis_t439488098::get_offset_of_U3CanalysisDataU3Ek__BackingField_0(),
	Analysis_t439488098::get_offset_of_U3CnameU3Ek__BackingField_1(),
	Analysis_t439488098::get_offset_of_onsetIndices_2(),
	Analysis_t439488098::get_offset_of__onsets_3(),
	Analysis_t439488098::get_offset_of__magnitude_4(),
	Analysis_t439488098::get_offset_of__magnitudeSmooth_5(),
	Analysis_t439488098::get_offset_of__flux_6(),
	Analysis_t439488098::get_offset_of__magnitudeAvg_7(),
	Analysis_t439488098::get_offset_of_t1_8(),
	Analysis_t439488098::get_offset_of_t2_9(),
	Analysis_t439488098::get_offset_of_p1_10(),
	Analysis_t439488098::get_offset_of_p2_11(),
	Analysis_t439488098::get_offset_of_start_12(),
	Analysis_t439488098::get_offset_of_end_13(),
	Analysis_t439488098::get_offset_of_totalFrames_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (AnalysisData_t108342674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[6] = 
{
	AnalysisData_t108342674::get_offset_of_U3CnameU3Ek__BackingField_0(),
	AnalysisData_t108342674::get_offset_of_U3CmagnitudeU3Ek__BackingField_1(),
	AnalysisData_t108342674::get_offset_of_U3CfluxU3Ek__BackingField_2(),
	AnalysisData_t108342674::get_offset_of_U3CmagnitudeSmoothU3Ek__BackingField_3(),
	AnalysisData_t108342674::get_offset_of_U3CmagnitudeAvgU3Ek__BackingField_4(),
	AnalysisData_t108342674::get_offset_of_U3ConsetsU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (Beat_t2695683572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[3] = 
{
	Beat_t2695683572::get_offset_of_length_0(),
	Beat_t2695683572::get_offset_of_bpm_1(),
	Beat_t2695683572::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (BeatTracker_t2801099156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[19] = 
{
	BeatTracker_t2801099156::get_offset_of_signalBuffer_0(),
	BeatTracker_t2801099156::get_offset_of_currentSignal_1(),
	BeatTracker_t2801099156::get_offset_of_upsampledSignal_2(),
	BeatTracker_t2801099156::get_offset_of_repetitionScore_3(),
	BeatTracker_t2801099156::get_offset_of_gapScore_4(),
	BeatTracker_t2801099156::get_offset_of_offsetScore_5(),
	BeatTracker_t2801099156::get_offset_of_peaks_6(),
	BeatTracker_t2801099156::get_offset_of_beatHistogram_7(),
	BeatTracker_t2801099156::get_offset_of_bestRepetition_8(),
	BeatTracker_t2801099156::get_offset_of_currentBeatLength_9(),
	BeatTracker_t2801099156::get_offset_of_syncOld_10(),
	BeatTracker_t2801099156::get_offset_of_sync_11(),
	BeatTracker_t2801099156::get_offset_of_frameLength_12(),
	BeatTracker_t2801099156::get_offset_of_beatTrackInterval_13(),
	BeatTracker_t2801099156::get_offset_of_index_14(),
	BeatTracker_t2801099156::get_offset_of__beatIndices_15(),
	BeatTracker_t2801099156::get_offset_of__beats_16(),
	BeatTracker_t2801099156::get_offset_of_U3CbeatIndicesU3Ek__BackingField_17(),
	BeatTracker_t2801099156::get_offset_of_U3CbeatsU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (LomontFFT_t895069557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[4] = 
{
	LomontFFT_t895069557::get_offset_of_U3CAU3Ek__BackingField_0(),
	LomontFFT_t895069557::get_offset_of_U3CBU3Ek__BackingField_1(),
	LomontFFT_t895069557::get_offset_of_cosTable_2(),
	LomontFFT_t895069557::get_offset_of_sinTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (Onset_t732596509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[3] = 
{
	Onset_t732596509::get_offset_of_index_0(),
	Onset_t732596509::get_offset_of_strength_1(),
	Onset_t732596509::get_offset_of_rank_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (OnsetType_t3680206797)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1769[5] = 
{
	OnsetType_t3680206797::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (RhythmEventProvider_t215006757), -1, sizeof(RhythmEventProvider_t215006757_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1770[17] = 
{
	RhythmEventProvider_t215006757_StaticFields::get_offset_of__eventProviders_2(),
	RhythmEventProvider_t215006757::get_offset_of_targetOffset_3(),
	RhythmEventProvider_t215006757::get_offset_of_offset_4(),
	RhythmEventProvider_t215006757::get_offset_of_lastBeatTime_5(),
	RhythmEventProvider_t215006757::get_offset_of_currentFrame_6(),
	RhythmEventProvider_t215006757::get_offset_of_interpolation_7(),
	RhythmEventProvider_t215006757::get_offset_of_totalFrames_8(),
	RhythmEventProvider_t215006757::get_offset_of_onBeat_9(),
	RhythmEventProvider_t215006757::get_offset_of_onSubBeat_10(),
	RhythmEventProvider_t215006757::get_offset_of_onOnset_11(),
	RhythmEventProvider_t215006757::get_offset_of_onChange_12(),
	RhythmEventProvider_t215006757::get_offset_of_timingUpdate_13(),
	RhythmEventProvider_t215006757::get_offset_of_onFrameChanged_14(),
	RhythmEventProvider_t215006757::get_offset_of_onSongLoaded_15(),
	RhythmEventProvider_t215006757::get_offset_of_onSongEnded_16(),
	RhythmEventProvider_t215006757_StaticFields::get_offset_of_eventProviderList_17(),
	RhythmEventProvider_t215006757_StaticFields::get_offset_of_EventProviderEnabled_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (BeatEvent_t33541086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (SubBeatEvent_t2499915164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (TimingUpdateEvent_t543213975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (FrameChangedEvent_t800671821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (OnsetEvent_t1329939269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (ChangeEvent_t1135638418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (OnNewSong_t4169165798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (RhythmTool_t215962618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[33] = 
{
	RhythmTool_t215962618::get_offset_of_SongLoaded_2(),
	RhythmTool_t215962618::get_offset_of_SongEnded_3(),
	RhythmTool_t215962618::get_offset_of_U3CcurrentSampleU3Ek__BackingField_4(),
	RhythmTool_t215962618::get_offset_of_U3ClastFrameU3Ek__BackingField_5(),
	RhythmTool_t215962618::get_offset_of_U3CtotalFramesU3Ek__BackingField_6(),
	RhythmTool_t215962618::get_offset_of_U3CcurrentFrameU3Ek__BackingField_7(),
	RhythmTool_t215962618::get_offset_of_U3CinterpolationU3Ek__BackingField_8(),
	RhythmTool_t215962618::get_offset_of_U3CanalysisDoneU3Ek__BackingField_9(),
	RhythmTool_t215962618::get_offset_of_U3CbpmU3Ek__BackingField_10(),
	RhythmTool_t215962618::get_offset_of_U3CbeatLengthU3Ek__BackingField_11(),
	RhythmTool_t215962618::get_offset_of_U3CframeLengthU3Ek__BackingField_12(),
	RhythmTool_t215962618::get_offset_of_U3CsongLoadedU3Ek__BackingField_13(),
	RhythmTool_t215962618::get_offset_of__preCalculate_14(),
	RhythmTool_t215962618::get_offset_of__calculateTempo_15(),
	RhythmTool_t215962618::get_offset_of__storeAnalyses_16(),
	RhythmTool_t215962618::get_offset_of__lead_17(),
	RhythmTool_t215962618::get_offset_of_beatTracker_18(),
	RhythmTool_t215962618::get_offset_of_segmenter_19(),
	RhythmTool_t215962618::get_offset_of_analyzeRoutine_20(),
	RhythmTool_t215962618::get_offset_of_queueRoutine_21(),
	RhythmTool_t215962618::get_offset_of_samples_22(),
	RhythmTool_t215962618::get_offset_of_monoSamples_23(),
	RhythmTool_t215962618::get_offset_of_spectrum_24(),
	RhythmTool_t215962618::get_offset_of_channels_25(),
	RhythmTool_t215962618::get_offset_of_lastDataFrame_26(),
	RhythmTool_t215962618::get_offset_of_totalSamples_27(),
	RhythmTool_t215962618::get_offset_of_sampleIndex_28(),
	RhythmTool_t215962618::get_offset_of_analyses_29(),
	RhythmTool_t215962618::get_offset_of__low_30(),
	RhythmTool_t215962618::get_offset_of__mid_31(),
	RhythmTool_t215962618::get_offset_of__high_32(),
	RhythmTool_t215962618::get_offset_of__all_33(),
	RhythmTool_t215962618::get_offset_of_audioSource_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (U3CQueueNewSongU3Ec__Iterator0_t1486555536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[5] = 
{
	U3CQueueNewSongU3Ec__Iterator0_t1486555536::get_offset_of_audioClip_0(),
	U3CQueueNewSongU3Ec__Iterator0_t1486555536::get_offset_of_U24this_1(),
	U3CQueueNewSongU3Ec__Iterator0_t1486555536::get_offset_of_U24current_2(),
	U3CQueueNewSongU3Ec__Iterator0_t1486555536::get_offset_of_U24disposing_3(),
	U3CQueueNewSongU3Ec__Iterator0_t1486555536::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[7] = 
{
	U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971::get_offset_of_frames_0(),
	U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971::get_offset_of_U3CsU3E__0_1(),
	U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971::get_offset_of_U3CanalyzeThreadU3E__0_2(),
	U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971::get_offset_of_U24this_3(),
	U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971::get_offset_of_U24current_4(),
	U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971::get_offset_of_U24disposing_5(),
	U3CAsyncAnalyzeU3Ec__Iterator1_t1906342971::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (Segmenter_t2695296026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[12] = 
{
	Segmenter_t2695296026::get_offset_of_analysis_0(),
	Segmenter_t2695296026::get_offset_of_lastDif_1(),
	Segmenter_t2695296026::get_offset_of_increaseStart_2(),
	Segmenter_t2695296026::get_offset_of_increaseEnd_3(),
	Segmenter_t2695296026::get_offset_of_increaseDetected_4(),
	Segmenter_t2695296026::get_offset_of_decreaseStart_5(),
	Segmenter_t2695296026::get_offset_of_decreaseEnd_6(),
	Segmenter_t2695296026::get_offset_of_decreaseDetected_7(),
	Segmenter_t2695296026::get_offset_of__changes_8(),
	Segmenter_t2695296026::get_offset_of_U3CchangesU3Ek__BackingField_9(),
	Segmenter_t2695296026::get_offset_of__changeIndices_10(),
	Segmenter_t2695296026::get_offset_of_U3CchangeIndicesU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (SongData_t3132760915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[5] = 
{
	SongData_t3132760915::get_offset_of_analyses_0(),
	SongData_t3132760915::get_offset_of_beatTracker_1(),
	SongData_t3132760915::get_offset_of_segmenter_2(),
	SongData_t3132760915::get_offset_of_name_3(),
	SongData_t3132760915::get_offset_of_length_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (Util_t4006552276), -1, sizeof(Util_t4006552276_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1786[4] = 
{
	Util_t4006552276_StaticFields::get_offset_of_fft_0(),
	Util_t4006552276_StaticFields::get_offset_of_magnitude_1(),
	Util_t4006552276_StaticFields::get_offset_of_upsampledSignal_2(),
	Util_t4006552276_StaticFields::get_offset_of_mono_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (HitZoneControl_t2657704264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[3] = 
{
	HitZoneControl_t2657704264::get_offset_of_m_isInZone_2(),
	HitZoneControl_t2657704264::get_offset_of_m_targetEnemy_3(),
	HitZoneControl_t2657704264::get_offset_of_playerHealthManager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (HitZoneManager_t1308216902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[12] = 
{
	HitZoneManager_t1308216902::get_offset_of_m_green_2(),
	HitZoneManager_t1308216902::get_offset_of_m_red_3(),
	HitZoneManager_t1308216902::get_offset_of_m_yellow_4(),
	HitZoneManager_t1308216902::get_offset_of_m_blue_5(),
	HitZoneManager_t1308216902::get_offset_of_m_greenHitZone_6(),
	HitZoneManager_t1308216902::get_offset_of_m_redHitZone_7(),
	HitZoneManager_t1308216902::get_offset_of_m_blueHitZone_8(),
	HitZoneManager_t1308216902::get_offset_of_m_yellowHitZone_9(),
	HitZoneManager_t1308216902::get_offset_of_m_greenTarget_10(),
	HitZoneManager_t1308216902::get_offset_of_m_redTarget_11(),
	HitZoneManager_t1308216902::get_offset_of_m_yellowTarget_12(),
	HitZoneManager_t1308216902::get_offset_of_m_blueTarget_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (CameraMotor_t550925884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[6] = 
{
	CameraMotor_t550925884::get_offset_of_lookAt_2(),
	CameraMotor_t550925884::get_offset_of_startOffset_3(),
	CameraMotor_t550925884::get_offset_of_moveVector_4(),
	CameraMotor_t550925884::get_offset_of_transition_5(),
	CameraMotor_t550925884::get_offset_of_animationDuration_6(),
	CameraMotor_t550925884::get_offset_of_animationOffset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (PlayerMotor_t2528789646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1790[6] = 
{
	PlayerMotor_t2528789646::get_offset_of_controller_2(),
	PlayerMotor_t2528789646::get_offset_of_moveVector_3(),
	PlayerMotor_t2528789646::get_offset_of_speed_4(),
	PlayerMotor_t2528789646::get_offset_of_verticalVelocity_5(),
	PlayerMotor_t2528789646::get_offset_of_gravity_6(),
	PlayerMotor_t2528789646::get_offset_of_animationDuration_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (TileManager_t3422405329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[8] = 
{
	TileManager_t3422405329::get_offset_of_tilePrefabs_2(),
	TileManager_t3422405329::get_offset_of_playerTransform_3(),
	TileManager_t3422405329::get_offset_of_spawnZ_4(),
	TileManager_t3422405329::get_offset_of_tileLength_5(),
	TileManager_t3422405329::get_offset_of_safeZone_6(),
	TileManager_t3422405329::get_offset_of_amnTilesOnScreen_7(),
	TileManager_t3422405329::get_offset_of_lastPrefabIndex_8(),
	TileManager_t3422405329::get_offset_of_activeTiles_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (AttackButtonManager_t2228488667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[2] = 
{
	AttackButtonManager_t2228488667::get_offset_of_hitZoneManager_2(),
	AttackButtonManager_t2228488667::get_offset_of_button_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (EnemyController_t2146768720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[5] = 
{
	EnemyController_t2146768720::get_offset_of_m_startPos_2(),
	EnemyController_t2146768720::get_offset_of_m_endPos_3(),
	EnemyController_t2146768720::get_offset_of_time_4(),
	EnemyController_t2146768720::get_offset_of_gameManagerObj_5(),
	EnemyController_t2146768720::get_offset_of_gameManager_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (FailScreenButtonControl_t1753294869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (GameManager_t2252321495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[4] = 
{
	GameManager_t2252321495::get_offset_of_m_rhythmTool_2(),
	GameManager_t2252321495::get_offset_of_m_audioClip_3(),
	GameManager_t2252321495::get_offset_of_playerHealthManager_4(),
	GameManager_t2252321495::get_offset_of_failScreenCanvas_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (PlayerHealthManager_t3067865410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[3] = 
{
	PlayerHealthManager_t3067865410::get_offset_of_playerHealth_2(),
	PlayerHealthManager_t3067865410::get_offset_of_playerMaxHealth_3(),
	PlayerHealthManager_t3067865410::get_offset_of_playerHealthBar_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (PlayerUserControl_t2572554735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[2] = 
{
	PlayerUserControl_t2572554735::get_offset_of_hitZoneManager_2(),
	PlayerUserControl_t2572554735::get_offset_of_animator_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (SpawnManager_t4269630218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[7] = 
{
	SpawnManager_t4269630218::get_offset_of_m_spawnObjects_2(),
	SpawnManager_t4269630218::get_offset_of_m_enemySpawn_3(),
	SpawnManager_t4269630218::get_offset_of_enemySpawn_4(),
	SpawnManager_t4269630218::get_offset_of_beatCount_5(),
	SpawnManager_t4269630218::get_offset_of_high_6(),
	SpawnManager_t4269630218::get_offset_of_mid_7(),
	SpawnManager_t4269630218::get_offset_of_low_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (Tutorial_1_Controller_t3740746359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[9] = 
{
	Tutorial_1_Controller_t3740746359::get_offset_of_debugLineOffset_2(),
	Tutorial_1_Controller_t3740746359::get_offset_of_rhythmTool_3(),
	Tutorial_1_Controller_t3740746359::get_offset_of_audioClip_4(),
	Tutorial_1_Controller_t3740746359::get_offset_of_low_5(),
	Tutorial_1_Controller_t3740746359::get_offset_of_startPosition_6(),
	Tutorial_1_Controller_t3740746359::get_offset_of_endPosition_7(),
	Tutorial_1_Controller_t3740746359::get_offset_of_sphere_8(),
	Tutorial_1_Controller_t3740746359::get_offset_of_enemyObj_9(),
	Tutorial_1_Controller_t3740746359::get_offset_of_enemyArray_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
