﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TileManager
struct  TileManager_t3422405329  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] TileManager::tilePrefabs
	GameObjectU5BU5D_t3057952154* ___tilePrefabs_2;
	// UnityEngine.Transform TileManager::playerTransform
	Transform_t3275118058 * ___playerTransform_3;
	// System.Single TileManager::spawnZ
	float ___spawnZ_4;
	// System.Single TileManager::tileLength
	float ___tileLength_5;
	// System.Single TileManager::safeZone
	float ___safeZone_6;
	// System.Int32 TileManager::amnTilesOnScreen
	int32_t ___amnTilesOnScreen_7;
	// System.Int32 TileManager::lastPrefabIndex
	int32_t ___lastPrefabIndex_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> TileManager::activeTiles
	List_1_t1125654279 * ___activeTiles_9;

public:
	inline static int32_t get_offset_of_tilePrefabs_2() { return static_cast<int32_t>(offsetof(TileManager_t3422405329, ___tilePrefabs_2)); }
	inline GameObjectU5BU5D_t3057952154* get_tilePrefabs_2() const { return ___tilePrefabs_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_tilePrefabs_2() { return &___tilePrefabs_2; }
	inline void set_tilePrefabs_2(GameObjectU5BU5D_t3057952154* value)
	{
		___tilePrefabs_2 = value;
		Il2CppCodeGenWriteBarrier(&___tilePrefabs_2, value);
	}

	inline static int32_t get_offset_of_playerTransform_3() { return static_cast<int32_t>(offsetof(TileManager_t3422405329, ___playerTransform_3)); }
	inline Transform_t3275118058 * get_playerTransform_3() const { return ___playerTransform_3; }
	inline Transform_t3275118058 ** get_address_of_playerTransform_3() { return &___playerTransform_3; }
	inline void set_playerTransform_3(Transform_t3275118058 * value)
	{
		___playerTransform_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerTransform_3, value);
	}

	inline static int32_t get_offset_of_spawnZ_4() { return static_cast<int32_t>(offsetof(TileManager_t3422405329, ___spawnZ_4)); }
	inline float get_spawnZ_4() const { return ___spawnZ_4; }
	inline float* get_address_of_spawnZ_4() { return &___spawnZ_4; }
	inline void set_spawnZ_4(float value)
	{
		___spawnZ_4 = value;
	}

	inline static int32_t get_offset_of_tileLength_5() { return static_cast<int32_t>(offsetof(TileManager_t3422405329, ___tileLength_5)); }
	inline float get_tileLength_5() const { return ___tileLength_5; }
	inline float* get_address_of_tileLength_5() { return &___tileLength_5; }
	inline void set_tileLength_5(float value)
	{
		___tileLength_5 = value;
	}

	inline static int32_t get_offset_of_safeZone_6() { return static_cast<int32_t>(offsetof(TileManager_t3422405329, ___safeZone_6)); }
	inline float get_safeZone_6() const { return ___safeZone_6; }
	inline float* get_address_of_safeZone_6() { return &___safeZone_6; }
	inline void set_safeZone_6(float value)
	{
		___safeZone_6 = value;
	}

	inline static int32_t get_offset_of_amnTilesOnScreen_7() { return static_cast<int32_t>(offsetof(TileManager_t3422405329, ___amnTilesOnScreen_7)); }
	inline int32_t get_amnTilesOnScreen_7() const { return ___amnTilesOnScreen_7; }
	inline int32_t* get_address_of_amnTilesOnScreen_7() { return &___amnTilesOnScreen_7; }
	inline void set_amnTilesOnScreen_7(int32_t value)
	{
		___amnTilesOnScreen_7 = value;
	}

	inline static int32_t get_offset_of_lastPrefabIndex_8() { return static_cast<int32_t>(offsetof(TileManager_t3422405329, ___lastPrefabIndex_8)); }
	inline int32_t get_lastPrefabIndex_8() const { return ___lastPrefabIndex_8; }
	inline int32_t* get_address_of_lastPrefabIndex_8() { return &___lastPrefabIndex_8; }
	inline void set_lastPrefabIndex_8(int32_t value)
	{
		___lastPrefabIndex_8 = value;
	}

	inline static int32_t get_offset_of_activeTiles_9() { return static_cast<int32_t>(offsetof(TileManager_t3422405329, ___activeTiles_9)); }
	inline List_1_t1125654279 * get_activeTiles_9() const { return ___activeTiles_9; }
	inline List_1_t1125654279 ** get_address_of_activeTiles_9() { return &___activeTiles_9; }
	inline void set_activeTiles_9(List_1_t1125654279 * value)
	{
		___activeTiles_9 = value;
		Il2CppCodeGenWriteBarrier(&___activeTiles_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
