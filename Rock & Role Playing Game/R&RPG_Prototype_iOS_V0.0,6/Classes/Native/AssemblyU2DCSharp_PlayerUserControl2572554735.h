﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// HitZoneManager
struct HitZoneManager_t1308216902;
// UnityEngine.Animator
struct Animator_t69676727;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerUserControl
struct  PlayerUserControl_t2572554735  : public MonoBehaviour_t1158329972
{
public:
	// HitZoneManager PlayerUserControl::hitZoneManager
	HitZoneManager_t1308216902 * ___hitZoneManager_2;
	// UnityEngine.Animator PlayerUserControl::animator
	Animator_t69676727 * ___animator_3;

public:
	inline static int32_t get_offset_of_hitZoneManager_2() { return static_cast<int32_t>(offsetof(PlayerUserControl_t2572554735, ___hitZoneManager_2)); }
	inline HitZoneManager_t1308216902 * get_hitZoneManager_2() const { return ___hitZoneManager_2; }
	inline HitZoneManager_t1308216902 ** get_address_of_hitZoneManager_2() { return &___hitZoneManager_2; }
	inline void set_hitZoneManager_2(HitZoneManager_t1308216902 * value)
	{
		___hitZoneManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___hitZoneManager_2, value);
	}

	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(PlayerUserControl_t2572554735, ___animator_3)); }
	inline Animator_t69676727 * get_animator_3() const { return ___animator_3; }
	inline Animator_t69676727 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(Animator_t69676727 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier(&___animator_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
