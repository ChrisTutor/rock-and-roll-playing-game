﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Action
struct Action_t3226471752;
// BeatTracker
struct BeatTracker_t2801099156;
// Segmenter
struct Segmenter_t2695296026;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.List`1<Analysis>
struct List_1_t4103576526;
// Analysis
struct Analysis_t439488098;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RhythmTool
struct  RhythmTool_t215962618  : public MonoBehaviour_t1158329972
{
public:
	// System.Action RhythmTool::SongLoaded
	Action_t3226471752 * ___SongLoaded_2;
	// System.Action RhythmTool::SongEnded
	Action_t3226471752 * ___SongEnded_3;
	// System.Single RhythmTool::<currentSample>k__BackingField
	float ___U3CcurrentSampleU3Ek__BackingField_4;
	// System.Int32 RhythmTool::<lastFrame>k__BackingField
	int32_t ___U3ClastFrameU3Ek__BackingField_5;
	// System.Int32 RhythmTool::<totalFrames>k__BackingField
	int32_t ___U3CtotalFramesU3Ek__BackingField_6;
	// System.Int32 RhythmTool::<currentFrame>k__BackingField
	int32_t ___U3CcurrentFrameU3Ek__BackingField_7;
	// System.Single RhythmTool::<interpolation>k__BackingField
	float ___U3CinterpolationU3Ek__BackingField_8;
	// System.Boolean RhythmTool::<analysisDone>k__BackingField
	bool ___U3CanalysisDoneU3Ek__BackingField_9;
	// System.Single RhythmTool::<bpm>k__BackingField
	float ___U3CbpmU3Ek__BackingField_10;
	// System.Single RhythmTool::<beatLength>k__BackingField
	float ___U3CbeatLengthU3Ek__BackingField_11;
	// System.Single RhythmTool::<frameLength>k__BackingField
	float ___U3CframeLengthU3Ek__BackingField_12;
	// System.Boolean RhythmTool::<songLoaded>k__BackingField
	bool ___U3CsongLoadedU3Ek__BackingField_13;
	// System.Boolean RhythmTool::_preCalculate
	bool ____preCalculate_14;
	// System.Boolean RhythmTool::_calculateTempo
	bool ____calculateTempo_15;
	// System.Boolean RhythmTool::_storeAnalyses
	bool ____storeAnalyses_16;
	// System.Int32 RhythmTool::_lead
	int32_t ____lead_17;
	// BeatTracker RhythmTool::beatTracker
	BeatTracker_t2801099156 * ___beatTracker_18;
	// Segmenter RhythmTool::segmenter
	Segmenter_t2695296026 * ___segmenter_19;
	// UnityEngine.Coroutine RhythmTool::analyzeRoutine
	Coroutine_t2299508840 * ___analyzeRoutine_20;
	// UnityEngine.Coroutine RhythmTool::queueRoutine
	Coroutine_t2299508840 * ___queueRoutine_21;
	// System.Single[] RhythmTool::samples
	SingleU5BU5D_t577127397* ___samples_22;
	// System.Single[] RhythmTool::monoSamples
	SingleU5BU5D_t577127397* ___monoSamples_23;
	// System.Single[] RhythmTool::spectrum
	SingleU5BU5D_t577127397* ___spectrum_24;
	// System.Int32 RhythmTool::channels
	int32_t ___channels_25;
	// System.Int32 RhythmTool::lastDataFrame
	int32_t ___lastDataFrame_26;
	// System.Int32 RhythmTool::totalSamples
	int32_t ___totalSamples_27;
	// System.Int32 RhythmTool::sampleIndex
	int32_t ___sampleIndex_28;
	// System.Collections.Generic.List`1<Analysis> RhythmTool::analyses
	List_1_t4103576526 * ___analyses_29;
	// Analysis RhythmTool::_low
	Analysis_t439488098 * ____low_30;
	// Analysis RhythmTool::_mid
	Analysis_t439488098 * ____mid_31;
	// Analysis RhythmTool::_high
	Analysis_t439488098 * ____high_32;
	// Analysis RhythmTool::_all
	Analysis_t439488098 * ____all_33;
	// UnityEngine.AudioSource RhythmTool::audioSource
	AudioSource_t1135106623 * ___audioSource_34;

public:
	inline static int32_t get_offset_of_SongLoaded_2() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___SongLoaded_2)); }
	inline Action_t3226471752 * get_SongLoaded_2() const { return ___SongLoaded_2; }
	inline Action_t3226471752 ** get_address_of_SongLoaded_2() { return &___SongLoaded_2; }
	inline void set_SongLoaded_2(Action_t3226471752 * value)
	{
		___SongLoaded_2 = value;
		Il2CppCodeGenWriteBarrier(&___SongLoaded_2, value);
	}

	inline static int32_t get_offset_of_SongEnded_3() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___SongEnded_3)); }
	inline Action_t3226471752 * get_SongEnded_3() const { return ___SongEnded_3; }
	inline Action_t3226471752 ** get_address_of_SongEnded_3() { return &___SongEnded_3; }
	inline void set_SongEnded_3(Action_t3226471752 * value)
	{
		___SongEnded_3 = value;
		Il2CppCodeGenWriteBarrier(&___SongEnded_3, value);
	}

	inline static int32_t get_offset_of_U3CcurrentSampleU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CcurrentSampleU3Ek__BackingField_4)); }
	inline float get_U3CcurrentSampleU3Ek__BackingField_4() const { return ___U3CcurrentSampleU3Ek__BackingField_4; }
	inline float* get_address_of_U3CcurrentSampleU3Ek__BackingField_4() { return &___U3CcurrentSampleU3Ek__BackingField_4; }
	inline void set_U3CcurrentSampleU3Ek__BackingField_4(float value)
	{
		___U3CcurrentSampleU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3ClastFrameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3ClastFrameU3Ek__BackingField_5)); }
	inline int32_t get_U3ClastFrameU3Ek__BackingField_5() const { return ___U3ClastFrameU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3ClastFrameU3Ek__BackingField_5() { return &___U3ClastFrameU3Ek__BackingField_5; }
	inline void set_U3ClastFrameU3Ek__BackingField_5(int32_t value)
	{
		___U3ClastFrameU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CtotalFramesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CtotalFramesU3Ek__BackingField_6)); }
	inline int32_t get_U3CtotalFramesU3Ek__BackingField_6() const { return ___U3CtotalFramesU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CtotalFramesU3Ek__BackingField_6() { return &___U3CtotalFramesU3Ek__BackingField_6; }
	inline void set_U3CtotalFramesU3Ek__BackingField_6(int32_t value)
	{
		___U3CtotalFramesU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CcurrentFrameU3Ek__BackingField_7)); }
	inline int32_t get_U3CcurrentFrameU3Ek__BackingField_7() const { return ___U3CcurrentFrameU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CcurrentFrameU3Ek__BackingField_7() { return &___U3CcurrentFrameU3Ek__BackingField_7; }
	inline void set_U3CcurrentFrameU3Ek__BackingField_7(int32_t value)
	{
		___U3CcurrentFrameU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CinterpolationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CinterpolationU3Ek__BackingField_8)); }
	inline float get_U3CinterpolationU3Ek__BackingField_8() const { return ___U3CinterpolationU3Ek__BackingField_8; }
	inline float* get_address_of_U3CinterpolationU3Ek__BackingField_8() { return &___U3CinterpolationU3Ek__BackingField_8; }
	inline void set_U3CinterpolationU3Ek__BackingField_8(float value)
	{
		___U3CinterpolationU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CanalysisDoneU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CanalysisDoneU3Ek__BackingField_9)); }
	inline bool get_U3CanalysisDoneU3Ek__BackingField_9() const { return ___U3CanalysisDoneU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CanalysisDoneU3Ek__BackingField_9() { return &___U3CanalysisDoneU3Ek__BackingField_9; }
	inline void set_U3CanalysisDoneU3Ek__BackingField_9(bool value)
	{
		___U3CanalysisDoneU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CbpmU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CbpmU3Ek__BackingField_10)); }
	inline float get_U3CbpmU3Ek__BackingField_10() const { return ___U3CbpmU3Ek__BackingField_10; }
	inline float* get_address_of_U3CbpmU3Ek__BackingField_10() { return &___U3CbpmU3Ek__BackingField_10; }
	inline void set_U3CbpmU3Ek__BackingField_10(float value)
	{
		___U3CbpmU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CbeatLengthU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CbeatLengthU3Ek__BackingField_11)); }
	inline float get_U3CbeatLengthU3Ek__BackingField_11() const { return ___U3CbeatLengthU3Ek__BackingField_11; }
	inline float* get_address_of_U3CbeatLengthU3Ek__BackingField_11() { return &___U3CbeatLengthU3Ek__BackingField_11; }
	inline void set_U3CbeatLengthU3Ek__BackingField_11(float value)
	{
		___U3CbeatLengthU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CframeLengthU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CframeLengthU3Ek__BackingField_12)); }
	inline float get_U3CframeLengthU3Ek__BackingField_12() const { return ___U3CframeLengthU3Ek__BackingField_12; }
	inline float* get_address_of_U3CframeLengthU3Ek__BackingField_12() { return &___U3CframeLengthU3Ek__BackingField_12; }
	inline void set_U3CframeLengthU3Ek__BackingField_12(float value)
	{
		___U3CframeLengthU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CsongLoadedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___U3CsongLoadedU3Ek__BackingField_13)); }
	inline bool get_U3CsongLoadedU3Ek__BackingField_13() const { return ___U3CsongLoadedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CsongLoadedU3Ek__BackingField_13() { return &___U3CsongLoadedU3Ek__BackingField_13; }
	inline void set_U3CsongLoadedU3Ek__BackingField_13(bool value)
	{
		___U3CsongLoadedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__preCalculate_14() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ____preCalculate_14)); }
	inline bool get__preCalculate_14() const { return ____preCalculate_14; }
	inline bool* get_address_of__preCalculate_14() { return &____preCalculate_14; }
	inline void set__preCalculate_14(bool value)
	{
		____preCalculate_14 = value;
	}

	inline static int32_t get_offset_of__calculateTempo_15() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ____calculateTempo_15)); }
	inline bool get__calculateTempo_15() const { return ____calculateTempo_15; }
	inline bool* get_address_of__calculateTempo_15() { return &____calculateTempo_15; }
	inline void set__calculateTempo_15(bool value)
	{
		____calculateTempo_15 = value;
	}

	inline static int32_t get_offset_of__storeAnalyses_16() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ____storeAnalyses_16)); }
	inline bool get__storeAnalyses_16() const { return ____storeAnalyses_16; }
	inline bool* get_address_of__storeAnalyses_16() { return &____storeAnalyses_16; }
	inline void set__storeAnalyses_16(bool value)
	{
		____storeAnalyses_16 = value;
	}

	inline static int32_t get_offset_of__lead_17() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ____lead_17)); }
	inline int32_t get__lead_17() const { return ____lead_17; }
	inline int32_t* get_address_of__lead_17() { return &____lead_17; }
	inline void set__lead_17(int32_t value)
	{
		____lead_17 = value;
	}

	inline static int32_t get_offset_of_beatTracker_18() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___beatTracker_18)); }
	inline BeatTracker_t2801099156 * get_beatTracker_18() const { return ___beatTracker_18; }
	inline BeatTracker_t2801099156 ** get_address_of_beatTracker_18() { return &___beatTracker_18; }
	inline void set_beatTracker_18(BeatTracker_t2801099156 * value)
	{
		___beatTracker_18 = value;
		Il2CppCodeGenWriteBarrier(&___beatTracker_18, value);
	}

	inline static int32_t get_offset_of_segmenter_19() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___segmenter_19)); }
	inline Segmenter_t2695296026 * get_segmenter_19() const { return ___segmenter_19; }
	inline Segmenter_t2695296026 ** get_address_of_segmenter_19() { return &___segmenter_19; }
	inline void set_segmenter_19(Segmenter_t2695296026 * value)
	{
		___segmenter_19 = value;
		Il2CppCodeGenWriteBarrier(&___segmenter_19, value);
	}

	inline static int32_t get_offset_of_analyzeRoutine_20() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___analyzeRoutine_20)); }
	inline Coroutine_t2299508840 * get_analyzeRoutine_20() const { return ___analyzeRoutine_20; }
	inline Coroutine_t2299508840 ** get_address_of_analyzeRoutine_20() { return &___analyzeRoutine_20; }
	inline void set_analyzeRoutine_20(Coroutine_t2299508840 * value)
	{
		___analyzeRoutine_20 = value;
		Il2CppCodeGenWriteBarrier(&___analyzeRoutine_20, value);
	}

	inline static int32_t get_offset_of_queueRoutine_21() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___queueRoutine_21)); }
	inline Coroutine_t2299508840 * get_queueRoutine_21() const { return ___queueRoutine_21; }
	inline Coroutine_t2299508840 ** get_address_of_queueRoutine_21() { return &___queueRoutine_21; }
	inline void set_queueRoutine_21(Coroutine_t2299508840 * value)
	{
		___queueRoutine_21 = value;
		Il2CppCodeGenWriteBarrier(&___queueRoutine_21, value);
	}

	inline static int32_t get_offset_of_samples_22() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___samples_22)); }
	inline SingleU5BU5D_t577127397* get_samples_22() const { return ___samples_22; }
	inline SingleU5BU5D_t577127397** get_address_of_samples_22() { return &___samples_22; }
	inline void set_samples_22(SingleU5BU5D_t577127397* value)
	{
		___samples_22 = value;
		Il2CppCodeGenWriteBarrier(&___samples_22, value);
	}

	inline static int32_t get_offset_of_monoSamples_23() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___monoSamples_23)); }
	inline SingleU5BU5D_t577127397* get_monoSamples_23() const { return ___monoSamples_23; }
	inline SingleU5BU5D_t577127397** get_address_of_monoSamples_23() { return &___monoSamples_23; }
	inline void set_monoSamples_23(SingleU5BU5D_t577127397* value)
	{
		___monoSamples_23 = value;
		Il2CppCodeGenWriteBarrier(&___monoSamples_23, value);
	}

	inline static int32_t get_offset_of_spectrum_24() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___spectrum_24)); }
	inline SingleU5BU5D_t577127397* get_spectrum_24() const { return ___spectrum_24; }
	inline SingleU5BU5D_t577127397** get_address_of_spectrum_24() { return &___spectrum_24; }
	inline void set_spectrum_24(SingleU5BU5D_t577127397* value)
	{
		___spectrum_24 = value;
		Il2CppCodeGenWriteBarrier(&___spectrum_24, value);
	}

	inline static int32_t get_offset_of_channels_25() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___channels_25)); }
	inline int32_t get_channels_25() const { return ___channels_25; }
	inline int32_t* get_address_of_channels_25() { return &___channels_25; }
	inline void set_channels_25(int32_t value)
	{
		___channels_25 = value;
	}

	inline static int32_t get_offset_of_lastDataFrame_26() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___lastDataFrame_26)); }
	inline int32_t get_lastDataFrame_26() const { return ___lastDataFrame_26; }
	inline int32_t* get_address_of_lastDataFrame_26() { return &___lastDataFrame_26; }
	inline void set_lastDataFrame_26(int32_t value)
	{
		___lastDataFrame_26 = value;
	}

	inline static int32_t get_offset_of_totalSamples_27() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___totalSamples_27)); }
	inline int32_t get_totalSamples_27() const { return ___totalSamples_27; }
	inline int32_t* get_address_of_totalSamples_27() { return &___totalSamples_27; }
	inline void set_totalSamples_27(int32_t value)
	{
		___totalSamples_27 = value;
	}

	inline static int32_t get_offset_of_sampleIndex_28() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___sampleIndex_28)); }
	inline int32_t get_sampleIndex_28() const { return ___sampleIndex_28; }
	inline int32_t* get_address_of_sampleIndex_28() { return &___sampleIndex_28; }
	inline void set_sampleIndex_28(int32_t value)
	{
		___sampleIndex_28 = value;
	}

	inline static int32_t get_offset_of_analyses_29() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___analyses_29)); }
	inline List_1_t4103576526 * get_analyses_29() const { return ___analyses_29; }
	inline List_1_t4103576526 ** get_address_of_analyses_29() { return &___analyses_29; }
	inline void set_analyses_29(List_1_t4103576526 * value)
	{
		___analyses_29 = value;
		Il2CppCodeGenWriteBarrier(&___analyses_29, value);
	}

	inline static int32_t get_offset_of__low_30() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ____low_30)); }
	inline Analysis_t439488098 * get__low_30() const { return ____low_30; }
	inline Analysis_t439488098 ** get_address_of__low_30() { return &____low_30; }
	inline void set__low_30(Analysis_t439488098 * value)
	{
		____low_30 = value;
		Il2CppCodeGenWriteBarrier(&____low_30, value);
	}

	inline static int32_t get_offset_of__mid_31() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ____mid_31)); }
	inline Analysis_t439488098 * get__mid_31() const { return ____mid_31; }
	inline Analysis_t439488098 ** get_address_of__mid_31() { return &____mid_31; }
	inline void set__mid_31(Analysis_t439488098 * value)
	{
		____mid_31 = value;
		Il2CppCodeGenWriteBarrier(&____mid_31, value);
	}

	inline static int32_t get_offset_of__high_32() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ____high_32)); }
	inline Analysis_t439488098 * get__high_32() const { return ____high_32; }
	inline Analysis_t439488098 ** get_address_of__high_32() { return &____high_32; }
	inline void set__high_32(Analysis_t439488098 * value)
	{
		____high_32 = value;
		Il2CppCodeGenWriteBarrier(&____high_32, value);
	}

	inline static int32_t get_offset_of__all_33() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ____all_33)); }
	inline Analysis_t439488098 * get__all_33() const { return ____all_33; }
	inline Analysis_t439488098 ** get_address_of__all_33() { return &____all_33; }
	inline void set__all_33(Analysis_t439488098 * value)
	{
		____all_33 = value;
		Il2CppCodeGenWriteBarrier(&____all_33, value);
	}

	inline static int32_t get_offset_of_audioSource_34() { return static_cast<int32_t>(offsetof(RhythmTool_t215962618, ___audioSource_34)); }
	inline AudioSource_t1135106623 * get_audioSource_34() const { return ___audioSource_34; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_34() { return &___audioSource_34; }
	inline void set_audioSource_34(AudioSource_t1135106623 * value)
	{
		___audioSource_34 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
