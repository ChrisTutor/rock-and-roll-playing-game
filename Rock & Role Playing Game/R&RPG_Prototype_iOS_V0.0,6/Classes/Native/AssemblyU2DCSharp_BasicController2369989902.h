﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// RhythmTool
struct RhythmTool_t215962618;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicController
struct  BasicController_t2369989902  : public MonoBehaviour_t1158329972
{
public:
	// RhythmTool BasicController::rhythmTool
	RhythmTool_t215962618 * ___rhythmTool_2;
	// UnityEngine.AudioClip BasicController::audioClip
	AudioClip_t1932558630 * ___audioClip_3;

public:
	inline static int32_t get_offset_of_rhythmTool_2() { return static_cast<int32_t>(offsetof(BasicController_t2369989902, ___rhythmTool_2)); }
	inline RhythmTool_t215962618 * get_rhythmTool_2() const { return ___rhythmTool_2; }
	inline RhythmTool_t215962618 ** get_address_of_rhythmTool_2() { return &___rhythmTool_2; }
	inline void set_rhythmTool_2(RhythmTool_t215962618 * value)
	{
		___rhythmTool_2 = value;
		Il2CppCodeGenWriteBarrier(&___rhythmTool_2, value);
	}

	inline static int32_t get_offset_of_audioClip_3() { return static_cast<int32_t>(offsetof(BasicController_t2369989902, ___audioClip_3)); }
	inline AudioClip_t1932558630 * get_audioClip_3() const { return ___audioClip_3; }
	inline AudioClip_t1932558630 ** get_address_of_audioClip_3() { return &___audioClip_3; }
	inline void set_audioClip_3(AudioClip_t1932558630 * value)
	{
		___audioClip_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioClip_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
