﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// LomontFFT
struct LomontFFT_t895069557;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Util
struct  Util_t4006552276  : public Il2CppObject
{
public:

public:
};

struct Util_t4006552276_StaticFields
{
public:
	// LomontFFT Util::fft
	LomontFFT_t895069557 * ___fft_0;
	// System.Single[] Util::magnitude
	SingleU5BU5D_t577127397* ___magnitude_1;
	// System.Single[] Util::upsampledSignal
	SingleU5BU5D_t577127397* ___upsampledSignal_2;
	// System.Single[] Util::mono
	SingleU5BU5D_t577127397* ___mono_3;

public:
	inline static int32_t get_offset_of_fft_0() { return static_cast<int32_t>(offsetof(Util_t4006552276_StaticFields, ___fft_0)); }
	inline LomontFFT_t895069557 * get_fft_0() const { return ___fft_0; }
	inline LomontFFT_t895069557 ** get_address_of_fft_0() { return &___fft_0; }
	inline void set_fft_0(LomontFFT_t895069557 * value)
	{
		___fft_0 = value;
		Il2CppCodeGenWriteBarrier(&___fft_0, value);
	}

	inline static int32_t get_offset_of_magnitude_1() { return static_cast<int32_t>(offsetof(Util_t4006552276_StaticFields, ___magnitude_1)); }
	inline SingleU5BU5D_t577127397* get_magnitude_1() const { return ___magnitude_1; }
	inline SingleU5BU5D_t577127397** get_address_of_magnitude_1() { return &___magnitude_1; }
	inline void set_magnitude_1(SingleU5BU5D_t577127397* value)
	{
		___magnitude_1 = value;
		Il2CppCodeGenWriteBarrier(&___magnitude_1, value);
	}

	inline static int32_t get_offset_of_upsampledSignal_2() { return static_cast<int32_t>(offsetof(Util_t4006552276_StaticFields, ___upsampledSignal_2)); }
	inline SingleU5BU5D_t577127397* get_upsampledSignal_2() const { return ___upsampledSignal_2; }
	inline SingleU5BU5D_t577127397** get_address_of_upsampledSignal_2() { return &___upsampledSignal_2; }
	inline void set_upsampledSignal_2(SingleU5BU5D_t577127397* value)
	{
		___upsampledSignal_2 = value;
		Il2CppCodeGenWriteBarrier(&___upsampledSignal_2, value);
	}

	inline static int32_t get_offset_of_mono_3() { return static_cast<int32_t>(offsetof(Util_t4006552276_StaticFields, ___mono_3)); }
	inline SingleU5BU5D_t577127397* get_mono_3() const { return ___mono_3; }
	inline SingleU5BU5D_t577127397** get_address_of_mono_3() { return &___mono_3; }
	inline void set_mono_3(SingleU5BU5D_t577127397* value)
	{
		___mono_3 = value;
		Il2CppCodeGenWriteBarrier(&___mono_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
