﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// animation_keys
struct  animation_keys_t385740455  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AnimationClip animation_keys::teclado
	AnimationClip_t3510324950 * ___teclado_2;

public:
	inline static int32_t get_offset_of_teclado_2() { return static_cast<int32_t>(offsetof(animation_keys_t385740455, ___teclado_2)); }
	inline AnimationClip_t3510324950 * get_teclado_2() const { return ___teclado_2; }
	inline AnimationClip_t3510324950 ** get_address_of_teclado_2() { return &___teclado_2; }
	inline void set_teclado_2(AnimationClip_t3510324950 * value)
	{
		___teclado_2 = value;
		Il2CppCodeGenWriteBarrier(&___teclado_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
