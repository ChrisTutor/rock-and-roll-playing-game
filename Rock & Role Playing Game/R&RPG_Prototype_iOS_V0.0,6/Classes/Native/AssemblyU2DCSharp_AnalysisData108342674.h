﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>
struct ReadOnlyCollection_1_t2262295624;
// ReadOnlyDictionary`2<System.Int32,Onset>
struct ReadOnlyDictionary_2_t28079572;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalysisData
struct  AnalysisData_t108342674  : public Il2CppObject
{
public:
	// System.String AnalysisData::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::<magnitude>k__BackingField
	ReadOnlyCollection_1_t2262295624 * ___U3CmagnitudeU3Ek__BackingField_1;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::<flux>k__BackingField
	ReadOnlyCollection_1_t2262295624 * ___U3CfluxU3Ek__BackingField_2;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::<magnitudeSmooth>k__BackingField
	ReadOnlyCollection_1_t2262295624 * ___U3CmagnitudeSmoothU3Ek__BackingField_3;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> AnalysisData::<magnitudeAvg>k__BackingField
	ReadOnlyCollection_1_t2262295624 * ___U3CmagnitudeAvgU3Ek__BackingField_4;
	// ReadOnlyDictionary`2<System.Int32,Onset> AnalysisData::<onsets>k__BackingField
	ReadOnlyDictionary_2_t28079572 * ___U3ConsetsU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AnalysisData_t108342674, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CmagnitudeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AnalysisData_t108342674, ___U3CmagnitudeU3Ek__BackingField_1)); }
	inline ReadOnlyCollection_1_t2262295624 * get_U3CmagnitudeU3Ek__BackingField_1() const { return ___U3CmagnitudeU3Ek__BackingField_1; }
	inline ReadOnlyCollection_1_t2262295624 ** get_address_of_U3CmagnitudeU3Ek__BackingField_1() { return &___U3CmagnitudeU3Ek__BackingField_1; }
	inline void set_U3CmagnitudeU3Ek__BackingField_1(ReadOnlyCollection_1_t2262295624 * value)
	{
		___U3CmagnitudeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmagnitudeU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CfluxU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AnalysisData_t108342674, ___U3CfluxU3Ek__BackingField_2)); }
	inline ReadOnlyCollection_1_t2262295624 * get_U3CfluxU3Ek__BackingField_2() const { return ___U3CfluxU3Ek__BackingField_2; }
	inline ReadOnlyCollection_1_t2262295624 ** get_address_of_U3CfluxU3Ek__BackingField_2() { return &___U3CfluxU3Ek__BackingField_2; }
	inline void set_U3CfluxU3Ek__BackingField_2(ReadOnlyCollection_1_t2262295624 * value)
	{
		___U3CfluxU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfluxU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CmagnitudeSmoothU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AnalysisData_t108342674, ___U3CmagnitudeSmoothU3Ek__BackingField_3)); }
	inline ReadOnlyCollection_1_t2262295624 * get_U3CmagnitudeSmoothU3Ek__BackingField_3() const { return ___U3CmagnitudeSmoothU3Ek__BackingField_3; }
	inline ReadOnlyCollection_1_t2262295624 ** get_address_of_U3CmagnitudeSmoothU3Ek__BackingField_3() { return &___U3CmagnitudeSmoothU3Ek__BackingField_3; }
	inline void set_U3CmagnitudeSmoothU3Ek__BackingField_3(ReadOnlyCollection_1_t2262295624 * value)
	{
		___U3CmagnitudeSmoothU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmagnitudeSmoothU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CmagnitudeAvgU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AnalysisData_t108342674, ___U3CmagnitudeAvgU3Ek__BackingField_4)); }
	inline ReadOnlyCollection_1_t2262295624 * get_U3CmagnitudeAvgU3Ek__BackingField_4() const { return ___U3CmagnitudeAvgU3Ek__BackingField_4; }
	inline ReadOnlyCollection_1_t2262295624 ** get_address_of_U3CmagnitudeAvgU3Ek__BackingField_4() { return &___U3CmagnitudeAvgU3Ek__BackingField_4; }
	inline void set_U3CmagnitudeAvgU3Ek__BackingField_4(ReadOnlyCollection_1_t2262295624 * value)
	{
		___U3CmagnitudeAvgU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmagnitudeAvgU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3ConsetsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AnalysisData_t108342674, ___U3ConsetsU3Ek__BackingField_5)); }
	inline ReadOnlyDictionary_2_t28079572 * get_U3ConsetsU3Ek__BackingField_5() const { return ___U3ConsetsU3Ek__BackingField_5; }
	inline ReadOnlyDictionary_2_t28079572 ** get_address_of_U3ConsetsU3Ek__BackingField_5() { return &___U3ConsetsU3Ek__BackingField_5; }
	inline void set_U3ConsetsU3Ek__BackingField_5(ReadOnlyDictionary_2_t28079572 * value)
	{
		___U3ConsetsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3ConsetsU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
