﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen170329564.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RhythmEventProvider/RhythmEvent`2<System.Int32,System.Int32>
struct  RhythmEvent_2_t1696632651  : public UnityEvent_2_t170329564
{
public:
	// System.Int32 RhythmEventProvider/RhythmEvent`2::_listenerCount
	int32_t ____listenerCount_5;

public:
	inline static int32_t get_offset_of__listenerCount_5() { return static_cast<int32_t>(offsetof(RhythmEvent_2_t1696632651, ____listenerCount_5)); }
	inline int32_t get__listenerCount_5() const { return ____listenerCount_5; }
	inline int32_t* get_address_of__listenerCount_5() { return &____listenerCount_5; }
	inline void set__listenerCount_5(int32_t value)
	{
		____listenerCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
