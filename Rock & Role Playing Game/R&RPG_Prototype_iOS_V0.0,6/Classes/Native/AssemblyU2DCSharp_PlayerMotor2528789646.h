﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.CharacterController
struct CharacterController_t4094781467;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMotor
struct  PlayerMotor_t2528789646  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.CharacterController PlayerMotor::controller
	CharacterController_t4094781467 * ___controller_2;
	// UnityEngine.Vector3 PlayerMotor::moveVector
	Vector3_t2243707580  ___moveVector_3;
	// System.Single PlayerMotor::speed
	float ___speed_4;
	// System.Single PlayerMotor::verticalVelocity
	float ___verticalVelocity_5;
	// System.Single PlayerMotor::gravity
	float ___gravity_6;
	// System.Single PlayerMotor::animationDuration
	float ___animationDuration_7;

public:
	inline static int32_t get_offset_of_controller_2() { return static_cast<int32_t>(offsetof(PlayerMotor_t2528789646, ___controller_2)); }
	inline CharacterController_t4094781467 * get_controller_2() const { return ___controller_2; }
	inline CharacterController_t4094781467 ** get_address_of_controller_2() { return &___controller_2; }
	inline void set_controller_2(CharacterController_t4094781467 * value)
	{
		___controller_2 = value;
		Il2CppCodeGenWriteBarrier(&___controller_2, value);
	}

	inline static int32_t get_offset_of_moveVector_3() { return static_cast<int32_t>(offsetof(PlayerMotor_t2528789646, ___moveVector_3)); }
	inline Vector3_t2243707580  get_moveVector_3() const { return ___moveVector_3; }
	inline Vector3_t2243707580 * get_address_of_moveVector_3() { return &___moveVector_3; }
	inline void set_moveVector_3(Vector3_t2243707580  value)
	{
		___moveVector_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(PlayerMotor_t2528789646, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_verticalVelocity_5() { return static_cast<int32_t>(offsetof(PlayerMotor_t2528789646, ___verticalVelocity_5)); }
	inline float get_verticalVelocity_5() const { return ___verticalVelocity_5; }
	inline float* get_address_of_verticalVelocity_5() { return &___verticalVelocity_5; }
	inline void set_verticalVelocity_5(float value)
	{
		___verticalVelocity_5 = value;
	}

	inline static int32_t get_offset_of_gravity_6() { return static_cast<int32_t>(offsetof(PlayerMotor_t2528789646, ___gravity_6)); }
	inline float get_gravity_6() const { return ___gravity_6; }
	inline float* get_address_of_gravity_6() { return &___gravity_6; }
	inline void set_gravity_6(float value)
	{
		___gravity_6 = value;
	}

	inline static int32_t get_offset_of_animationDuration_7() { return static_cast<int32_t>(offsetof(PlayerMotor_t2528789646, ___animationDuration_7)); }
	inline float get_animationDuration_7() const { return ___animationDuration_7; }
	inline float* get_address_of_animationDuration_7() { return &___animationDuration_7; }
	inline void set_animationDuration_7(float value)
	{
		___animationDuration_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
